/*jshint laxcomma: true, smarttabs: true*/
var requirejs = require('requirejs');
var path = require('path');
module.exports = requirejs.config({
	baseUrl: path.resolve( path.join( __dirname, "v2.0"))
	,nodeRequire: require
	,packages:[{
		name: 'codeMirror'
		,location:'../packages/CodeMirror'
	},{
		name:'collections'
		,location:'../packages/Collections/v2.0'
	},{
		name:'compression'
		,location:'../packages/Compression'
	},{
		 name:'crypto'
		,location:"../packages/Crypto"
	},{
		 name:'jscrypto'
		,location:"../packages/Crypto"
	},{
		name:'slick'
		,location:"../packages/Slick"
	},{
		name:'swig'
		,location:"../packages/Swig"
	},{
		name:"Native"
		,location:"../packages/Native/v2.0"
	}]
});
