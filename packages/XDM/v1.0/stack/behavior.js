/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*global define, setTimeout*/

/**
 * Behaviors modify the way a transport works.<br/>
 * By sending all events and messages through a pipeline of behaviors, each behavior can be used to inspect, modify, block, etc...<br/>
 * Each behavior is automatically chained with the others so they form a stack, where in principal, each layer talks to the corresponding layer on the other end.
 * @module Behaviors
 */

define(['require', 'exports', 'core', 'json', 'class', 'functools'], function(require, exports, core, json, _class, functools) {
	var typeOf = core.typeOf;
	/**
	 * This is a behavior that tries to make the underlying transport reliable by using acknowledgements.
	 * @class ReliableBehavior
	 */
	var ReliableBehavior = new _class.Class({
		incoming: function(message, origin) {
			var idx = message.indexOf('_')
				, ack = message.substring(0, idx).split(',');
			message = message.substring(idx + 1);
			if (ack[0] === this.idOut) {
				this.currentMessage = '';
				if (this.callback) {
					this.callback(true);
					this.callback = null;
				}
			}
			if (message.length > 0) {
				this.down.outgoing(ack[1] + ',' + this.idOut + '_' + this.currentMessage, origin);
				if (this.idIn != ack[1]) {
					this.idIn = ack[1];
					this.up.incoming(message, origin);
				}
			}
		}
		, outgoing: function(message, origin, fn) {
			this.currentMessage = message;
			this.callback = fn; // the callback to execute when we have a confirmed success/failure
			this.down.outgoing(this.idIn + ',' + (++this.idOut) + '_' + message, origin);
		}
		/**
		 * Initialize!
		 * @method initialize
		 * @memberof ReliableBehavior
		 * @param {Object} config The behaviors configuration
		 */
		, initialize: function(config) {
			this.callback = null;
			this.currentMessage = '';
			this.idIn = 0;
			this.idOut = 0;

			this.config = config;
		}
	});
	/**
	 * This is a behavior that enables queueing of messages. <br/>
	 * It will buffer incoming messages and dispach these as fast as the underlying transport allows.
	 * This will also fragment/defragment messages so that the outgoing message is never bigger than the
	 * set length.
	 * @class QueueBehavior
	 */
	var QueueBehavior = new _class.Class({
		/***
		 * This will remove an element from its stack while leaving the stack functional.
		 * @method _removeFromStack
		 * @private
		 * @param {Object} element The elment to remove from the stack.
		 */
		_removeFromStack: function(element) {
			element.up.down = element.down;
			element.down.up = element.up;
			element.up = element.down = null;
		}
		, dispatch: function() {
			var message;
			if (this.config.remove && this.queue.length === 0) {
				this._removeFromStack(this);
				return;
			}
			if (this.waiting || this.queue.length === 0 || this.destroying) {
				return;
			}
			this.waiting = true;
			message = this.queue.shift();
			this.down.outgoing(message.data, message.origin, function(success) {
				this.waiting = false;
				if (message.callback) {
					setTimeout(function() {
						message.callback(success);
					}, 0);
				}
				this.dispatch();
			}.bind(this));
		}
		, callback: function(success) {
			var up;
			this.waiting = false;
			up = this.up; // in case dispatch calls _removeFromStack
			this.dispatch();
			up.callback(success);
		}
		, incoming: function(message, origin) {
			var idx, seq;
			if (this.doFragment) {
				idx = message.indexOf('_');
				seq = parseInt(message.substring(0, idx), 10);
				this.inMessage += message.substring(idx + 1);
				if (seq === 0) {
					if (this.config.encode) {
						this.inMessage = decodeURIComponent(this.incoming);
					}
					this.up.incoming(this.inMessage, origin);
					this.inMessage = '';
				}
			} else {
				this.up.incoming(message, origin);
			}
		}
		, outgoing: function(message, origin, fn) {
			var fragments = [], fragment;
			if (this.config.encode) {
				message = encodeURIComponent(message);
			}
			if (this.doFragment) {
				// fragment into chunks
				while (message.length !== 0) {
					fragment = message.substring(0, this.maxLength);
					message = message.substring(fragment.length);
					fragments.push(fragment);
				}
				// enqueue the chunks
				while ((fragment = fragments.shift())) {
					this.queue.push({
						data: fragments.length + '_' + fragment
						, origin: origin
						, callback: fragments.length === 0 ? fn : null
					});
				}
			} else {
				this.queue.push({
					data: message
					, origin: origin
					, callback: fn
				});
			}
			if (this.lazy) {
				this.down.unlaze();
			} else {
				this.dispatch();
			}
		}
		, destroy: function() {
			this.destroying = true;
			this.down.destroy();
		}
		/**
		 * @param config [optional] {Object} The behaviors configuration
		 */
		, initialize: function(config) {
			this.destroying = false;
			this.doFragment = !!config.maxLength;
			this.inMessage = '';
			this.lazy = !!config.lazy;
			this.maxLength = config.maxLength || 0;
			this.queue = [];
			this.waiting = true;

			if (typeOf(config) === 'undefined') {
				this.config = {};
			} else {
				this.config = config;
			}
		}
	});
	/**
	 * This behavior will verify that communication with the remote end is possible, and will also sign all outgoing,
	 * and verify all incoming messages. This removes the risk of someone hijacking the iframe to send malicious messages.
	 * @class VerifyBehavior
	 * @param {Object} config The behaviors configuration.
	 * @cfg {Boolean} initiate If the verification should be initiated from this end.
	 */
	var VerifyBehavior = new _class.Class({
		_startVerification: function() {
			this.mySecret = Math.random().toString(16).substring(2);
			this.down.outgoing(this.mySecret);
		}
		, incoming: function(message, origin) {
			var idx = message.indexOf('_');
			if (idx === -1) {
				if (message === this.mySecret) {
					this.up.callback(true);
				} else if (!this.theirSecret) {
					this.theirSecret = message;
					if (!this.config.initiate) {
						this._startVerification();
					}
					this.down.outgoing(message);
				}
			} else {
				if (message.substring(0, idx) === this.theirSecret) {
					this.up.incoming(message.substring(idx + 1), origin);
				}
			}
		}
		, outgoing: function(message, origin, fn) {
			this.down.outgoing(this.mySecret + '_' + message, origin, fn);
		}
		, callback: function(success) {
			if (this.config.initiate) {
				this._startVerification();
			}
		}
		, initialize: function(config) {
			this.mySecret = '';
			this.theirSecret = '';
			this.verified = false;

			this.config = config;
		}
	});

	/**
	 * This uses JSON-RPC 2.0 to expose local methods and to invoke remote methods and have responses returned over the the string-based transport stack.<br/>
	 * Exposed methods can return values synchronous, asyncronous, or bet set up to not return anything.
	 * @class RpcBehavior
	 * @param {Object} proxy The object to apply the methods to.
	 * @param {Object} config The definition of the local and remote interface to implement.
	 * @cfg {Object} local The local interface to expose.
	 * @cfg {Object} remote The remote methods to expose through the proxy.
	 * @cfg {Object} serializer The serializer to use for serializing and deserializing the JSON. Should be compatible with the HTML5 JSON object. Optional, will default to JSON.
	 */
	var RpcBehavior = new _class.Class({
		/**
		 * Serializes and sends the message
		 * @method _send
		 * @memberof RpcBehavior
		 * @private
		 * @param {Object} data The JSON-RPC message to be sent. The jsonrpc property will be added.
		 */
		_send: function(data) {
			data.jsonrpc = '2.0';
			this.down.outgoing(this.serializer.encode(data));
		}
		/**
		 * Creates a method that implements the given definition
		 * @private
		 * @param {Object} The method configuration
		 * @param {String} method The name of the method
		 * @return {Function} A stub capable of proxying the requested method call
		 */
		, _createMethod: function(definition, method) {
			var callback
				, l = arguments.length
				, message = { method: method }
				, slice = Array.prototype.slice;
			return functools.bind(function() {
				if (l > 0 && typeOf(arguments[l - 1]) === 'function') {
					//with callback, procedure
					if (l > 1 && typeOf(arguments[l - 2]) === 'function') {
						// two callbacks, success and error
						callback = {
							success: arguments[l - 2]
							, error: arguments[l - 1]
						};
						message.params = slice.call(arguments, 0, l - 2);
					} else {
						// single callback, success
						callback = {
							success: arguments[l - 1]
						};
						message.params = slice.call(arguments, 0, l - 1);
					}
					this._callbacks['' + (++this._callbackCounter)] = callback;
					message.id = this._callbackCounter;
				} else {
					// no callbacks, a notification
					message.params = slice.call(arguments, 0);
				}
				if (definition.namedParams && message.params.length === 1) {
					message.params = message.params[0];
				}
				// Send the method request
				this._send(message);
			}, this);
		}
		/**
		 * Executes the exposed method
		 * @private
		 * @param {String} method The name of the method
		 * @param {Number} id The callback id to use
		 * @param {Function} method The exposed implementation
		 * @param {Array} params The parameters supplied by the remote end
		 */
		, _executeMethod: function(method, id, fn, params) {
			var error, result, success, that;
			that = this;
			if (!fn) {
				if (id) {
					that._send({
						id: id
						, error: {
							// -32601 -> server error: requested method not found
							code: -32601
							, message: 'Procedure not found.'
						}
					});
				}
				return;
			}
			if (id) {
				success = function(result) {
					success = function() {};
					that._send({
						id: id
						, result: result
					});
				};
				error = function(message, data) {
					var msg;
					error = function() {};
					msg = {
						id: id
						, error: {
							// -32099 -> custom error
							code: -32099
							, message: message
						}
					};
					if (data) {
						msg.error.data = data;
					}
					that._send(msg);
				};
			} else {
				success = error = function() {};
			}
			if (typeOf(params) !== 'array') {
				params = [params];
			}
			try {
				result = fn.method.apply(fn.scope, params.concat([success, error]));
				if (typeOf(result) !== 'undefined') {
					success(result);
				}
			}
			catch (ex1) {
				error(ex1.message);
			}
		}
		, incoming: function(message, origin) {
			var callback
				, data = this.serializer.decode(message);
			if (data.method) {
				// A method call from the remote end
				if (this.config.handle) {
					this.config.handle(data, this._send);
				} else {
					this._executeMethod(data.method, data.id, this.config.local[data.method], data.params);
				}
			} else {
				// A method response from the other end
				callback = this._callbacks[data.id];
				if (data.error) {
					if (callback.error) {
						callback.error(data.error);
					}
				} else if (callback.success) {
					callback.success(data.result);
				}
				delete this._callbacks[data.id];
			}
		}
		, destroy: function() {
			var method;
			for (method in this.config.remote) {
				if (this.config.remote.hasOwnProperty(method) && this.proxy.hasOwnProperty(method)) {
					delete this.proxy[method];
				}
			}
			this.down.destroy();
		}
		, initialize: function(proxy, config) {
			var method;
			this._callbackCounter = 0;
			this._callbacks = {};
			if (config.remote) {
				// Implement the remote sides exposed methods
				for (method in config.remote) {
					if (config.remote.hasOwnProperty(method)) {
						proxy[method] = this._createMethod(config.remote[method], method);
					}
				}
			}
			this.serializer = config.serializer || json;
			this.proxy = proxy;

			this.config = config;
		}
	});

	exports.Reliable = ReliableBehavior;
	exports.Queue = QueueBehavior;
	exports.Verify = VerifyBehavior;
	exports.Rpc = RpcBehavior;

});