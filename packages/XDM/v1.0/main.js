/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*global ActiveXObject, define, document, location, navigator, parent, setInterval, setTimeout, window*/

/**
 * AMD Package to handle cross domain communication client side
 * @module XDM
 * @author Eric Satterwhite
 * @author Ghazi Belhaj
 * @requires core
 * @requires url
 */

define([
	'require'
	, 'exports'
	, 'module'
	, 'core'
	, 'url'
	, './utility'
	, './stack/sameorigin'
	, './stack/postmessage'
	, './stack/flash'
	, './stack/frameelement'
	, './stack/name'
	, './stack/hash'
	, './stack/behavior'
], function(require, exports, module, core, url, utility, sameorigin, postmessage, flash, frameelement, name, hash, behavior) {
	var channelId = Math.floor(Math.random() * 10000) // randomize the initial id in case of multiple closures loaded
		, flashVersion // will be set if using Flash
		, typeOf = core.typeOf;

	function undef(v) {
		return typeOf(v) === 'undefined';
	}
	function hasFlash() {
		var flash
			, name = 'Shockwave Flash'
			, description
			, mimeType = 'application/x-shockwave-flash';
		if (!!navigator.plugins && typeOf(navigator.plugins[name]) === 'object') {
			description = navigator.plugins[name].description;
			if((description && !!navigator.mimeTypes) && navigator.mimeTypes[mimeType] && navigator.mimeTypes[mimeType].enabledPlugin) {
				flashVersion = description.match(/\d+/g);
			}
		}
		if (!flashVersion) {
			try {
				flash = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
				flashVersion = Array.prototype.slice.call(flash.GetVariable('$version').match(/(\d+),(\d+),(\d+),(\d+)/), 1);
				flash = null;
			} catch(notSupportedException) {
				/* pass */
				return false;
			}
		}
		return !!flashVersion;
	}
	// build the query object either from location.query, if it contains the xdm_e argument, or from location.hash
	var query = (function(input){
		input = input.substring(1).split("&");
		var data = {}, pair, i = input.length;
		while (i--) {
			pair = input[i].split("=");
			data[pair[0]] = decodeURIComponent(pair[1]);
		}
		return data;
	}(/xdm_e=/.test(location.search) ? location.search : location.hash));
	/**
	 * Check whether a domain is allowed using an Access Control List.
	 * The ACL can contain * and ? as wildcards, or can be regular expressions.
	 * If regular expressions they need to begin with ^ and end with $.
	 * @param {Array/String} acl The list of allowed domains
	 * @param {String} domain The domain to test.
	 * @return {Boolean} True if the domain is allowed, false if not.
	 */
	function checkAcl(acl, domain) {
		var i, re;
		// normalize into an array
		if (typeOf(acl) === 'string') {
			acl = [acl];
		}
		i = acl.length;
		while (i--) {
			re = acl[i];
			re = new RegExp(re.substr(0, 1) == '^' ? re : ('^' + re.replace(/(\*)/g, '.$1').replace(/\?/g, '.') + '$'));
			if (re.test(domain)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * Prepares an array of stack-elements suitable for the current configuration
	 * @private
	 * @param {Object} config The Transports configuration. See xdm.Socket for more.
	 * @return {Array} An array of stack-elements with the TransportElement at index 0.
	 */
	function prepareTransportStack(config) {
		var protocol = config.protocol
			, stackEls;

		config.isHost = config.isHost || undef(query.xdm_p);

		if (!config.props) {
			config.props = {};
		}
		if (!config.isHost) {
			config.channel = query.xdm_c.replace(/["'<>\\]/g, "");
			config.secret = query.xdm_s;
			config.remote = query.xdm_e.replace(/["'<>\\]/g, "");
			protocol = query.xdm_p;
			if (config.acl && !checkAcl(config.acl, config.remote)) {
				throw new Error('Access denied for ' + config.remote);
			}
		} else {
			config.remote = url.resolveUrl(config.remote);
			config.channel = config.channel || "default" + channelId++;
			config.secret = Math.random().toString(16).substring(2);
			if (undef(protocol)) {
				if (url.getLocation(location.href) == url.getLocation(config.remote)) {
					/*
					 * SameOriginTransport
					 * Both documents has the same origin, lets use direct access.
					 */
					protocol = '4';
				} else if (typeOf(window.postMessage) === 'function' || typeOf(document.postMessage) === 'function') {
					/*
					 * PostMessageTransport
					 * This is supported in IE8+, Firefox 3+, Opera 9+, Chrome 2+ and Safari 4+
					 */
					protocol = '1';
				} else if (config.swf && typeOf(window.ActiveXObject) === 'function' && hasFlash()) {
					/*
					 * FlashTransport
					 */
					protocol = '6';
				} else if (navigator.product === 'Gecko' && 'frameElement' in window && navigator.userAgent.indexOf('WebKit') == -1) {
					/*
					 * FrameElementTransport
					 * This is supported in Gecko (Firefox 1+)
					 */
					protocol = '5';
				}
				// else if (config.remoteHelper) {
				// 	/*
				// 	 * NameTransport
				// 	 * This is supported in all browsers that retains the value of window.name when
				// 	 * navigating from one domain to another, and where parent.frames[foo] can be used
				// 	 * to get access to a frame from the same domain
				// 	 */
				// 	protocol = '2';
				// } else {
				// 	/*
				// 	 * HashTransport
				// 	 * This is supported in all browsers where [window].location is writable for all
				// 	 * The resize event will be used if resize is supported and the iframe is not put
				// 	 * into a container, else polling will be used.
				// 	 */
				// 	protocol = '0';
				// }
			}
		}
		config.protocol = protocol ? protocol: "6"; // for conditional branching
		switch (protocol) {
			case '0':// 0 = HashTransport
				utility.apply(config, {
					interval: 100
					, delay: 2000
					, useResize: true
					, useParent: false
					, usePolling: false
				}, true);
				if (config.isHost) {
					if (!config.local) {
						// If no local is set then we need to find an image hosted on the current domain
						var domain = location.protocol + '//' + location.host, images = document.body.getElementsByTagName('img'), image;
						var i = images.length;
						while (i--) {
							image = images[i];
							if (image.src.substring(0, domain.length) === domain) {
								config.local = image.src;
								break;
							}
						}
						if (!config.local) {
							// If no local was set, and we are unable to find a suitable file, then we resort to using the current window
							config.local = window;
						}
					}

					var parameters = {
						xdm_c: config.channel,
						xdm_p: 0
					};

					if (config.local === window) {
						// We are using the current window to listen to
						config.usePolling = true;
						config.useParent = true;
						config.local = location.protocol + '//' + location.host + location.pathname + location.search;
						parameters.xdm_e = config.local;
						parameters.xdm_pa = 1; // use parent
					} else {
						parameters.xdm_e = url.resolveUrl(config.local);
					}

					if (config.container) {
						config.useResize = false;
						parameters.xdm_po = 1; // use polling
					}
					config.remote = url.appendQueryParams(config.remote, parameters, config.hash || false);
				} else {
					utility.apply(config, {
						channel: query.xdm_c,
						remote: query.xdm_e,
						useParent: !undef(query.xdm_pa),
						usePolling: !undef(query.xdm_po),
						useResize: config.useParent ? false : config.useResize
					});
				}
				stackEls = [
					new hash.Transport(config)
					, new behavior.Reliable({})
					, new behavior.Queue({
						encode: true
						, maxLength: 4000 - config.remote.length
					})
					, new behavior.Verify(config)
				];
				break;
			case '1':
				stackEls = [ new postmessage.Transport(config) ];
				break;
			case '2':
				config.remoteHelper = url.resolveUrl(config.remoteHelper);
				stackEls = [
					new name.Transport(config)
					, new behavior.Queue()
					, new behavior.Verify(config)
				];
				break;
			case '3':
				break;
			case '4':
				stackEls = [ new sameorigin.Transport(config) ];
				break;
			case '5':
				config.extras = {};
				config.extras.xdm_e = query.xdm_e;
				stackEls = [ new frameelement.Transport(config) ];
				break;
			case '6':
				if (!flashVersion) {
					hasFlash();
				}
				stackEls = [ new flash.Transport(config) ];
				break;
		}
		// this behavior is responsible for buffering outgoing messages, and for performing lazy initialization
		stackEls.push(new behavior.Queue({
			lazy: config.lazy
			, remove: true
		}));
		return stackEls;
	}
	/**
	 * Chains all the separate stack elements into a single usable stack.<br/>
	 * If an element is missing a necessary method then it will have a pass-through method applied.
	 * @param {Array} stackElements An array of stack elements to be linked.
	 * @return {xdm.stack.StackElement} The last element in the chain.
	 */
	function chainStack(stackElements) {
		var defaults
			, i
			, len = stackElements.length
			, stackEl;
		defaults = {
			incoming: function(message, origin) {
				this.up.incoming(message, origin);
			}
			, outgoing: function(message, recipient) {
				this.down.outgoing(message, recipient);
			}
			, callback: function(success) {
				this.up.callback(success);
			}
			, destroy: function() {
				this.down.destroy();
			}
		};
		for (i = 0; i < len; i++) {
			stackEl = stackElements[i];
			utility.apply(stackEl, defaults, true);
			if (i !== 0) {
				stackEl.down = stackElements[i - 1];
			}
			if (i !== len - 1) {
				stackEl.up = stackElements[i + 1];
			}
		}
		return stackEl;
	}
	/**
	 * This class creates a transport channel between two domains that is usable for sending and receiving string-based messages.<br/>
	 * The channel is reliable, supports queueing, and ensures that the message originates from the expected domain.<br/>
	 * Internally different stacks will be used depending on the browsers features and the available parameters.
	 *
	 * If you are unable to upload the <code>name.html</code> file to the consumers domain then remove the <code>remoteHelper</code> property
	 * and xdm will fall back to using the HashTransport instead of the NameTransport when not able to use any of the primary transports.
	 * @cfg {String/Window} local The url to the local name.html document, a local static file, or a reference to the local window.
	 * @cfg {Boolean} lazy (Consumer only) Set this to true if you want xdm to defer creating the transport until really needed.
	 * @cfg {String} remote (Consumer only) The url to the providers document.
	 * @cfg {String} remoteHelper (Consumer only) The url to the remote name.html file. This is to support NameTransport as a fallback. Optional.
	 * @cfg {Number} delay The number of milliseconds xdm should try to get a reference to the local window.  Optional, defaults to 2000.
	 * @cfg {Number} interval The interval used when polling for messages. Optional, defaults to 300.
	 * @cfg {String} channel (Consumer only) The name of the channel to use. Can be used to set consistent iframe names. Must be unique. Optional.
	 * @cfg {Function} onMessage The method that should handle incoming messages.<br/> This method should accept two arguments, the message as a string, and the origin as a string. Optional.
	 * @cfg {Function} onReady A method that should be called when the transport is ready. Optional.
	 * @cfg {DOMElement|String} container (Consumer only) The element, or the id of the element that the primary iframe should be inserted into. If not set then the iframe will be positioned off-screen. Optional.
	 * @cfg {Array/String} acl (Provider only) Here you can specify which '[protocol]://[domain]' patterns that should be allowed to act as the consumer towards this provider.<br/>
	 * This can contain the wildcards ? and *.  Examples are 'http://example.com', '*.foo.com' and '*dom?.com'. If you want to use reqular expressions then you pattern needs to start with ^ and end with $.
	 * If none of the patterns match an Error will be thrown.
	 * @cfg {Object} props (Consumer only) Additional properties that should be applied to the iframe. This can also contain nested objects e.g: <code>{style:{width:"100px", height:"100px"}}</code>.
	 * Properties such as 'name' and 'src' will be overrided. Optional.
	 * <h2>How to set up</h2>
	 *
	 * @example
	 * //Setting up the provider:
	 *
	 * var socket = new xdm.Socket({
	 * &nbsp; local: "name.html",
	 * &nbsp; onReady: function(){
	 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the socket
	 * &nbsp; &nbsp; socket.postMessage("foo-message");
	 * &nbsp; },
	 * &nbsp; onMessage: function(message, origin) {
	 * &nbsp;&nbsp; alert("received " + message + " from " + origin);
	 * &nbsp; }
	 * });
	 * @example
	 * // Setting up the consumer:

	 * var socket = new xdm.Socket({
	 * &nbsp; remote: "http:&#47;&#47;remotedomain/page.html",
	 * &nbsp; remoteHelper: "http:&#47;&#47;remotedomain/name.html",
	 * &nbsp; onReady: function(){
	 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the socket
	 * &nbsp; &nbsp; socket.postMessage("foo-message");
	 * &nbsp; },
	 * &nbsp; onMessage: function(message, origin) {
	 * &nbsp;&nbsp; alert("received " + message + " from " + origin);
	 * &nbsp; }
	 * });
	 *
	 * @class
	 */
	exports.Socket = function(config) {
		var recipient
			, stack = chainStack(prepareTransportStack(config).concat([{
				incoming: function(message, origin) {
					config.onMessage(message, origin);
				}
				, callback: function(success) {
					if (config.onReady) {
						config.onReady(success);
					}
				}
			}]));
		// set the origin
		this.origin = recipient = url.getLocation(config.remote);
		/**
		 * Initiates the destruction of the stack.
		 */
		this.destroy = function() {
			stack.destroy();
		};
		/**
		 * Posts a message to the remote end of the channel
		 * @param {String} message The message to send
		 */
		this.postMessage = function(message) {
			stack.outgoing(message, recipient);
		};


	};
	/**
	 * Creates a proxy object that can be used to call methods implemented on the remote end of the channel, and also to provide the implementation
	 * of methods to be called from the remote end.<br/>
	 * The instantiated object will have methods matching those specified in <code>config.remote</code>.<br/>
	 * This requires the JSON object present in the document, either natively, using json.org's json2 or as a wrapper around library spesific methods.
	 * @example
	 * var rpc = new xdm.Rpc({
	 * &nbsp; &#47;&#47; this configuration is equal to that used by the Socket.
	 * &nbsp; remote: "http:&#47;&#47;remotedomain/...",
	 * &nbsp; onReady: function(){
	 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the proxy
	 * &nbsp; &nbsp; rpc.foo(...
	 * &nbsp; }
	 * },{
	 * &nbsp; local: {..},
	 * &nbsp; remote: {..}
	 * });
	 * </code></pre>
	 *
	 * <h2> Exposing functions (procedures)</h2>
	 * @example
	 *
	 * var rpc = new xdm.Rpc({
	 * &nbsp; ...
	 * },{
	 * &nbsp; local: {
	 * &nbsp; &nbsp; nameOfMethod: {
	 * &nbsp; &nbsp; &nbsp; method: function(arg1, arg2, success, error){
	 * &nbsp; &nbsp; &nbsp; &nbsp; ...
	 * &nbsp; &nbsp; &nbsp; }
	 * &nbsp; &nbsp; },
	 * &nbsp; &nbsp; &#47;&#47; with shorthand notation
	 * &nbsp; &nbsp; nameOfAnotherMethod:  function(arg1, arg2, success, error){
	 * &nbsp; &nbsp; }
	 * &nbsp; },
	 * &nbsp; remote: {...}
	 * });
	 * </code></pre>

	 * The function referenced by  [method] will receive the passed arguments followed by the callback functions <code>success</code> and <code>error</code>.<br/>
	 * To send a successfull result back you can use
	 *     <pre><code>
	 *     return foo;
	 *     </pre></code>
	 * or
	 *     <pre><code>
	 *     success(foo);
	 *     </pre></code>
	 *  To return an error you can use
	 *     <pre><code>
	 *     throw new Error("foo error");
	 *     </code></pre>
	 * or
	 *     <pre><code>
	 *     error("foo error");
	 *     </code></pre>
	 *
	 * <h2>Defining remotely exposed methods (procedures/notifications)</h2>
	 * @example
	 * var rpc = new xdm.Rpc({
	 * &nbsp; ...
	 * },{
	 * &nbsp; local: {...},
	 * &nbsp; remote: {
	 * &nbsp; &nbsp; nameOfMethod: {}
	 * &nbsp; }
	 * });
	 * </code></pre>

	 * <h2>To call a remote method use</h2>
	 * @example
	 * rpc.nameOfMethod("arg1", "arg2", function(value) {
	 * &nbsp; alert("success: " + value);
	 * }, function(message) {
	 * &nbsp; alert("error: " + message + );
	 * });
	 * </code></pre>
	 * Both the <code>success</code> and <code>errror</code> callbacks are optional.<br/>
	 * When called with no callback a JSON-RPC 2.0 notification will be executed.
	 * Be aware that you will not be notified of any errors with this method.
	 *
	 * If you do not want to use the JSON2 library for non-native JSON support, but instead capabilities provided by some other library
	 * then you can specify a custom serializer using <code>serializer: foo</code>

	 * @example
	 * // Specifying a custom serializer
	 * var rpc = new xdm.Rpc({
	 * &nbsp; ...
	 * },{
	 * &nbsp; local: {...},
	 * &nbsp; remote: {...},
	 * &nbsp; serializer : {
	 * &nbsp; &nbsp; parse: function(string){ ... },
	 * &nbsp; &nbsp; stringify: function(object) {...}
	 * &nbsp; }
	 * });
	 * </code></pre>
	 *
	 * If <code>serializer</code> is set then the class will not attempt to use the native implementation.
	 * @param {Object} config The underlying transports configuration. See xdm.Socket for available parameters.
	 * @param {Object} jsonRpcConfig The description of the interface to implement.
	 * @class
	 */
	exports.Rpc = function(config, jsonRpcConfig) {
		var member, method, stack;
		// expand shorthand notation
		if (jsonRpcConfig.local) {
			for (method in jsonRpcConfig.local) {
				if (jsonRpcConfig.local.hasOwnProperty(method)) {
					member = jsonRpcConfig.local[method];
					if (typeOf(member) === 'function') {
						jsonRpcConfig.local[method] = {
							method: member
						};
					}
				}
			}
		}
		// create the stack
		stack = chainStack(prepareTransportStack(config).concat([new behavior.Rpc(this, jsonRpcConfig), {
			callback: function(success) {
				if (config.onReady) {
					config.onReady(success);
				}
			}
		}]));
		// set the origin
		this.origin = url.getLocation(config.remote);
		/**
		 * Initiates the destruction of the stack.
		 */
		this.destroy = function(){
			stack.destroy();
		};
		this.getStack = function(){
			return stack;
		}
	};
});
