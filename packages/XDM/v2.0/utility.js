/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*global define, document, location, parent, setInterval, setTimeout, window*/
define(['require', 'exports', 'core', 'log', 'dom', 'object'], function(require, exports, core, log, dom, object) {
	var _map = {}
		, typeOf = core.typeOf;
	/**
	 * Applies properties from the source object to the target object.<br/>
	 * @method apply
	 * @param {Object} target The target of the properties.
	 * @param {Object} source The source of the properties.
	 * @param {Boolean} noOverwrite Set to True to only set non-existing properties.
	 */
	exports.apply = function(destination, source, noOverwrite) {
		var member, prop;
		for (prop in source) {
			if (source.hasOwnProperty(prop)) {
				if (prop in destination) {
					member = source[prop];
					if (typeOf(member) === 'object') {
						exports.apply(destination[prop], member, noOverwrite);
					}
					else if (!noOverwrite) {
						destination[prop] = source[prop];
					}
				}
				else {
					destination[prop] = source[prop];
				}
			}
		}
		return destination;
	};
	/**
	 * Creates a frame and appends it to the DOM.
	 * @method createFrame
	 * @param config {object} This object can have the following properties
	 * <ul>
	 * <li> {object} prop The properties that should be set on the frame. This should include the 'src' property.</li>
	 * <li> {object} attr The attributes that should be set on the frame.</li>
	 * <li> {DOMElement} container Its parent element (Optional).</li>
	 * <li> {function} onLoad A method that should be called with the frames contentWindow as argument when the frame is fully loaded. (Optional)</li>
	 * </ul>
	 * @return The frames DOMElement
	 * @type DOMElement
	 */
	exports.createFrame = function(config) {
		var form
			, frame
			, input
			, src
			, style;
		frame = new dom.Element('iframe', {
			id: config.props.name
			, name: config.props.name
		});
		delete config.props.name;
		if (typeOf(config.container) === 'string') {
			config.container = dom.id(config.container);
		}

		if (!config.container) {
			// This needs to be hidden like this, simply setting display:none and the like will cause failures in some browsers.
			frame.setStyles( {
				position: 'absolute'
				, top: '-2000px'
				// Avoid potential horizontal scrollbar
				, left: '0px'
			});
			config.container = dom.Document.body;
		}
		// HACK: IE cannot have the src attribute set when the frame is appended
		//       into the container, so we set it to 'javascript:false' as a
		//       placeholder for now.  If we left the src undefined, it would
		//       instead default to 'about:blank', which causes SSL mixed-content
		//       warnings in IE6 when on an SSL parent page.
		src = config.props.src;
		config.props.src = 'javascript:false';
		// transfer properties to the frame
		frame.set(config.props);
		frame.set({
			border: 0
			, frameBorder: 0
			, allowTransparency: true
		});
		frame.inject(config.container);

		if (config.onLoad) {
			dom.whenReady(config.onLoad, this);
		}
		// set the frame URL to the proper value (we previously set it to
		// 'javascript:false' to work around the IE issue mentioned above)
		if (config.usePost) {
			form = new dom.Element('form', {
				target: frame.name
				, action: src
				, method: 'POST'
			}).inject(config.container);
			if (typeOf(config.usePost) === 'object') {
				for (var i in config.usePost) {
					if (config.usePost.hasOwnProperty(i)) {
						input = new dom.Element('input', {
							name: i
							, value: config.usePost[i]
						}).inject(form);
					}
				}
			}
			form.submit();
			form.dispose();
		} else {
			frame.set('src', src);
		}
		config.props.src = src;
		frame.set({
			c: config.channel || ""
			, s: config.secret || ""
			, e: window.location.origin ? window.location.origin : (window.location.protocol + "//" + window.location.host )  || "" 
			, p: config.protocol || ""
		})
		return frame;
	};

	if ( window.addEventListener ) {
	    exports["addEvent"] = function(target, type, listener) {
	        target.addEventListener(type, listener, false);
	    };
	    exports["removeEvent"] = function(target, type, listener) {
	        target.removeEventListener(type, listener, false);
	    };
	} else if (window.attachEvent) {
	    exports["addEvent"] = function(object, sEvent, fpNotify){
	        object.attachEvent('on' + sEvent, fpNotify);
	    };
	    exports["removeEvent"] = function(object, sEvent, fpNotify){
	        object.detachEvent('on' + sEvent, fpNotify);
	    };
	} else {
	    log.error('Browser not supported');
	}

	// TODO: YEAH! This might be a problem!!
	/**
	 * Stores a function using the given name for reference
	 * @param {String} name The name that the function should be referred by
	 * @param {Function} fn The function to store
	 * @namespace easyXDM.fn
	 */
	exports.set = function(name, fn) {
		_map[name] = fn;
	};
	/**
	 * Retrieves the function referred to by the given name
	 * @param {String} name The name of the function to retrieve
	 * @param {Boolean} del If the function should be deleted after retrieval
	 * @return {Function} The stored function
	 * @namespace easyXDM.fn
	 */
	exports.get = function(name, del) {
		var fn = _map[name];
		if (del) {
			delete _map[name];
		}
		return fn;
	};

});
