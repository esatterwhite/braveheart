/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Native extensions for the String object. All methods from {@link module:string} will be avaliable on all strings, as well as static methods on the String object itself
 * @module Native/String
 * @author Eric Satterwhite
 * @requires string
 * @requires core
 * @see module:string
 * @example braveheart(["Native/String"], function(){
    var str = "hello world!"

    str = str.slugify() // "hello-world"
    str = str.camelCase(); // "helloWorld"

 });
</code></pre>
 <h3> With out braveheart</h3>
 * @example
// Once required, You can work out side of a breavheart callback
var str = "TRUE";
str = str.toLowerCase().typecast();

typeof str // Boolean
</code></pre>
 <h3>Static Methods</h3>
 * @example
var str = "This     is   A messy    String"

str = String.clean( str ); // "This is A messy String"
 **/
define(function(require, exports){
    var  string = require( 'string' )
    	, core = require("core")
    	, Primitive = core.Primitive


    String = new Primitive("String", String);
    String.extend("from", string.from);
    String.extend("id", string.id);
    String.extend( "pluralize", string.pluralize );


    String.implement({
    	capitalize: function( ){
    		return string.capitalize( this );
    	},

    	clean: function( ){
    		return string.clean( this );
    	},

    	contains: function( substr ){
    		return string.contains( this, substr);
    	},

    	endsWith: function( suffix ){
    		return string.endsWith( this, suffix );
    	},

    	startsWith: function( prefix ){
    		return string.startsWith( this, prefix );
    	},

    	padLeft: function( minlen, fill ){
    		return string.padLeft( minlen, fill );
    	},

    	pxtoInt: function( ){
    		return string.pxtoInt( this );
    	},

    	reverse: function( ){
    		return string.reverse( this );
    	},

    	stripNonAlpha: function( ){
    		return string.stripNonAlpha( this );
    	},

    	stripScripts: function( execute ){
    		return string.stripScripts( this, execute );
    	},

    	stripTags: function( tag, contents ){
    		return string.stipTags( this, tag, contents )
    	},

    	substitute: function( obj, regex ){
    		return string.substitute( this, obj, regex );
    	},

    	toInt: function( base ){
    		return string.toInt( this, base );
    	},

    	toFloat: function( ){
    		return string.toFloat( this );
    	},

    	trim: function( ){
    		return string.trim( this );
    	},

    	escapeRegex: function( ){
    		return string.escapeRegex( this );
    	},

    	truncate: function( maxlen, tail ){
    		return string.truncate( this, maxlen, tail );
    	},

    	camelCase: function( ){
    		return string.camelCase( this );
    	},

    	hyphenate: function( ){
    		return string.hyphenate( this );
    	},

    	rgbToHsb: function( ){
    		return string.rgbToHsb( this )
    	},

    	hsbToRgb: function( ){
    		return string.hsbToRgb( this );
    	},

    	removeNonWord: function( ){
    		return string.removeNonWord( this );
    	},

    	replaceAccents: function( ){
    		return string.replaceAccents( this )
    	},

    	slugify: function( ){
    		return string.slugify( this );
    	},

    	normalizeLinebreaks: function( lineEnd ){
    		return string.normalizeLinebreaks( this, lineEnd );
    	},

    	typeCast: function( ){
    		return string.typeCast( this )
    	},

    	smartPlural: function( ){
    		return string.smartPlural( this )
    	}

    })
});
