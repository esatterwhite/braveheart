/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Andy Ortlieb
 **/
define(
	["require", "exports", "module", "./RFC1951/inflate", "./RFC1951/deflate"]
	,function(require, exports, module, inflate, deflate){

		exports.inflate = inflate
		exports.deflate = deflate
	}

);
