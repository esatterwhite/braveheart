if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

define(['require', 'exports', 'module', './lib', './md5', './sha1', './sha256', './hmac', './base64', './uuid'], 
	function( require, exports, module, lib, md5, sha1, sha256, hmac, base64, uuid ){
		return{
			MD5:md5
			,SHA1:sha1
			,SHA256:sha256
			,Binary:lib.Binary
			,UTF8:lib.UTF8
			,Util:lib.util
			,HMAC:hmac
			,Base64:base64
			,UUID:uuid
		}

	}
);
