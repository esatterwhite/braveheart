define(['require','exports','module', './queue', './hash'], function( require, exports, module, queue, hash ){

	exports.Queue = queue.Queue;
	exports.Hash = hash.Hash;
});
