define(['require', 'exports','module', 'class', "object"], function( require, exports, module, _class, object ){
	var Options
		,Events
		,Class
		,fn;

	Options = _class.Options
	Events  = _class.Events
	Class   = _class.Class;
	fn = new Function();
	exports.Hash = new Class({

		Implements:[Events, Options]
		,OnEvents:true
		,options:{
			 onChange: fn
			,onCreate: fn
			,onRemove: fn
			,onEmpty: fn
		}
		,initialize: function( initial, options ){

			var map = initial || {}

			this.$getMap = function(){
				return map;
			}

			this.setOptions( options )
		}
		,has: Object.prototype.hasOwnProperty
		,get: function( key ){
			var map = this.$getMap();

			return map.hasOwnProperty( key ) ? map[key] : null;
		}
		,set: function( key, value ){
			var map
				,evt
				,old_value;

			map = this.$getMap();

			evt = map.hasOwnProperty( key ) ? 'change' : 'create';
			old_value = map[key]
			map[key] = value;
			this.fireEvent( evt, array.clean([ old_value, value]) )

		}
		,clone: function( map, options ){
			var map = map || object.clone( this.$getMap() );
			var opts = !!options ? object.merge( options, this.options ) : {};


			return new exports.Hash( object.clone( map), object.clone( opts))
		}
		,each: function(fn, bind ){
			var map = this.$getMap();
			return object.each( map, fn, bind)
		}
		,erase: function( key ){
			var map = this.$getMap();

			if( map.hasOwnProperty( key )){
				var value = map[key]
				delete map[key]
				this.fireEvent('remove', value, key)
			}
		}
		,map: function(fn, bind){
			var map = this.$getMap();
			return new exports.Hash( object.map( map, fn, bind ), object.clone( this.options ));
		}
		,keys: function(){
			return object.keys( this.$getmap() )
		}
		,values: function(){
			return object.values( this.$getMap() )
		}
		,empty: function(){
			var map = this.$getMap();
			for( var key in map ){
				delete map[key];
			}
			this.fireEvent('empty')
			return this;
		}

	})
})