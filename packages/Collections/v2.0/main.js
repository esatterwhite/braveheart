/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * a collections lib
 * @module collections
 * @author Eric Satterwhite
 * @requires queue
 * @requires hash
 **/

define(['require','exports','module', './queue', './hash'], function( require, exports, module, queue, hash ){

	exports.Queue = queue;
	exports.Hash = hash
});
