
var braveheart = require({
		baseUrl:"v2.0"
		,context:'trunk'
		,paths:{
			"Class":"class",
			"Request":"request",
			"Array":"array",
			"Dom":"dom"
		}
		,packages:[{
			name: 'codeMirror'
			,location:'../packages/CodeMirror'
		},{
			name:'collections'
			,location:'../packages/Collections/v2.0'
		},{
			name:'compression'
			,location:'../packages/Compression'
		},{
			 name:'crypto'
			,location:"../packages/Crypto"
		},{
			name:'xdm'
			,location:'../packages/XDM'
		},{
			name:'slick'
			,location:"../packages/Slick"
		},{
			name:'swig'
			,location:"../packages/Swig"
		},{
			name:"Native"
			,location:"../packages/Native/v2.0"
		},{
			name:"data"
			,location:"../packages/Data/v2.0"
		}]
});

//======================================================
//		Add additional context configurations here
//======================================================

/*
require({
		baseUrl:"/static/js/Murtaugh/<version>"
		,context:<named context>
		,packages:[
			...
		]
});
*/
