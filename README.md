Braveheart
==========

### Forward
Braveheart is a collection of javascript modules build in the [AMD](http://wiki.commonjs.org/wiki/Modules/AsynchronousDefinition) format, to provide a low level abstraction on top of javascript with no external dependencies ( such as jquery, dojo or other plugins ) in an effort to make JavaScript, as a language, suck *less*.

Each module ( array, color, class, etc ) profides a single focused unit of functionality which can be required individiually on demand. This allows developers to create complex applications without having to search for add ons to meet their needs.

Because of the standardized, modular format and sandboxed versions, **braveheart** allows code reuse not only between developers, but between projects without having to worry about incompatible differences in code. Braveheart is compatible in all modern browsers including (IE 7+ ) as well [NodeJS](http://www.nodejs.org)


### Modules
Modules make up the core of braveheart. Each module aims to have a narrow focus and keep its list of dependencies to a minimum.

Currently( v2.0 ) braveheart ships with 35 modules

{@link module:core|core}: The basis of braveheart which provides the {@link module:core.Primitive|Primitive} wrapper and a number of common helper functions

{@link module:accessor|accessor}: A module that augments any object with static functions for defining look ups for later access by other modules

{@link module:analytics|analytics}: A helper module for logging data to google analytics

{@link module:array|array}: A large set of functions for working exclusively with JavaScript Arrays

{@link module:assets|assets}: A module for loading static asses - Images, Scripts, and CSS

{@link module:async|async}: *WIP*: Still under active development and considerations for future viability

{@link module:class|class}: Provides the base Class implementation for object orientated programming

{@link module:color|color}: A module for translating and interpolating colors. It also provides the {@link module:color.Color|Color} Class

{@link module:cookie|cookie}: An abstration around reading and writting individual cookies. It also Provides the {@link module:cookie.Cookie|Cookie} Class

{@link module:crossroads|crossroads}: A module for client side URL Routing

{@link module:date|date}: A module for formating dates and performing complex Date Math

{@link module:functools|functools}: A helper module providing common methods for dealing with functions, like timed execution and scope binding

{@link module:fx|fx}: A library for interpolating numbers between to values along a mathematical curve

{@link module:hasher|hasher}: A module for watching changes to URL Hastags - A sister module to {@link module:crossroads|crossroads}

{@link module:i18n|i18n}: A loader plugin for loading translation dictionaries

{@link module:is|is}: A module for detecting Objects types

{@link module:iter|iter}: A module that provides an abstration around complex iterations over single or multiple objects. It also provides the {@link module:iter.Iterable|Iterable} class

{@link module:json|json}: A module for cross browser encoding and decoding of JSON

{@link module:jsonselect|jsonselect}: A module for traversing complex JavaScript Objects with a CSS selector-like sytax

{@link module:keymaster|keymaster}: A cross browser implementation for handeling keyboard events

{@link module:localstorage|localstorage} a cross browser abstraction for client side data persistance

{@link module:log|log} : Cross browser implementation around console log. Where console log will probably crash your apps in IE, log will not

{@link module:modernizr|modernizr}: A copy of the popular [Modernizer](http://modernizr.com/) library

{@link module:number|number}: module for working with Javascript Numbers

{@link module:object|object}: A module for working with and manipulating javascript object literals

{@link module:operator|operator}: a helper module for logical operators

{@link module:q|q}: A module for CommonJS Promises

{@link module:request|request}: Cross browser module for performing XMLHttpRequests

{@link module:signals|signals}: A unified Event handeling system

{@link module:string|string}: helper module for working with and manipulating strings

{@link module:swf|swf} : module for creating and communicating with  SWF objects

{@link module:text|text}: A Loader plugin for loading files as plain text with out evaluation

{@link module:threading|threading}: A cross-browser implementation of WebWorkers

{@link module:underscore|underscore}: a copy of the [underscore](http://underscorejs.org/) library

{@link module:url|url}: a small library for parsing urls into its logical fragments

{@link module:useragant|useragant}: A small module for user agent detection

{@link module:util|util}: a small utility library used internally by braveheart

{@link module:wallace|wallace.io}: The complimetary module for connecting to a [Wallace.io](https://git.corvisa.com/projects/PB/repos/wallace-io/browse) enabled application

### Packages
When modules require a large set of other modules, they are broken out into packages. The Scope of a package is inherently broader than that of a module and as a result provides a greater set of functionality. Braveheart ships with ten packages

`CodeMirror`: A Crossbrowser code editing widget

`Collections`: Abstract Data Collections

`Compression`: Provides the Standard RFC1951 Deflate & Inflate compression algorythms

`Crypto`: A Package for dealing with data encryption and hashing

{@link data|Data}: A micro MVC and Router pacakge

`Native`: A package that applies the core primitive modules ( {@link module:array|array}, {@link module:object|object}, etc) and Native JavaScript extensions

`Slick`: A light weight CSS Selector Engine

`SWIG`: A templating engine similart to the Django Template Engine

`XDM`: A packages enabeling WebSocket and Cross Domain RPC functionality

{@link dom|DOM}: A cross browser abstraction around Creating & Manipulating the DOM


### Tools
Braveheart is not only javascript library, but ships with a small set of development tools to help developers focus on development and not on routine tasks. Currently the Toolchain comes with

 * JSHint - Automatically Lint entire code bases, identifying potential problems
 * JSDoc  - Automatically generate API documentation for any Javascript Code Base
 * Test Runner - Execute unit tests from the command line or in the browser to ensure code is free of bugs
 * Groc - Automatically generate annotated Source code from inline comments and documentation
 * Build Tool - Create optimized builds of braveheart, or any codebase that adheres to the AMD format
