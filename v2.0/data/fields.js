/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Provides the default field classes for the data package
 * @module module:data.fields
 * @author Eric Satterwhite
 * @requires module:data.Field
 * @requires module:data.NumberField
 * @requires module:data.DateField
 **/
define([
			"require"
			,"exports"
			,"module"
			,'./Fields/Field'
			,'./Fields/NumberField'
			,'./Fields/DateField'
			,'./Fields/IntegerField'
			,'./Fields/BooleanField'
			,'./Fields/CharField'
			,'./Fields/SlugField'
			,'./Fields/PositiveIntegerField'
			,'./Fields/PKField'
			,'./Fields/ForeignKeyField'
			,'./Fields/ManyToManyField'
		]
		,function( require, exports, module, Field, NumberField, DateField, IntegerField, BooleanField, CharField, SlugField, PositiveIntegerField, PKField, ForeignKeyField, ManyToManyField){
		return {
			""			 : Field
			,auto   	 : Field
			,Field  	 : Field
			,number 	 : NumberField
			,date   	 : DateField
			,"int" 		 : IntegerField
			,"integer"   : IntegerField
			,bool   	 : BooleanField
			,"boolean"   : BooleanField
			,"char" 	 : CharField
			,posint 	 : PositiveIntegerField
			,pk     	 : PKField
			,fk     	 : ForeignKeyField
			,m2m		 : ManyToManyField
		}
	}
);
