/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires model/
 * @requires moduleB
 * @requires moduleC
 **/
define([
		"require"
		,"module"
		,"exports"
		,"data/model/engine/Memory"
	],function(require, module, exports, Memory){
    var  Memory = require( './model/engine/Memory' );


    return {
    	"":Memory
    	,"memory":Memory
    }
});
