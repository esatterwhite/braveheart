/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.validators
 * @author Eric Satterwhite
 **/
define([
		"require"
		,"exports"
		,"module"
		,'./validators/Presence'
		,'./validators/Email'
		,'./validators/Format'
		,'./validators/Include'
		,'./validators/Exclude'
		,'./validators/Length'
	]
	,function(require, exports, module, Presence, Email, Format, Include, Exclude, Length){

		return {
			"":        Presence
			,presence: Presence
			,length:   Length
			,email :   Email
			,format:   Format
			,include:  Include
			,exclude:  Exclude
		}
	}
);
