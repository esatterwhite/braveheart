/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 **/
define([
		"require"
		,"module"
		,"exports"
		,"class"
		,"./Field"
		,"functools"
	],function(require, module, exports, Class, Field, functools ){
	var  PositiveIntegerField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	PositiveIntegerField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Field,

		clean: functools.protect( function( val ){
			try{
				return Math.abs( parseInt( val, 10 ) )
			} catch( e ){
				return val;
			}
		})
	});

	return PositiveIntegerField;
});
