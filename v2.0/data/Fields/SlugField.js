/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 * @requires string
 **/
define([
		"require"
		,"module"
		,"exports"
		,"class"
		,"./Field"
		,"functools"
		,"string/slugify"
	],function(require, module, exports, Class, Field, functools, slugify){
	var  SlugField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	SlugField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Field,
		clean: functools.protect( function( val ){
			return slugify( ""+val );

		})
	});

	return SlugField;
});
