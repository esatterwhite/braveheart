/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.Field.BooleanField
 * @author Eric Satterwhite
 * @requires class
 * @requires functools
 * @requires Field
 **/
define(["require", "module", "exports", "class", "functools", "./Field"],function(require, module, exports, Class, functools, Field){
	var  BooleanField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	BooleanField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Field,
		options:{
			defaultValue: false
			,nullable: false
		}
		,clean: functools.protect( function( val ){
			return !!val;
		})
	});

	return BooleanField;
});
