/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Simple helper for fields that deal with related items
 * @module data/util
 * @author Eric Satterwhite
 * @requires core
 * @requires array
 **/
define(function(require, exports){
	var  core = require( 'core' )
		, array = require( 'array' )
		, typeOf = core.typeOf
		, Relation;

	/**
	 * DESCRIPTION
	 * @class module:data/util.Relation
	 * @param {Model|Array} Any Number of Model instances you want to instanciate the collection with. Or an array of instances
	 */
	Relation = function(){
		if( arguments.length ){
			this.push.apply( this, arguments);
		}
	};

	Relation.prototype = [];

	Relation.implement(/** @lends module:data/util.Relation.prototype */{

		/**
		 * Length of the Relation Collection
		 * @property Length
		 * @Type Number
		 **/

		 length:0

		 /**
		  * Returns the item at a specified index, if one exists
		  * @method data/util.Relation#getAt
		  * @param {Number} idx ...
		  **/
		 ,getAt: function( idx ){
		 	return this[ idx ]
		 }

		 /**
		  * Executes a function for every item in the collection
		  * @method module:data/util.Relation
		  * @chainable
		  * @see module:array#each
		  * @param {fn} The function to execute
		  * @param {scope} the context to execute the function under
		  * @return {Relation} The current relation instance
		  **/
		 ,each: function(fn, scope ){
		 	array.each.call( scope, this, fn);
		 	return this;
		 }

		 /**
		  * Finds the first model that passes the check function
		  * @method module:data/util.Relation#findBy
		  * @param {Function} checkFn function used to test each item in the collection. will be passed the item, and its index. This function should return true or false
		  * @param {Object} [scope] The context to execute the passed in function under
		  * @return {Model|null} The model instance that passed, if there is one
		  **/
		 ,findBy: function( fn, scope ){
		 	for( var x = 0; x<this.length; x++ ){
		 		if( fn.call( scope, this[x], x) ){
		 			return this[x]
		 		}
		 	}
		 	return null;
		 }

		 /**
		  * Injects a model instance into the collection at a specified instance
		  * @chainable
		  * @method module:data/util.Relation#insert
		  * @param {Model} item The model instance to insert
		  * @param {Number} [index=0] The index at which to insert the model
		  * @return {Relation} The current relation instance
		  **/
		 ,insert: function(item, idx ){
		 	if( !item.isModel ){
		 		return this;
		 	}

		 	array.insert(this, item, idx);

		 	this.length++
		 	return this;
		 }

		 /**
		  * Removes the specified item from the collection
		  * @chainable
		  * @method module:data/util.Relation#remove
		  * @param {Model} item The model instance to remove
		  * @return {Relation} The current Relation instance
		  **/
		 ,remove: function( item ){
		 	array.erase( item )
		 	this.length--;
		 	return this;
		 }

		 /**
		  * Creates a new collection containing only the items which passed the filter function
		  * @method module:data/util.Relation#collect
		  * @see module:array#filter
		  * @param {Function} fn The function to filter items out of the current collection. If it returns true, the item will be included
		  * @param {Object} [scope] The context to execute the function under
		  * @return {Relation} A new Relation instance
		  **/
		 ,collect: function( fn, scope ){
		 	var collected = [];

		 	collected = array.filter( this, fn, this);

		 	return new Relation( collected );
		 }
		/**
		 * push any number of model instances. You can also push another Relation collection and they will be combined
		 * @chainable
		 * @method module:data/util.Relation#push
		 * @param {Model|Array|Relation} item The item to push onto the collection
		 * @return {Relation} The current instance
		 **/
		,push: function( ){
			var len = arguments.length
				,arg
			for( var x = 0; x < len; x++ ){
				arg = arguments[ x ];
				if(typeOf( arg ) === 'array' || arg instanceof Relation ){
					this.push.apply( this, Array.prototype.slice.call( arg, 0))
				} else if( arg.isModel ){
					this[this.length++] = arg;
				}
			}
			return this;
		}
	});
	return Relation;

});
