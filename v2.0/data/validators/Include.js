/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data/validators/Validator.Include
 * @author Eric Satterwhite
 * @requires class
 * @requires array
 * @requires Validator
 **/
define(["require", "module", "exports", "class", "./Validator", "array/from", "array/contains" ],function(require, module, exports, Class, Validator, arrayfrom, arraycontains ){
	var Include;
	Include = new Class({
		Extends:Validator
		,options:{
			include: []
		}

		, validate: function( value ){
			var includes = arrayfrom( this.options.include );
			return arraycontains( includes, value );
		}
	});

	return Include;

});

