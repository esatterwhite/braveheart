/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data/validators/format
 * @author Eric Satterwhite
 * @requires class
 * @requires module:data/validators/Validator
 **/
define(["require", "module", "exports", "class", "./Validator"], function(require, module, exports, Class, Validator ){
	var Format;


	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Format = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Validator
		,options:{
			test:/[w+]/
		}

		, validate: function( value ){
			var test = this.options.test;

			if( typeof test === "function"){
				return test( value );
			}

			return test.test( value )
		}
	})

	return Format;

});

