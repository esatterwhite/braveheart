/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Custom Error for Router Class
 * @module data.Error.404
 * @author Eric Satterwhite
 * @requires core
 **/
define(["require", "exports", "module", "core"],function(require, exports, module, core){

	return new core.error.NamedError("404", "Unable to Match Route");
});