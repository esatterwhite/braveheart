define(function(){
	var BraveheartError = function( name, msg ){
		this.message = msg
		this.stack = new Error().stack
		this.name = name || "BraveheartError"
	}

	BraveheartError.prototype = new Error();

	return BraveheartError;
})
