/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Provides the LocalStorage interface for model storage engines. This engine doesn't actually do anything.
 * @module module:data.engines.Base
 * @author Eric Satterwhite
 * @requires class
 * @requires object
 **/
define(['require', 'exports', './Base', 'class', 'object'], function(require, exports, Base, Class, object){
	var noop = function(){}
		,_localstorage = require( "localstorage" )
		,LocalStorage
		,getId;


	getId = function( ){
		return ( (+ new Date()).toString(36) );
	};

	/**
	 * DESCRIPTION
	 * @class module:data.engines.LocalStorage
	 * @param {TYPE} module:data.engines.LocalStorage DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	LocalStorage = new Class(/** @lends module:data.engines.LocalStorage.prototype */{
		 Implements:[ Class.Options ]
		,Extends:Base

		,storageKey:"braveheart:model"
		,storageId:null
		/**
		 * DESCRIPTION
		 * @method module:data.engines.LocalStorage#POST
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,POST: function( id, val ){
			val = val || {};
			_localstorage.set( (this.storageKey + ":" + id), val);
			return val;
		}

		/**
		 * DESCRIPTION
		 * @method module:data.engines.LocalStorage#GET
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,GET: function( id ){
			return _localstorage.get( (this.storageKey + ":" + id) );
		}

		/**
		 * DESCRIPTION
		 * @method module:data.engines.LocalStorage#PUT
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,PUT: function( id, val ){
			val = val || {};
			var mem
				,key;

			key = (this.storageKey + ":" + id);
			mem = _localstorage.get ( key );
			mem = object.merge( mem, val );



			_localstorage.set( key, mem);

			return mem;

		}

		/**
		 * DESCRIPTION
		 * @method module:data.engines.LocalStorage#DELETE
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,"DELETE": function( id ){
			_localstorage.remove( this.storageKey + ":" + id);
		}
	});

	return LocalStorage;
});
