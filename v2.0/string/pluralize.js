/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/pluralize
 * @see {@link module:string.pluralize|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        /**
         * Based on a count, will return either a specified singluar or plural version of a word. 
         * Will return the singular version if the count is 1.
         * @param {Number} count The count to use as a qualifier
         * @param {String} singular The singular form of the word
         * @param {String} plural The plural form of the word
         * @return {String} either the singular or plural word based on the count
         * @name pluralize
         * @function
         * @memberof module:string
         */
        return function( count, singular, plural ){
            return ( Math.abs( count ) === 1 ) ? singular : plural;
        };

 });
