/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/stripNonAlpha
 * @see {@link module:string.stripNonAlpha|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
	

		/**
		 * Strip all non alpha characters from a string
		 * @param {String} str The string to strip
		 * @returns {String}
		 * @name stripNonAlpha
         * @function
         * @memberof module:string
		 */      
		return function( str ){
			var s = String( str );
			return s.replace(/[^A-Za-z ]+/g, "");
		};

 });


