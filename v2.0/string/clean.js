/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/clean
 * @see {@link module:string.clean|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'string/trim']
    ,function( require, exports, module, trim ) {

        /**
         * Removes extraneous white space from a string
         * @param {String} str The string clean
         * @returns {String}
         * @name clean
         * @function
         * @memberof module:string
         * @example braveheart([ 'string '], function( string ){
        string.clean( " this     is a lame      sentance") // this is a lame sentance
    })
         */
        return function(str) {
            var s = String( str );
            return trim((s).replace(/\s+/g, ' '));
        };

 });

