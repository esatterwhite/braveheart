/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/startsWith
 * @see {@link module:string.startsWith|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
   

		/**
		 * Determines if a string starts with a specified bit of text
		 * @param {String} str the string to inspect
		 * @param {String} prefix The bit of text you want to check for
		 * @returns {Boolean} true if the specified string ends with the prefix
		 * @name startsWith
         * @function
         * @memberof module:string
		 * @example braveheart(['string'], function( string ){
		string.endsWith("Helloworld", "world") // true
		string.endsWith("Helloworld", "hello") // false
	})
		 */   
		return function(str, prefix) {
			var s = String( str );
			prefix = prefix || "";

			return s.indexOf( prefix ) !== -1;
		};

 });


