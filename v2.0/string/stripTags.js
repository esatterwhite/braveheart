/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/stripTags
 * @see {@link module:string.stripTags|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        var getRegexForTag = function(tag, contents){
            tag = tag || '';
            var regstr = contents ? "<" + tag + "(?!\\w)[^>]*>([\\s\\S]*?)<\/" + tag + "(?!\\w)>" : "<\/?" + tag + "([^>]+)?>",
                reg = new RegExp(regstr, "gi");
            return reg;
        };
        
        /**
         * Strips specific tags from html strings
         * @param {String} str The string to strip tags from
         * @param {String} tag
         * @param {Boolean} contents Set to true if you want to contents of the tag striped as well
         * @returns {String}
         * @name stripTags
         * @function
         * @memberof module:string
         * @example braveheart(['string'], function( string ){
        string.stripTags( "&lt;div&gt;&lt;span&gt;hello world&lt;/span&gt;&lt;/div&gt;", 'span' ) // &lt;div&gt;hello world&lt;/div&gt;
        string.stripTags( "&lt;div&gt;&lt;span&gt;hello world&lt;/span&gt;&lt;/div&gt;", "span", true ) // &lt;div&gt;&lt;/div&gt; 
    })
         */    
        return function(str, tag, contents ){
            var s = String( str );
            return s.replace( getRegexForTag(tag, contents), "" );
        };

 });



