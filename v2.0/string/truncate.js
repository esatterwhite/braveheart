/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/truncate
 * @requires module:array
 * @see {@link module:string.truncate|Method documentation}
 */
define(
    ['require', 'exports', 'module','array/from', 'array/append']
    ,function( require, exports, module, from, append ) {

		/**
		 * Attempts to truncate a string at a cutoff point
		 * @param {String} str The string to truncate
		 * @param {Number} max The maximum number of characters
		 * @param {String} tail An optional string to append to the string as a truncation indicator. If spcified, the number of characters in the tail will be count towards the character limit
		 * @param {String}
		 * @name truncate
		 * @function
		 * @memberof module:string
		 */      
		return function(str, maxLength, tail) {
			var s = String( str );
			if (s == null || s.length <= maxLength || maxLength < 0) {
				// readable version, not the String object
				return s.toString();
			} else if (tail != null) {
				s = s.slice(0, Math.max(0, maxLength - tail.length));
				if (typeof s === 'string') {
					return s + tail;
				} else {
					return append(from(s), from(tail));
				}
			} else {
				return s.slice(0, maxLength);
			}
		};

 });