/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/contains
 * @see {@link module:string.contains|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      	
	    /**
		 * Tries to determine if a part of a string exists within another string
		 * @param {String} substr The string to look for
		 * @param {String} str The string to look through
		 * @return {Boolean} True if the substring is found
		 * @name contains
         * @function
         * @memberof module:string
		 * @example braveheart([ 'string '], function( string ){
		string.contains("hell", "hello world") // true
		string.contains("bell", "hello world") // false
	})	
		 */
		return function(str, substr) {
			return str != null && substr != null && str.indexOf(substr) >= 0;
		};

 });