/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/camelCase
 * @see {@link module:string.camelCase|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Takes a hyphenated string and converts it to a camelCased string 
         * @param {String} str The string to camelCase
         * @returns {String}
         * @name camelCase
         * @function
         * @memberof module:string
         */     
        return function( str ) {
            var s = String( str );
            return s.replace(/-\D/g, function(match){
                return match.charAt(1).toUpperCase();
            });
        }; 

 });
