/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/from
 * @see {@link module:string.from|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        /**
         * Creates a string from the passed in item
         * @param {Object} item The object to convert to a string
         * @return {String}
         * @name from
         * @function
         * @memberof module:string
         */  
        return function(item) {
            return "" + item;
        };

 });