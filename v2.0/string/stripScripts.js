/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/stripScripts.js
 * @see {@link module:string.stripScripts|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {

        var exec = function(text){
                if (!text){
                    return text;
                }
                if (window.execScript){
                    window.execScript(text);
                } else {
                    var script = document.createElement('script');
                    script.setAttribute('type', 'text/javascript');
                    script.text = text;
                    document.head.appendChild(script);
                    document.head.removeChild(script);
                }
                return text;
        };
        
        /**
         * Will attempt to strip anything that looks like script tags, and anything inside of script tags from a string
         * @param {String} str The string to strip
         * @param {Boolean|Function} exec If set to true. if a function is passed, it will be used as the function to execute the scripts
         * @return {String} the text from within the script tag
         * @name stripScripts
         * @function
         * @memberof module:string
         */ 
        return function( str, execute ){
            var scripts = "";
            var s = String( str );
            var text = s.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function(all, code){
                            scripts += code + '\n';
                            return '';
                        });

            if( typeof execute === 'boolean' ){
                exec( scripts );
            } else if( typeof execute === 'function'){
                execute( scripts );
            }

            return text;
        };

 });