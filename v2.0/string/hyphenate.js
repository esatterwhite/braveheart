/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/hyphenate
 * @see {@link module:string.hyphenate|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
		
		/**
		 * Takes a CamelCased string string and converts it to a hyphenated string
		 * @param {String} str The string to camelCase
		 * @return {String}
		 * @name hyphenate
         * @function
         * @memberof module:string
		 */      
		return function( str ){
			return str.replace( /[A-Z]/g, function( match ){
				return ("-" + match.charAt(0).toLowerCase() );
			});
		};

 });



