/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/removeNonWord 
 * @see {@link module:string.removeNonWord|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {

        /**
         * Attempts to remove any non word strings of text from a string
         * @param {String} str The string to parse
         * @return {String} A new string with any non words removed
         * @name removeNonWord
         * @function
         * @memberof module:string
         */      
        return function( str ){
            var s = String( ( str|| "") );
            return (s).replace(/[^0-9a-zA-Z\xC0-\xFF \-]/g, ''); //remove non-word chars

        };

 });
