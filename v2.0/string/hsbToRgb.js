/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/hsbToRgb
 * @requires module:array
 * @see {@link module:string.hsbToRgb|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/hsbToRgb']
    ,function( require, exports, module, hsbToRgb ) {
        
         /**
         * Converts an HSB string to an array of RGB values
         * @param {String} str The string to convert
         * @returns {Array} An array of rgb values. Will return null if an HSB string is not identified
         * @name hsbToRgb
         * @function
         * @memberof module:string
         * @example braveheart(['string'], function( string ){
        string.hsbToRgb( "hsb( 0, 0, 9 )" ) // [22, 22, 22]
    })
         */
        return function( str ){
            var hsb = str.match(/\d{1,3}/g);
            return (hsb) ? hsbToRgb( hsb ) : null;
        };

 });
