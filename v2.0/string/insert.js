/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/insert
 * @see {@link module:string.insert|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        /**
         * Inserts a string at the specified index.
         * @param {String} string The string to insert into
         * @param {Number} index The index at which to insert the new string
         * @param {String} string The string to insert
         * @name insert
         * @function
         * @memberof module:string
         */      
         return function (str, index, string) {
            if (index > 0) {
                return str.substring(0, index) + string + str.substring(index, str.length);
            } else {
                return string + str;
            }
        };

 });