/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/toFloat
 * @see {@link module:string.toFloat|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
 
        /**
         * Attempts to convert a string to a floating point number
         * @param {String} str The string to convert
         * @name toFloat
         * @function
         * @memberof module:string
         * @return {Number}
         */     
        return function( str ){
            var num = parseFloat( str );

            return isNaN( num ) ? 0.0 : num;
        };

 });



