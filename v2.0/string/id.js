/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/id
 * @see {@link module:string.id|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        /**
         * Generates a unique ID
         * @returns {String}     
         * @name id
         * @function
         * @memberof module:string
         */
        return function( ){
            return (+ new Date()).toString( 36 );
        };

 });