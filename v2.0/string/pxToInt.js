/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/pxToInt
 * @see {@link module:string.pxToInt|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'string/toInt']
    ,function( require, exports, module, toInt ) {
        
        /**
         * Converts a string in the format of 12px and converts it into an integer
         * @param {String} str the string to convert
         * @return {Number}
         * @name pxToInt
         * @function
         * @memberof module:string
         */      
        return function( str ){
            return toInt( str.split('px')[0], 10);
        };

 });


