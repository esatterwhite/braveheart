/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/toInt
 * @see {@link module:string.toInt|Method documentation}
 */

define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Attempts to convert a string to a number
         * @param {String} str The string to convert
         * @param {Number} base The numeric base to use as a conversion radix. The default is 10
         * @return {Number} The string as an integer
         * @name toInt
         * @function
         * @memberof module:string
         */
        return function( str, base ){
            var num = parseInt( str, base || 10 );

            return isNaN( num ) ? 0 : num;
        };

 });
