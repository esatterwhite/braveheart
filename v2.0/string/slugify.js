/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}


/**
 * @author Eric Satterwhite
 * @module string/slugify
 * @see {@link module:string.slugify|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'string/trim', 'string/replaceAccents', 'string/removeNonWord']
    ,function( require, exports, module, trim, replaceAccents, removeNonWord ) {
        

       /**
         * Converts a string into a url safe slug
         * @param {String} str the string to convert
         * @return {String} slug a new slug
         * @name slugify
         * @function
         * @memberof module:string
         */   
        return function( str ){
            var s = String( str || "" );

            s = trim(
                    removeNonWord(
                        replaceAccents(
                            s
                        )
                    )
                );

            s = s.replace(/ +/g, "-").toLowerCase();

            return s;

        };

 });
