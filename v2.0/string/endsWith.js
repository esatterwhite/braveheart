/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/endsWith
 * @see {@link module:string.endsWith|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {


        /**
         * Determines if a string ends with a specified bit of text
         * @param {String} str the string to inspect
         * @param {String} suffix The bit of text you want to check for
         * @returns {Boolean} true if the specified string ends with the suffix
         * @name endsWith
         * @function
         * @memberof module:string
         * @example braveheart(['string'], function( string ){
        string.startsWith("Helloworld", "hello") // true
        string.startsWith("Helloworld", "world") // false
    })
         */
        return function(str, suffix) {
            var s = String( str );
            suffix = suffix || "";

            return s.indexOf( suffix, s.length - suffix.length ) !== -1;
        };

 });





