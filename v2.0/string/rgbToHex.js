/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/rgbToHex
 * @see {@link module:string.rgbToHex|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/rgbToHex']
    ,function( require, exports, module, rgbToHex ) {
        
        /**
         * Convert string RGB value to hex
         * @param {String} str The RGB string to convert to HEX
         * @returns {String}
         * @name rgbToHex
         * @function
         * @memberof module:string
         */     
        return function( str ){
            var rgb = String(str).match(/\d{1,3}/g);
                return (rgb) ? rgbToHex(rgb) : null;
        };

 });