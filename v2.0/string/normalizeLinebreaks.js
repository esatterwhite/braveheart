/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/normalizeLinebreaks
 * @see {@link module:string.normalizeLineBreaks|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
         /**
         * Attempts to normalize line breaks in a string
         * @param {String} str the string to normalize
         * @param {String} lineEnd The character to use as the break character. Defaults to "\n"
         * @return {String} str a new string with normalized line breaks
         * @name normalizeLineBreaks
         * @function
         * @memberof module:string
         */
        return function( str, lineEnd ){
            var s = String( str || "" );

            lineEnd = lineEnd || '\n';

            s = s
                .replace(/\r\n/g, lineEnd) // DOS
                .replace(/\r/g, lineEnd)   // Mac
                .replace(/\n/g, lineEnd);  // Unix

            return s;
        };

 });
