/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/trim
 * @see {@link module:string.trim|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        /**
         * Trim whitespace at beginning and end of a string
         * @param {String} str The string to trim
         * @returns {String}
         * @name trim
         * @function
         * @memberof module:string
         * @example braveheart([ 'string '], function( string ){
        string.trim( "  this is it  " ) //this is it
    })
         */     
		return function(str) {

			var s = String( str );
			return s.replace(/^\s+|\s+$/g, '');
		};

 });
