/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/padLeft
 * @see {@link module:string.padLeft|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
		
		/**
		 * Pads at the beginning of a string with a passed in string
		 * @param {String} str the string to pad
		 * @param {Number} minLength The length at which to stop padding the string
		 * @param {String} fillChar The characters to pad the string with on each iteration
		 * @return {String}
		 * @name padLeft
         * @function
         * @memberof module:string
		 * @example braveheart(['string'], function( string ){
		string.padLeft("sh", 4, "x") // xxsh
	})
		 */      
		return function(str, minLength, fillChar) {
			var s = String( str || "" );
			fillChar = fillChar || " ";

			while (s.length < minLength) {
				s =  fillChar + s;
			}

			return s;
		};

 });