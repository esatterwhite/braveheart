/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/typeCast
 * @see {@link module:string.typeCast|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
        /**
         * Attempts to convert a string to an analogous data type ( ex "true" -> true )
         * Works on null, booleans, undefineds, number and floats
         * @param {String} str the string to type cast
         * @return {Mixed} a data type that matched the passed in item
         * @name typeCast
         * @function
         * @memberof module:string
         */
        return function( val ){
            var r
                ,UNDEF;

            if( val === null || val === 'null'){
                return null;
            } else if( val === 'true' ){
                return true;
            } else if( val === 'false' ){
                return false;
            } else if( val === UNDEF || val === 'undefined' ){
                r = UNDEF;
            } else if( val === '' || isNaN( val )){
                r = val;
            } else{
                r = parseFloat( val );
            }

            return r;
        };

 });