/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/escapeRegEx
 * @see {@link module:string.escapeRegEx|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        /**
         * Escapes Regular expression chars from a string
         * @param {String} str The string to escape
         * @returns {String} An escaped string
         * @name escapeRegEx
         * @function
         * @memberof module:string
         */       
        return function( str ){
            return String( str ).replace( /([-.*+?^${}()|[\]\/\\])/g, '\\$1' );
        };

 });
