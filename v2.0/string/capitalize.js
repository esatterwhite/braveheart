/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/capitalize
 * @see {@link module:string.capitalize|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
        /**
         * Capitalizes every word in a string
         * @param {String} str The string to capitalize
         * @returns {String}
         * @name capitalize
         * @function
         * @memberof module:string
         * @example braveheart([ 'string '], function( string ){
        string.capitalize( "this is it" ) // This Is It
    })
         */
        return function( str ){
            var s = String( str );
            return s.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };

 });