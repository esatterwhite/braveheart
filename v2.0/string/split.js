/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/split
 * @see {@link module:string.split|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        /**
         * Splits a string on a specified separator and returns a limited subset of pieces
         * @param {String} str The string to split
         * @param {String} separatar The separator to split the string on. Defaults to newline
         * @param {Number} max The maximum number of string peices to return. If not specified, all peices are returned
         * @name split
         * @function
         * @memberof module:string
         **/     
       return function(str, separator, max) {
            if (!str) {
                return str;
            }
            var s = String( str );
            separator = separator || "\n";

            var bits = s.split(separator);
            if (max == null || max >= bits.length - 1) {
                return bits;
            }

            bits.splice(max, bits.length, bits.slice(max, bits.length).join(separator));
            return bits;
        };

 });
 