/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/padRight
 * @see {@link module:string.padRight|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {

		/**
		 * Pads at the end of a string with a passed in string to match the specified length
		 * @param {String} str the string to pad
		 * @param {Number} minLength The length at which to stop padding the string
		 * @param {String} fillChar The characters to pad the string with on each iteration
		 * @return {String}
		 * @name padRight
         * @function
         * @memberof module:string
		 * @example braveheart(['string'], function( string ){
		string.padRight("sh", 4 "x") // shxx
	})
		 */      
		return function(str, minLength, fillChar) {
			var s = String( str || "" );
			fillChar = fillChar || " ";

			while (s.length < minLength) {
				s += fillChar;
			}

			return s;
		};

 });


