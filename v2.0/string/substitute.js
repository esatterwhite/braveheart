/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/substitute
 * @see {@link module:string.substitute|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
            /**
             * Replaces place placeholders( {holder} ) in a string with the values of matching keys in an object
             * @param {String} str The string to manipulate
             * @param {Object} obj The object to pull values from
             * @param {RegEx} regex A regular expression to use instead of the default
             * @name substitute
             * @function
             * @memberof module:string
             * @example braveheart(['string']. function( string ){
            string.substitute( "Hello {place}, my name is {name}!",
            {
                place:'world',
                name:'Joe'
        })     //Hello world, my name is Joe!
             */
        return function(str, obj, regex) {
            str = str || "";

            var s = String( str );
            s = s.replace(regex || (/\\?\{([^{}]+)\}/g), function(match, name) {
                if (match.charAt(0) == '\\') {
                    return match.slice(1);
                }
                return (obj[name] != null) ? obj[name] : "";
            });

            return s;
        };

 });