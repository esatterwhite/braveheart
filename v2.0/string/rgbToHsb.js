/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * Converts an rgb string to an array of HSB values
 * @author Eric Satterwhite
 * @module string/rgbToHsb
 * @requires module: array
 * @see {@link module:string.rgbToHsb|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/rgbToHsb']
    ,function( require, exports, module, rgbToHsb ) {
        
        /**
         * Converts an rgb string to an array of HSB values
         * @param {String} str The string to convert
         * @returns {Array} An array of HSB values. will return null if an rgb string is not identified
         * @name rgbToHsb
         * @function
         * @memberof module:string
         * @example braveheart(['string'], function( string ){
    string.rgbToHSB( "rgb( 22, 22, 22 )" ) // [0, 0, 9]
    })
         */  
        return function( str ){
            var rgb = str.match(/\d{1,3}/g);
            return (rgb) ? rgbToHsb( rgb ) : null;
        };

 });


