/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module string/reverse
 * @see {@link module:string.reverse|Method documentation}
 */

define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
        
        /**
         * Attempts to reverse the characters of a string
         * @param {String} str the string to reverse
         * @return {String}
         * @name reverse
         * @function
         * @memberof module:string
         */            
        return function( str ){
            var s = String( str || "");
            return s.split('').reverse().join('');
        };

 });



