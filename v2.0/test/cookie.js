var cookie
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	, nodeRequire: require
	,project: __dirname
});

cookie = requirejs('./test/modules/cookie');

exports['Cookie Tests'] = testCase(cookie.tests);