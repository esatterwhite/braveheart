var util
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	, nodeRequire: require
});

util = requirejs('./test/modules/util');

exports['Utilities Tests'] = testCase(util.tests);