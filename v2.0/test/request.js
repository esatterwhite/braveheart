var request
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	,nodeRequire: require
	,packages:[{
			name:'slick'
			,location:"../packages/Slick"
		}
		,{
			name:'crypto'
			,location:"../packages/Crypto"
		}
		]
});

request = requirejs('./test/modules/request');

exports['Request Tests'] = testCase(request.tests);