var useragent
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	,nodeRequire: require
	,packages:[{
			name:'slick'
			,location:"../packages/Slick"
		}
		]
});

useragent = requirejs('./test/modules/useragent');

exports['User Agent Tests'] = testCase(useragent.tests);