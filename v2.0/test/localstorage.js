 var localstorage
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	, nodeRequire: require
	,project: __dirname
});

localstorage = requirejs('./test/modules/localstorage');

exports['Localstorage Tests'] = testCase(localstorage.tests);