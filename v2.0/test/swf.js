var swf
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	,nodeRequire: require
	,packages:[{
			name:'slick'
			,location:"../packages/Slick"
		}
		]
});

swf = requirejs('./test/modules/swf');

exports['SWF Tests'] = testCase(swf.tests);