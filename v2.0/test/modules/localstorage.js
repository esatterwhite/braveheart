define(['require', 'module', 'exports', 'localstorage', 'test/zombie/zombieserver'], function( require, module, exports, localstorage, zombieserver ) {
	exports.tests = {
		setUp: zombieserver.start

		,tearDown: zombieserver.kill

		,get: function( test ) {
			test.expect(3);
			var that = this;
			
			that.browser.visit( zombieserver.url, function() {       
				window = that.browser.window;

				//Set via native method
				var lunch1 = { food: 'tacos', drink: 'sodapop' };
				window.localStorage.setItem( 'lunch', JSON.stringify(lunch1) );
				window.localStorage.setItem( 'snack', 'plantains' );

				//Get via Braveheart
				var lunch2 = localstorage.get( 'lunch' );
				var snack = localstorage.get( 'snack' );
				var nully = localstorage.get( 'nullsauce' );


				test.deepEqual( lunch1, lunch2, 'Set and retrieved objects should be identical' );
				test.equal( snack, 'plantains', 'Set and retreived strings should be identical' );
				test.equal( nully, null, 'Tryin g to get nonexistent localstorage item should return null' );
				test.done();            
			});
		}

		,set: function( test ) {
			test.expect(2);
			var that = this;

			that.browser.visit( zombieserver.url, function() {    
				window = that.browser.window;

				var lunch1 = { food: 'tacos', drink: 'sodapop' };

				//Set via Braveheart
				localstorage.set( 'lunch', lunch1 );
				localstorage.set( 'snack', 'plantains' );


				//Get via native method
				var lunch2 = JSON.parse( window.localStorage.getItem('lunch') );
				var snack = window.localStorage.getItem( 'snack' );

				test.deepEqual( lunch1, lunch2, 'Set and retrieved objects should be identical' );
				test.equal( snack, 'plantains', 'Set and retrieved strings should be identical' );
				test.done();            
			});
		}

		,remove: function( test ) {
			test.expect(1);
			var that = this;

			that.browser.visit( zombieserver.url, function() {    
				window = that.browser.window;

				var lunch1 = { food: 'tacos', drink: 'sodapop' };

				//Set, delete, and get via Braveheart
				localstorage.set( 'lunch', lunch1 );
				localstorage.remove( 'lunch' );
				var x = localstorage.get( 'lunch' );

				test.equal( x, null, 'Removed storage objects should be retrieved as null' );
				test.done();            
			});
		}
	};
});