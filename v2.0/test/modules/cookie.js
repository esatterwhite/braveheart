define(['require', 'module', 'exports', 'cookie', 'test/zombie/zombieserver'], function( require, module, exports, cookie, zombieserver ) {
  	exports.tests = {

    	setUp: zombieserver.start

		,tearDown: zombieserver.kill

		,read: function( test ) {
      		test.expect(3);
			var that = this;
			
			that.browser.visit(zombieserver.url, function() {
  				window = that.browser.window;

  				//Set via zombie method and retrieve via Braveheart method
  				that.browser.setCookie('thisShould', 'beWritten');
          		that.browser.setCookie('emptyVal', '');

  				test.equal('beWritten', cookie.read('thisShould'), "Cookies should be grabbable by key");
          		test.equal(cookie.read('tacos'), null, "Nonexistent cookies should return null");
          		test.equal(cookie.read('emptyVal'), '', "Empty string cookies should return empty strings");
  				test.done();  					
  			});

		}

		,write: function( test ) {
      		test.expect(3);
			var that = this;
			
			this.browser.visit(zombieserver.url, function() {
	          	window = that.browser.window;
	          
	          	//Set via Braveheart method and retrieve via zombie method
		        cookie.write('strShould', 'beWritten');
		        cookie.write('numShould', 101);
		        cookie.write('shouldEncode', ' val');

  				//Get via zombie method
  				var strVal = that.browser.getCookie('strShould');
          		var numVal = that.browser.getCookie('numShould'); 	

  				test.equal('beWritten', strVal, "Cookies should be writable: string");
		        test.equal('101', numVal, "Cookies should be writable: number");
		        test.ok(that.browser.document.cookie.indexOf('shouldEncode=%20val') !== -1, "Cookie values should be encoded by default");
  				test.done();  					
  	  		});    
	  	}
	};
});