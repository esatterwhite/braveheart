define(['require', 'module', 'exports', 'useragent', 'dom', 'test/zombie/zombieserver'] 
	,function( require, module, exports, useragent, dom, zombieserver ) {
		
	exports.tests = {
		setUp: zombieserver.start

		,tearDown: zombieserver.kill

		,basic: function( test ) {
			test.expect(2);
			var that = this;
		
			that.browser.visit( zombieserver.url, function() {

				//So that Braveheart's dom-ish modules can find DOM/BOM objects in Zombie
				window = that.browser.window, navigator = window.navigator, document = window.document;

				test.ok(typeof useragent.whoami === 'string' && useragent.whoami.length > 0, 'User agent string should be returned');
				test.ok(useragent.isZombie, 'UA should initially be identified as Zombie');
				test.done();                 
			});
		}

	};
});