define(['require', 'module', 'exports', 'test/zombie/zombieserver', 'dom', 'core'] , function( require, module, exports, zombieserver, dom, core) {

	exports.tests = {
		setUp: zombieserver.start

		,tearDown: zombieserver.kill

		,id: function( test ){
			test.expect(4);
			var that = this;

			that.x = that.browser.visit( zombieserver.url, function() {
				  
			    //So that Braveheart's dom-ish modules can find DOM/BOM objects in Zombie
				window = that.browser.window, navigator = window.navigator, document = window.document;

				var div, _tag;

				div = dom.id('id');

				test.ok( !!div, "Element.id with a string should should return an Element with the same id");
				div = dom.id( div );

				test.equal(div.$family(), 'element', "Element.id on an existing Element instance should just return the same element");
				_tag = div.get('tag');

				test.equal(_tag, 'div', 'should return a div element');

				div = dom.id( div );
				test.equal(div.$family(), "element", "Element.id on an DOM Element should return a wrapped element instance");
				test.done();                 
			});
		}

		,createAndVerify: function( test ){
			test.expect(13);
			var that = this;

			that.browser.visit( zombieserver.url, function() {
				window = that.browser.window, navigator = window.navigator, document = window.document;

				var div = dom.id("id");

				var el = new dom.Element("div", {
					styles:{
						color:"red"
						,backgroundColor:"black"
						,height:100
						,width:100
						,opacity:0.5
						,"float":"right"
					}
					,events: {
						'test': function() {
							test.ok(true, "Element event callback should fire");
						}
					}
					,'class': 'testelement'
				});
				
				test.strictEqual(null, el.getParent(), "Element should not have a parent element until injected into the document" );

				el.inject( div );

				test.ok(el.isNode, 'The element should be a node');
				test.strictEqual("id", el.getParent().get("id"), "Element should not have a parent element until injected into the document" );
				test.ok(el.hasClass("testelement"), "element should have had the css class testelement");
				
				test.equal(el.getStyle("color"), "red", "Text color should have been set to red");
				test.equal(el.getStyle("backgroundColor"), "black", "background color should have been set to black");
				
				test.equal(el.getStyle("height"), "100px", "height should have been set to 100px");
				test.equal(el.getHeight(), 100, "Height should also be accessible via getHeight() (unitless)");
				
				test.equal(el.getStyle("width"), "100px", "width should have been set to 100px");
				test.equal(el.getWidth(), 100, "Width should also be accessible via getWidth() (unitless)");
				
				test.equal(el.getStyle("opacity"), 0.5, "opacity should have been set to 0.5");
				test.equal(el.getStyle("float"),"right", "float should have been set to \"right\"");

				el.fireEvent("test");
				test.done();
			});		
		}

		,from: function( test ){
			test.expect(3);
			var that = this;

			that.browser.visit( zombieserver.url, function() {
				window = that.browser.window, navigator = window.navigator, document = window.document;	
			
				var els = dom.Elements.from("<div><span>hello world</span></div>");

				var spans = els.getFirst("span");
				test.equal(core.typeOf( spans ), "elements", "elements finder methods should return Elements instances" );

				var span = spans[0];
				test.equal(core.typeOf( span ), "element", "Elements Instance should contain Element Instances" );

				var s = span.get('node');

				test.ok(s.id !== 'undefined' && s.id !== null, "should have found a span element" );
				test.done();
			});
		}

		,cssClass: function( test ) {
			test.expect(5);
			var that = this;
			
			that.browser.visit( zombieserver.url, function() {
				window = that.browser.window, navigator = window.navigator, document = window.document;	

				var el = new dom.Element('div');
				el.addClass('fake-stuffz');
				
				test.throws(function() { el.addClass('fu fun funk');}, 'Should throw an exception if multiple classes are added');
				test.ok(el.hasClass('fake-stuffz'), 'Element should have added class');

				el.toggleClass('fake-stuffz');
				test.equal(el.hasClass('fake-stuffz'), false, 'Class should be toggled off');

				el.toggleClass('fake-stuffz');
				test.ok(el.hasClass('fake-stuffz'), 'Class should be toggled back on');

				test.equal(el.hasClass('airport'), false, 'Element should not have bogus class');
				test.done();
			});
		}

		,events: function( test ) {
			test.expect(3);
			var that = this;

			that.browser.visit( zombieserver.url, function() {
				window = that.browser.window, navigator = window.navigator, document = window.document;	

				var el = new dom.Element('div');

				var fired1 = null
					,fired2 = null
					,fired3 = null;

				function firedThree() {
					fired3 = true;
				}

				el.addEvent('taco', function() {
					fired1 = true;
				});

				el.addEvents({
					yogurt: function() {
						fired2 = true;
					}
					,granola: firedThree
				});

				that.browser.wait(function() {
					el.removeEvent('granola', firedThree);

					el.fireEvent('taco');
					el.fireEvent('yogurt');
					el.fireEvent('granola')
					
					test.ok(fired1, 'Single events should add and be fired');
					test.ok(fired2, 'Multiple-added events should add and be fired');
					test.equal(fired3, null, 'Removed event callbacks should not be fired');			
					test.done();
				});
			});
		}

		,familyStuff: function( test ) {
			test.expect(5);
			var that = this;

			that.browser.visit( zombieserver.url, function() {

				window = that.browser.window, navigator = window.navigator, document = window.document;	

				var el = new dom.Element('div', {id: 123});
				var childEl = new dom.Element('div');
				var anotherChild = childEl.clone();

				el.appendChild(childEl);
				el.appendChild(anotherChild);
				
				test.ok(el.contains(childEl), 'Parent el should contain child');
				test.ok(anotherChild !== childEl, 'Cloned child should not be reference to the original');
				test.equal(el.getChildren().length, 2, 'There should be two child nodes');
				test.equal(childEl.getParent().get('id'), 123, 'The correct parent should have custody'); 

				el.empty();
				test.ok(!el.contains(childEl), 'Parent el should be empty after empty()');

				test.done();
			});
		}

		,styles: function( test ) {
			var that = this;

			that.browser.visit( zombieserver.url, function() {
				window = that.browser.window, navigator = window.navigator, document = window.document;	

				var el = new dom.Element('div');
				el.setStyle('color', 'orange');
				el.setStyles({
					backgroundColor: 'pink'
					,'float': 'right'
					,width: 399
				})

				test.equal(el.getStyle('color'), 'orange', 'Styles should be read as they were set');
				test.equal(el.getStyle('float'), 'right', 'Styles set in bulk should be readable');
				test.equal(el.getStyle('width'), '399px', 'Width and height should (?) be retrieved with unit');
				test.done();
			});
		},inject: function( test ) {
			test.expect(2);

			var that = this;

			that.browser.visit( zombieserver.url, function() {
				window = that.browser.window, navigator = window.navigator, document = window.document;	
				var x,
					container = new dom.Element('div',{
						id : "container",
						html : "container"
					}),
					startingEl1 = new dom.Element('div',{
						id : "startingEl1",
						html : "startingEl1"
					}),
					startingEl2 = new dom.Element('div',{
						id : "startingEl2",
						html : "startingEl2"
					}),
					topDiv = new dom.Element('div',{
						id : "topDiv",
						html : "topDiv"
					}),
					bottomDiv = new dom.Element('div',{
						id : "bottomDiv",
						html : "bottomDiv"
					}),
					children;

				startingEl1.inject(container);
				startingEl2.inject(container);
				topDiv.inject(container,'top');
				bottomDiv.inject(container,'bottom');
				container.inject(dom.Document.body);

				var children = container.getChildren();

				test.equal(children[0].get('id'),'topDiv', 'First child should have an id of "topDiv"');
				test.equal(children[3].get('id'),'bottomDiv', 'First child should have an id of "bottomDiv"');

				test.done();
			});
		}
	};
});