define(['require', 'module', 'exports', 'swf', 'dom', 'test/zombie/zombieserver'] , function( require, module, exports, swf, dom, zombieserver ) {
	exports.tests = {

		setUp: zombieserver.start

		,tearDown: zombieserver.kill

		,Swiff: function( test ) {
			test.expect(2);
			var that = this;
		
			that.browser.visit( zombieserver.url, function() {
					
				//So that Braveheart's dom-ish modules can find DOM/BOM objects in Zombie
				window = that.browser.window, navigator = window.navigator, document = window.document;

				//Generate/inject flash object
				var flash = new swf.Swiff('/v2.0/test/zombie/assets/sample-flash.swf', {
					container: 'container'
				});

				//Verify
				var isInjected = ( typeof document.getElementById('container').childNodes[0] ) === 'object';

				test.ok( flash instanceof swf.Swiff, 'Should generate a flash object' );
				test.ok( isInjected, 'Should inject flash object into container' );
				test.done();              
			});
		}
	};
});