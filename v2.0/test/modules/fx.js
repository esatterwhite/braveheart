define(['require', 'exports', 'fx'
	, "fx/functions/Back"
	, "fx/functions/Bounce"
	, "fx/functions/Circ"
	, "fx/functions/Elastic"
	, "fx/functions/Expo"
	, "fx/functions/Pow"
	, "fx/functions/Sine"]
	, function(require, exports, FX, Back, Bounce, Circ, Elastic, Expo, Pow, Sine) {


	exports.Transitions = {
		setUp: function( callback ){
			this.effect = new FX({
				duration:100
			});
			callback()
		}

		,linear: function( test ){

			test.expect( 4 )
			this.effect.on("chainComplete", function(){
				test.ok( true )
				test.done();
			});
			this.effect.addEvent("complete", function( fx ){
				test.equal( fx.frame, fx.frames, "effects should stop when the frame is equal to the total frames" )
			});
			this.effect.on( "stop", function( ){
				test.ok(true)
			})
			this.effect.setOptions({
				transition:"linear"
				,link:'chain'
			});
			this.effect.start(1, 2 )
			this.effect.start(1, 2 )

		}
		,back: function( test ){
			// test.expect( 1 )


			this.effect.addEvent("complete", function( fx ){

				test.ok( true );
				test.ok( Math.round( Back(10) ), 2456 )
				test.equal( fx.frame, fx.frames, "effects should stop when the frame is equal to the total frames" )
				test.done();
			});

			this.effect.setOptions({
				transition:"back"
			})
			this.effect.start(1, 2 )
		}
		,bounce: function( test ){
			// test.expect( 1 )
			this.effect.addEvent("complete", function( fx ){
				test.ok( true );
				test.equal( Math.round( Bounce(10 ) ), -612 )
				test.equal( fx.frame, fx.frames, "effects should stop when the frame is equal to the total frames" )
				test.done();
			});

			this.effect.setOptions({
				transition:"bounce"
			})
			this.effect.start(1, 2 )
		}
		,circ: function( test ){
			// test.expect( 1 )
			this.effect.addEvent("complete", function( fx ){
				test.ok( true );
				test.equal( Circ( 1), 1 )
				test.equal( fx.frame, fx.frames, "effects should stop when the frame is equal to the total frames" )
				test.done();
			});

			this.effect.setOptions({
				transition:"circ"
			})
			this.effect.start(1, 2 )
		}
		,elastic: function( test ){
			// test.expect( 1 )
			this.effect.addEvent("complete", function( fx ){
				test.ok( true );
				test.equal( Elastic( 1), 1 )

				test.equal( fx.frame, fx.frames, "effects should stop when the frame is equal to the total frames" )
				test.done();
			});

			this.effect.setOptions({
				transition:"elastic"
			})
			this.effect.start(1, 2 )
		}
		,expo: function( test ){
			// test.expect( 1 )
			this.effect.addEvent("complete", function( fx ){
				test.ok( true );
				test.equal( Expo( 2 ), 256 )
				test.equal( fx.frame, fx.frames, "effects should stop when the frame is equal to the total frames" )
				test.done();
			});

			this.effect.setOptions({
				transition:"expo"
			});
			this.effect.start(1, 2 )
		}
		,pow: function( test ){
			// test.expect( 1 )
			this.effect.addEvent("complete", function( fx ){
				test.ok( true );

				test.equal( Pow( 2 ), 64)
				test.equal( fx.frame, fx.frames, "effects should stop when the frame is equal to the total frames" )
				test.done();
			});

			this.effect.setOptions({
				transition:"pow"
			})
			this.effect.start(1, 2 )
		}
		,sine: function( test ){
			// test.expect( 1 )
			this.effect.addEvent("complete", function( fx ){
				test.ok( true );
				test.equal( Math.round( Sine(10 ) ) ,2)
				test.equal( fx.frame, fx.frames, "effects should stop when the frame is equal to the total frames" )
				test.done();
			});

			this.effect.setOptions({
				transition:"sine"
			})
			this.effect.start(1, 2 )
		}
	};



})
