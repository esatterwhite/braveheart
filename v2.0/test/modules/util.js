define(['require', 'exports', 'util'], function(require, exports, util) {


	exports.tests = {
		setUp: function( callback ){
			callback();
		}

		,typeOf: function( test ) {
			
			var str = 'lolz';
			var obj = { 'l': 'olz' };
			var func = function() {
				return 'lolz';
			};

			test.equal(util.typeOf(str), 'string', 'should recognize string');
			test.equal(util.typeOf(obj), 'object', 'should recognize object');
			test.equal(util.typeOf(func), 'function', 'should recognize function');
			test.done();
		}

		,compare: function( test ) {
			var array1 = ['1', '2', '3'];
			var array2 = ['3', '1'];
			var array3 = ['3', '2', '1'];
			var date1 = new Date('2000-01-01');
            var date2 = new Date('2012-12-20');

            test.equal(util.compare(array1, array2), 1, 'should interpret longer array as being greater');
            test.equal(util.compare(array1, array3), -1, 'if equal length, should interpret array whose first item is greater as being greater');
            
            test.equal(util.compare(date1, date2), -1, 'should interpret later date as greater');
            test.done();
		}

	};

});