/*jshint laxcomma:true, smarttabs: true */

define(['require','exports', 'module', 'string'], function( require, exports, module, string ){


	exports.cleaning ={
		setUp: function( callback ){
			this.str = "This is a pretty decent sentence. it probably has some    Problems   ";
			callback();
		}

		,clean: function( test){
			test.equal( this.str.length, 69 );
			test.equal( string.clean(this.str).length , 63, "clean method should remove leading, trailing and all extraneous whitespace" );
			test.done();
		}
		,trim: function( test){
			test.equal( this.str.length, 69 );
			test.equal( string.trim(this.str ).length , 66, "trim method should remove only the leading and trailing whitespace" );
			test.notEqual( string.trim(this.str ).length , this.str.length, "trim method should remove only the leading and trailing whitespace" );
			test.equal( string.trim("   Hello world, I have    a    lot of space    " ).length , 40, "trim method should remove only the leading and trailing whitespace" );
			
			var t = "  HELLO WORLD  ";

			test.strictEqual(string.trim( t ), "HELLO WORLD", "trim method should not alter character bytes")

			test.done();
		}
	};

	exports.stripping = {
		setUp: function( callback ){
			this.str  =
				"<div>"
					+"<script>var x = \"hello world\";console.log( x );</script>"
					+"<span> this is some text & the number 5</span>"
				+"</div>";

			callback();
		}
		,scripts: function( test ){
			var text = string.stripScripts( this.str );
			test.expect( 3 );
			test.strictEqual( text.indexOf( 'script' ), -1, "stripScripts method should remove all script tags");
			test.strictEqual( text.indexOf( 'console' ), -1, "stripScripts method should remove all contents within the script tag");
			try{
				string.stripScripts( this.str, eval );
				test.ok(1, "executing of scripts should not throw an error");
			} catch ( e ){
				test.done();
			}
			test.done();
		}
		,tags: function( test ){
			var str = string.stripTags( this.str, "span");
			test.equal( str.indexOf( 'span'), -1, "stripTags should remove the passed in tag ");
			test.notEqual(str.indexOf( 'world'), -1, "stripTags, by default should not remove the contents of the tag that was removed ");

			str = string.stripTags( this.str, "span", true );
			test.done();
		}
		,nonAlpha: function( test ){
			var str = string.stripNonAlpha( this.str );
			test.strictEqual( str.indexOf( "5" ), -1, "stripNonAlpha should strip out numbers");
			test.strictEqual( str.indexOf( ">" ), -1, "stripNonAlpha should strip special characters ( <,> )");
			test.strictEqual( str.indexOf( "&" ), -1, "stripNonAlpha should strip special characters ( &, # )");
			test.done();
		}
	};

	exports.munging ={

		setUp: function( callback ){
			this.string = "abc123";
			callback();
		}

		,reverse: function( test ){
			var s = string.reverse( this.string );

			test.equal(typeof s, "string", "reverse should return a string");
			test.equal( this.string, 'abc123', "revese should not alter the original string");
			test.strictEqual( s, "321cba", "reverse should return the characters in the opposing order the started");
			test.done();
		}
		,"String: Hyphenate": function( test ){
			var str;


			str = "helloWorld";
			test.strictEqual( string.hyphenate( str ), "hello-world", "Expected \"hello-world\" got :" + string.hyphenate( str ) );

			str = "thisIsIt";
			test.strictEqual( string.hyphenate( str ), "this-is-it", "Expected \"this-is-it\" got :" + string.hyphenate( str ) );

			str = "thisIsNotit";
			test.strictEqual( string.hyphenate( str ), "this-is-notit", "Expected \"this-is-notit\" got :" + string.hyphenate( str ) );

			str = "powerToTheP3ople";
			test.strictEqual( string.hyphenate( str ), "power-to-the-p3ople", "Expected \"power-to-the-p3ople\" got :" + string.hyphenate( str ) );

			test.done();
		}
		,"String: camelCase": function( test ){
			var str;


			str = "hello-world";
			test.strictEqual( string.camelCase( str ), "helloWorld", "Expected \"helloWorld\" got :" + string.camelCase( str ) );

			str = "this-is-it";
			test.strictEqual( string.camelCase( str ), "thisIsIt", "Expected \"thisIsIt\" got :" + string.camelCase( str ) );

			str = "this-is-notit";
			test.strictEqual( string.camelCase( str ), "thisIsNotit", "Expected \"thisIsNotit\" got :" + string.camelCase( str ) );

			str = "power-to-the-p3ople";
			test.strictEqual( string.camelCase( str ), "powerToTheP3ople", "Expected \"powerToTheP3ople\" got :" + string.camelCase( str ) );

			test.done();
		}
		,"String: slugify": function( test ){
			var str;


			str = "hello world";
			test.strictEqual( string.slugify( str ), "hello-world", "Expected \"hello-world\" got :" + string.slugify( str ) );

			str = "this is it";
			test.strictEqual( string.slugify( str ), "this-is-it", "Expected \"this-is-it\" got :" + string.slugify( str ) );

			str = "this is notit";
			test.strictEqual( string.slugify( str ), "this-is-notit", "Expected \"this-is-notit\" got :" + string.slugify( str ) );

			str = "power to the p3ople";
			test.strictEqual( string.slugify( str ), "power-to-the-p3ople", "Expected \"power-to-the-p3ople\" got :" + string.slugify( str ) );

			test.done();
		}
		,"String: truncate": function( test ){
			var str
				,tail = "...";

			str = "abc123";

			test.strictEqual( string.truncate(str, 1, tail), tail, "Passing a maxlength smaller than the tails size should return the tail");

			test.strictEqual( string.truncate(str, 4, tail), "a" + tail, "Max length should include the length of the tail - A length of 4, got a length of " + string.truncate(str, 4, tail).length );


			test.strictEqual( string.truncate(str, 10, tail), str, "Passing a maxlength larger than the string size should return the string");

			test.done();
		}
		, "String: padRight" : function( test ){
			test.strictEqual( string.padRight( this.string, 10, "x"),"abc123xxxx" )
			test.done()
		}
		,"String: padLeft": function( test ){
			test.strictEqual( string.padLeft( this.string, 10, "x"),"xxxxabc123" )

			test.done()
		}
	};

	exports.evaluating = {
		setUp: function( callback ){
			this.str = "hello_world-This is $(#)33";
			callback();
		}
		,contains: function( test ){
			test.ok(string.contains( this.str, "hello"), "string should return true" );
			test.ok(string.contains( this.str, "hello_world"), "string should return true" );
			test.ok(string.contains( this.str, "hello"), "string should return true" );
			test.strictEqual(false, string.contains( this.str, "hello world"), "string should return false" );
			test.done();
		}

	};

	exports.toInt = {
		setUp: function( callback ){
			this.str = "100.84903px";
			callback();
		}

		,"String: toint": function( test ){
			test.equal( string.toInt( this.str ), "100", "toInt should remove decimals");
			test.strictEqual( string.toInt( this.str ), 100, "toInt should return a Number object");
			test.strictEqual( string.toInt( this.str, 2), 4, "toInt should accept a second praram as a numeric base. Expected 4, got: " + string.toInt( this.str, 2));
			test.equal( typeof string.toInt( "hello"), "number", "toInt should always return a number");
			test.strictEqual( string.toInt( "hello"), 0, "expected 0, got"  + string.toInt( "hello"));


			test.done();
		}
		,"String: px to Int": function( test ){
			test.equal( string.pxToInt( this.str), "100",  "pxToInt should remove trailing \"px\" ");
			test.equal( typeof string.pxToInt( this.str), "number", "pxToInt should always return a number");
			test.equal( typeof string.pxToInt( this.str), "number", "pxToInt should always return a number");

			test.done();
		}
	};

	exports.toFloat = {
		setUp: function( callback ){
			this.str = "100.191";
			callback();
		}
		,"String: toFloat": function( test ){
			test.equal( string.toFloat( this.str ), "100.191", "toFloat should remove decimals");
			test.strictEqual( string.toFloat( this.str ), 100.191, "toFloat should return a Number object");

			test.equal( typeof string.toFloat( "hello"), "number", "toFloat should always return a number");

			test.strictEqual( string.toFloat( "hello"), 0, "expected 0, got"  + string.toFloat( "hello"));


			test.done();
		}
	};
	exports.ID = {

		"string unique id": function( test ){
			var id = string.id()
				,count = 0
				,interval
				,next;

			interval = setInterval( function(){
				if( count > 11 ){

					next = string.id();

					test.notEqual( id, next, "string.id should return a unique ID");
					id = next;
					count++;
				}else{
					clearInterval( interval );
					test.done();
				}
			}, 1);

		}
	};

	exports.pluralize = {
		setUp: function( callback ){
			this.easyWord = "Power"
			this.mildWord = "fish"
			this.moderateWord = "Index"
			this.hardword = "knife";
			callback();
		}
		,"pluralize w/ Count": function( test ){
			test.expect( 3 );
			test.equal( string.pluralize(1, this.easyWord, this.easyWord+"s" ), "Power", "Expected Power, but got " + string.pluralize(1, this.easyWord, this.easyWord+"s" ) )
			test.equal( string.pluralize(0, this.easyWord, this.easyWord+"s" ), "Powers" , "Expected Powers, but got " + string.pluralize(1, this.easyWord, this.easyWord+"s" ) )
			test.equal( string.pluralize(2, this.easyWord, this.easyWord+"s" ), "Powers" , "Expected Powers, but got " + string.pluralize(1, this.easyWord, this.easyWord+"s" ) )
			test.done();
		}
		,"Smart pluralize Power": function( test ){
			test.equal( string.smartPlural(this.easyWord ), "Powers")
			test.done();
		}
		,"Smart pluralize fish": function( test ){
			test.equal( string.smartPlural(this.mildWord ), "fishes")

			test.done();
		}
		,"Smart pluralize Index": function( test ){
			test.equal( string.smartPlural(this.moderateWord ), "Indcies")
			test.done();
		}
		,"Smart pluralize knife": function( test ){
			test.equal( string.smartPlural(this.hardword ), "knives")

			test.done();
		}
	}
});
