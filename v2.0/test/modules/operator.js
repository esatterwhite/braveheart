define(["require", "exports", "operator", "iter"], function( require, exports, operator, iter){

	exports.comparison = {

	};

	exports.logical = {
		setUp: function( callback ){

			this.op1 = 1;
			this.op2 = 10;
			this.op3 = 0;
			this.op4 = undefined;
			callback( )
		}
		, truth: function( test ){
			test.strictEqual( operator.truth( this.op1 ), true );
			test.strictEqual( operator.truth( this.op2 ), true );
			test.strictEqual( !operator.truth( this.op3 ), true );
			test.strictEqual( !operator.truth( this.op4 ), true );

			test.done();
		}

		, "logical not": function( test ){
			test.ok( operator.logicalnot( this.op3 ) );
			test.ok( operator.logicalnot( this.op4 ) );

			test.done();
		}
		, "logical or": function( test ){
			test.strictEqual( operator.logor(this.op1, this.op2), this.op1 );
			test.strictEqual( operator.logor(this.op3, this.op2), this.op2 );
			test.strictEqual( operator.logor(this.op4, this.op3), this.op3 );

			test.done();
		}

		,"logical and": function( test ){
			test.strictEqual( operator.logand( this.op3, this.op1), this.op3);
			test.strictEqual( operator.logand( this.op1, this.op3), this.op3);

			test.done();
		}
	};

	exports.itemgetter = {
		setUp: function( callback ){
			this.obj1 = "Hello world"
			this.obj2 = [
				 [1,2,5,8]
				,[7,5,3,76]
				,[4,76,99,1]
			];

			this.obj3={
				 key1:"value1"
				,key2:"value2"
				,key3:"value3"
				,key4:"value4"
			}
			callback()
		}

		,"Simple Lookups": function( test ){
			test.equal( (operator.itemgetter( "key1" )).call(this, this.obj3), "value1")
			test.equal( (operator.itemgetter( "key2" )).call(this, this.obj3), "value2")
			test.equal( (operator.itemgetter( "key3" )).call(this, this.obj3), "value3")
			test.equal( (operator.itemgetter( "key4" )).call(this, this.obj3), "value4")
			test.done();
		}
		,"Multiple Indecies": function( test ){
			var getter = operator.itemgetter(0,2,8);

			var results = getter( this.obj1 )

			test.strictEqual( results[0], "H")
			test.strictEqual( results[1], "l")
			test.strictEqual( results[2], "r")
			test.done();
		}

		,"Item Getter: Complex": function( test ){
			var getter = operator.itemgetter( 0, 2 )

			var results = getter( this.obj2 )

			test.equal( results[0], this.obj2[0])
			test.equal( results[1], this.obj2[2])
			test.done();
		}
		,"Item Getter:Complex Iter Map": function( test ){
			var getter = operator.itemgetter( 0,2,3 );
			var results = iter.map( getter, this.obj2)


			var t0 = results[0];

			test.strictEqual( t0[0], this.obj2[0][0])
			test.strictEqual( t0[1], this.obj2[0][2])
			test.strictEqual( t0[2], this.obj2[0][3])
			test.done();
		}
	};

	exports.comparison = {
		setUp: function( callback ){
			this.val1 = 1;
			this.val2 = 2;
			this.val3 = 3;
			this.val4 = 4;
			this.val5 = 5;

			this.val6 = new Date("10/10/2011")
			this.val7 = new Date("10/10/2012")
			this.val8 = [1,2,3]
			this.val9 = [3,4]

			this.obj = {
				 key1:"value1"
				,key2:"value2"
				,key3:"value3"
				,key4:"value4"
			}
			callback();
		}

		,"Strict Equality": function( test ){

			test.strictEqual( operator.compeq( this.val1, 1), true )
			test.strictEqual( operator.compeq( this.val2, 2), true )
			test.strictEqual( operator.compeq( this.val3, 3), true )
			test.strictEqual( operator.compeq( this.val4, 4), true )
			test.strictEqual( operator.compeq( this.val5, 5), true )
			test.strictEqual( operator.compeq( this.val7, new Date("10/10/2012")), true )

			test.done();
		}
		,"Strict Inequality": function( test ){

			test.ok( operator.compneq( this.val1, 5) );
			test.ok( operator.compneq( this.val2, 4) );
			test.ok( operator.compneq( this.val3, 2) );
			test.ok( operator.compneq( this.val4, 3) );
			test.ok( operator.compneq( this.val5, 1) );

			test.done();
		}

		,"Less Than": function( test ){

			test.strictEqual( operator.complt( this.val6, this.val7), true);
			test.strictEqual( operator.complt( this.val7, this.val6), false);
			test.strictEqual( operator.complt( this.val8, this.val9), false);
			test.strictEqual( operator.complt( this.val9, this.val8), true);


			test.done()
		}
		,"greater Than": function( test ){

			test.strictEqual( operator.compgt( this.val5, this.val4), true)
			test.strictEqual( operator.compgt( this.val1, this.val2), false)
			test.strictEqual( operator.compgt( this.val7, this.val6), true)

			test.done();
		}

		,Contains: function( test ){

			test.strictEqual( operator.contains( this.obj, "key1"), true);
			test.strictEqual( operator.contains( this.obj, "key10"), false);
			test.strictEqual( operator.contains( this.obj, "key3"), true);

			test.done()
		}
	};

	exports.bitshift = {
		setUp : function( callback ){
			this.val1 = 1;
			this.val2 = 2;
			this.val3 = 3;
			this.val4 = 4;
			this.val5 = 5;

			callback()
		}

		,"bitnot": function(test){
			test.strictEqual( operator.bitnot( this.val1), -2)
			test.strictEqual( operator.bitnot( this.val2), -3)
			test.strictEqual( operator.bitnot( this.val3), -4)
			test.strictEqual( operator.bitnot( this.val4), -5)

			test.done();
		}
		,"XOR": function( test ){
			test.strictEqual( operator.xor( this.val1, 3), 2)
			test.strictEqual( operator.xor( this.val2, 3), 1)
			test.strictEqual( operator.xor( this.val3, 3), 0)
			test.strictEqual( operator.xor( this.val4, 3), 7)
			test.strictEqual( operator.xor( this.val5, 3), 6)

			test.done()
		}
		,"OR": function( test ){
			test.strictEqual( operator.or( 1, 0), 1)
			test.strictEqual( operator.or( 0, 0), 0)
			test.strictEqual( operator.or( 0, 1), 1)
			test.done()
		}

		,BitLShift: function( test ){
			test.strictEqual( operator.bitlshift( 1, 2), 4)
			test.strictEqual( operator.bitlshift( 1, 3), 8)
			test.strictEqual( operator.bitlshift( 1, 4), 16)
			test.strictEqual( operator.bitlshift( 1, 5), 32)

			test.done();
		}
		,BitRShift: function( test ){
			test.strictEqual( operator.bitrshift( 10, 1), 5)
			test.strictEqual( operator.bitrshift( 10, 2), 2)
			test.strictEqual( operator.bitrshift( 10, 3), 1)
			test.done();
		}
		,BitZRShift: function( test){
			test.strictEqual( operator.bitzrshift( -1, 2 ),1073741823)

			test.done()
		}
	};

	exports.ops = {
		setUp: function( callback ){
			callback()
		}
	}
});
