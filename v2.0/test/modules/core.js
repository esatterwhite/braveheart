/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

define(["require", "exports", "module", "core"], function(require, exports, module, core){
	var Primitive = core.Primitive
	exports.primitives = {
		setUp: function( callback ){
			this.baseFn = function(){}
			callback();
		},
		customs: function( test ){
			test.expect( 6 )
			var Custom = function(){}

			Custom = new Primitive("Custom", Custom);
			var c = new Custom();
			test.equal( typeof Primitive.isCustom, "function", "creating a new type should add a type check function")
			test.ok(Primitive.isCustom( c )," should create a new Primitive Check");

			Custom.implement("fn", function(){
				test.ok( true, "implement should add a function to existing instances")
			})

			test.doesNotThrow( function(){
				c.fn();
			})


			Custom.implement({
				one: function(){
					test.ok( true, "implement should accept an object")
				},
				two: function(){
					test.ok( true, "implement should accept an object")
				}
			})

			c.one();
			c.two();

			test.done();
		},

		natives: function( test ){

			Array = new Primitive("Array", Array);

			var x = [];

			test.equal( x.push(2), 1, "Native methods should still behave the same")

			Array.implement("fourth", function( ){
				return this[3]
			});
			x = [1,2,3,4,5]
			test.equal( x.fourth(), 4, "expected 4, got "+ x.fourth );
			test.done()
		}
	}

	exports.enumerables ={
		arraylike: function( test ){

			var x = function(){
				this.name = "test"
			};

			x.prototype = new Array();
			x = new Primitive("X", x);
			test.ok( Primitive.isEnumerable( []), "array is enumerable" )
			// test.ok( Primitive.isEnumerable( {}, "object is enumerable" ) )
			test.ok( !Primitive.isEnumerable( 1), "numbers not enumerable" )
			test.ok( Primitive.isEnumerable( new x()  ), "x should be enumerable" )
			test.done();
		},

		objectlike: function( test ){
			var d = new Date("10/10/2012");
			var a = [];

			var o = {
				length:1
			}

			test.ok(
				Primitive.isEnumerable( o ),
				"Object with a length property should be considered enumerable"
			)
			test.done()
		}
	}
});
