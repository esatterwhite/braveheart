define(['require', 'module', 'exports', 'colors', 'test/zombie/zombieserver' ] , function( require, module, exports, colors, zombieserver ) {

    exports.tests = {
        setUp: zombieserver.start

		,tearDown: zombieserver.kill

		,getJSON: function( test ) {

            test.expect(4);
			var that = this;

			that.browser.visit( zombieserver.url, function() {

                //So Braveheart can find relevant DOM/BOM objects in Zombie
                window = that.browser.window, XMLHttpRequest = window.XMLHttpRequest;

                var Request = require('request')
                    ,success = false
                    ,complete = false
                    ,init = false;

                var xhr = new Request.JSON({
                    method: 'get'
                    ,url: 'json'
                    ,onRequest: function() {
                      init = true;
                    }
                    ,onComplete: function() {
                      complete = true;
                    }
                    ,onSuccess: function() {
                      success = true;
                    }
                }).send();
              
                //Wait for XHR to complete
                that.browser.wait(function() {
                    test.ok( init, 'request should be initiated');
                    test.ok( complete, 'request should complete' );
                    test.ok( success, 'onSuccess method should fire on...success' );
                    test.equal( xhr.response.json.someJSON, 'welcome to some json!!!', 'JSON response should be correct');
                    test.done();     
                });
		    });
		}

        ,getHTML: function( test ) {

            test.expect(5);
            var that = this;

            that.browser.visit( zombieserver.url, function() {

                //So Braveheart can find relevant DOM/BOM objects in Zombie
                window = that.browser.window, navigator = window.navigator, document = window.document, XMLHttpRequest = window.XMLHttpRequest;

                var Request = require('request')
                    ,success = false
                    ,complete = false
                    ,init = false;

                var xhr = new Request.HTML({
                    method: 'get'
                    ,url: 'html'
                    ,onRequest: function() {
                      init = true;
                    }
                    ,onComplete: function() {
                      complete = true;
                    }
                    ,onSuccess: function() {
                      success = true;
                    }
                    ,update: 'container'

                }).send();
                  
                  //Wait for XHR to complete
                that.browser.wait(function() {
                    test.ok( init, 'request should be initiated');
                    test.ok( complete, 'request should complete' );
                    test.ok( success, 'onSuccess method should fire on...success' );
                    test.ok( (/burritos/i).test(xhr.response.html), 'html response should contain the right stuff' );
                    test.ok( document.getElementById('container').childNodes.length === 1, 'response HTML should update the contents of #container');
                    test.done();     
                });
            });
        }

        ,post: function( test ) {

            test.expect(4);
            var that = this;

            that.browser.visit( zombieserver.url, function() {

              //So Braveheart can find relevant DOM/BOM objects in Zombie
                window = that.browser.window, navigator = window.navigator, document = window.document, XMLHttpRequest = window.XMLHttpRequest;

                var Request = require('request')
                    ,success = false
                    ,complete = false
                    ,init = false;

                var xhr = new Request.JSON({
                    method: 'post'
                    ,url: 'post'
                    ,headers: {
                      //This fake server-browser combo won't work without content type
                      'Content-Type': 'application/x-www-form-urlencoded'
                    }
                    ,data: {
                      'funActivity': 'running'
                      ,'weakActivity': 'boring stuff'
                    }
                    ,onRequest: function() {
                      init = true;
                    }
                    ,onComplete: function() {
                      complete = true;
                    }
                    ,onSuccess: function() {
                      success = true;
                    }
                }).send();
                  
                //Wait for XHR to complete
                that.browser.wait(function() {
                    test.ok( init, 'request should be initiated');
                    test.ok( complete, 'request should complete' );
                    test.ok( success, 'onSuccess method should fire on...success' );
                    test.ok( xhr.response.json.funActivity === 'running', 'response should contain the same data we posted');
                    test.done();     
                });
            });
        }
	};
});