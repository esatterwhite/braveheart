define(['require', 'module', 'exports', 'test/zombie/zombieserver' ] , function( require, module, exports, zombieserver ) {

	exports.tests = {
		setUp: zombieserver.start

		,tearDown: zombieserver.kill

		,js: function( test ) {
			test.expect(2);
			var that = this;
	
			that.browser.visit( zombieserver.url, function() {
				  
			    //So that Braveheart's dom-ish modules can find DOM/BOM objects in Zombie
				window = that.browser.window, navigator = window.navigator, document = window.document;

				//Require assets after bootstrapping fake browser, setting references
				var assets = require( 'assets' ), dom = require( 'dom' );

				//Generate/inject script
				var script = new assets.JS('v2.0/test/zombie/assets/sample-script.js');

				//Verify injection
				var scripts = document.head.getElementsByTagName( 'script' )
					,scr = scripts[ scripts.length-1 ].src
					,isInserted = /sample-script\.js$/.test( scr );
				  
				test.ok( script instanceof dom.Element, 'Script instance should be generated' );
				test.ok( isInserted, 'Scripts should be inserted into the bottom of html head' );
				test.done()                 
			});
		}

		,img: function ( test ) {
			test.expect(2);
			var that = this;

			that.browser.visit( zombieserver.url, function() {
				window = that.browser.window, navigator = window.navigator, document = window.document, Image = window.Image;      
				var assets = require( 'assets' ), dom = require( 'dom' );

				//Generate/inject
				var img = new assets.IMG('v2.0/test/zombie/assets/sample-image.jpg');
				img.inject(document.body);

				//Verify
				var images = document.getElementsByTagName( 'img' );

				test.ok( img instanceof dom.Element, 'Image instance should be generated' );
				test.ok( images.length > 0, 'Image should be inserted into document' );
				test.done();            
		  	});
		}

		,css: function( test ) {
			test.expect(3);
			var that = this;

			that.browser.visit( zombieserver.url, function() {        
				window = that.browser.window, navigator = window.navigator, document = window.document;
				var assets = require( 'assets' ), dom = require( 'dom' );

				//Set injection location...oh, Zombie. Works fine in real browsers.
				dom.Document.head = window.document.head;

				//Generate/inject
				var css = new assets.CSS('v2.0/test/zombie/assets/sample-styles.css');
				new assets.CSS('body{background: yellow; color: red}', {}, true);
				
				//Verify
				var stylesheets = document.getElementsByTagName( 'link' );
				var styleTags = document.head.getElementsByTagName( 'style' )	
				test.ok( css instanceof dom.Element, 'CSS instance should be generated' );
				test.ok(stylesheets.length > 0, 'External stylesheet should be inserted into document');
				test.ok(styleTags.length > 0, 'Inline styles should be inserted into document head');
				test.done();            
			});
		}
	};
});