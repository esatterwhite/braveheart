define(['require', 'exports', 'url'], function(require, exports, url) {

	exports.tests = {
		getDomainName: function(test) {
			test.expect(3);
			test.strictEqual(url.getDomainName('http://www.example.com'), 'www.example.com', 'getDomainName() should return "www.example.com"');
			test.strictEqual(url.getDomainName('https://www.example.com/index.html'), 'www.example.com', 'getDomainName() should return "www.example.com"');
			test.strictEqual(url.getDomainName('http://www.example.com:8080/index.html?a=1&b=2'), 'www.example.com', 'getDomainName() should return "www.example.com"');
			test.done();
		}
		, getPort: function(test) {
			test.expect(3);
			test.strictEqual(url.getPort('http://www.example.com'), '', 'getPort() should return an empty string');
			test.strictEqual(url.getPort('https://www.example.com:4433/index.html'), ':4433', 'getPort() should return ":4433"');
			test.strictEqual(url.getPort('http://www.example.com:8080/index.html?a=1&b=2'), ':8080', 'getPort() should return ":8080"');
			test.done();
		}
		, getLocation: function(test) {
			test.expect(4);
			test.strictEqual(url.getLocation('http://www.example.com'), 'http://www.example.com', 'getLocation() should return "http://www.example.com"');
			test.strictEqual(url.getLocation('https://www.example.com:443/index.html'), 'https://www.example.com', 'getLocation() should return "https://www.example.com"');
			test.strictEqual(url.getLocation('http://www.example.com:80/index.html?a=1&b=2'), 'http://www.example.com', 'getLocation() should return "http://www.example.com"');
			test.strictEqual(url.getLocation('http://www.example.com:88/index.html?foo=a#bar'), 'http://www.example.com:88', 'getLocation() should return "http://www.example.com:88"');
			test.done();
		}
		, resolveUrl: function(test) {
			test.expect(2);
			test.strictEqual(url.resolveUrl('http://www.example.com/foo/bar/../../1'), 'http://www.example.com/1', 'resolveUrl() should return "http://www.example.com/1"');
			test.strictEqual(url.resolveUrl('http://www.example.com:88/test/../index.html?foo=a#bar'), 'http://www.example.com:88/index.html?foo=a#bar', 'resolveUrl() should return "http://www.example.com:88/index.html?foo=a#bar"');
			test.done();
		}
		, appendQueryParams: function(test) {
			test.expect(4);
			test.strictEqual(url.appendQueryParams('http://www.example.com/?a=1', {b: 2, c: '3'}), 'http://www.example.com/?a=1&b=2&c=3', 'appendQueryParams() should return "http://www.example.com/?a=1&b=2&c=3"');
			test.strictEqual(url.appendQueryParams('http://www.example.com:88/?foo=bar', {success: true}), 'http://www.example.com:88/?foo=bar&success=true', 'appendQueryParams() should return "http://www.example.com:88/?foo=bar&success=true"');
			test.strictEqual(url.appendQueryParams('http://www.example.com/?a=1', {b: 2, c: '3'}, true), 'http://www.example.com/?a=1#b=2&c=3', 'appendQueryParams() should return "http://www.example.com/?a=1#b=2&c=3"');
			test.strictEqual(url.appendQueryParams('http://www.example.com:88/index.html', {foo: "bar"}, true), 'http://www.example.com:88/index.html#foo=bar', 'appendQueryParams() should return "http://www.example.com:88/index.html#foo=bar"');
			test.done();
		}
	};

});