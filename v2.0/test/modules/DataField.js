/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

define(["require", "exports", "module", "data/Fields/Field", "data/Fields/mixins/Mapping"],function(require, exports, module, Field, Mapping){
	exports.values = {
		setUp: function( callback ){
			this.field = new Field({
				nullable: true
			})
			callback();
		}

		,tearDown: function( callback ){
			delete this.field;
			callback();
		}

		,"nullable": function( test ){
			test.expect( 10 );
			var value;
			value = this.field.getValue();

			test.strictEqual( value, null, "nullable fields should return null if no value specified. Got: " + String(value) );

			this.field.setValue( 1 );
			value = this.field.getValue( );

			test.strictEqual( value, 1, "nullable fields should return value once one is specified. Expected: 1 Got: " + String(value) );

			this.field.setValue(  );
			value = this.field.getValue( );
			test.strictEqual( value, null, "nullable fields should return value once one is specified. Expected: null Got: " + String(value) );

			this.field.setValue( 0 );
			value = this.field.getValue()
			test.strictEqual( value, 0, "nullable fields should return null if no value specified. Got: " + String(value) );

			this.field.setValue( 1 );
			value = this.field.getValue( );

			test.strictEqual( value, 1, "nullable fields should return value once one is specified. Expected: 1 Got: " + String(value) );

			this.field.setValue(  );
			value = this.field.getValue( );
			test.strictEqual( value, null, "nullable fields should return value once one is specified. Expected: null Got: " + String(value) );



			var fld = new Field();
			value = fld.getValue();

			test.strictEqual( value, "", "nullable fields should return null if no value specified. Got: " + String(value) );

			fld.setValue( 1 );
			value = fld.getValue( );

			test.strictEqual( value, 1, "nullable fields should return value once one is specified. Expected: 1 Got: " + String(value) );

			fld.setValue(  );
			value = fld.getValue( );
			test.strictEqual( value, "", "nullable fields should return value once one is specified. Expected: \"\" Got: " + String(value) );

			fld.setValue( 0 );
			value = fld.getValue( );
			test.strictEqual( value, 0, "nullable fields should return value once one is specified. Expected: 0 Got: " + String(value) );

			test.done();
		}
		,"Converting Values": function( test ){
			var field = new Field({
				convert: function( val ){
					return parseInt( val || 0, 10 ) + 10
				}
			});


			var value = field.getValue();

			test.equal( value, 10 );

			field.setOptions({
				convert: function( val ){
					return "" + value + " Hello World"
				}
			});

			field.setValue( 10 );

			value = field.getValue( )
			test.equal( value, "10 Hello World" );

			test.done();
		}
	};


	exports.Mapping = {
		setUp: function( callback ){
			this.mapping = "data.test.stuff.value"
			this.field = new Field({
				mapping:this.mapping
			});

			this.mapper = new Mapping();

			this.json = {
				data:{
					test:{
						stuff:{
							value:4
						}
					}
				}
			}
			callback();
		}

		,tearDown: function( callback ){
			delete this.field;
			delete this.mapper;
			callback();
		}

		,"Standalone Mapping":function( test ){
			test.expect( 2 );
			this.mapper.mapping = this.mapping

			var x = this.mapper.resolve( this.json)
			test.strictEqual( x, 4 )

			this.mapper.mapping = "data.test"
			var x = this.mapper.resolve( this.json )

			test.strictEqual( x.stuff.value, 4 )

			test.done();
		}
		,"Field Mapping": function( test ){

			test.expect( 2 )
			this.field.setValue( this.json );

			var value = this.field.getValue();
			test.strictEqual( value, 4, "Test that field mapping traverses json objects");

			this.field.setOptions({
				mapping:"data.test"
			});

			value = this.field.getValue()
			test.strictEqual( value, 4, "setting options should not alter mappings");
			test.done();
		}


	};

	exports.Selector = {
		setUp: function( callback ){
			this.json = {
				children:[{
					first_name: "beth"
					,age:12
				},{
					first_name:"kenny"
					,age:5
				}]
				,nephew: {
					age:8
					,first_name:"Sammy"
				}
				,stuff:{
					things:{
						key:1
						,stuff:"Hello"
					}
				}
				,poop:{
					test:{
						wipe:[1,"2",3,{data:4}]
						,mash:{
							key1: undefined
							,a:{
								b:{
									c:{
										d:"You found me!"
									}
								}
							}
						}
						,mish: "Hello"
					}
				}
			}
			callback();
		}
		,"Array Selectors": function( test ){
			var field = new Field({
				selector:".children :first-child .age"
			});

			field.setValue( this.json );
			var value = field.getValue();

			test.strictEqual( value, 12, "Test json selector on known objecct")
			test.equal( field.$cached, null, "get value shouldn't cache selectors" )

			field.sync();

			test.strictEqual( field.$cached, 12, "Syncing a field should sync and bypass selectors" )

			test.done()
		}
		,"With Defaults": function( test ){
			var field = new Field({
				  selector:".wipe ~ object .key1"
				, defaultValue:12
			});

			field.setValue( this.json );

			var value = field.getValue();

			test.equal( value, 12)
			test.done();
		}
		,"Multiple Value Selectors": function( test ){
			var field = new Field({
				selector:".first_name"
			});

			field.setValue( this.json );
			var value = field.getValue();

			test.strictEqual( value, "beth", "Selector Should return Single Values");

			test.equal( field.$cached, null, "get value shouldn't cache selectors" );

			field.setSelectAll( true );
			value = field.getValue();
			test.strictEqual( value.length, 3, "Settign select all to true should returnan array of values" );
			test.strictEqual( value[0], "beth", "Validate found values" );
			test.strictEqual( value[1], "kenny", "Validate found values" );
			test.strictEqual( value[2], "Sammy", "Validate found values" );

			test.done();
		}
	};

	exports.DualMapping = {
		setUp: function( callback ){
			this.json = {
				children:[{
					first_name: "beth"
					,age:12
				},{
					first_name:"kenny"
					,age:5
				}]
				,nephew: {
					age:8
					,first_name:"Sammy"
				}
				,stuff:{
					things:{
						key:1
						,stuff:"Hello"
					}
				}
				,poop:{
					test:{
						wipe:[1,"2",3,{data:4}]
						,mash:{
							key1: null
							,a:{
								b:{
									c:{
										d:"You found me!"
									}
								}
							}
						}
						,mish: "Hello"
					}
				}
			}
			callback();
		}
		,"Select First": function( test ){
			var field = new Field({
				  selector:".wipe ~ object .a"
				, mapping : "b.c.d"
				, defaultValue:12
			});


			field.setValue( this.json );


			var value = field.getValue();

			test.equal( value, "You found me!");



			test.done();
		}

		,"Select First - Default Value": function( test ){

			var field = new Field({
				  selector:".wipe ~ object .key1"
				, mapping : "key1"
				, defaultValue:12
			});


			field.setValue( this.json )
			var value = field.getValue()

			test.strictEqual( value, 12)
			this.json.poop.test.mash.key1 = "Hello";

			field.setValue( this.json );

			value = field.getValue();
			test.strictEqual( value, "Hello")

			test.done();
		}

		,"Map First": function( test ){

			var field = new Field({
				  selector:".wipe :nth-child(2)"
				, mapping : "poop.test"
				, defaultValue:12
			});

			field.setValue( this.json );

			var value = field.getValue();
			test.strictEqual( value, 2, "Test json select type casting?")
			test.done();
		}


	};

	exports.jsonblobs = {

	};
});
