define(['require', 'exports', 'json'], function(require, exports, json) {


	exports.tests = {
		setUp: function( callback ){
			this.sample1 = '{"valid": "true", "pizza": "always"}';
			this.sample2 = {
                "links": {
             	    "lists": "http://api.rottentomatoes.com/api/public/v1.0/lists.json"
       	            ,"movies": "http://api.rottentomatoes.com/api/public/v1.0/movies.json"
                    }
            };
			callback();
		}

		,validate: function( test ){
			test.equal( true, json.validate(this.sample1), "Sample 1 should validate" );
			test.equal( false, json.validate(this.sample2), "Sample 2 should NOT validate" );
			test.done();
		}

		,encode: function( test ) {
			var sample2encoded = json.encode(this.sample2);
			test.equal( true, json.validate(sample2encoded), "Sample 2 should validate now");
			test.done();			
		}

		,decode: function( test ) {
			var sample1decoded = json.decode(this.sample1);
			var obj = {valid: "true", pizza: "always"};
			test.equal( JSON.stringify(obj), JSON.stringify(sample1decoded) );
			test.done();
		}
	};

});