define(['require', 'exports', 'jsonselect'], function(require, exports, jsonselect) {

	var matchFn = function(test, object, results) {
		for (var key in results) {
			test.deepEqual(jsonselect.match(key, object), results[key], 'match() should return ' + results[key]);
		}
	};

	var forEachFn = function(test, object, results) {
		for (var key in results) {
			jsonselect.forEach(key, object, function(item) {
				var idx = results[key].indexOf(item);
				if (idx > -1) {
					results[key].splice(idx, 1);
				}
			});
			test.strictEqual(results[key].length, 0, 'forEach() should iterate over all matches found');
		}
	};

	exports.level1 = {
		setUp: function(callback) {
			this.testObject = {
				'name': {
					'first': 'Lloyd'
					, 'last': 'Hilaiel'
				}
				, 'favoriteColor': 'yellow'
				, 'languagesSpoken': [
					{
						'language': 'Bulgarian'
						, 'level': 'advanced'
					}, {
						'language': 'English'
						, 'level': 'native'
					}, {
						'language': 'Spanish'
						, 'level': 'beginner'
					}
				]
				, 'seatingPreference': [
					'window'
					, 'aisle'
				]
				, 'drinkPreference': [
					'beer'
					, 'whiskey'
					, 'wine'
				]
				, 'weight': 172
			};
			this.testResults = {
				'string:first-child': ['window', 'beer']
				, 'string.level,number': ['advanced', 'native', 'beginner', 172]
				, '.favoriteColor': ['yellow']
				, '.language': ['Bulgarian', 'English', 'Spanish']
				, '."weight"': [172]
				, 'string.favoriteColor': ['yellow']
				, 'string:last-child': ['aisle', 'wine']
				, 'string:nth-child(-n+2)': ['window', 'aisle', 'beer', 'whiskey']
				, 'string:nth-child(odd)': ['window', 'beer', 'wine']
				, 'string:nth-last-child(1)': ['aisle', 'wine']
				, ':root': [this.testObject]
				, 'string': ['Lloyd', 'Hilaiel', 'yellow', 'Bulgarian', 'advanced', 'English', 'native', 'Spanish', 'beginner', 'window', 'aisle', 'beer', 'whiskey', 'wine']
				, 'number': [172]
			}
			callback();
		}
		, match: function(test) {
			test.expect(13);
			matchFn(test, this.testObject, this.testResults);
			test.done();
		}
		, forEach: function(test) {
			test.expect(13);
			forEachFn(test, this.testObject, this.testResults);
			test.done();
		}
	};

	exports.level2 = {
		setUp: function(callback) {
			this.testObject = {
				'a': 1
				, 'b': 2
				, 'c': {
					'a': 3
					, 'b': 4
					, 'c': {
						'a': 5
						, 'b': 6
					}
				}
				, 'd': {
					'a': 7
				}
				, 'e': {
					'b': 8
				}
			};
			this.testResults = {
				':root > .a ~ .b': [2]
				, ':root .a ~ .b': [2, 4, 6]
				, '.a ~ .b': [2, 4, 6]
			}
			callback();
		}
		, match: function(test) {
			test.expect(3);
			matchFn(test, this.testObject, this.testResults);
			test.done();
		}
		, forEach: function(test) {
			test.expect(3);
			forEachFn(test, this.testObject, this.testResults);
			test.done();
		}
	};

	exports.level3 = {
		setUp: function(callback) {
			this.testObject = {
				'widget': {
					'debug': 'on'
					, 'window': {
						'title': 'Sample Konfabulator Widget'
						, 'name': 'main_window'
						, 'width': 500
						, 'height': 500
					}
					, 'image': {
						'src': 'Images/Sun.png'
						, 'name': 'sun1'
						, 'hOffset': 250
						, 'vOffset': 250
						, 'alignment': 'center'
					}
					, 'text': {
						'data': 'Click Here'
						, 'size': 36
						, 'style': 'bold'
						, 'name': 'text1'
						, 'hOffset': 250
						, 'vOffset': 100
						, 'alignment': 'center'
						, 'onMouseUp': 'sun1.opacity = (sun1.opacity / 100) * 90;'
					}
				}
			};
			this.testResults = {
				':root > object:has(string.first):has(string.last)': []
				, ':has(:root > .first)': []
				, 'object:has .language)': []
				, 'object:has(.language': []
				, '.languagesSpoken object:has       (       .language       ) ': []
				, ':has(:root > .language, :root > .last)': []
				, '.languagesSpoken object:has(.language)': []
				, ':has(:val("Lloyd")) object:has(:val("Hilaiel"))': []
			}
			callback();
		}
		, match: function(test) {
			// test.expect();
			// forEachFn(test, this.testObject, this.testResults);
			// test.done();
		}
		, forEach: function(test) {
			// test.expect();
			// forEachFn(test, this.testObject, this.testResults);
			// test.done();
		}
	};

});