define(["require", "exports", "accessor"], function( require, exports, accessor ){
	exports.tests = {
		setUp: function( callback ){
			this.obj = function(){}

			accessor.call(this.obj, "Test");

			this.obj.defineTest("testfn", function( t ){
				t.ok(true);
			})
			callback();
		}
		,tearDown: function( callback ){

			this.obj = null;
			callback();
		}
		,defineSingular: function( test ){
			var type = typeof this.obj.defineTest;

			test.equal( type, "function", 'Expected function, got: '+ type );

			this.obj.defineTest("number", 1)

			this.obj.defineTest("object", {
				test1:1
				,test2:2
			});

			var t = this.obj.lookupTest("number")

			test.strictEqual( t, 1 )

			t = this.obj.lookupTest("object");

			test.strictEqual( t.test1, 1 )
			test.strictEqual( t.test2, 2 )

			test.done();
		}
		,definePlural: function( test ){
			var type = typeof this.obj.defineTests;

			test.equal( type, "function", 'Expected function, got: '+ type );

			this.obj.defineTests({
				"number":1
				,"object": {
					test1:1
					,test2:2
				}
			})

			var t = this.obj.lookupTest("number")

			test.strictEqual( t, 1 )

			t = this.obj.lookupTest("object");

			test.strictEqual( t.test1, 1 )
			test.strictEqual( t.test2, 2 )
			test.done();
		}
		,lookupSingular: function( test ){
			test.expect( 2 )
			var type = typeof this.obj.lookupTest;

			test.equal( type, "function", 'Expected function, got: '+ type );

			var t = this.obj.lookupTest("testfn").call(this, test )
			test.done();
		}
		,lookupPlural: function( test ){
			var type = typeof this.obj.lookupTests;
			test.equal( type, "function", 'Expected function, got: '+ type );
			this.obj.defineTest("killer", true);

			var t = this.obj.lookupTest("killer");

			console.log( t )
			test.done();
		}
		,possibleFoo: function( test ){

			test.done();
		}
		,eachSingular: function( test ){

			test.done();
		}
	}
});