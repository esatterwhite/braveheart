var assets
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	,nodeRequire: require
	,packages:[{
			name:'slick'
			,location:"../packages/Slick"
		}
		]
});

assets = requirejs('./test/modules/assets');

exports['Assets Tests'] = testCase(assets.tests);