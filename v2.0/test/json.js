var json
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	, nodeRequire: require
});

json = requirejs('./test/modules/json');

exports['JSON Tests'] = testCase(json.tests);