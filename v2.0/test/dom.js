var dom
	, requirejs = require('requirejs')
	, path = require('path')
	, testCase = require('nodeunit').testCase;

requirejs.config({
	baseUrl: path.resolve(path.join(__dirname, '..'))
	,nodeRequire: require
	,packages:[{
			name:'slick'
			,location:"../packages/Slick"
		}
		]
});

dom = requirejs('./test/modules/dom');

exports['dom Tests'] = testCase(dom.tests);