/*jshint laxcomma:true, smarttabs: true */

/**
 * Module for firing up server with Express to feed resources to Zombie
 * @module zombieserver
 **/

if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

 define(['require', 'module', 'exports', 'path', 'express', 'zombie', 'colors'], function(require, module, exports, path, express, Browser, colors) {
 	
 	//Where zombie will listen for requests
	var port = 1998;
	exports.url = 'http://localhost:' + port;

 	/**
	 * Uses express.js to create and start up a server and instantiate a browser
	 * @param {Function} callback This is provided for and used by nodeunit for setUp and tearDown
	 * @return Returns a function which is to be used as the setUp function in nodeunit tests
	 **/
 	exports.start = function( callback ){

    	this.browser = new Browser({debug: false});	
    	
        var app = express()	
    	var root = path.resolve( module.uri,'../../../../' );
    	var options = {
    	  root: path.resolve( root , "v2.0", "test", 'zombie') 
    	}

    	//Server config
    	app.use( express.static( root ) );
    	app.use( express.json() );
		app.use( express.urlencoded() );
		app.set( 'views', path.resolve( module.uri, '../' ) );
		app.set( 'view engine', 'jade' );	
    	
    	//Serve shell html doc for most tests
    	app.get( '/', function( req, res ){
    	  res.render('fixture.jade', options);
    	});	
    	
    	//Serve some static JSON
    	app.get( '/json', function( req, res ){
    		res.send({someJSON: 'welcome to some json!!!'});
    	});	
    	
    	//Serve some HTML!
		app.get( '/html', function( req, res ){
    		res.send('<h1>Free Burritos, <span>Tonight</span> Only!</h1>');
    	});	
    	
    	//Handle POST requests and send the data right back
		app.post( '/post', function( req, res ) {
			res.send(200, JSON.stringify(req.body));
		}); 	
    	
    	//Dynamically serve some JSON
    	app.get( '/:day', function( req, res ){
			var days = { m : 'tacos', t : 'sandwiches', w : 'just sauce'
				,th : 'pasta', f: 'lobster' };
    		res.send('Your food for today is: ' + days[req.params.day] + '!!1!!');  
		});	

    	//Fire it up
    	this.server = app.listen(port)	
    	callback();
	};

	/**
	 * Closes zombie browser, kills express server, and executes nodeunit callback
	 * @param {Function} callback This is provided for and used by nodeunit for setUp and tearDown
	 * @return Returns a function which is to be used as the tearDown function in nodeunit tests
	 **/
	exports.kill = function( callback ) {
      this.browser.close();
      this.server.close();
	  callback();
	}
 });