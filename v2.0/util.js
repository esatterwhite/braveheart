/*jshint laxcomma:true, smarttabs: true, laxbreak:true, eqnull:true */

if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Module for utilities to solve common patterns.
 * @module util
 * @author Eric Satterwhite
 * @requires core
 */

define([ 'require', 'exports' ,'core' ], function( require, exports, core ){

	var typeOf
		,ComparatorRegistry
		,repr
		,compareDateLike
		,compareArrayLike;


	/**
	 * @see module:core.typeOf
	 */
	exports.typeOf = typeOf = core.typeOf;

	// the 'is' which has these function requires util.
	// just trying to work around circular references.
	function dateLike( ){
		var args = Array.prototype.slice.call ( arguments )
			,x;
		for( x = 0; x < args.length; x ++ ){
			if( typeOf( args[ x ]) !== 'date'){
				return false;
			}
		}
		return true;
	}
	function arrayLike(  ){

		var args
			,x;
		args = Array.prototype.slice.call ( arguments );


		for( x = 0; x < args.length; x ++ ){
			if( typeOf( args[ x ]) == 'date'){
				return false;
			}
		}
		return true;
	}

	compareDateLike = function( a,b ){
		return exports.compare(a.getTime(), b.getTime());
	};
	compareArrayLike = function( a, b ){
		var len
			,rvalue
			,cmp
			,point;

		len = a.length;
		rvalue = 0;

		if( len > b.length ){
			rvalue = 1;
			len = b.length;
		} else if ( len < b.length ){
			rvalue = -1;
		} else {

			for( point = 0; point < len; point++ ){
				cmp =exports.compare( a[ point ], b[ point ]);
				if( cmp ) {
					return cmp;
				}
			}
		}
		return rvalue;

	};

	/**
	 * Holds currently registerd ( arrayLike & dateLike )
	 *
	 */
	exports.ComparatorRegistry = ComparatorRegistry = new core.BaseRegistry();

	ComparatorRegistry.register({name:"dateLike", check:dateLike, wrap:compareDateLike});
	ComparatorRegistry.register({name:"arrayLike", check:arrayLike, wrap:compareArrayLike});

	repr = function (o) {
		var ostring;
		if (typeof(o) == "undefined") {
				return "undefined";
		} else if (o === null) {
				return "null";
		}
		try {
				if (typeof(o.__repr__) == 'function') {
					return o.__repr__();
				} else if (typeof(o.repr) == 'function' && o.repr != arguments.callee) {
					return o.repr();
				}
			return core.reprRegistry.match(o);
		} catch (e) {
				try {
						if (typeof(o.NAME) == 'string' && (
									o.toString == Function.prototype.toString ||
									o.toString == Object.prototype.toString
								)) {
								return o.NAME;
						}
				} catch (ignore) {
				}
		}
		try {
				ostring = (o + "");
		} catch (e) {
				return "[" + typeof(o) + "]";
		}
		if (typeof(o) == "function") {
			ostring = ostring.replace(/^\s+/, "").replace(/\s+/g, " ");
			ostring = ostring.replace(/,(\S)/, ", $1");
			var idx = ostring.indexOf("{");
			if (idx != -1) {
					ostring = ostring.substr(0, idx) + "{...}";
			}
		}
		return ostring;
	};


	/**
	 * Compares object that are difficult to compare, ( arrays, and Dates by default )
	 * @param {Object} itemA An item to compare
	 * @param {Object} itemB An item that is of the same type as itemA
	 * @returns 1 if itemA is greater that itemB, 0 if the are equal, and -1 if itemA is less than itemB
	 * @throws {TypeError} If it can not figure out a way to compare the objects
	 */
	exports.compare = function(a, b){
		if( a == b ){
			return 0;
		}

		var aIsNull
			,bIsNull
			,primitives;

		primitives = {'boolean':true, 'number':true, 'string':true};
		aIsNull = typeof a == null;
		bIsNull = typeof b == null;

		if( !(typeof( a ) in primitives && typeof( b ) in primitives )){
			try{
				return ComparatorRegistry.match(a, b);
			} catch( e ){
				if( e != core.error.NotFound ){
					throw e;
				}
			}
		}

		if( a < b ){
			return -1;
		}if ( a > b){
			return 1;
		}
		// Oh Shit! I don't know how to compare that shit!
		throw new TypeError("Object Not Comparable");
	};
});
