if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Provides a standard way to encode / decode Javascript objects
 * @module json
 * @author Eric Satterwhite
 */

define(['require','exports', 'module', 'core','object', 'array/clean', 'array/map'], function( require, exports, module, core, object, clean, map ){
	    var _global;
		try{
			_global = window;
		} catch( e ){
			_global = GLOBAL;
		}
		var JSON = _global.JSON  ||  {};
		var special = {
			'\b': '\\b',
			'\t': '\\t',
			'\n': '\\n',
			'\f': '\\f',
			'\r': '\\r',
			'"': '\\"',
			'\\': '\\\\'
		};

		var escape = function(chr) {
			return special[chr] || '\\u' + ('0000' + chr.charCodeAt(0).toString(16)).slice(-4);
		};

		exports.validate = function(string) {
			if ( typeof string !== 'string') return false;
			
			string = string.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
			replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
			replace(/(?:^|:|,)(?:\s*\[)+/g, '');

			return (/^[\],:{}\s]*$/).test(string);
		};

		/**
		 * Converts a JavaScript object into a JSON String
		 * @param {Object} obj Javascript object to encode to a string
		 * @returns {String} JSON A JSON encode string
		 */
		exports.encode = JSON.stringify ?
		function(obj) {
			return JSON.stringify(obj);
		} : function(obj) {
			if (obj && obj.toJSON){
				obj = obj.toJSON();
			}

			switch (core.typeOf(obj)) {
			case 'string':
				return '"' + obj.replace(/[\x00-\x1f\\"]/g, escape) + '"';
			case 'array':
				return '[' + clean( map(obj, exports.encode) ) + ']';
			case 'object':
				var string = [];
				object.each(obj, function(value, key) {
					var json = exports.encode(value);
					if (json){
						string.push(exports.encode(key) + ':' + json);
					}
				});
				return '{' + string + '}';
			case 'number':
			case 'boolean':
				return '' + obj;
			case 'null':
				return 'null';
			}

			return null;
		};

		/**
		 * Converts a valid JSON String
		 * @param {String} json JSON String to decode into a javascript object
		 * @param {Boolean} secure If set to true the string will be run through a string parser. Otherwise it will be eval'd
		 * @return {Object} Decoded JavaScript object
		 */
		exports.decode = JSON.parse ?
		function( str ){
			return JSON.parse( str );
		}:function(string, secure) {
			if (!string || typeof string != 'string'){
				return null;
			}

			if (secure) {
				if (!exports.validate(string)){
					throw new Error('JSON could not decode the input; security is enabled and the value is not secure.');
				}
			}

			return eval('(' + string + ')');
		};
});
