/*jslint onevar:true, undef:true, newcap:true, regexp:true, bitwise:true, maxerr:50, indent:4, white:false, nomen:false, plusplus:false */
/*global window:false, global:false*/

/*!!
 * JS Signals <http://millermedeiros.github.com/js-signals/>
 * Released under the MIT license <http://www.opensource.org/licenses/mit-license.php>
 * @author Miller Medeiros <http://millermedeiros.com/>
 * @version 0.6.3
 * @build 187 (07/11/2011 10:14 AM)
 */
define(function() {

	/**
	 * @namespace Signals Namespace - Custom event/messaging system based on AS3 Signals
	 * @name signals
	 */
	var signals = /** @lends signals */
	{
		/**
		 * Signals Version Number
		 * @type String
		 * @const
		 */
		VERSION : '0.6.3'
	};

	// SignalBinding -------------------------------------------------
	//================================================================

	/**
	 * Object that represents a binding between a Signal and a listener function.
	 * <br />- <strong>This is an internal constructor and shouldn't be called by regular users.</strong>
	 * <br />- inspired by Joa Ebert AS3 SignalBinding and Robert Penner's Slot classes.
	 * @author Miller Medeiros
	 * @constructor
	 * @internal
	 * @name signals.SignalBinding
	 * @param {signals.Signal} signal	Reference to Signal object that listener is currently bound to.
	 * @param {Function} listener	Handler function bound to the signal.
	 * @param {boolean} isOnce	If binding should be executed just once.
	 * @param {Object} [listenerContext]	Context on which listener will be executed (object that should represent the `this` variable inside listener function).
	 * @param {Number} [priority]	The priority level of the event listener. (default = 0).
	 */
	function SignalBinding(signal, listener, isOnce, listenerContext, priority) {

		/**
		 * Handler function bound to the signal.
		 * @type Function
		 * @private
		 */
		this._listener = listener;

		/**
		 * If binding should be executed just once.
		 * @type boolean
		 * @private
		 */
		this._isOnce = isOnce;

		/**
		 * Context on which listener will be executed (object that should represent the `this` variable inside listener function).
		 * @memberOf signals.SignalBinding.prototype
		 * @name context
		 * @type Object|undefined|null
		 */
		this.context = listenerContext;

		/**
		 * Reference to Signal object that listener is currently bound to.
		 * @type signals.Signal
		 * @private
		 */
		this._signal = signal;

		/**
		 * Listener priority
		 * @type Number
		 * @private
		 */
		this._priority = priority || 0;
	}


	SignalBinding.prototype = /** @lends signals.SignalBinding.prototype */
	{

		/**
		 * If binding is active and should be executed.
		 * @type boolean
		 */
		active : true,

		/**
		 * Default parameters passed to listener during `Signal.dispatch` and `SignalBinding.execute`. (curried parameters)
		 * @type Array|null
		 */
		params : null,

		/**
		 * Call listener passing arbitrary parameters.
		 * <p>If binding was added using `Signal.addOnce()` it will be automatically removed from signal dispatch queue, this method is used internally for the signal dispatch.</p>
		 * @param {Array} [paramsArr]	Array of parameters that should be passed to the listener
		 * @return {*} Value returned by the listener.
		 */
		execute : function(paramsArr) {
			var handlerReturn, params;
			if(this.active && !!this._listener) {
				params = this.params ? this.params.concat(paramsArr) : paramsArr;
				handlerReturn = this._listener.apply(this.context, params);
				if(this._isOnce) {
					this.detach();
				}
			}
			return handlerReturn;
		},
		/**
		 * Detach binding from signal.
		 * - alias to: mySignal.remove(myBinding.getListener());
		 * @return {Function|null} Handler function bound to the signal or `null` if binding was previously detached.
		 */
		detach : function() {
			return this.isBound() ? this._signal.remove(this._listener) : null;
		},
		/**
		 * @return {Boolean} `true` if binding is still bound to the signal and have a listener.
		 */
		isBound : function() {
			return (!!this._signal && !!this._listener);
		},
		/**
		 * @return {Function} Handler function bound to the signal.
		 */
		getListener : function() {
			return this._listener;
		},
		/**
		 * Delete instance properties
		 * @private
		 */
		_destroy : function() {
			delete this._signal;
			delete this._listener;
			delete this.context;
		},
		/**
		 * @return {boolean} If SignalBinding will only be executed once.
		 */
		isOnce : function() {
			return this._isOnce;
		},
		/**
		 * @return {string} String representation of the object.
		 */
		toString : function() {
			return '[SignalBinding isOnce: ' + this._isOnce + ', isBound: ' + this.isBound() + ', active: ' + this.active + ']';
		}
	};

	/*global signals:true, SignalBinding:false*/

	// Signal --------------------------------------------------------
	//================================================================

	function validateListener(listener, fnName) {
		if( typeof listener !== 'function') {
			throw new Error('listener is a required param of {fn}() and should be a Function.'.replace('{fn}', fnName));
		}
	}

	/**
	 * Custom event broadcaster
	 * <br />- inspired by Robert Penner's AS3 Signals.
	 * @author Miller Medeiros
	 */
	signals.Signal = function() {
		/**
		 * @type Array.<SignalBinding>
		 * @private
		 */
		this._bindings = [];
	};

	signals.Signal.prototype = {


		_shouldPropagate : true,


		active : true,


		_registerListener : function(listener, isOnce, scope, priority) {

			var prevIndex = this._indexOfListener(listener), binding;

			if(prevIndex !== -1) {//avoid creating a new Binding for same listener if already added to list
				binding = this._bindings[prevIndex];
				if(binding.isOnce() !== isOnce) {
					throw new Error('You cannot add' + ( isOnce ? '' : 'Once') + '() then add' + (!isOnce ? '' : 'Once') + '() the same listener without removing the relationship first.');
				}
			} else {
				binding = new SignalBinding(this, listener, isOnce, scope, priority);
				this._addBinding(binding);
			}

			return binding;
		},

		_addBinding : function(binding) {
			//simplified insertion sort
			var n = this._bindings.length;
			do {--n;
			} while (this._bindings[n] && binding._priority <= this._bindings[n]._priority);
			this._bindings.splice(n + 1, 0, binding);
		},

		_indexOfListener : function(listener) {
			var n = this._bindings.length;
			while(n--) {
				if(this._bindings[n]._listener === listener) {
					return n;
				}
			}
			return -1;
		},

		add : function(listener, scope, priority) {
			validateListener(listener, 'add');
			return this._registerListener(listener, false, scope, priority);
		},

		addOnce : function(listener, scope, priority) {
			validateListener(listener, 'addOnce');
			return this._registerListener(listener, true, scope, priority);
		},

		remove : function(listener) {
			validateListener(listener, 'remove');

			var i = this._indexOfListener(listener);
			if(i !== -1) {
				this._bindings[i]._destroy();
				//no reason to a SignalBinding exist if it isn't attached to a signal
				this._bindings.splice(i, 1);
			}
			return listener;
		},

		removeAll : function() {
			var n = this._bindings.length;
			while(n--) {
				this._bindings[n]._destroy();
			}
			this._bindings.length = 0;
		},

		getNumListeners : function() {
			return this._bindings.length;
		},

		halt : function() {
			this._shouldPropagate = false;
		},

		dispatch : function(params) {
			if(!this.active) {
				return;
			}

			var paramsArr = Array.prototype.slice.call(arguments), bindings = this._bindings.slice(), //clone array in case add/remove items during dispatch
			n = this._bindings.length;

			this._shouldPropagate = true;
			//in case `halt` was called before dispatch or during the previous dispatch.

			//execute all callbacks until end of the list or until a callback returns `false` or stops propagation
			//reverse loop since listeners with higher priority will be added at the end of the list
			do {
				n--;
			} while (bindings[n] && this._shouldPropagate && bindings[n].execute(paramsArr) !== false);
		},

		dispose : function() {
			this.removeAll();
			delete this._bindings;
		},

		toString : function() {
			return '[Signal active: ' + this.active + ' numListeners: ' + this.getNumListeners() + ']';
		}
	};
	return signals;
})
