/*jshint laxcomma:true, smarttabs: true */


/**
 * AMD Module for working with Object Literals
 * @module object
 * @author Eric Satterwhite
 * @requires array
 */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

define(function( require, exports ){
	var mergeOne
		,cloneOf
		,typeOf
		,cloneArray
		,hasOwnProperty;

	hasOwnProperty = ( Object.prototype.hasOwnProperty );

	cloneArray = function( arr ){
		var len, clone;
		len = arr.length;
		clone = new Array(len);
		while(len--) {
				clone[len] = arr[len];
		}
		return clone;
	};
	typeOf = function(item){
		return ({}).toString.call(item).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
	};
	mergeOne = function( source, key, current ){
		switch( typeOf(  current ) ){
			case 'object':
				if( typeof source[key] === 'object'){
					exports.merge( source[key], current);
				} else {
					source[key] = exports.clone( current );
				}
				break;

			case 'array':
				source[key] = cloneArray( current );
				break;
			default:
			source[ key ] = current;
		}
		return source;
	};

	cloneOf = function( item ){
		switch( typeOf( item ) ){
			case "array":
				return cloneArray( item );
			case 'object':
				return exports.clone( item );
			default:
				return item;
		}
	};


	/**
	 * Merges multiple objects, overriding duplicate keys
	 * @param {Object} source The primary object to merge other values into
	 * @param {Object} obj Object to copy values from
	 * @returns {Object} A new object with all the values from the specified objects
	 */
	exports.merge = function( source, k, v){
		var i
			,key
			,object
			,l;

		if( typeof k  == 'string'){
			return mergeOne(source,k,v);
		}
		for( i = 1, l=arguments.length; i< l; i++){
			object = arguments[i];
			for( key in object ){
				mergeOne( source, key, object[key] );
			}
		}
		return source;
	};


	/**
	 * Recursivly clones objects. - Be careful of objects with circular references of Deeply nested objects to avoid Exceeded maximum recursion errors
	 * @param {Object} obj The object to clone
	 * @returns {Object} A new object that matches the passed in object
	 */
	exports.clone = function(object){
		var cln
			,key;

		cln = {};
		for( key in object ){
			cln[key] = cloneOf( object[key] );
		}

		return cln;
	};

	/**
	 * Copies top level values to a new object
	 * @param {Object} obj the object to clone
	 * @returns {Object} A new object
	 */
	exports.cloneShallow =  function( obj ){
		var cln,key;
		cln = {};
		for( key in obj ){
			if( obj.hasOwnProperty( key )){
				cln[key] = obj[key];
			}
		}
		return cln;
	};

	/**
	 * Executes a funcution on each of the top level keys of an object. <br/>
	 * <p>The function will be passed:</p>
	 * <code><strong>value</strong></code> - the current value in the iteration
	 * <code><strong>key</strong></code> - the current key in the iteration
	 * <code><strong>obje</strong></code> - The original object
	 * @example braveheart(['object'], function( object ){
	var obj = {type:"json", "username":"boobie", "password":"miles"};

	var result = object.each( obj, function( value, key, obj ){
		console.log( key + " " + value )
	});

	// "type json"
	// "username boobie"
	// "password miles"

});
	 */
	exports.each =  function(obj, fn, bind){
		for( var key in obj ){
			if( hasOwnProperty.call( obj, key )){
				fn.call( bind, obj[key], key, obj);
			}
		}
	};
	/**
	 * Returns an array of all the keys in an object
	 * @param {Object} obj The object to extract keys from
	 * @param {Boolean} strict If true, only keys that pass teh hasOwnProperty will be included
	 * @returns {Array} An array with the keys found in the object
	 * @example braveheart(['object'], function( object ){
	var obj = {type:"json", "username":"boobie", "password":"miles"};

	var result = object.keys( obj );

	console.log( result ) // ["type", "username", "password" ]
});
	 */
	exports.keys =  function( obj, strict ){
	var keys = []
			,key;

		for( key in obj ){
			if( !!strict ){
				if( obj.hasOwnProperty( key ) ){
					keys.push( key );
				}
			} else{
				keys.push( key );
			}
		}
		return keys;
	};

	/**
	 * Returns an array of all the values in an object
	 * @param {Object} obj The object to extract values from
	 * @returns {Array} An array with the values found in the object
	 * @example braveheart(['object'], function( object ){
	var obj = {type:"json", "username":"boobie", "password":"miles"};

	var result = object.values( obj );

	console.log( result ) // ['json', 'boobie', 'miles' ]
});
	 */
	exports.values =  function( obj ){
		var values = []
			,key;

		for( key in obj ){
			if( !!obj ){
				if( obj.hasOwnProperty( key ) ){
					values.push( obj[key] );
				}
			} else{
				values.push( key );
			}
		}
		return values;
	};

	/**
	 * Returns an array of arrays [ [ key1, value1]. [ key2, value2 ], ... ]
	 * @param {Object} obj
	 * @return {Array} an array of arrays
	 **/
	exports.iteritems = function( obj ){
		var values = [];
		for( var key in obj ){
			if( obj.hasOwnProperty( key ) ){
				values.push( [key, obj[key] ]);
			}
		}

		return values;
	};

	/**
	 * converts an object into a query string in the format of < key1 >=< value1 >&< key2 >=< key2 >
	 * @param {Object} obj The object to convert
	 * @returns {String} The resulting query string
	 * @example braveheart(['object'], function( object ){
	var obj = {type:"json", "username":"boobie", "password":"miles"};

	var result = object.toQueryString( obj );

	console.log( result ) // "type=json&username=boobie&password=miles"
});
	 */
	exports.toQueryString =  function( obj, base, split ){

		var queryString = [];

		exports.each(obj, function(value, key){
			if (base) {key = base + '[' + key + ']';}
			var result, qs,val;
			switch (typeOf(value)){
				case 'object': result = exports.toQueryString(value, key, split); break;
				case 'array':
					qs = {};
					if( !!split ){
						for(var _x = 0; _x< value.length; _x++ ){
							qs[_x] = val;
						}
						result = exports.toQueryString(qs, key);
					} else {
						result = obj[key];
					}
				break;
				default: result = key + '=' + encodeURIComponent(value);
			}
			if (value != null) {
				queryString.push(result);
			}
		});

		return queryString.join('&');
	};

	/**
	 * Appends the values of multiple objects to another object. A direct copy with no checks
	 * @param {Object} obj The object to append values to
	 * @param {Object} obj1...objN Any number of objects to extract keys and values from
	 * @param {Object} The original Object with the appended values.
	 * @example braveheart([ 'object' ], function( object ){
	 var obj1 = {a:1, b:2, c:3, d:4};
	 var obj2 = {"v1":"hello", "v2":"world"};
	 var obj3 = {"a":"toes"};

	 var result = object.every( obj, obj2, obj3);

	 console.log( result ); // {a:"toes", b:2, c:3, d:4, v1:"hello", v2:"world" }
})
	 */
	exports.append = function( original  ){
		var key
			,extended
			,x,l;

		for( x = 1, l=arguments.length; x < l; x++){
			extended = arguments[x] || {};
			for( key in extended ){
				original[key] = extended[key];
			}
		}
		return original;
	};

	/**
	 * Returns an object with only the keys specified
	 * @param {Object} obj The object to inspect
	 * @param {Array} keys An array of key name you want to keep
	 * @returns {Object} A new object with only the keys specified
	 * @example braveheart([ 'object' ], function( object ){
	 var obj = {a:1, b:2, c:3, d:4};

	 var result = object.subjset( obj, ["a", "c"])	;

	 console.log( result ); // {a:1, c:3}
})
	 */
	exports.subset = function( obj, keys ){
		var results = {}
			,x
			,k
			,len;

		for( x = 0, len = keys.length; x < len; x++ ){
			k = keys[x];
			if( k in obj ){
				results[k] = obj[k];
			}
		}
		return results;
	};

	/**
	 * Iterates over the keys of an object assiging the result of a function to its values
	 * @param {Object} obj The object to re-map
	 * @param {Function} fn The function to execute. Will be passed:<br/>
	 * <ul>
	 *  <li><strong>value</strong> - the current value in the iteration</li>
	 *  <li><strong>key</strong> - the current key in the iteration</li>
	 *  <li><strong>obj</strong> - The original object</li>
	 * </ul>
	 * @param {Object} context The object under whose context the execut the function in
	 * @returns {Object} A new object with its values remapped
	 * @example braveheart(['object', log], function( object, log ){
	var obj = {key1: 1, key2: 2, key3: 3};
	function mapper(value, key, o ){
	    return (value * 5);
	}

	var newObj = object.map( obj, mapper );

	log.log( newObj ) // {key1: 5, key2: 10, key3: 15}
});
	*/
	exports.map = function( obj, fn, bind ){
		var results = {}
			,key;

		for( key in obj ){
			if( Object.prototype.hasOwnProperty.call( obj, key )){
				results[key] = fn.call( bind, obj[key], key, obj );
			}
		}

		return results;
	};

	/**
	 * Filters keys out of an object with a test function
	 * @param {Object} obj The object to filter
	 * @param {Function} fn <p>The test function. Should return true or false. If false, the value will be excluded from the results.<p>
	 * <p>The function will be passed:<p>
	 *<ul>
	 * <li><strong>value</strong> - The current value in the iteration</li>
	 * <li><strong>key</strong> - The current key in the iteration</li>
	 * <li><strong>object</strong> - The original object</li>
	 *</ul>
	 * @param {Object} context The object under whose context the function will be executed
	 * @returns {Object} A new object with only the keys and values that passed the test
	 * @example braveheart(['object', 'log'], function( object, log ){
	var obj = {key1: 1, key2: 2, key3: 3, key4: 4};
	function testFn( value, key, o ){
		return value % 2 === 0;
	}

	var newObj = object.filter( obj, testFn );
	log.log( newObj ) // {key2: 2, key4:4 }

});
	*/
	exports.filter = function( obj, fn, bind ){
		var results = {}
			,key
			,value;

		for( key in obj ){
			value = obj[key];
			if(
				Object.prototype.hasOwnProperty.call( obj, key ) &&
				fn.call( bind, value, key, obj )
			){
				results[key] = value;
			}
		}

		return results;

	};
	/**
	 * Given a function, will test every value in an object and return true if they all pass.
	 * @param {Object} obj The object to test
	 * @param {Function} testFn The function to use as the test. Should return a boolean value
	 * <p>The function will be passed:<p>
	 *<ul>
	 * <li><strong>value</strong> - The current value in the iteration</li>
	 * <li><strong>key</strong> - The current key in the iteration</li>
	 *</ul>
	 * @returns {Boolean} True if all values passed the test
	 * @example braveheart([ 'object' ], function( object ){
	 var obj = {a:1, b:2, c:3, d:4};

	 var passed = object.every( obj, function( value, key ){
		return typeof value === 'number'
	 })	;

	 console.log( passed ); // true
})
	 * @example braveheart([ 'object' ], function( object ){
	 var obj = {a:1, b:2, c:"hello", d:4};

	 var passed = object.every( obj, function( value, key ){
		return typeof value === 'number'
	 })	;

	 console.log( passed ); // false
})
	 */
	exports.every = function(obj, fn, bind ){
		for( var key in obj ){
			if( Object.prototype.hasOwnProperty.call( obj, key) && !fn.call( bind, obj[key], key)){
				return false;
			}
		}
		return true;
	};

	/**
	 * Given a function, will test every value in an object and return true if any pass
	 * @param {Object} obj The object to test
	 * @param {Function} testFn The function to use as the test. Should return a boolean value
	 * <p>The function will be passed:<p>
	 *<ul>
	 * <li><strong>value</strong> - The current value in the iteration</li>
	 * <li><strong>key</strong> - The current key in the iteration</li>
	 *</ul>
	 * @returns {Boolean} True if all values passed the test
	 * @example braveheart([ 'object' ], function( object ){
	 var obj = {a:1, b:2, c:3, d:4};

	 var passed = object.some( obj, function( value, key ){
		return typeof value === 'string'
	 })	;

	 console.log( passed ); // false
})
	 * @example braveheart([ 'object' ], function( object ){
	 var obj = {a:1, b:2, c:3, 4:"hello"};

	 var passed = object.every( obj, function( value, key ){
		return typeof value === 'string'
	 })	;

	 console.log( passed ); // true
})
	 */
	 exports.some =  function( obj, fn, bind){
		for( var key in obj ){
			if( Object.prototype.hasOwnProperty.call( obj, key) && fn.call(bind, obj[key], key)){
				return true;
			}
		}
		return false;
	};

	/**
	 * Returns the number of top level keys in an object ( length )
	 * @param {Object} obj The object to inspect
	 * @returns {Number} The number of keys
	 * @example braveheart([ 'object' ], function( object ){
	 var obj = {a:1, b:2, c:3, d:4};

	 var len = object.keys( obj, function( value, key ){
		return typeof value === 'number'
	 })	;

	 console.log( len ); // 4
})
	 */
	exports.length =  function( obj ){
		return this.keys( obj ).length;
	};

	/**
	 * attempts to find the key a given value is stored under in an object
	 * @param {Object} obj The object to inspect
	 * @param {Object} item The item in the object to locate
	 * @returns {String | Null} The name of the key the value is located at.
	 * @example braveheart([ 'object' ], function( object ){
	 var obj = {a:1, b:2, c:3, d:4};

	 var key = object.keyOf( obj, 1);

	 console.log( key ); // "a"
})
	 */
	exports.keyOf =  function( obj, val ){
		for( var key in obj ){
			if( Object.prototype.hasOwnProperty.call( obj, key ) && obj[key] === val ){
				return key;
			}
		}
		return null;
	};

	/**
	 * Determines if a value is contained in an object
	 * @param {Object} obj The object to inspect
	 * @param {Object} item The item to find
	 * @returns {Boolean} true if the item is found in the object
	 * @example braveheart(['object'], function( object ){
  var x = [];

   var obj1 = {key1:[]}
   var obj2 = {key2: x }

   console.log( object.contains( obj1, x ) ) // false
   console.log( object.contains( obj2, x ) ) // true
});

	 */
	exports.contains =  function( obj, value){
		return this.keyOf( obj, value ) != null;
	};

	exports.create = Object.create || function create(obj) {
		var F = function(){};
		F.prototype = obj;
		return new F();
	};
});
