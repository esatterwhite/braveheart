/*jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * AMD Module for working with arrays.
 * @author Eric Satterwhite
 * @module array
 * @requires module:array/append
 * @requires module:array/associate
 * @requires module:array/clean
 * @requires module:array/clone
 * @requires module:array/combine
 * @requires module:array/contains
 * @requires module:array/each
 * @requires module:array/empty
 * @requires module:array/erase
 * @requires module:array/filter
 * @requires module:array/flatten
 * @requires module:array/from
 * @requires module:array/getLast
 * @requires module:array/getRandom
 * @requires module:array/hexToRgb
 * @requires module:array/hsbToRgb
 * @requires module:array/include
 * @requires module:array/indexOf
 * @requires module:array/insert
 * @requires module:array/link
 * @requires module:array/map
 * @requires module:array/max
 * @requires module:array/min
 * @requires module:array/rgbToHex
 * @requires module:array/rgbToHsb
 * @requires module:array/unique
 */
define(
	['require'
	,'exports'
	,'module'
	,'array/append'
	,'array/associate'
	,'array/clean'
	,'array/clone'
	,'array/combine'
	,'array/contains'
	,'array/each'
	,'array/empty'
	,'array/erase'
	,'array/filter'
	,'array/flatten'
	,'array/from'
	,'array/getLast'
	,'array/getRandom'
	,'array/hexToRgb'
	,'array/hsbToRgb'
	,'array/include'
	,'array/indexOf'
	,'array/insert'
	,'array/link'
	,'array/map'
	,'array/max'
	,'array/min'
	,'array/rgbToHex'
	,'array/rgbToHsb'
	,'array/unique']
	,function(
		require
		,exports
		,module
		,append
		,associate
		,clean
		,clone
		,combine
		,contains
		,each
		,empty
		,erase
		,filter
		,flatten
		,from
		,getLast
		,getRandom
		,hexToRgb
		,hsbToRgb
		,include
		,indexOf
		,insert
		,link
		,map
		,max
		,min
		,rgbToHex
		,rgbToHsb
		,unique ) 
		{
			//Export the required submodules so they can be conveniently accessed through the array module
			exports.append 		= append;
			exports.associate 	= associate;
			exports.clean 		= clean;
			exports.clone	 	= clone;
			exports.combine 	= combine;
			exports.contains 	= contains;
			exports.each 		= each;
			exports.empty 		= empty;
			exports.erase 		= erase;
			exports.filter 		= filter;
			exports.flatten 	= flatten;
			exports.from 		= from;
			exports.getLast 	= getLast;
			exports.getRandom 	= getRandom;
			exports.hexToRgb 	= hexToRgb;
			exports.hsbToRgb 	= hsbToRgb;
			exports.include 	= include;
			exports.indexOf 	= indexOf;
			exports.insert 		= insert;
			exports.link 		= link;
			exports.map 		= map;
			exports.max 		= max;
			exports.min 		= min;
			exports.rgbToHex 	= rgbToHex;
			exports.rgbToHsb	= rgbToHsb;
			exports.unique 		= unique;
});
