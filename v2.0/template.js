/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires dom
 * @requires moduleB
 * @requires moduleC
 **/
define([
		  "require"
		, "module"
		, "exports"
		, 'core'
		, "class"
		, "dom"
		, "array/each"
		, "array/getLast"
		, "array/erase"
		, "array/from"
		, "array/append"
		, "array/filter"
		, "array/empty"
	]
	,function( require, module, exports, core, Class, dom, arrayeach, getLast, arrerase, arrayfrom, arrayappend, arrayfilter, arrayempty ){
		var htmlTags, templates, Template, Templates, engine;

		htmlTags = [
			"a", "abbr", "address", "area", "article", "aside", "audio",
			"b", "base", "bdo", "blockquote", "body", "br", "button",
			"canvas", "caption", "cite", "col", "colgroup", "command",
			"datalist", "dd", "del", "details", "dialog", "dfn", "div", "dl", "dt",
			"em", "embed",
			"fieldset", "figure",
			"footer", "form",
			"h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html",
			"i", "iframe", "img", "input", "ins",
			"keygen", "kbd",
			"label", "legend", "li", "link",
			"map", "mark", "menu", "meta", "meter",
			"nav", "noscript",
			"object", "ol", "optgroup", "option", "output",
			"p", "param", "pre", "progress",
			"q",
			"rp", "rt", "ruby",
			"samp", "script", "section", "select", "small", "source", "span", "strong", "style", "sub", "sup",
			"table", "tbody", "td", "textarea", "tfoot", "th", "thead", "time", "title", "tr",
			"ul",
			"var", "video",
			"acronym", "applet", "basefont", "big", "center", "dir", "font", "frame", "frameset", "noframes", "s", "strike", "tt", "u", "xmp",
			"code"
		];

		engine = {
			callstack:[]
			,tags:{}
		};
		exports.tags = engine.tags;
		exports.evaluate = function( template, data, bind ){
			var elements = [];
			engine.callstack.push(template);
			if (template.prepared == false) {
			    template.code = exports.prepare.call( exports, template.code);
			    template.prepared = true;
			}
			var data = arrayfrom( data || {} )
			arrayeach( data, function(params, index) {
			    if (bind) {
			        template.code.apply(bind, [params, index]);
			    } else {
			        template.code(params, index);
			    }
			    elements = arrayappend( elements, arrayfilter(template, function(node) {
			        return node.getParent() === null;
			    }));
			    arrayempty( template );
			}, exports );

			engine.callstack.pop();

			if (engine.callstack.length) {
			    if (template.elementRefs) {
			        arrayappend( getLast( engine.callstack ).elementRefs, template.elementRefs);
			    }
			}

			return (elements.length > 1) ? elements : elements.shift();
		};

		exports.prepare = function( code ){
			var html = code.toString();
			var args = html.match(/\(([a-zA-Z0-9,\s]*)\)/)[1].replace(/\s/g, '').split(',');
			var body = html.match(/\{([\s\S]*)\}/m)[1];

			for (var i= htmlTags.length; --i >= 0; ) {
			    body = body.replace(new RegExp('(^|[^\\w.])(' + htmlTags[i] + ')([\\s]*(?=\\())', 'g'), '$1engine.tags.$2$3')
			}
			return new Function(args, body);
		};

		arrayeach( htmlTags, function( tag ){
			engine.tags[ tag ] = function(){
				var tpl = getLast( engine.callstack );
				var el  = new dom.Element( tag );

				for( var x=0, len=arguments.length; x<len; x++ ){
					var argument = arguments[x];
					var arg_type  = core.typeOf( argument );

					if( arg_type == "function" ){
						argument = argument();
					}


					switch( arg_type ){
						case "array":
						case "element":
						case "collection":
							el.adopt( argument )
							break;

						case "string":
							if( tpl ){
								arrayeach( el.getChildren(), function( child ){
									arrerase( tpl, child );
								});
							}
							el.set('html', el.get('html') + argument)
							break;

						case "number":
								el.appendText( argument.toString() );
							break;

						case "object":
							if( x === 0){
								if( tpl && tpl.elementRefs && arguments.id ){
									tpl.elementRefs[ argument.id ] = el;
								}
								el.set( argument );
							} else if( core.typeOf( argument.toElement ) == "function"){
								el.adopt( argument.toElement() );
							}

							break;
					}// end switch
				} // end for

				if( tpl ){
					tpl.push( el )
				}

				return el
			}
		});

		/**
		 * DESCRIPTION
		 * @class module:NAME.Thing
		 * @param {TYPE} NAME DESCRIPTION
		 * @example var x = new NAME.Thing({});
		 */
		Template = new Class(/** @lends module:NAME.Thing.prototype */{
			Extends: Array
			,initialize: function( name, code, options){
				if( options && options.elementRefs && core.typeOf( options.elementRefs ) === 'object'){
					this.elementRefs = options.elementRefs
				}

				this.name = name;
				this.code = code;
				this.prepared = false;
			}

			/**
			 * This does something
			 * @param {TYPE} name DESCRPTION
			 * @param {TYPE} name DESCRIPTION
			 * @returns {TYPE} DESCRIPTION
			 */
			,render: function( data, bind ){
				return exports.evaluate( this, data, bind)
			}

		});

		Templates = new Class({
			templates:{}
			,registerTemplate: function( name, code, options ){
				var template = this.templates[ name ];
				return (template) ? template : this.templates[ name ] = new Template( name, code, options );
			}
			,renderTemplate: function(name, data, bind){
				var template = this.templates[ name ]

				return ( template ) ? template.render( data, ( bind || this ) ) : null
			}
		})
		Template.implement('tags', engine.tags)
		Templates.implement('tags', engine.tags)

		exports.Template = Template;
		exports.Templates = Templates;
	}
);
