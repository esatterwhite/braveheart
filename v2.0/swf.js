/*jshint laxcomma:true, smarttabs: true */

/**
 * module for creating and communicating with SWF/flash movies
 * @module swf
 * @requires class
 * @requires object
 * @requires array
 * @requires string
 * @requires dom/element
 */

define(function( require, exports ){

	var Class     = require('class')
		,dom      = require('dom')
		,object   = require('object')
		,arrcombine    =  require('array/combine')
		,strsubstitute   = require('string/substitute')
		,Options  = Class.Options
		,$callBacks
		,remote
		,ua
		,UA
		,is_ie;

	window.$callBacks = $callBacks = {};
	ua = navigator.userAgent.toLowerCase();
	UA = ua.match(/(opera|ie|firefox|chrome|version)[\s\/:]([\w\d\.]+)?.*?(safari|version[\s\/:]([\w\d\.]+)|$)/) || [null, 'unknown', 0];

	is_ie = 'ie' === ( (UA[1] == 'version') ? UA[3] : UA[1] );

	remote = function(swiff, funcName ){
		var movie = swiff.get('node');
		var rs = movie.CallFunction('<invoke name="' + funcName + '" returntype="javascript">' + __flash__argumentsToXML( arguments, 2) + '</invoke>');
		return eval( rs );
	};

	/**
	 * Creates a flash object and embeds it into a current DOM Element
	 * @class
	 * @param {String} path
	 * @param {Object} options The options for the flash movie
	 * The options object, and specifically is container property, is required.
	 * Set options.container to the ID of the currently existing DOM element where the flash vid will be injected.
	 */
	exports.Swiff = new Class( /** @lends module:swf.Swiff.prototype */{
		Implements:[ Options ],
		/**
		 * @lends module:swf.Swiff.prototype.options
		 */
		options:{

			/**
			 * height
			 * @type Number
			 */
			height:1
			,width:1
			,container:null
			,params:{
				quality:'high'
				,allowScriptAccess: 'always'
				,wmode:'window'
				,swLiveConnect: true
			}
			,properties:{}
			,callBacks:{}
			,vars:{}
		},

		initialize: function( path, options ){
			this.instance = "Swiff_" + (+ new Date()).toString(36);
			this.setOptions( options );
			var that = this;
			this.id = this.options.id || this.instance;
			var container = dom.id( this.options.container );
			$callBacks[this.instance] = {};
			var params = this.options.params
				,vars = this.options.vars
				,oCbs = this.options.callBacks;

			var properties = object.append({
				height: this.options.height
				,width: this.options.width
			}, this.options.properties);


			for( var callback in oCbs ){
				$callBacks[this.instance][callback] = ( function( option ){

					return function(){
						return option.apply( that.object, arguments );
					};

				}(oCbs[callback]));
				vars[callback] = "$callBacks." + this.instance +'.' + callback;
			}

			params.flashVars = object.toQueryString( vars );

			// stupid IE
			if( is_ie ){
				properties.classid = "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000";
				params.movie = path;
			} else {
				properties.type = "application/x-shockwave-flash";
			}
			properties.data = path;
			this.path = path;
			var build = this.build( params, properties );

			this.object = ( (container) ? container.empty() : new dom.Element('div')).set('html', build ).getFirst();
		},

		/**
		 * returns the flash object
		 * @private
		 * @returns {Object}
		 */
		toElement:function( ){
			return this.object;
		},

		/**
		 * replaces a node with a different node
		 * @private
		 * @returns {Swiff} swf The swf object
		 */
		replaces: function( element ){
			var el = dom.id( element );
			el = el.isNode ? el.node : el;
			el.parentNode.replaceChild( this.toElement, el );

			return this;
		},

		inject: function( element ){
			var el = dom.id( element );

			this.toElement().inject( el );
			return this;
		},

		/**
		 * Calls a function inside of the flash movie
		 * @param {String} name The name of the function to call
		 * @param {Mixed} args any aditional pararms will be passed to the function as is
		 */
		invoke: function(){
			var args = arrcombine( [this.toElement()], arguments );
			remote.apply( this, args );
		},

		/**
		 * Constructs the flash object html needed
		 * @protected
		 * @param {Object} params flash parameters to send to the movie at load
		 * @param {Object} properties Properties to set on theobject html tag
		 * @returns {String} html The html string for the object
		 */
		build: function( params, properties ){

			params = params || this.options.params;
			properties = properties || this.options.properties;
			var buildstr = '<object id="' + this.instance + '"';

			for( var prop in properties ){
				buildstr += ' ' + prop + '="' + properties[prop] +'"';
			}

			buildstr += '>';

			for( var param in params ){
				if( params[param] ){
					buildstr += '<param name="' + param + '" value="' + params[param] + '" />';
				}
			}

			var data = object.merge( params, properties );

			data.path = this.path;

			this.options.vars.instance = this.instance;
			data.vars = object.toQueryString(this.options.vars);
			data.id = this.id;

			buildstr += strsubstitute( '<embed type="application/x-shockwave-flash"'+
												'id="{id}"'+
												'name="{id}"' +
												'src="{path}"' +
												'height="{height}"' +
												'width="{width}"' +
												'allowScriptAccess="{allowScriptAccess}"' +
												'allowFullScreen="{allowFullScreen}"' +
												'FlashVars="{vars}"' +
												'wmode="transparent"/> ', data);
			buildstr += '</object>';
			return buildstr;


		}.protect(),

		repr: function(){
			return "<Swiff: " + ( this.path || this.id ) + " />";
		},

		toString: function(){
			return this.repr();
		}
	});
});
