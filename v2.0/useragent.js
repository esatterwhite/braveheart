/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*global define, document, navigator, window, ActiveXObject*/

/**
 * A UserAgent detection module. It relies on the navigator.userAgent property.
 * @module useragent
 * @requires functools
 */

define(['require', 'exports', "functools"], function(require, exports, functools) {

	var ua = (window.navigator && navigator.userAgent).toLowerCase() || '';
	var UA = ua.match(/(opera|ie|firefox|chrome|version)[\s\/:]([\w\d\.]+)?.*?(safari|version[\s\/:]([\w\d\.]+)|$)/) || [null, 'unknown', 0];
	var mode = UA[1] == 'ie' && document.documentMode;
	var platform = navigator.platform.toLowerCase();
	var flVersion = (functools.attempt(
			function(){
				return navigator.plugins["Shockwave Flash"].description;
			}
			,function(){
				return new ActiveXObject('ShockwaveFlash.ShackwaveFlash').getVariable('$version');
			}
		) || "0 r0").match(/\d+/g);

	exports.Engine = {};

	/**
	 * Contains boolean values for common browser features
	 * @type {Object}
	 * @property {Boolean} xpath true if the browser has xpath suppoert
	 * @property {Boolean} air true if the browser has Adobe Air installed
	 * @property {Boolean} query true if the browser support querySelectorAll
	 * @property {Boolean} json true if the browser supports JSON natively
	 */
	exports.Features = {
		xpath:  !!( document && document.evaluate)
		,air:   !!(window && window.runtime)
		,query: !!(document && document.querySelector)
		,json:  !!( window && window.JSON )
	};

	var setEngine = function( name, version ){
		exports.Engine.name = name;
		exports.Engine[ name + version ] = true;
		exports.Engine.version = version;
	};

	/**
	 * The name of the current Browser, "chrome", "firefox",
	 * @type {String}
	 */
	exports.name = ( UA[1] == 'version') ? UA[3] : UA[1];

	/**
	 * The version number of the browser
	 * @type {Number}
	 */
	exports.version = mode || parseFloat( ( UA[1] == 'opera' && UA[4]) ? UA[4] : UA[2] );

	/**
	 * Contains boolean values for common browser features
	 * @type {Object}
	 * @property {Boolean} name A boolean value stashed under the key which matches the name property.

###### Possiblities are:

+  mac
+  win
+  linux
+  ios
+  android
+  webos
+  other

	 * @property {String} name The name of the user's computer platform,
	 * @example braveheart(['useragent'], function( ua ){
    ua.Platform.name // 'linux'
    ua.Platform.linux // true
})
	 */
	exports.Platform = {
		name: ua.match(/ip(?:ad|od|hone)/) ? 'ios' : (ua.match(/(?:webos|android)/) || platform.match(/mac|win|linux/) || ['other'])[0]
	};

	/**
	 * Holds information about browser plugins
	 * @type {Object}
	 * @property {Object} Flash details about the current Flash Plugin
	 * @property {String} Flash.version version string of the flash installation
	 * @property {String} Flash.build version string of the build number of the flash instalation
	 */
	exports.Plugins = {
		Flash:{
			version: Number( flVersion[0] || '0.' +flVersion[1] ) || 0
			,build: Number( flVersion[2]) || 0
		}
	};

	exports[exports.name] = true;
	exports[exports.name + parseInt( exports.version, 10) ] = true;
	exports.Platform[ exports.Platform.name] = true;

	if( exports.ie ){
		exports.Engine.trident = true;

		switch( exports.version){
			case 6:
				setEngine( 'trident', 4);
				break;
			case 7:
				setEngine('trident', 5);
				break;
			case 8:
				setEngine('tridet', 6);
		}
	}

	if( exports.firefox ){
		exports.Engine.gecko = true;

		if( exports.version >= 3 ){
			setEngine('gecko', 19);
		} else {
			setEngine( 'gecko', 18);
		}
	}

	if( exports.safari || exports.chrome ){
		exports.Engine.webkit = true;

		switch( exports.version ){
			case 2:
				setEngine('webkit', 419);
				break;
			case 3:
				setEngine( 'webkit', 420 );
				break;
			case 4:
				setEngine('webkit', 525);
		}
	}

	if( exports.name === 'unknown' ){
		switch(ua.match(/(?:webkit|khtml|gecko)/ || []) [0]) {
			case 'webkit':
			case 'khtml':
				exports.Engine.webkit = true;
				break;
			case 'gecko':
				exports.Engine.gecko = true;
		}
	}

	/**
	 * Checks if the browser is Chrome or compatible.
	 * @return {Boolean} True if the browser is Chrome or compatible
	 */
	exports.isChrome = function() {
		return (/webkit\W.*(chrome|chromium)\W/i).test(ua);
	}();

	/**
	 * Checks if the browser is Firefox.
	 * @return {Boolean} True if the browser is Firefox
	 */
	exports.isFirefox = function() {
		return (/mozilla.*\Wfirefox\W/i).test(ua);
	}();

	/**
	 * Checks if the browser is using the Gecko engine.<br/>
	 * Used to identify Firefox and other browsers that use XulRunner.
	 * @return {Boolean} True if the browser is using the Gecko engine
	 */
	exports.isGecko = function() {
		return (/mozilla(?!.*webkit).*\Wgecko\W/i).test(ua);
	}();

	/**
	 * Checks if the browser is Internet Explorer.
	 * @return {Boolean} True if the browser is Internet Explorer
	 */
	exports.isIE = function() {
		return navigator.appName === 'Microsoft Internet Explorer';
	}();

	/**
	 * Checks if the browser is running on Kindle.
	 * @return {Boolean} True if the browser is running on Kindle
	 */
	exports.isKindle = function() {
		return (/\W(kindle|silk)\W/i).test(ua);
	}();

	/**
	 * Checks if the browser is running on a mobile device.
	 * @return {Boolean} True if the browser is running on a mobile device
	 */
	exports.isMobile = function() {
		return (/(iphone|ipod|(android.*?mobile)|blackberry|nokia)/i).test(ua);
	}();

	/**
	 * Checks if the browser is Opera.
	 * @return {Boolean} True if the browser is Opera
	 */
	exports.isOpera = function() {
		return (/opera.*\Wpresto\W/i).test(ua);
	}();

	/**
	 * Checks if the browser is Safari.
	 * @return {Boolean} True if the browser is Safari
	 */
	exports.isSafari = function() {
		return (/webkit\W(?!.*chrome).*safari\W/i).test(ua);
	}();

	/**
	 * Checks if the browser is running on a tablet.
	 * @return {Boolean} True if the browser is running on a tablet
	 */
	exports.isTablet = function() {
		/*
		 * One way to distinguish Android mobiles from tablets is that the
		 * mobiles contain the string "mobile" in their UserAgent string.
		 * If the word "Android" isn't followed by "mobile" then its a
		 * tablet.
		 */
		return (/(ipad|android(?!.*mobile))/i).test(ua);
	}();

	/**
	 * Checks if the browser is running on a TV.
	 * @return {Boolean} True if the browser is running on a TV
	 */
	exports.isTV = function() {
		return (/googletv|sonydtv/i).test(ua);
	}();

	/**
	 * Checks if the browser is using the WebKit engine.
	 * @return {Boolean} True if the browser is using the WebKit engine
	 */
	exports.isWebKit = function() {
		return (/webkit\W/i).test(ua);
	}();

	/**
	 * Checks if we're rolling with zombie.js headless browser
	 * @return {Boolean} True if Zombie  
	 **/
	exports.isZombie = function() {
		return (/zombie/i).test(ua);
	}();

	/**
	 * Returns the complete UserAgent string verbatim.
	 * @return {String} The user-agent
	 */
	exports.whoami = function() {
		return ua;
	}();


});
