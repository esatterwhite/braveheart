/*jshint laxcomma: true, smarttabs: true*/
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Provids foundational functionality for the braveheart library
 * @author Eric Satterwhite
 * @module core
 * @requires functools
 */
define(['require','exports','module', 'functools'],function( require, exports, module, functools ){


	var objectResolver
		,resolve
		,NamedError
		,repr
		,registry
		,error = {}
		,hooks = {}
		,hooksOf
		,extend
		,implement
		,_bind
		,typeOf
		,enumerables
		,Primitive
		,overloadSetter
		,overloadGetter
		,toString
		,typeCheck
		,toType;

	toString = Object.prototype.toString;

	typeCheck = /\[object\s(\w*)\]/;

	toType = function(item) {
		return toString.call(item).replace(typeCheck, '$1').toLowerCase();
	};


	overloadSetter = functools.overloadSetter;
	overloadGetter = functools.overloadGetter;

	enumerables = true;
	for (var i in {toString: 1}) {
		enumerables = null;
	}

	enumerables = ['hasOwnProperty', 'valueOf', 'isPrototypeOf', 'propertyIsEnumerable', 'toLocaleString', 'toString', 'constructor'];

	_bind = function( fn, scope, args, appendArgs ){
		if( arguments.length === 2 ){
			return fn.apply( scope, arguments );
		}

		var method = fn
			,slice = Array.prototype.slice;

			return function(){
				var callArgs = args || arguments;

				if(appendArgs === true ){
					callArgs = slice.call( arguments, 0 );
					callArgs = callArgs.concat( args );
				}
				return method.apply( scope || window, callArgs );
			};
	};

	typeOf = function( item ){
		if(item === null) {
			return 'null';
		} else if(item === undefined) {
			return 'undefined';
		}
		if( item.$family != null ){
			return item.$family();
		}
		if( !!item.$constructor ){
			return 'class';
		}
		var type = typeof item
			,xtype;

		if(!!item.superclass) {
				xtype = item.getXType();
				return xtype ? xtype : 'class';
		}

		if(item === 'object') {
			if(item.nodeType !== undefined) {
				if(item.nodeType === 3) {
					return (/\S/).test(item.nodeValue) ? 'textnode' : 'whitespace';
				} else {
					return 'element';
				}
			}
		}
		return toString.call( item ).replace( typeCheck, "$1").toLowerCase();
	};
	/*
	 * returns the hooks for an objects type. If there are none, an empty array is creates, stashed in it's slot and returned
	 * @function module:core.hooksOf
	 * @private
	 * @inner
	 * @param obj {Object} an object for whos tpye you whish to retireve hooks for
	 * @return {Array} an array of associated hooks
	 */
	hooksOf = function( obj ){
		var type = typeOf( obj.prototype );
		return hooks[type] || ( hooks[type] = []);
	};

	/**
	 * Will extend an object with another method as long as the method is not protected or hidden
	 * @param {String} name the name to store the method as
	 * @param {Function} method the function to extend
	 */
	extend = function( name, method ){
		if( method && method.$hidden ){
			return;
		}
		var previous = this[name];
		if( previous == null || !previous.$proected){
			this[name] = method;
		}
	};

	implement = function( name, method ){
		var  hooks
			,hook
			,previous
			,i;

		hooks = hooksOf( this );
		for( i = 0; i < hooks.length; i++){
			hook = hooks[i];
			if( typeOf( hook ) == 'type' ){
				implement.call( hook, name, method );
			} else {
				hook.call( this, name, method );
			}
		}
		previous = this.prototype[name];
		if( previous == null || !previous.$protected ){
			this.prototype[ name ] = method;
		}
		if( this[ name ] == null && typeOf( method ) === 'function' ){

			// extends this with a wrapper function which calls the passed in method
			extend.call( this, name, function( item ){
				return method.apply( item, Array.prototype.slice.call( arguments, 1) );
			});
		}

	};

	objectResolver = function( obj, arr, quiet ){
		var container = obj || window
			,klass  = container[arr[0]];

		arr = arr.slice(1);
		if ( klass == undefined ) {
			if( !!quiet ){
				return null
			}
			throw "In valid object path";
		}
		while (arr.length){
			return objectResolver( klass, arr, quiet );
		}

		return klass;
	};

	resolve = function( classname, obj, splitter, quiet ){
		classname = '' + classname;
		splitter = splitter || "."
		return objectResolver.apply( this, [obj, classname.split( splitter ), quiet ]);
	};

	Function.prototype.extend = overloadSetter( function( key, value ){
		this[key] = value;
	});

	Function.prototype.implement = overloadSetter( function( key, value ){
		this.prototype[key] = value;
	});


	Function.extend({
		attempt: function(){
			for( var i=0, l=arguments.length; i<l; i++){
				try{
					return arguments[i]();
				} catch( e ){}
			}
			return null;
		}
	});
	Function.implement({
		protect: function( ){
			this.$protected = true;
			return this;
		}

	});

	// A little more node friendly
	if( !Function.prototype.hasOwnProperty( 'bind' ) ){
		Function.implement({
			bind: function( scope ){
				var that = this
					,args;

				args = ( arguments > 1 )? Array.prototype.slice.call( arguments, 1 ) :null;

				return function(){
					if( !args && !arguments.length ){
						return that.call( scope );
					}
					if( args && arguments.length ){
						return that.apply( scope, args.concat( Array.prototype.slice.call(item)( arguments ) ));
					}

					return that.apply( scope, args || arguments );
				};
			}
		})
	} else{
		//
	}

		/**
		 * <strong>Provides the foundation for custom objects.</strong>
		 *
		 * Ideal for situations where you don't need a complex class, but you don't want to do raw prototype coding. ex. The Braveheart {@link module:class.Class} is a Primitive.<br/>
		 *
		 * <strong>NOTE:</strong> The Primitive System can also be used on Native objects to normalize or augment existing APIs
		 * @class
		 * @param {String} name The name of the new type to create
		 * @param {Object} obj The object to use as the prototype for the new Primitive
		 * @return {Object} A new Primitive instance which can be extended to create new things
		 *
		 * @example braveheart(['core'], function( core ){
	var Primitive = core.Primitive;

	var Collection = new Primitive("Collection", function(){
		this.memory = [];
	});

	Collection.implement({
		add: function( item ){
			this.memory.push( item )
		}
		,remove: function( item ){
			var len = this.memory.length;
			var idx = -1;
			for(var x = 0; x < len; x++){
				if( this.memory[x] === item ){
					idx = x;
					break;
				}
			}
			return idx;
		}
	});

	var Queue = new Collection();

	Queue.add(1);
	Queue.remove( 1 );
});
		 * </code></pre>
		 * <h3>Augment native object</h3>
		 * <p>Primitive can also be used to wrap native JS object to override and add methods.
		 * @example braveheart(['core'], function( core ){
		var Primitive = core.Primitive;

		Array = new Primitive("Array", Array);

		Array.implement({
			map: function( fn, bind ){
				for( var x = 0; x < this.length; x++;){
					this[x] = fn.call(this, this[x], x, this)
				}

				return this;
			}
		})
	}); // end braveheart call

	// Native Arrays!
	var x = [1,2,3,4].map( function(val, idx, arr){
		return val * 2;
	})

	// [2,4,6,8]
		 * </code></pre>
		 * <h3>Primitive Checking</h3>
		 * <p>When a new Primitive is created, a type check( is<Primitive> ) specfic to the type is created on the Primitive function it self.
		 * @example braveheart(['core'], function( core ){
	var Primitive = core.Primitive;

	Array = new Primitive("Array", Array);

	var a = []
	var b = {}

	Primitive.isArray( b ) // false
	Primitive.isArray( a ) // true
});
		 */
	Primitive = function Primitive( name, obj ){
		var lower
			,typeCheck;

		if( name ){
			lower =  typeof name === 'string' ? name.toLowerCase() : null;
			if(!lower){
				throw "<Primitive: core> name must be a string";
			}
			typeCheck = function( item ){
				return typeOf( item ) === lower;
			};

			Primitive['is' + name ] = typeCheck;

			if( obj != null ){
				obj.prototype.$family = functools.hide(function(){
					return lower;
				});


			}
		}

		if( obj == null ){
			return null;
		}

		obj.extend( this );
		obj.$constructor = Primitive;
		obj.prototype.$constructor = obj;

		return obj;
	};

	/**
	 * does stuff
	 * @memberOf module:core.Primitive
	 * @property isEnumberable
	 */
	Primitive.isEnumerable = function( item ){
		return ( item != null ) && typeOf( item.length ) == 'number' && typeOf( item ) !== 'function';
	};


	Primitive.implement(/** @lends module:core.Primitive.prototype */{
		/**
		 * Attaches property onto the Primitive Object
		 * @method module:core.Primitive#extend
		 * @param {Object} members key / value pairs where the key is the name and the value is the property to attach onto the Primitive
		 */
		extend: overloadSetter( extend ),
		/**
		 * Implements member onto the prototype of a given type. Also adds a static method of the same name which performs the same action on the same type. Also creates a type check function for the new type
		 * @method module:core.Primitive#implement
		 * @param {Object} members key / value pairs where the key is the name and the value is the property to implement on the new Primitive.
		 * @example braveheart(["core"], function( core ){
	var Primitive = core.Primitive;

	var Test = function( value ){
		this.items = [ value ]
	};

	Test = new Primitive("Test", Test);

	Test.implement({
		push: function( item ){
			this.items.push( item );
		},

		length: function( ){
			return this.items.length;
		}
	});


	var t = new Test( 1 );

	t.push( 2 ); // [1, 2]

	Test.push(t, 3), [ 1,2,3 ]

	Primitive.isTest( t ) // True
})
		 */
		implement: overloadSetter( implement ),

		/**
		 * Aliases on function to another name
		 * @param {String} alias The name to alias another function to
		 * @param {String} original The name of the function you wish to alias
		 */
		alias: function( name, existing ){
			implement.call( this, name, this.prototype[existing] );
		},

		/**
		 * Given another Primitive, when a method is added via the implement function to the current Primitive, it will be automatically added to the second type
		 * @param {Primitive|Class} type The type that should mirror the current Primitive.
		 */
		mirror: function( hook ){
			hooksOf( this ).push( hook );
			return this;
		}
	});


	/**
	 * Allows check / wrap pairs to be registerd under a name
	 * @constructor
	 * @param {Object} config object with members:
	<ul>
		<li><strong>name:</strong>  string - name of the comaparing pair</li>
		<li><strong>check:</strong> function - a function which, givent two objects, will return true if they are comparable</li>
		<li><strong>wrap:</strong>  function - function to perform the comparison. should return 0 if object are equal, -1 if a < b, and 1 if a > b.</li>
	</ul>
	 * @return {Object} A new Primitive instance which can be extended to create new things
	 */
	exports.BaseRegistry = registry = new Primitive("Registry", function(){
		this.pairs = [];
	});

	registry.implement(/** @lends module:core.BaseRegistry.prototype */{

		/**
		 * registers a check / wrap pair under a name
		 * @param {Object} config {name:str, check:fn, wrap:fn, override:bool }
		 */
		register: function( config ){
			if( !!config.override ){
				this.pairs.unshift(config);
			} else {
				this.pairs.push( config );
			}
		},

		/**
		 * attempts if the check function passes will return the result of the wrap function
		 * @param {Object} config a config object
	<ul>
		<li><strong>name:</strong>  string - name of the comaparing pair</li>
		<li><strong>check:</strong> function - a function which, givent two objects, will return true if they are comparable</li>
		<li><strong>wrap:</strong>  function - function to perform the comparison. should return 0 if object are equal, -1 if a < b, and 1 if a > b.</li>
	</ul>
		 */
		match: function( ){
			var x
			,pair;

			for( x = 0; x < this.pairs.length; x++ ){
				pair = this.pairs[ x ];
				if( pair.check.apply( this, arguments) ){
					return pair.wrap.apply( this, arguments);
				}
			}
			throw error.NotFound;
		},

		/**
		 * Un registers a check / wrap paif for a given name
		 * @param {String} name removes the regisered pair for the given name
		 */
		unregister: function( name ){
			var pair
				,x;

			for( x = 0; x < this.paris.length; x++){
				pair = this.pairs[ x ];
				if( pair.name == name){
					this.pairs.splice( x, 1);
					return true;
				}
			}
			return false;
		}
	});

	NamedError = function( name, message ){
		this.message = message || "Object Not Found";
		this.name = name;
	};

	// I'm not sure what I was going to use this for... -ERS
	var filter =  function( fn, lst, self ){
		var  rvalue = []
			,i
			,o;

		if( fn == null ){
			fn = function( a ){
				return !!a;
			};

		}
		if( typeof Array.prototype.filter === 'function'){
			return Array.prototype.filter.call( lst, fn, self );

		} else if( typeof self == null ){
			for(i=0; i < lst.length; i++){
				o = lst[i];
				if( fn( o ) ){
					rvalue.push( o );
				}
			}
		}

		return rvalue;
	};
	NamedError.prototype = new Error();
	NamedError.implement({
		repr: function(){
			if( this.message && this.message !=this.name ){
				return this.name + "(" + repr( this.message ) + ")";
			} else {
				return this.name + "()";
			}
		}
		,toString: function(){
			return this.repr( arguments );
		}
	});



	exports._newNamedError = function( module, name, message, func ){
		func.prototype = new NamedError(name, message);
		func.prototype.constructor = func;
	};


	exports.resolve = resolve;

	/**
	 * given a JSON path string will attempt to look up values in a deeply nested objects
	 * @function
	 * @static
	 * @param {Object} item The item to inspect
	 */
	exports.getattr = resolve;
	
	/**
	 * given a JSON path string will attempt to set values in a deeply nested objects
	 * @function
	 * @static
	 * @param {Object} item The item to inspect
	 * @param {String} path a JSON path the the value to set
	 * @param {value} value The Value to set at the end of the path traversal
	 * @param {String} [sep="."] The separator in the path string to denote levels of traversal
	 * @example braveheart(['core'], function( core ){
	var some_object = {
		"foo":{
			"bar":{
				"bazz":1
			}
		}
	}

	core.setattr( "foo.bar.bazz" , some_object 100) // 1
	core.getattr(some_object, "foo.bar.bazz" ) // 100
 })*/
	exports.setattr = function( mainobj, path, value, sep ){
		var ref
			,len
			,traverse;



		ref = mainobj || {};

		sep = sep || "."
		if( typeof path == "string"){
			path = path.split( sep );
		} else if( path instanceof Array ){
			path = path;
		} else {
			throw "path specification must be a valid file path or Array";
		}

		traverse = function( obj, path, value ){
			var container = obj || {}
				, key = path[0]
				,klass = container[ key ];

			path = path.splice(1);

			if( !klass ){
				klass = container[ key ] = { };
			}

			while( path.length ){
				return traverse( klass, path, value)
			}
			container[key] = value;
			return value;
		}

		traverse( ref, path, value )
		return mainobj;
	}
	/**
	 * Collective namespace for Standard errors
	 * @type {Object}
	 * @property {Error} NamedError An error type that can be named
	 * @property {NamedError} NotFound A named error to be thrown when an object is not found
	 * @property {NamedError} NotImplemented a named error to be thrown when specific methods have not been implemented
	 * @example braveheart(['core'], function( core ){ 
    var StupidError = new core.error.NamedError("Stupid Error", "You shouldn't write code that makes you look stupid");
    try{
    	myObject.test( thing )
    } catch( err ){
    	if( err === StupidError ){
    		alert("You Are Stupid!")
    	}
    }
})
	 * @example braveheart(['core'], function( core ){
	try:
	   something_risky()
	catch(e){
		if( e === core.error.NotFound ){
			do_something_else_risky();
		} else{
			throw e;
		}
	}
})
	 */
	exports.error = {
		/**
		 * A predefined Error which allows for better error checking
		 * @type Function
		 * @memberOf module:core.error
		 * @param {String} name Name of the error
		 * @param {String} message The error message to display
		 * @example 
	try:
	   something_risky()
	catch(e){
		if( e === core.error.NotFound ){
			do_something_else_risky();
		} else{
			throw e;
		}
	}
 })*/
		NamedError:NamedError,
		NotFound:new NamedError("Not Found", "Object Not Found"),
		NotImplemented:new NamedError("Not Implemented", "Method Not Implemented")
	};




	exports.Primitive = new Primitive("Primitive", Primitive);

	// Backwards compat
	exports.Type = Primitive

	/**
	 * A smart version of the built-in typeof operator. Is aware of Braveheart & EXT Classes, anything that defines a $family function as well as all native JS object types
	 * @param {Object} item The item to inspect
	 */
	exports.typeOf = typeOf;

	exports.bind = _bind;

	exports.filter = filter;

});
