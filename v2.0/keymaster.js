/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*globals window, document, define*/

define(['require', 'exports'], function(require, exports) {

	var _handlers = {}
		, k
		// special keys
		, _MAP = {
			backspace: 8
			, tab: 9
			, clear: 12
			, enter: 13
			, 'return': 13
			, esc: 27
			, escape: 27
			, space: 32
			, pageup: 33
			, pagedown: 34
			, end: 35
			, home: 36
			, left: 37
			, up: 38
			, right: 39
			, down: 40
			, del: 46
			, 'delete': 46
			, ';': 186
			, '=': 187
			, ',': 188
			, '-': 189
			, '.': 190
			, '/': 191
			, '`': 192
			, '[': 219
			, ']': 221
			, '\\': 220
			, '\'': 222
		}
		// modifier keys
		, _MODIFIERS = {
			'⇧': 16
			, shift: 16
			, '⌃': 17
			, ctrl: 17
			, control: 17
			, '⌥': 18
			, alt: 18
			, option: 18
			, '⌘': 91
			, command: 91
		}
		, _mods = {
			16: false
			, 17: false
			, 18: false
			, 91: false
		}
		, _scope = 'all';

	// IE doesn't support Array#indexOf, here's a simple replacement
	function index(array, item) {
		var i = array.length;
		while(i--) {
			if (array[i] === item) {
				return i;
			}
		}
		return -1;
	}

	// cross-browser events
	function addEvent(object, event, method) {
		if (object.addEventListener) {
			object.addEventListener(event, method, false);
		} else if (object.attachEvent) {
			object.attachEvent('on' + event, function() { method(window.event); });
		}
	}

	// parse and assign shortcut
	function assignKey(key, scope, method) {
		var keys, mods, i, mi;

		if (method === undefined) {
			method = scope;
			scope = 'all';
		}
		// remove whitespace
		key = key.replace(/\s/g,'');
		// for multiple shortcuts that do the same thing
		keys = key.split(',');

		// for shortcuts with a comma
		if ((keys[keys.length - 1]) === '') {
			keys[keys.length - 2] += ',';
		}
		// for each shortcut
		for (i = 0; i < keys.length; i++) {
			// set modifier keys if any
			mods = [];
			key = keys[i].split('+');
			if (key.length > 1) {
				mods = key.slice(0, key.length-1);
				for (mi = 0; mi < mods.length; mi++) {
					mods[mi] = _MODIFIERS[mods[mi]];
				}
				key = [key[key.length-1]];
			}
			// convert to keyCode and ...
			key = key[0];
			key = _MAP[key] || key.toUpperCase().charCodeAt(0);
			// ... store handler
			if (!(key in _handlers)) {
				_handlers[key] = [];
			}
			_handlers[key].push({ shortcut: keys[i], scope: scope, method: method, key: keys[i], mods: mods });
		}
	}

	// handle keydown event
	function dispatch(event) {
		var handler
			, i
			, k
			, key
			, modifiersMatch;

		key = event.keyCode;

		// if a modifier key, set the key.<modifierkeyname> property to true and return
		if (key === 93 || key === 224) {
			key = 91; // right command on webkit, command on Gecko
		}
		if (key in _mods) {
			_mods[key] = true;
			// 'assignKey' from inside this closure is exported to window.key
			for (k in _MODIFIERS) {
				if (_MODIFIERS[k] == key) {
					assignKey[k] = true;
				}
			}
			return;
		}

		// see if we need to ignore the keypress (filter() can can be overridden)
		// by default ignore key presses if a select, textarea, or input is focused
		if (!assignKey.filter.call(this, event)) {
			return;
		}

		// abort if no potentially matching shortcuts found
		if (!(key in _handlers)) {
			return;
		}

		// for each potential shortcut
		for (i = 0; i < _handlers[key].length; i++) {
			handler = _handlers[key][i];

			// see if it's in the current scope
			if(handler.scope == _scope || handler.scope == 'all'){
				// check if modifiers match if any
				modifiersMatch = handler.mods.length > 0;
				for (k in _mods) {
					if ((!_mods[k] && index(handler.mods, +k) > -1) || (_mods[k] && index(handler.mods, +k) == -1)) {
						modifiersMatch = false;
					}
				}
				// call the handler and stop the event if neccessary
				if((handler.mods.length === 0 && !_mods[16] && !_mods[18] && !_mods[17] && !_mods[91]) || modifiersMatch) {
					if(handler.method(event, handler) === false) {
						if (event.preventDefault) {
							event.preventDefault();
						} else {
							event.returnValue = false;
						}
						if (event.stopPropagation) {
							event.stopPropagation();
						}
						if (event.cancelBubble) {
							event.cancelBubble = true;
						}
					}
				}
			}
		}
	}

	// unset modifier keys on keyup
	function clearModifier(event) {
		var k
			, key = event.keyCode;

		if (key == 93 || key == 224) {
			key = 91;
		}
		if (key in _mods) {
			_mods[key] = false;
			for (k in _MODIFIERS) {
				if (_MODIFIERS[k] == key) {
					assignKey[k] = false;
				}
			}
		}
	}

	function resetModifiers() {
		var k;

		for (k in _mods) {
			_mods[k] = false;
		}
		for (k in _MODIFIERS) {
			assignKey[k] = false;
		}
	}

	function filter(event) {
		var tagName = (event.target || event.srcElement).tagName;
		// ignore keypressed in any elements that support keyboard data input
		return !(tagName == 'INPUT' || tagName == 'SELECT' || tagName == 'TEXTAREA');
	}

	function getScope() {
		return _scope || 'all';
	}

	function setScope(scope) {
		_scope = scope || 'all';
	}

	// delete all handlers for a given scope
	function deleteScope(scope) {
		var i, handlers, key;

		for (key in _handlers) {
			handlers = _handlers[key];
			for (i = 0; i < handlers.length;) {
				if (handlers[i].scope === scope) {
					handlers.splice(i, 1);
				} else {
					i++;
				}
			}
		}
	}

	// F1-F19 keys
	for (k = 1; k < 20; k++) {
		_MODIFIERS['f' + k] = 111 + k;
	}

	// initialize key.<modifier> to false
	for (k in _MODIFIERS) {
		assignKey[k] = false;
	}

	// set the handlers globally on document
	addEvent(document, 'keydown', dispatch);
	addEvent(document, 'keyup', clearModifier);
	// reset modifiers to false whenever the window is (re)focused.
	addEvent(window, 'focus', resetModifiers);

	exports.key = assignKey;
	exports.key.setScope = setScope;
	exports.key.getScope = getScope;
	exports.key.deleteScope = deleteScope;
	exports.key.filter = filter;

});