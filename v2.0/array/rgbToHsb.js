/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/rgbToHsb
 * @see {@link module:array.rgbToHsb|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
        /**
         * Converts an array of [RR, GG, BB] values to [H, S, B] values
         * @name rgbToHsb
         * @function
         * @memberof module:array
         */
        return function( arr ){
            var red = arr[0],
                    green = arr[1],
                    blue = arr[2],
                    hue = 0;
            var max = Math.max(red, green, blue),
                    min = Math.min(red, green, blue);
            var delta = max - min;
            var brightness = max / 255,
                    saturation = (max != 0) ? delta / max : 0;
            if (saturation != 0){
                var rr = (max - red) / delta;
                var gr = (max - green) / delta;
                var br = (max - blue) / delta;
                if (red == max){
                    hue = br - gr;
                } else if (green == max){
                    hue = 2 + rr - br;
                } else{
                    hue = 4 + gr - rr;
                }
                hue /= 6;
                if (hue < 0){
                    hue++;
                }
            }
            return [Math.round(hue * 360), Math.round(saturation * 100), Math.round(brightness * 100)];
        }; 

 });