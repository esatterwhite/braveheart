/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/insert
 * @see {@link module:array.insert|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
        /**
         * Inserts an item into an array at the specified index
         * @param {Array} The array to operate on
         * @param {Object} item The item to insert into the specifiec array
         * @param {Number} [idx=0] the index you want to insert the item at
         * @return {Array} The original array passed in, modified with the additional item
         * @name insert
         * @function
         * @memberof module:array
         **/
        return function( arr, item, idx ){
            idx = (idx > 0) ? idx : 0;

            return arr.splice( idx,0, item);
        };

 });