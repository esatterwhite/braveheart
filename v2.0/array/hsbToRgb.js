/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/hsbToRgb
 * @see {@link module:array.hsbToRgb|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Converts an array of [H, S, B] values to [RR, GG, BB] values
         * @name hsbToRgb
         * @function
         * @memberof module:array
         */
        return function( arr ){
            var br = Math.round(arr[2] / 100 * 255);
            if (arr[1] == 0){
                return [br, br, br];
            } else {
                var hue = arr[0] % 360;
                var f = hue % 60;
                var p = Math.round((arr[2] * (100 - arr[1])) / 10000 * 255);
                var q = Math.round((arr[2] * (6000 - arr[1] * f)) / 600000 * 255);
                var t = Math.round((arr[2] * (6000 - arr[1] * (60 - f))) / 600000 * 255);
                switch (Math.floor(hue / 60)){
                    case 0: return [br, t, p];
                    case 1: return [q, br, p];
                    case 2: return [p, br, t];
                    case 3: return [p, q, br];
                    case 4: return [t, p, br];
                    case 5: return [br, p, q];
                }
            }
            return false;
        }; 

 });