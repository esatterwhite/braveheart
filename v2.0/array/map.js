/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/map
 * @requires array/from
 * @see {@link module:array.map|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/from']
    ,function( require, exports, module, from ) {
      

        /**
         * Creates array whose elements are the result of the supplied function when passed the related element of the supplied arrays
         * @param  {Function} fn the function use to modify the original arrays
         * @param  {Array} array An array to iterrate over, each element will be passed to the supplied function
         * @param  {Array} array2..arrayN<optional> if more than one array its elements will be appened as positional arguments to each function call
         * @return {Array} the resultant array
         * @name map
         * @function
         * @memberof module:array
         * @example braveheart(['array'], function( array ){
    var result
    result = array.map( [1,2,3,4,5,6], function( item ){
        return item * 2;
    });

    console.log( result ) //[2, 4, 6, 8, 10, 12]
})

         * @example braveheart(['array'], function( array ){
    var result;
    result = array.map( [1,2,3,4,5,6] [ 6,5,4,3,2,1 ], function( x, y ){
        return x % 2 === 0 ? ( x/2 ) + y : y;
    });

    console.log( result ) //[ 6, 5, 4, 5, 2, 4]
})
         */
        return function(arr, fn /*, [arr2 ... ]*/){
            var rvalue = []
                ,x, x2, mapper
            ;

            mapper = arguments[arguments.length-1];

            if( arguments.length <= 2 ){
                arr = from( arr );

                // if null is passed as the first argument, just return the array
                if( !fn ){
                    return arr;
                }

                for( x = 0; x < arr.length; x++ ){
                    rvalue.push( fn( arr[ x ], x ) );
                }
                return rvalue;
            } else {
                if( !fn ){
                    fn = Array;
                }
                for( x = 0; x < arguments.length; x++ ){
                    var args = [];
                    for( x2 = 0; x2< arguments.length-1; x2++ ){
                        args.push( arguments[x2][x] );
                    }
                    rvalue.push( mapper.apply( this, args ) );
                }
                return rvalue;
            }
        };

 });