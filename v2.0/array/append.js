/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/append
 * @requires module:array/from
 * @see {@link module:array.append|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/from']
    ,function( require, exports, module, from ) {
      

        /**
         * Unconditionally pushes array to the end of another.
         * @param {Array} source The host array to receive additional items
         * @param {Array} dest The array of items to append to the host
         * @return {Array} array an array with the elements of the two arrays combined
         * @name append
         * @function
         * @memberof module:array 
         * @example braveheart(['array'], function( array ){
    var a = [1,2,3];

    var b = array.append( a, [4,5,6] );

    console.log( a, b )// [1, 2, 3], [1, 2, 3, 4, 5, 6]
})
         */
        return function( source, dest ) {
            var cln = from( source );
            cln.push.apply( cln, dest );
            return cln;
        };

 });