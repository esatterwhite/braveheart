/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/erase
 * @see {@link module:array.erase|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
        /**
         * Erase item from an array
         * @param {Array} arr the array
         * @param {Object} obj the item to erase
         * @return {Array} The resultant array
         * @name erase
         * @function
         * @memberof module:array
         */
        return function( arr, obj ){
            for( var x = 0; x < arr.length; x++ ){
                if( arr[x ] === obj ){
                    arr.splice(x,1);
                }
            }

            return arr;
        };

 });