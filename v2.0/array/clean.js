/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/clean
 * @requires module:array/filter
 * @see {@link module:array.clean|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/filter']
    ,function( require, exports, module, filter ) {
      

        /**
         * Remove all items that are null or undefined
         * @param {Array} array the array to clean
         * @return {Array} An array void of invalid elements
         * @name clean
         * @function
         * @memberof module:array
         */
        return function( arr ) {
            return filter(arr, function( item ) {
                return item != null;
            });
        };

 });