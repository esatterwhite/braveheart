/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/from
 * @see {@link module:array.from|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
        function isEnumerable( item ){
            return (item != null &&  typeof( item.length ) == 'number' && Object.prototype.toString.call(item) != '[object Function]' );
        }   
        /**
         * Creates an array out of the passed in item. If the item is an array, it will just be returned.
         * @param {Object} item the itme to convert to an array
         * @return {Array} The resultant array
         * @name from
         * @function
         * @memberof module:array
         */
        return function from(item) { /**  Named function expression in this submodule to avoid using arguments.callee below */

            if(item == null){
                return [];
            }
            if(arguments.length > 1){
                return from(arguments);
            }
            return (isEnumerable(item) && typeof item != 'string') ? (Object.toString(item) == '[object Array]') ? item : Array.prototype.slice.call(item) : [item];

        };

 });