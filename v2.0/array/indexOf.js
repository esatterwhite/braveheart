/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/indexOf
 * @see {@link module:array.indexOf|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Attempts to identify at which index an item exists in an array
         * @param {Array} array the array to look through
         * @param {Object} item the item to look for
         * @param {Number} from [optional] the index to start from
         * @return {Number} the index number the item was found at, -1 if it is not found
         * @name indexOf
         * @function
         * @memberof module:array
         */
        return function( arr, item, from ){

            var len = arr.length >>> 0
                ,x;

            for( x = ( from < 0 ) ? Math.max(0, len + from) : from || 0; x < len; x++){
                if( arr[x] === item ){
                    return x;
                }
            }
            return -1;
        };
 });