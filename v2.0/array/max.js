/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/max
 * @requires module:array/each
 * @see {@link module:array.max|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/each']
    ,function( require, exports, module, each ) {
        /**
         * @name max
         * @function
         * @memberof module:array
         */
        return function( arr, fn ){
            if (arr.length && !fn) {
                return Math.max.apply(Math, arr);
            } else if (!arr.length) {
                return Infinity;
            } else {
                var result,
                    compare = -Infinity,
                    tmp;
                each(arr, function(val, i, list){
                    tmp = fn(val, i, list);
                    if (tmp > compare) {
                        compare = tmp;
                        result = val;
                    }
                });
                return result;
            }
        };
});