/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/clone
 * @see {@link module:array.clone|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
        /**
         * Creates a clone of the current array
         * @return {Array}
         * @name clone
         * @function
         * @memberof module:array
         */
        return function( arr ) {
            var len, clone;
            len = arr.length;
            clone = new Array(len);
            while(len--) {
                    clone[len] = arr[len];
            }
            return clone;
        };

 });