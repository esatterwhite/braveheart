/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/unique
 * @requires module:array/combine
 * @see {@link module:array.unique|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/combine']
    ,function( require, exports, module, combine ) {
      

        /**
         * Returns an array with no duplicate values
         * @param {Array} array the array to work on
         * @name unique
         * @function
         * @memberof module:array
         */
        return function(arr) {
            return combine([], arr);
        };

 });