/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module module:array/equals
 * @requires json
 * @see {@link module:array.include|Method documentation}
 */
define([
     'require'
    ,'exports'
    ,'module'
    ,'json'
],function( require, exports, module, json ) {


        /**
         * Compares two arrays and determines if their contents are equal
         * @param  {Array} a the left-hand argument
         * @param  {Array} b the right-hand argument
         * @return {bool} equals true if the arrays match
         * @name equals
         * @function
         * @memberof array
         */
        return function arrayEquals(a, b) {

            if (a === b) return true;
            if (a == null || b == null) return false;
            if (a.length != b.length) return false;

            return json.encode(a) == json.encode(b);
        }

 });
