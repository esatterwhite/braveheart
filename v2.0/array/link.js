/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/link
 * @see {@link module:array.link|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * @name link
         * @function
         * @memberof module:array
         */     
       return function(arr, object){
            var result = {};
            for (var i = 0, l = arr.length; i < l; i++){
                for (var key in object){
                    if (object[key](arr[i])){
                        result[key] = arr[i];
                        delete object[key];
                        break;
                    }
                }
            }
            return result;
        };

 });