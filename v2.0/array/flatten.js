/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/flatten
 * @see {@link module:array.flatten|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      
        var typeOf;
        typeOf = function( item ){
        return ({}).toString.call(item).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
        };
        
        /**
         * Returns a flattened (one-dimensional) copy of the passed array
         * @param  {Array} arr the array to flatten
         * @name flatten
         * @function
         * @memberof module:array
         */
        return function flatten ( arr ){ /** Named function expression to avoid arguments.callee below */
            var rvalue = []
                ,x
                ,len
                ,_type;
            for( x = 0, len = arr.length; x < len; x++ ){
                _type = typeOf( arr[x] );
                if( arr[x] == null ){
                    continue;
                }
                rvalue = rvalue.concat( ( _type == "array" || _type =="arguments" || arr[x] instanceof Array ) ? flatten(arr[x]) : arr[x]);
            }
            return rvalue;
        };

 });