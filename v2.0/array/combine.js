/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/combine
 * @requires module:array/from
 * @requires module:array/include
 * @see {@link module:array.combine|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/from', 'array/include']
    ,function( require, exports, module, from, include ) {
      

        /**
         * Combine function
         * @param  {Array} arrayA
         * @param  {Array} arrayB
         * @name combine
         * @function
         * @memberof module:array
    `    * return {Array} A single array with all of the elements from both arrays passed in
         */
        return function( a1, a2, skip ){
            var x,l;
            a2 = from( a2 );
            x  =0;
            l  = a2.length;
            x  = ( typeof skip === 'number' ) ? skip : 0;

            for(x; x< l; x++) {
                include( a1, a2[x]);
            }

            return a1;
        };

 });