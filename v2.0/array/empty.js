/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/empty
 * @see {@link module:array.empty|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Empties out the passed in array. Modifies the original array.
         * @param {Array} arr the array to empty
         * @param {Boolean} force if true elements in the array will manually be deleted. if false, the length property will be set to 0
         * @name empty
         * @function
         * @memberof module:array
         */
        return function(arr, force) {
            var i = arr.length;
            if(force) {
                for(i; i--; ) {
                    delete arr[i];
                }
                return arr;
            }
            arr.length = 0;
            return arr;
        };

 });