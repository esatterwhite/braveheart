/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/contains
 * @requires module:array/indexOf
 * @see {@link module:array.contains|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/indexOf']
    ,function( require, exports, module, indexOf ) {
      

        /**
         * Determines if an item is in the provided array.
         * @param  {Array} arr [optional] the array to inspect
         * @param  {Object} item [optional] the item to look for
         * @param  {Number} from [optional] the index number to start from
         * @return {Boolean} true if the item was found in the array
         * @name contains
         * @function
         * @memberof module:array
         */
        return function(arr, item, from) {
            return indexOf(arr, item, from ) != -1;
        };

 });