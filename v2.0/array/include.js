/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/include
 * @requires module:array/contains
 * @see {@link module:array.include|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/contains']
    ,function( require, exports, module, contains ) {
      

        /**
         * Adds items to an array if it does not already exist in the array
         * @param  {Array} arr the array to ann items to
         * @param  {Object} item the item to include
         * @return {Array} arr the resulting array
         * @name include
         * @function
         * @memberof array
         */
        return  function( arr, item ){
            if( !contains( arr, item ) ){
                arr.push( item );
            }
            return arr;
        };

 });