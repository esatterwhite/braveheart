/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/each
 * @requires module:array/from
 * @see {@link module:array.each|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/from']
    ,function( require, exports, module, from ) {
      

        /**
         * Executes a function for every item in an array
         * @param {Array} array the array to iterate over
         * @param {Function}fn  the function to execute, it will be passed: the current item in the array,
         * the current index the function is working on, and the original array passed in
         * @function
         * @memberof module:array
         * @param {Object} scope the context in which the function should execute
         * @example braveheart(['array'], function( array ){
    array.each( ["a" "b", "c","d"], function( item ){
        console.log( item.charCodeAt(0) )
    }) // 97, 98, 99, 100
})
         */
        return function(arr, fn, scope){
            var ret = from( arr )
                ,x;

            for( x=0; x<ret.length; x++){
                fn.call(scope, ret[x], x, arr);
            }
            return ret;
        };

 });