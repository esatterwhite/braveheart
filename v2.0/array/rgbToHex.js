/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/rgbToHex
 * @see {@link module:array.rgbToHex|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Converts RGB to hex
         * @name rgbToHex
         * @function
         * @memberof module:array
         */     
        return function(arr, toArray){
            if (arr.length < 3){ return null;}
            if (arr.length == 4 && arr[3] == 0 && !arr){ return 'transparent';}
            var hex = [];
            for (var i = 0; i < 3; i++){
                var bit = (arr[i] - 0).toString(16);
                hex.push((bit.length == 1) ? '0' + bit : bit);
            }
            return (toArray) ? hex : '#' + hex.join('');
        }; 

 });