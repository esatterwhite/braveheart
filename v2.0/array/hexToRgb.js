/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/hexToRgb
 * @requires module:array/hexToRgb
 * @see {@link module:array.hexToRgb|Method documentation}
 */
define(
    ['require', 'exports', 'module', 'array/map']
    ,function( require, exports, module, map ) {
      

        /**
         * Converts hex to RGB
         * @name hexToRgb
         * @function
         * @memberof module:array
         */     
        return function(arr, toArray ){
            if (arr.length != 3){ return null;}
            var rgb = map(arr, function(value){
                if (value.length == 1){
                    value += value;
                }
                return parseInt(value, 16);
            });
            return (toArray) ? rgb : 'rgb(' + rgb + ')';
        };

 });