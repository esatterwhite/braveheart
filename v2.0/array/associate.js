/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/associate
 * @see {@link module:array.associate|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * takes two arrays and builds key /value pairs from them.
         * @param {Array} keys array used as the keys for the resultant object
         * @param {Array} values array used as the values
         * @name associate
         * @function
         * @memberof module:array
         * @return {Object} result An object of key/value pairs derived from the passed in arrays
         * @example braveheart(['array'], function( array ){
    var result = array.associate(
                    ["number", "object", "array"],
                    [1, {key:"value"}, [1, 2, 3]]
                );

    console.log( result ) // {number: 1, object:{key:"value"}, array:[1,2,3]}
})
         */
        return function( keys, values ){
            var obj = {}
                ,length = Math.min( values.length, keys.length );

                for( var x = 0; x < length; x++){
                    obj[keys[x]] = values[x];
                }

                return obj;
        };

 });