/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/filter
 * @see {@link module:array.filter|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Filters values out of an array
         * @param {Array} array The array to filter
         * @name filter
         * @function
         * @memberof module:array
         * @param {Function} fn A function used to filter the items in the array. If it returns true, the item will be included. The function will be passed
         * the current item to inspect, the current index in the array, and the original array passed in.
         */
        return function(arr, fn, bind) {
            var results = []
                ,i
                ,l;
            for(i = 0, l = arr.length; i < l; i++) {
                if( ( i in arr) && fn.call(bind, arr[i], i, arr)){
                    results.push(arr[i]);
                }
            }
            return results;
        };

 });