/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/getLast
 * @see {@link module:array.getLast|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Returns the last item in an array
         * @param  {Array} arr the array to retrive an itme from
         * @return  {Object} item the last item of the array if one exists
         * @name getLast
         * @function
         * @memberof module:array
         */
        return function( arr ) {
            return (arr.length ) ? arr[arr.length - 1] : null;
        }; 

 });