/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * @author Eric Satterwhite
 * @module array/getRandom
 * @see {@link module:array.getRandom|Method documentation}
 */
define(
    ['require', 'exports', 'module']
    ,function( require, exports, module ) {
      

        /**
         * Attempts to return a random element out of the given array
         * @parm  {Array} arr Array to extract a random item from
         * @return {Object} an object from the array
         * @name getRandom
         * @function
         * @memberof module:array
         */
        return function( arr ){
            var rand = Math.floor( Math.random() * ( arr.length - 1 )  );
            return ( arr.length ) ? arr[ rand ] : null;
        };


 });