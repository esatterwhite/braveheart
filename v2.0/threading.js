/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * A Crosbrowser Abstraction around Web Workers
 * @module threading
 * @author Eric Satterwhite
 * @requires class
 * @requires assets
 * @requires iter
 **/
define(
	[
		"require"
		,"exports"
		,"module"
		,"core"
		,"functools"
		,"class"
		,"array"
		,"log"
		,"request/Request"
	]
	,function(require, exports, module, core, functools, Class, array, log, Request){
		var   WebWorker
			, noop
			, Socket
			, Primitive
			, getHead
			, imported;

		Primitive = core.Primitive;

		noop     = function(){};
		imported = {};
		getHead  = function(){
			return document.getElementsByTagName("HEAD")[0];
		};



		// The will be what is used as out web worker.
		// this keeps the interface consistent and I
		// don't have to wrap everything in if checks
		Socket = (function(){
			return functools.attempt(
				// Has Workers
				function(){
					if( !window.Worker ){
						throw new Error();
					}

					return window.Worker;
				},
				/**
				 * The low level abstraction around a Web worker
				 * @class module:threading.Proxy
				 * @returns {Proxy}
				 */
				// Fake Workers!
				function(){

					var Proxy = new Primitive("Worker", function( script ){
						var that = this;
						var onmessage;
						var _data;
						this.onmessage = null;
						this.onerror = null;

						this.$imported = {};


						var postMessage = function( data ){
							if ( "function" == typeof that.onmessage )
							{
								return that.onmessage( { "data" : data } ) ;
							}
							return false ;
						} ;

						this.onTick = function(){
							try
							{
								if ( "function" == typeof onmessage )
								{
									onmessage({ "data" : _data });
								}
								return true ;
							}
							catch( ex )
							{
								if ( "function" == typeof that.onerror )
								{
									return that.onerror( ex ) ;
								}
							}
							return false ;
						};


						this.load = function( script ){
							var req; // this might need to be a global ?

							req = new Request({
								url:script
								,method:"get"
								,async:false
								,onSuccess: function( text ){
									// #### Warning
									// There is an eval here.

									// if the script has an onmessage`,` or `postMessage`, this will effectivly
									// over right the `onmessage` & `postMessage` methods defined above
									// giving us delegate functions to the "worker code"
									eval( text );

								}
							});

							req.once("error", function( err ){
								log.error(  err  );
							});

							req.send();
						}.protect();


						this.load( script );
					});

					Proxy.implement(/** @lends module:threading.Proxy.prototype */{
						/**
						 * used to start the internal timer for the fake worker
						 * @private
						 * @method module:threading.Proxy#iter
						 **/
						iter:function(  ){
							this.timer = setTimeout( this.onTick, 1);
							return true;
						}
						/**
						 * Closes the open worker thread
						 * @method module:threading.Proxy#terminate
						 **/
						,terminate:function(){
							this.$active = false;
							clearTimeout( this.$timer );
							return true;
						}
						/**
						 * Starts the worker thread passing any arguments along
						 * @method module:threading.Proxy#postMessage
						 * @param {Object} data data to pass into the worker thread
						 **/
						,postMessage:function( data ){
							this.data = data;
							this.iter();
						}
						/**
						 * Loads script files into the context of the worker
						 * @method module:threading.Proxy#importScripts
						 * @param {String} scripts any number of path strings to scripts to load into the worker context
						 * @return imported true if the file was loaded. files will only be loaded once.
						 **/
						,importScripts:function( scripts ){
							scripts = array.from( arguments );

							array.each( scripts, function( url ){
								var script = document.createElement("SCRIPT")
									,head  = getHead();

								if( imported[ url ] ){
									return false;
								}

								script.src = url;
								script.setAttribute("type", "text/javascript");
								head.appendChild( script );
								imported[  url ] = true;
								head.removeChild( script );

								return true;
							});

							return true;
						}
					});
					return Proxy;
				}
			);
		}());

		/**
		 * Abstraction Class for dealing with web Workers
		 * @class module:threading.Worker
		 * @mixes module:class.Events
		 * @mixes module:class.Options
		 * @param {String} script Url to the script to run in a separate thread
		 * @param {Object} options Class configuration
		 */
		exports.Worker = WebWorker = new Class(/** @lends module:threading.Worker.prototype */{

			Implements:[ Class.Options, Class.Events ]
			,options:{
				onMessage:noop
				,onError: noop
			}
			,initialize: function( script, options ){
				this.$active = true;
				this.setOptions( options );
				this.$worker = new Socket( script );

				this.$worker.onmessage = function(){

					this.fireEvent( "message", arguments );
				}.bind( this );

				this.$worker.onerror = function( e ){

					this.fireEvent( "error", e );
				}.bind( this );

			}
			/**
			* Starts the worker thread and passes any data specified into it
			* @method module:threading.Worker#postMessage
			* @param {Object} data The data to pass to the worker thread
			*/
			,postMessage: function( data ){
				if( this.$active ){

					this.$worker.postMessage( data || null );
				}
			}

			/**
			 * Closes the associated worker thread
			 * @method module:threading.Worker#terminate
			 * @param {TYPE} NAME
			 * @param {TYPE} NAME
			 **/
			,terminate: function(){
				this.$worker.terminate();
				this.$active = false;
			}
		});

		exports.Socket = Socket;
	}
);
