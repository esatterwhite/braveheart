/*jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

/**
 * Module for manipulating string objects
 * @module string
 * @author Eric Satterwhite
 * @requires module:string/camelCase
 * @requires module:string/capitalize
 * @requires module:string/clean
 * @requires module:string/contains
 * @requires module:string/endsWith
 * @requires module:string/escapeRegEx
 * @requires module:string/from
 * @requires module:string/hsbToRgb
 * @requires module:string/hyphenate
 * @requires module:string/id
 * @requires module:string/insert
 * @requires module:string/normalizeLinebreaks
 * @requires module:string/padLeft
 * @requires module:string/padRight
 * @requires module:string/pluralize
 * @requires module:string/pxToInt
 * @requires module:string/randomKey
 * @requires module:string/removeNonWord
 * @requires module:string/replaceAccents
 * @requires module:string/reverse
 * @requires module:string/rgbToHex
 * @requires module:string/rgbToHsb
 * @requires module:string/slugify
 * @requires module:string/smartPlural
 * @requires module:string/split
 * @requires module:string/startsWith
 * @requires module:string/stripNonAlpha
 * @requires module:string/stripScripts
 * @requires module:string/stripTags
 * @requires module:string/substitute
 * @requires module:string/toFloat
 * @requires module:string/toInt
 * @requires module:string/trim
 * @requires module:string/truncate
 * @requires module:string/typeCast
 */

define(
	['require'
	,'exports'
	,'module'
	,'string/camelCase'
	,'string/capitalize'
	,'string/clean'
	,'string/contains'
	,'string/endsWith'
	,'string/escapeRegEx'
	,'string/from'
	,'string/hsbToRgb'
	,'string/hyphenate'
	,'string/id'
	,'string/insert'
	,'string/normalizeLinebreaks'
	,'string/padLeft'
	,'string/padRight'
	,'string/pluralize'
	,'string/pxToInt'
	,'string/randomKey'
	,'string/removeNonWord'
	,'string/replaceAccents'
	,'string/reverse'
	,'string/rgbToHex'
	,'string/rgbToHsb'
	,'string/slugify'
	,'string/smartPlural'
	,'string/split'
	,'string/startsWith'
	,'string/stripNonAlpha'
	,'string/stripScripts'
	,'string/stripTags'
	,'string/substitute'
	,'string/toFloat'
	,'string/toInt'
	,'string/trim'
	,'string/truncate'
	,'string/typeCast']
	,function(
		require
		,exports
		,module
		,camelCase
		,capitalize
		,clean
		,contains
		,endsWith
		,escapeRegEx
		,from
		,hsbToRgb
		,hyphenate
		,id
		,insert
		,normalizeLinebreaks
		,padLeft
		,padRight
		,pluralize
		,pxToInt
		,randomKey
		,removeNonWord
		,replaceAccents
		,reverse
		,rgbToHex
		,rgbToHsb
		,slugify
		,smartPlural
		,split
		,startsWith
		,stripNonAlpha
		,stripScripts
		,stripTags
		,substitute
		,toFloat
		,toInt
		,trim
		,truncate
		,typeCast ) {

			//Export the required submodules so they can be conveniently accessed through the string module
			exports.camelCase = camelCase;
			exports.capitalize = capitalize;
			exports.clean = clean;
			exports.contains = contains;
			exports.endsWith = endsWith;
			exports.escapeRegEx = escapeRegEx;
			exports.from = from;
			exports.hsbToRgb = hsbToRgb;
			exports.hyphenate = hyphenate;
			exports.id = id;
			exports.insert = insert;
			exports.normalizeLinebreaks = normalizeLinebreaks;
			exports.padLeft = padLeft;
			exports.padRight = padRight;
			exports.pluralize = pluralize;
			exports.pxToInt = pxToInt;
			exports.randomKey = randomKey;
			exports.removeNonWord = removeNonWord;
			exports.replaceAccents = replaceAccents;
			exports.reverse = reverse;
			exports.rgbToHex = rgbToHex;
			exports.slugify = slugify;
			exports.smartPlural = smartPlural;
			exports.split = split;
			exports.startsWith = startsWith;
			exports.stripNonAlpha = stripNonAlpha;
			exports.stripScripts = stripScripts;
			exports.stripTags = stripTags;
			exports.substitute = substitute;
			exports.toFloat = toFloat;
			exports.toInt = toInt;
			exports.trim = trim;
			exports.truncate = truncate;
			exports.typeCast = typeCast;
});
