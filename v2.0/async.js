/*jshint laxcomma: true, smarttabs:true */
define(
	['require', 'exports', 'module',"core", "array", 'class', 'request', "functools" ]
	,function( require, exports, module, core, array, Class, request, functools ){

		var Options
			,Events
			,AlreadyCalledError
			,CancelledError
			,GenericError
			,Chain
			,ne
			,callLater
			,wait
			,startRequest;

		Options = Class.Options;
		Events  = Class.Events;
		Chain   = Class.Chain;

		ne = functools.partial( core._newNamedError, this );

		AlreadyCalledError = ne(
			"AlreadyCalledError" ,
			"Deferred Callbacks can only be called once",
			function( deferred ){
				this.deferred = deferred;
			}
		);

		GenericError = ne(
			  "GenericError"
			, ""
			, function( message ){
				this.message = message;
			}
		);

		CancelledError= ne(
			"CancelledError"
			, "Deffered object has already been cancelled"
			, function( deferred ){
				this.deferred = deferred;
			}
		);

		wait = function( seconds, value ){
			var d = new exports.Deferred({
				cancelFn:function(){
					try{
						clearTimeout( timeout );
					} catch( e ){ /* pass */}
				}
			});
			var cb = functools.bind( "callback", d, value);
			var timeout = setTimeout( cb, Math.floor( seconds * 1000 ));

			return d;
		};

		exports.callLater = callLater = function( seconds /*, fn */ ){
			var pfn = functools.partial.apply( array, array.combine( [], arguments, 1 ));

			return wait( seconds ).addCallback(
				function( res ){
					return pfn( res );
				}
			);
		};


		exports.Deferred = new Class({
			Implements:[Options, Events]
			,options:{
				cancelFn: function(){}
			}

			/**
			 * The Constructor
			 * @constuctor
			 **/
			,initialize: function( options ){
				this.setOptions( options );
				this.$chain            = [];
				this.$id               = ( + new Date() ).toString( 36 );
				this.fired             = -1;
				this.paused            = 0;
				this.results           = [null, null]; // [callback, errback]
				this.cancelFn          = this.options.cancelFn;
				this.silentlyCancelled = false;
				this.chained           = false;
				this.finalized         = false;
			}

			/**
			 * Returns a readible representation of this class
			 * @method repr
			 * @protected
			 * @return {String} string representation of this class
			 */
			,repr: function(){
				return "Deferred(" + this.id + ', ' + this.state()  + " )";
			}

			/**
			 * Human readible version of the class
			 * @method toString
			 * @return {String}
			 *
			 **/
			,toString: function(){
				return this.repr();
			}

			/**
			 * Returns a human readible representation of the internal state
			 * @method state
			 * @return {String}
			 *
			 */
			,state: function(){
				if( this.fired === -1 ){
					return "unfired";
				} else if( this.fired === 0 ){
					return 'success';
				} else {
					return 'error';
				}
			}

			/**
			 * Attempts to cancel the deferred chain with the cancelFn
			 * @method cancel
			 * @param e {Error}
			 *
			 */
			,cancel: function( e ){

				if( this.fired === -1 ){
					if( this.cancelFn ){
						this.cancelFn( this );
					} else{
						this.silentlyCancelled = true;
					}

					if( typeof( e ) === "string"){
						e = new GenericError( e );
					} else if( !( e instanceof Error )){
						e = new CancelledError( this );
					}
					this.errback( e );
				} else if( (this.fired === 0)  && ( this.results[0] instanceof exports.Deferred )){
					this.results[0].cancel( e );
				}
			}

			/**
			 * function that starts the callback chain
			 * @method callback
			 * @param
			 *
			 */
			,callback: function( res ){
				this.check();
				if( res instanceof exports.Deferred ){
					throw new Error( "Deferred instances can only be chained if the are the results of a callback");
				}
				return this.resback( res );
			}


			,errback: function( res ){
				this.check();

				if( res instanceof exports.Deferred ){
					throw new Error("Deferred instance can only be chained if they are the result of a callback");
				}
				if(! (res instanceof Error)){
					res = new GenericError( res );
				}
				this.resback( res );
			}

			/**
			 * uses a single function as both the callback and errback
			 * @method addBoth
			 * @param fn {Function} function to be used as both callback and errback
			 * @return {Deferred}
			 */
			,addBoth: function( fn ){
				if( arguments.length > 1 ){
					fn = functools.partial.apply( null, arguments );
				}
				return this.addCallbacks( fn, fn );
			}
			/**
			 * Adds a pair where the error callback is null
			 * @method addcallback
			 * @param callback {Function} function to be used as the success callback
			 * @return the Deferred object instance
			 **/
			,addCallback: function( fn ){
				if( arguments.length > 1 ){
					fn = functools.partial.apply( null, arguments );
				}
				return this.addCallbacks( fn, null );
			}

			/**
			 * Adds a pair of callback functions to the deferred chain where the success callback is null
			 * @method addErrback
			 * @param errback {Function} function to be used as the error callback handler
			 * @return the Deferred object instance
			 **/
			,addErrback: function( fn ){
				if( arguments.length > 1 ){
					fn = functools.partial.apply( null, arguments );
				}
				return this.addCallbacks(null, fn);
			}

			/**
			 * Adds a pair of callback functions to the deferred chain
			 * @method addCallbacks
			 * @param callback {Function} function to be used as the success callback
			 * @param errback {Function} function to be used as the error callback handler
			 * @return the Deferred object instance
			 **/
			,addCallbacks: function( cb, eb ){
				if( this.chained ){
					throw new Error("Chained Deferreds can not be reused");
				}
				if( this.finalized ){
					throw new Error('Finalized Deferreds can not be reused');
				}

				this.$chain.push( [cb,eb]);

				if( this.fired >= 0 ){
					this.fire();
				}
				return this;

			}

			/**
			 * sets the finalizer function
			 * @method setFinalizer
			 * @param fn {Function} the function to be used as the final function envoked when the callback chain is empty
			 *
			 **/
			,setFinalizer: function( fn ){
				if( this.chained ){
					throw new Error("Chained Deferreds can not be reused");
				}
				if( this.finalized ){
					throw new Error('Finalized Deferreds can not be reused');
				}
				if( arguments.length > 1 ){
					fn = functools.partial.apply(null, arguments);
				}
				this.$finalizer = fn;
				if( this.fired >= 0){
					this.fire();
				}
				return this;
			}

			/**
			 * Envoked when the call back is not an error from the callback function
			 * @protected
			 * @method resback
			 * @param res {Function} the function to be used as the call back or errback
			 */
			,resback: functools.protect( function( res ){
				this.fired = ( (res instanceof Error) ? 1 : 0 );

				this.results[this.fired] = res;
				if( this.paused === 0){
					this.fire();
				}
			})

			/**
			 * checks if the operation chain can continus based on the current state
			 * @protected
			 * @method check
			 *
			 */
			,check: functools.protect(function(){
				if( this.fired != -1){
					if( !this.silentlyCancelled ){
						throw new AlreadyCalledError( this );
					}
					this.silentlyCancelled = false;
					return;
				}
			})

			/**
			 * Exausts the callback sequence when a result is available
			 * @method fire
			 * @protected
			 */
			,fire: functools.protect(function(){
				var chain = this.$chain
					,fired = this.fired
					,res = this.results[fired]
					,self = this
					,cb = null
					,pair
					,fn;

				while( chain.length > 0 && this.paused === 0 ){
					pair = chain.shift();
					fn = pair[fired];

					if( fn === null){
						continue;
					}

					try{
						res = fn( res );
						fired = ( res instanceof Error ) ? 1 : 0 ;
						if( res instanceof exports.Deferred ){
							cb = function( res ){
								self.paused--;
								self.resback( res );
							};
							this.paused++;
						}
					}catch( err ){
						fired = 1;
						if( !(err instanceof Error)){
							err = new GenericError( err );
						}
						res = err;
					} // end try/catch
				}//wnd while
				this.fired = fired;
				this.results[fired] = res;
				if( this.$chain.length === 0 && this.paused === 0 && this.$finalizer){
					this.finalized = true;
					this.$finalizer( res );
				}
				if( cb && this.paused ){
					res.addBoth( cb );
					res.chained = true;
				}
			})
		}); // end Deferred



		startRequest = function( url, opts ){
			var options = opts || {};
			options.url = url;

			var rqst = new request.JSON( options );

			var deferred = new exports.Deferred({
				cancelFn: rqst.abort
			});
			// deferred.addCallback( )
			rqst.send();
			return deferred;
		};

		/*
		 * Ajax request that returns a deferred that starts the callback chain at the
		 * @method request
		 * @param url {String} url to point
		 * @param options {Object} options object for the request.JSON class
		 * @return {Deferred} a deffered object
		 */
		exports.request = function( url, opts ){
			return callLater(0, startRequest, url, opts);
		};

	}// end module function

);
