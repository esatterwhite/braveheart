/**
 * @module cookie
 * @author Eric Satterwhite
 * @requires class
 * @requires object
 */

define(['require','exports','module', 'class', "object", "string/escapeRegEx"], function( require, exports, module, Class, object, escapeRegEx){
		var Options = Class.Options
		,Cookie;

	/**
	 * Base Class for dealing with Cookies
	 * @class
	 * @param {String} key
	 * @param {Object} options
	 */
	exports.Cookie = Cookie = new Class(/** @lends module:cookie.Cookie.prototype */{
		Implements: [Options],
		options:{
			path:"/"
			,domain: false
			,duration: false
			,secure: false
			,encode: true
		},
		initialize: function( key, options ){
			this.key = key;
			this.setOptions( options );
			this.options.document = window.document;
		},

		/**
		 * Writes a cookie to current borwser session
		 * @returns {Cookie} Current Cookie Instance
		 */
		write: function(value){
			if (this.options.encode) value = encodeURIComponent(value);
			if (this.options.domain) value += '; domain=' + this.options.domain;
			if (this.options.path) value += '; path=' + this.options.path;
			if (this.options.duration){
				var date = new Date();
				date.setTime(date.getTime() + this.options.duration * 24 * 60 * 60 * 1000);
				value += '; expires=' + date.toGMTString();
			}
			if (this.options.secure) value += '; secure';
			this.options.document.cookie = this.key + '=' + value;
			return this;
		},

		/**
		 * Reads the value for the current cookie instance
		 * @return {String} The stored value
		 */
		read: function(){
			var value = this.options.document.cookie.match('(?:^|;)\\s*' + escapeRegEx(this.key) + '=([^;]*)');
			return (value) ? decodeURIComponent(value[1]) : null;
		},

		/**
		 * Disposes the current cookie associsted to the class instance
		 * @return {Cookie} The current Cookie Instance
		 */
		dispose: function(){
			new Cookie(this.key, object.merge(this.options, {duration:-1})).write('');
			return this;
		}
	});

	/**
	 * writes a new Cookie to the current browser session
	 * @param {String} key
	 * @param {String} value
	 * @param {Options} options Configuration options for the new cookie instance, if any
	 * @returns {Cookie} A new Cookie instance
	 */
	exports.write = function(key, value, options){
		return new Cookie( key, options).write( value );
	};

	/**
	 * reads a cookie from the browser
	 * @param {String} key The name of the cookie to read
	 * @returns {Cookie} a new cookie instance
	 */
	exports.read = function(key){
		return new Cookie( key ).read();
	};

	/**
	 * Disposes a cookie from the browser session
	 * @param {String} key The name of the cookie to dispose
	 * @param {Object} object Config options for the Cookie instance, if any
	 * @returns {Cookie} A new Cookie instance
	 */
	exports.dispose = function(key, options){
		return new Cookie( key, options).dispose();
	};

});
