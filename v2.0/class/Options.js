/*jshint laxcomma:true, smarttabs: true */
// ## Options
// As the name would imply, the `Options` Class provides a simple way to
// manage configurable options for a class instance
/**
 * Mixin Class the provides configurable options for class instances
 * @module module:class.Options
 * @author Eric Satterwhite
 * @requires modules:class.Class
 * @requires core
 * @requires array
 * @requires object
 * @version 1.1
 **/
define(function(require, exports){
    var  Class = require( './Class' )
    	,core  = require("core")
        ,arrappend = require("array/append")
    	,object = require("object")
    	,Options

    /**
     * Mixin class that allows for default and overrideable class configurations
     * @class module:class.Options
     * @author Eric Satterwhite
     */
    Options = new Class( /** @lends module:class.Options.prototype */{

        /**
         * merge incomming options into existing class options over riding the defaults
         * @pararm {Object} options key value pairs to use as the class options / configs
         * @return {Class} the current class object
         */
        setOptions: function( options ){
            var opt;
            // This class works on the instance property called `options`
            // if the parent class does not have a property called `options`
            // one will be created. If one does exist it will be merged with any object
            // passed in
            this.options = object.merge.apply( null, arrappend( [{}, this.options], arguments ) );
            options = this.options;

            if( !!this.addEvent ){
                // the `Options` Mixin works side by side with the `Events` Mixin.
                // If events are found, any `functions` that looke like `onFoo` will be
                // treated as the default event handler for the event `foo`
                for( opt in options ){
                    if( core.typeOf( options[ opt ] ) !== 'function' || !(/^on[A-z]/).test(opt)){
                        continue;
                    }
                    this.addEvent( opt, options[ opt ]);
                    delete options[opt];
                }
            }
            return this;
        }
    });

    return Options
});
