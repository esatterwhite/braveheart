/*jshint laxcomma:true, smarttabs: true */

/**
 * provides the functioncalls to be queued for later execution
 * @module module:class.Chain
 * @author Eric Satterwhite
 * @requires module:class.Class
 * @requires array
 * @version 1.1
 **/
define(function(require, exports){
    var  Class       = require( './Class' )
      , arrayappend  = require( "array/append" )
      , arrayflatten = require( "array/flatten" )
      , arrayempty   = require( "array/empty" )
      , Chain;

   /**
    * allows for method calls to be queued up for later execution
    * @class module:class.Chain
    * @author Eric Satterwhite
    */
   exports.Chain = Chain = new Class(/** @lends module:class.Chain.prototype */{
       $chain:[],

       /**
        * appends a number of function to the chain
        * @return {Class} the class instance
        */
       chain: function(){
           this.$chain = arrayappend(this.$chain, arrayflatten( arguments ) );
           return this;
       },

       /**
        * removes the first function off the chain and executes it
        * @return {Mixed} the result of the next function
        */
       callChain: function(){
           return !!this.$chain.length ? this.$chain.shift().apply(this, arguments ) : false;
       },

       /**
        * empties out the current chain
        * @return {Class} returns the class instance
        */
       clearChain: function(){
           arrayempty( this.$chain, true);
           return this;
       }
   });

   return Chain;
});
