/*jshint laxcomma:true, smarttabs: true */

/**
 * Provides a Mixin class for deling with evens in a standard way
 * @module module:class.Events
 * @author Eric Satterwhite
 * @requires module:class.Class
 * @requires signals
 * @requires array
 * @version 1.1
 **/
define(function(require, exports){
    var  Class = require( './Class' )
        ,signals = require("signals")
        ,arrayfrom = require( 'array/from')
    	,functools = require( 'functools')
        ,Signal = signals.Signal
        ,Events
        ,removeOn

    removeOn = function( str ){
        return str.replace(/^on([A-Z])/, function( full, first ){
            return first.toLowerCase();
        });
    };

   /**
     * Mixin Class which add event functionality
     * @author Eric Satterwhite
     * @class module:class.Events
     */
    exports.Events = Events = new Class( /** @lends module:class.Events.prototype */ {

        $events: {},
        /**
         * adds an event handler to the class instance
         * @chainable
         * @param  {String} name the name of the event listener
         * @param  {Function} fn the event handler to attach
         * @param  {Boolean} internal if set to true, the event handler cannot be removed
         */
        addEvent:function( name, fn, internal ){
            var type;

            type = removeOn( name, fn, internal );

            if( !this.$events.hasOwnProperty( type ) ){
                this.$events[type] = new Signal();
            }
            this.$events[type].add( fn );
            fn.internal = !!internal;
            return this;
        },

        /**
         * removes specified event from a class instance
         * @chainable
         * @param  {String} name the name of the method to remove
         * @param  {Function} fn the function to remove. Must be the same function initially used ( non-anonymous )
         * @return {Class} the class instance
         */
        removeEvent: function( name, fn ){
            var type = removeOn( name )
                ,sig;

            sig = this.$events[name];

            if( sig && fn && !fn.internal){
                sig.remove( fn );
            }
            return this;
        },

        /**
         * Fires a named event
         * @chainable
         * @param {String} name the name of the event to fire
         * @param {Array} args the array of arguments to pass to the handler. Single arguments need not be an array
         * @param {Object} bind the object whose context the function should be executed in
         */
        fireEvent: function( name, args, bind, delay){
            if(this.$suspended){
                return;
            }

            var type = removeOn( name )
                ,sig = this.$events[type]
                ,args;

            if( !sig ){
                return this;
            }

            args = arrayfrom( args );
            sig.dispatch.apply( ( bind || sig ) , args );
            return this;
        },

        /**
         * Short to addevent for adding Events in bulk as key value pairs where the key is the name of the event name and the value is the event handler<br />
         * @example
         *  MyClassInstance.addEvents({
         *      click: function(){},
         *      cutomeevt:function(){}
         *  });
         * @chainable
         * @param  {Object} events object with name / handler pairs
         * @return {Class} The class Instance
         */
        addEvents: function( events ){
            for( var type in events ){
                this.addEvent(type, events[type]);
            }
            return this;
        },

        /**
         * Shortcut to remove event allowing key / value pairs to be passed  to perform a bulk operatino
         * @chainable
         * @param  {Object} events object containing event names and handler pairs
         * @return {Class} The class instance
         */
        removeEvents: function( events ){
            var type, fns, sig;

            if( typeof events == 'object' ){
                for( type in events ){
                    this.removeEvent( type, events[type] );
                }
                return this;
            }

            if( !events ){
                for( type in this.$events ){
                    try{

                        this.$events[ type ].removeAll();
                    } catch( e ){}
                }


            }

            return this;
        },

        /**
         * Adds an event handler that will be removed after its first executeion. eg. It will only be executed once
         * @chainable
         * @param {String} name Name of the event the bind a handler to
         * @param {Function} fn The function to use as the event handler
         * @param {Boolean} internal If set to true, the event handler can not be removed
         * @return {Class} The current class instance
         **/
        once: function(name, fn, internal){
            var type;

            type = removeOn( name, fn, internal );

            if( !this.$events.hasOwnProperty( type ) ){
                this.$events[type] = new Signal();
            }
            this.$events[type].addOnce( fn );
            fn.internal = !!internal;
            return this;
        },

        /**
         * Prevents any events from being fired
         * @chainable
         * @return {Class} the current class instance
         **/
        suspendEvents: function(){
            this.$suspended = true;
            return this
        },

        /**
         * allows events to be fired if previosly suspended
         * @chainable
         * @return {Class} the class instance
         **/
        resumeEvents: function(){
            this.$suspended = false;
        },

        on: function( ){
            this.addEvent.apply( this, arguments );
        },

        un: function( ){
            this.removeEvent.apply( this, arguments );
        }
    });

    Events.extend("observe", function( obj, fn, bind){
        if( typeof obj.prototype.fireEvent !== "function"){
            return;
        }
        obj.prototype.fireEvent = functools.intercept(obj.prototype.fireEvent, fn, bind)

    });


    return Events;
});
