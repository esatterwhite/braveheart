/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Provides the standard wrapper for Class INterfaces
 * @module module:class.Interface
 * @author Eric Satterwhite
 * @requires class
 * @version 1.1
 **/
define(function(require, exports){
    var  Class = require( './Class' )
        ,object = require( "object" )
        ,Interface;

    /**
     * Allows for proper class interfaces with type checking on class memvers
     * @class module:class.Interface
     * @requires module:class.Options
     * @author Eric Satterwhite
     *
     */
    exports.Interface = Interface = new Class(/** @lends module:class.Interface.prototype */{

        initialize: function( name, options ){
            this.$name = name;

            this.toObject = function( ){
                return options || {};
            };
        }

        ,error: function( type ){
            return " error " + type;
        }


        ,toObject: function( ){
            return this.$contract;
        }

        ,toString: function( ){
            return this.$name;
        }

        ,repr: function( ){
            return "<Interface: " + this.$name + " />";
        }


    });

    return Interface
});
