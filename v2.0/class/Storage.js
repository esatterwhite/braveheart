/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
// ## Storage

// The Storage class is a way to store arbitrary data in an instance specific memory space.
// Originally intended for data hiding where stahing private data on the class instance may not be
// ideal. Although intended as a `mixin` for other classes, `Storage` is a standalone class that has no
// other dependancies
/**
 * Provides the functionality of storing and retrieving arbitrary bits of data specific to a class instance
 * @module module:class.Storage
 * @author Eric Satterwhite
 * @requires module:class.Class
 * @version 1.1
 **/
define(function(require, exports){
    var  Class = require( './Class' )
        ,Storage
	    ,KEY                // Storage Key prefix
	    ,storage            // Memory cache for Storage mixin
	    ,storageFor         // method to find the right storage object for a class instance
        ,removeStorageFor   // method to delete storage for an object from memory
	    ,uid = 0;           // an id counter


	KEY = "__store:" +  (+ new Date()).toString(36);
	storage = {};



    storageFor = function( obj, key ){
        var uid = obj[KEY] || ( obj[KEY] = ( (+ new Date()).toString(36) ) );
        return storage[uid] || ( storage[uid] = {} );
    };

    removeStorageFor = function( obj ){
        var uid = obj[KEY];

        delete storage[uid];
    }

        /**
         * Allows Class instance to store arbitrary peices of data in memory
         * @class module:class.Storage
         * @author Eric Satterwhite
         * @example braveheart(['class', 'log' ], function( Class, log ){
        ,Storage = Class.Storage
        ,MyClass;

    MyClass = new Class({
        Implements:[ Storage ]
        ,initialize: function( value ){
            this.value = value
        }
        ,getValue: function(){
            return this.value;
        }
    });


    var instance = new MyClass( 2 );

    log.info( instance.getValue() ); // 2
    log.info( instance.value ); // 2

    instance.store("hidden", {data: "words"});

    log.info( instance.hidden) // undefined
    log.info( instance.retrieve('hidden') ) // {data: "words"}

    instance.eliminate('hidden');
    log.info( instance.retrieve('hidden') ) // undefined

})
         */
         // ### The Storage Class
         // The storage class provides 4 basic methods, `store`, `retrieve`, `eliminate`, and `purge`.
         // This allows you to associate arbitrary data with a class instance that is
         // hidden from the public, and does not leak memory
        Storage = new Class( /** @lends module:class.Storage.prototype */ {
            /**
             * Store and arbitray item in memory storage
             * @param {String} key The name to store the data under
             * @param {Object} item The item to store
             * @returns {Class} The Class instance
             */
            store: function store( key, value ){
                // generates a new id for each class instance so keys don't clobber eachother
                if( !this.$id ){
                    this.$id = (+ new Date() ).toString(16) + ( ++uid );
                }

                var _key = key + ":" + this.$id;
                storageFor(this)[ _key ] = value;
                return this;
            },

            /**
             * Retreives a specific value out of memory
             * @pararm {String} key The name storage key to retreive
             * @param {Object} default The default value to return if a a null / undefined value is encounted. If no default value is specified, the value retreive will still be returned.
             * @returns {Object} The stored object
             */
            retrieve: function retrieve( key, defaultVal ){
                var store = storageFor( this );
                var key = key +":" + this.$id;
                return ( store[key] != null ) ? store[key] : ( defaultVal || store[key] );
            },

            /**
             * Clears the data out of the specified storage space
             * @param {String} key the name of the slot to delete
             * @returns {Class} The class instance
             */
            eliminate: function eliminate( key ){
                delete storageFor( this )[ key +":" + this.$id ];
                return this;
            },

            /**
             * removes all stored items related to the current class
             * @return {Class} current class instance
             **/
             // attempts to clear all data in memory associated with the calling instance
            purge: function(){
                removeStorageFor( this );
                return this;
            }
        });
    return Storage;
});
