/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Provides a Mixin class for deling with evens in a standard way
 * @module module:class.Plus
 * @author Eric Satterwhite
 * @requires module:class.Class
 * @requires crossroads
 * @requires array
 * @version 0.9
 **/
define([

		"require"
		,"module"
		,"exports"
		,"array/from"
		,"crossroads"
		,"class/Class"
	]
	,function(require, module, exports, arrayfrom, crossroads, Class){
		var  removeOn // Function to normailize event names
			,Plus;    // The Events Plus Class

		removeOn = function( str ){
			return str.replace(/^on([A-Z])/, function( full, first ){
				return first.toLowerCase();
			});
		};
 
		/**
		 * Similar to the Events mixin with advances pattern matching
		 * @class module:class.Plus
		 */
		Plus = new Class(/** @lends module:class.Plus.prototype */{
			$events:{}
			,$hub:null
			/**
			 * adds an event handler to the class instance
			 * @chainable
			 * @method module:class.Plus#addEvent
			 * @param  {String} pattern the pattern to listen for
			 * @param  {Function} fn the event handler to attach
			 * @param  {Object} rules Rules object to clean and validate event parameters
			 */
			,addEvent: function(name, fn, rules){
				var type = removeOn( name );


				if( this.$hub == null ){
					this.$hub                = crossroads.create();
					this.$hub.greedy		 = true;
					this.$hub.shouldTypecast = true;
					this.$hub.ignoreState    = true;
				}


				if(!this.$events.hasOwnProperty( type )){
					this.$events[ type ] = this.$hub.addRoute( type );
					this.$events[ type ].rules = rules || null;
				}

				this.$events[ type ].matched.add( fn );
				return this;
			}

			/**
			 * removes specified event from a class instance
			 * @chainable
			 * @method module:class.Plus#removeEvent
			 * @param  {String} name the name of the method to remove
			 * @param  {Function} fn the function to remove. Must be the same function initially used ( non-anonymous )
			 * @return {Class} the class instance
			 */
			,removeEvent: function(name, fn ){
				var sig;

				sig = this.$events[ name ].matched;

				if( sig && fn ){
					sig.remove( fn );
				}

				return this;
			}

			/**
			 * Fires a named event
			 * @chainable
			 * @method module:class.Plus#fireEvent
			 * @param {String} name the name of the event to fire
			 * @param {Array} [args=null] the array of arguments to pass to the handler. Single arguments need not be an array
			 * @param {Number} [delay=0] the amount of time in millisecond to wait before dispatching the event
			 */
			,fireEvent:function( name, args, delay){
				if( !this.$hub || this.$suspended ){ return; }

				var type;  // The normailized event name

				type = removeOn( name );

				args = arrayfrom( args );

				this.$hub.parse( name, args );
				return this;
			}

			/**
			 * Short to addevent for adding Events in bulk as key value pairs where the key is the name of the event name and the value is the event handler<br />
			 * @method module:class.Plus#addEvents
			 * @example
			 *  MyClassInstance.addEvents({
			 *      "foo_{bar*}": function(){},
			 *      "before:action:":function(){}
			 *  });
			 * @chainable
			 * @param  {Object} events object with name / handler pairs
			 * @return {Class} The class Instance
			 */
			,addEvents: function( events ){
				for( var type in events ){
					this.addEvent( type, events[type] );
				}
				return this;
			}

			/**
			 * Shortcut to remove event allowing key / value pairs to be passed  to perform a bulk operatino
			 * @chainable
			 * @method module:class.Plus#removeEvents
			 * @param  {Object} events object containing event names and handler pairs
			 * @return {Class} The class instance
			 */
			,removeEvents: function( events ){
				var type;
				if(!events){
					this.$hub.removeAllRoutes();
					for( type in this.$events ){
						delete this.$events[type];
					}
				}

				if( typeof events == "object"){
					for( type in events ){
						this.$events[ type ].dispose();
					}
				}
				return this;
			}

			/**
			 * Adds an event handler that will be removed after its first executeion. eg. It will only be executed once
			 * @chainable
			 * @method module:class.Plus#once
			 * @param {String} name Name of the event the bind a handler to
			 * @param {Function} fn The function to use as the event handler
			 * @param  {Object} rules Rules object to clean and validate event parameters
			 * @return {Class} The current class instance
			 **/
			,once: function(name, fn, rules){
				var type = removeOn( name );


				if( this.hub == null ){
					this.$hub                = crossroads.create();
					this.$hub.shouldTypecast = true;
					this.$hub.ignoreState    = true;
				}


				if(!this.$events.hasOwnProperty( type )){

					this.$events[ type ] = this.$hub.addRoute( name );
				}

				this.$events[ type ].matched.addOnce( fn );
				this.$events[ type ].rules = rules || null;

				return this;
			}

			/**
			 * Prevents any events from being fired
			 * @chainable
			 * @method module:class.Plus#suspendEvents
			 * @return {Class} the current class instance
			 **/
			,suspendEvents: function(){
				this.$suspended = true;
				return this;
			}

			/**
			 * allows events to be fired if previosly suspended
			 * @chainable
			 * @method module:class.Plus#resumeEvents
			 * @return {Class} the class instance
			 **/
			,resumeEvents:function(){
				this.$suspended = false;
				return this;
			}

			/**
			 * Alias for addEvent
			 * #method module:class.Plus#on
			 * @return {Class} Class instance
			 **/
			,on: function(){
				return this.addEvent.apply( this, arguments );
			}

			/**
			 * Alias for remove event
			 * @method module:class.Plus#un
			 * @return {Class} clas
			 **/
			,un: function(){
				return this.removeEvent.apply( this, arguments );
			}

		});

		return Plus;
	}
);
