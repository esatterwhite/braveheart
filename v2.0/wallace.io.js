/*jshint laxcomma:true, smarttabs: true, laxbreak:true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

/**
 * Client Library for io
 * @module io
 * @authors Andy Ortlieb, Eric Satterwhite
 * @requires class
 * @requires iter
 * @requires log
 * @requires cookie
 * @requires dom
 * @requires array
 * @requires object
 * @requires date
 * @requires functools
 * @requires crypto/uuid
 **/
define([
		  "require"
		, "exports"
		, "core"
		, "class"
		, "iter"
		, "log"
		, 'cookie'
		, "dom"
		, "array"
		, "object"
		, "date"
		, "functools"
		, "crypto/uuid"
	], function(require, exports, core, Class, iter, log, cookie, dom, array, object, date, functools, uuid ){
		var arrappend = array.append
			, arreach = array.each
			, arrinclude = array.include
			, arrayclone = array.clone
			, arraycontains = array.contains;
		// Private look up for servers.
		// We need to manage exactly one connection per server address.
		var wallaceServers = {}
			,Client
			,Server
			,WallaceError
			,COOKIE_NAME
			,parseOrigin;

		COOKIE_NAME = "wallace_session_id";

		WallaceError = function( name, msg ){
			this.message = msg;
			this.stack = new Error().stack;
			this.name = name || "WallaceError";
		};

		WallaceError.prototype = new Error();

		// A class for Applications
		// This is the main class that programmers interact with, but they
		// can only get to it through exports.getApplication(<SERVERADDR>,<APPNAME>)

		/**
		 * A class for Applications
		 * This is the main class that programmers interact with, but they
		 * can only get to it through exports.getApplication(<SERVERADDR>,<APPNAME>)
		 * @class module:io.Client
		 */
		Client = new Class(/** @lends module:io.Client.prototype */{
			 Extends: Class.Events.Plus

			,$authorized:{}
			,$pending:{}
			,options:{

			}
			,initialize: function(name, server, options ){
				var auth_required = false;

				// some internal variables
				this.$server   = server;
				this.$name     = name;
				this.$instance = ( +new Date() ).toString(36);
				this.$created  = ( + new Date() );
				this.$origin   = uuid.uuid()

				this.isAuthRequired = function( ){
					// log.log( "auth required? ", auth_required)
					return auth_required;
				};

				// Allow The first Event to bypass checks
				this.once('setauth', function( auth ){
					auth_required = !!auth;
					var data = {};
					if( this.options.hasOwnProperty('postback') ){
						data = this.options.postback.connect || data;
					}
					data.application = this.$name;
					data.origin = [this.$origin, this.$server.$origin].join(":")
					this.$server.emit('client-connect',  data );
					if(!auth_required){
						this.flushEvents();
					}
				}.bind( this ));

				auth_required = true; // All Events must be validated unless notified other wise

				if( this.$server != null ){
					this.$server.listen( this );
				}

				this.setOptions( options );
			}

			/**
			 * Relays signals from the server
			 * @method module:io.Client#signal
			 * @private
			 * @param {String} evt The name of the event being fired
			 * @param {Object} data The data packet to send
			 * @param {Object} meta The meta object set from the Wallace server
			 **/
			, signal: function( evt, data, meta ){
				var is_replay = !!meta.replay
				if( is_replay ){
					origins = ( meta.origin || "" ).split(":");
					client_origin = origins[0];
					server_origin = origins[1];
					log.debug( client_origin)
					if( client_origin !== this.$origin){ log.warn("ignoring replay event: client ids do not match"); return; }

					log.debug("replaying " + evt )
				}

				this.fireEvent("event", [evt, data, meta]);
				this.fireEvent( evt, [data, meta ,evt] );
			}

			/**
			 * Does the work of spreading broad cast events out to worker clients
			 * @private
			 * @method module:io.Client#relay
			 * @param {String} event The name of the event being relayed
			 * @param {Object} data The Data packet to send to clients
			 * @param {Object} meta The meta data from the server event
			 **/
			,relay: function( evt, data, meta ){
				this.fireEvent("broadcast", [evt, data, meta] );
				this.fireEvent( evt, [data, meta ,evt] );
			}

			/**
			 * Special implementation of addEvent that extracts auth credentials and authoriazes event bindings
			 * @chainable
			 * @method module:io.Client#addEvent
			 * @param {String} name The name of the event to bind the handler to
			 * @param {Function|Object} fnOrObject The handler, or an object with a handler and auth credentials to bind to the event
			 * @param {Function} fnOrObject.fn The event handler function
			 * @param {Object} fnOrObject.auth Authorization credantials to pass the the associated app server
			 * @param {Boolean} internal Internal event handlers can not be removed
			 **/
			,addEvent: function( name, fn_or_obj, internal, force ){
				var fn
					,creds;
				// extract the bits
				if( core.typeOf( fn_or_obj ) === "object"){
					fn = fn_or_obj.fn || function(){};
					creds = fn_or_obj.auth || null;
				} else{
					fn = fn_or_obj;
					creds = null;
				}

				// do the work
				if( this.check( name, force ) ){
					// log.log("Adding Events!", arguments );
					this.$authorized[ name ] = true;
					return this.parent( name, fn, internal );
				} else{
					this.stash( name, fn, creds );
				}

				return this;
			}

			/**
			 * Special implementation of setOption that will take an object of {fn:Function, auth:Object }
			 * @method module:io.Client#setOptions
			 * @pararm {Object} options key value pairs to use as the class options / configs
			 * @return {Class} the current class object
			 */
			,setOptions: function( options ){
				var opt;

				this.options = object.merge.apply( null, arrappend( [{}, this.options], arguments ) );
				options = this.options;


				for( opt in options ){
					var fn
						,current_opt;

					current_opt = options[ opt ];

					if( core.typeOf( current_opt ) === "object" ){
						fn = current_opt.fn;
					} else{
						fn = current_opt;
					}


					if( core.typeOf( fn ) !== 'function' || !(/^on[A-z]/).test(opt)){
						continue;
					}

					this.addEvent( opt, options[ opt ]);

					delete options[opt];
				}

				return this;
			}
			/**
			 * Pushes function calls on a stach to be applied at a later time
			 * @method module:io.Client#stash
			 * @param {String} name The name of event the handler will be bound to later
			 * @param {Function} fn The handler function that will be bound to the evet
			 * @param {Object} credentials The auth credentials that will be used to authorize the event binding
			 **/
			, stash: function( name, fn, creds ){
				this.$pending[ ++this.$created ] = functools.partial( this.addEvent.bind(this), name, fn , false, true );
				try{
					// log.warn("Stashing Events " + name, this.$name, this.$created );
					this.$server.emit("subscribe", this.$name, this.$created, creds );
				}catch(e){
					log.error("unable to subscribe! unknown server host");
				}
			}.protect()

			/**
			 * Checks to see if autorization for events is required
			 * @private
			 * @method module:io.Client#check
			 * @param {String} NAME ...
			 * @return {Boolean} True if the calling function can proceed
			 **/
			,check: function( name, force ){

				if( !!force || ( !this.isAuthRequired() ) ){
					return true;
				}

				return !!this.$authorized[ name ];
			}.protect()

			/**
			 * Shortcut for {@link addEvent}. Accepts an object of name/handler pairs
			 * @method module:io.Client#addEvents
			 * @param {Ojbect} events Takes an object of key / handler pairs
			 **/
			,addEvents: function( events, force ){
				for(var key in events ){
					this.addEvent( key, events[ key ], false, !!force);
				}
			}

			/**
			 * Alias for {@link addEvent}
			 * @method module:io.Client#on
			 * @param {String} name The name of the event to bind the handler to
			 * @param {Function|Object} fnOrObject The handler, or an object with a handler and auth credentials to bind to the event
			 * @param {Function} fnOrObject.fn The event handler function
			 * @param {Object} fnOrObject.auth Authorization credantials to pass the the associated app server
			 * @param {Boolean} internal Internal event handlers can not be removed
			 **/
			,on: function( name, fn, internal, force  ){

				if( this.check(name, fn) ){
					return this.parent( name, fn, internal, force );
				}

				return false;
			}

			/**
			 * Same as addEvent but the handler will be removed as soon as it has been called
			 * @method module:io.Client#once
			 * @param {String} name The name of the event to bind the handler to
			 * @param {Function|Object} fnOrObject The handler, or an object with a handler and auth credentials to bind to the event
			 * @param {Function} fnOrObject.fn The event handler function
			 * @param {Object} fnOrObject.auth Authorization credantials to pass the the associated app server
			 * @return {Boolean} False if the event handler didn't get attached
			 **/
			,once: function( name, fn, internal ){
				if( this.check( name, fn ) ){
					return this.parent( name, fn, internal );
				}

				return false;
			}

			/**
			 * Removes all pending event handlers
			 * @method module:io.Client#flushEvents
			 * @param {TYPE} NAME
			 * @param {TYPE} NAME
			 **/
			,flushEvents: function( ){
				for( var key in this.$pending ){
					this.$pending[ key ].call( this );
					delete this.$pending[key];
				}

			}

			/**
			 * Pulls a stash function call out of the stach and calls it
			 * @chainable
			 * @protected
			 * @method module:io.Client#bindStashed
			 * @param {Number} id the id of the function to call
			 * @return {Client} The current client class instance
			 **/
			,bindStashed: function( id ){
				var fn = this.$pending[ id ];

				if( fn ){
					fn.call( this );
				}
				delete this.$pending[ id ];
				return this;
			}

			/**
			 * Deletes a specific function from the stack
			 * @chainable
			 * @protected
			 * @method module:io.Client#purgeStashed
			 * @param {Number} id The id of the function to delete
			 * @return {Client} The current Client instance
			 **/
			, purgeStashed: function( id ){
				log.warn("purging stashed event handler", id );
				delete this.$pending[ id ];
				return this;
			}

			/**
			 * Disconnects the current client from its associated server instance
			 * @method module:io.Client#disconnect
			 **/
			,disconnect: function( ){
				this.$server.remove( this.$name );
				this.$server = null;
				this.removeEvents();
			}

			/**
			 * Allows clients of the same session to pass data to eachother
			 * @method module:io.Client#broadcast
			 * @param {TYPE} event The name of the event being sent
			 * @param {TYPE} data Any arbitrarty data you wish to send
			 * @return {Client} THe client instance
			 **/
			,broadcast: function( name, data ){
				var now = new Date();
				var packet = {
					"meta":{
						"type":"broadcast"
						,"event":name
						,"id":uuid.uuid()
						,"timestamp":date.format( now, "%a, %d %b %Y %H:%M:%S %Z" )
						,"time": + ( now/1E3|0 )
						,"application": this.$name
						,"client":this.$instance
                        ,"session":this.$server.sessionid
						,'user-agent': window.navigator.userAgent
					}
					,"data":data
				};

				this.$server.emit('broadcast', packet);
				return this;
			}

			, connected: function(){
				return !!this.$server;
			}
			, send: function( action, data ){
				var now = new Date();
				var packet = {
					"meta":{
						"type":"message"
						,"event":action
						,"id":uuid.uuid()
						,"timestamp":date.format( now, "%a, %d %b %Y %H:%M:%S %Z" )
						,"time":+ ( now/1E3|0 )
						,"application": this.$name
						,"client":this.$instance
                        ,"session":this.$server.sessionid
						,'user-agent': window.navigator.userAgent
					}
					,"data":data
				};

				this.$server.send(action, packet);
			}
		});


		/**
		 * An abstraction around a socket.io
		 * @class module:io.Server
		 * @param {Object} options configuration for the Server instance
		 */
		Server = new Class(/** @lends module:io.Server.prototype */{

			 Implements:[ Class.Options, Class.Events, Class.Storage ]
			,options:{
				addr:null
				,clients:{

				}
			}
			,queued:[]
			,initialize: function( options ){
				var that = this;
				this.$applications = {};
				this.sessionid     = null;
				this.$channels     = [];
				this.$origin       = uuid.uuid();
				this.setOptions( options );

				dom.Window.addEvent('beforeunload', function( ){
					that.emit('sigterm', that.$origin);
				}.bind( this ) );

				// this first time a session is created we
				// need to create the default clients
				this.once("session", functools.bind(function( id ){
					id = id || cookie.read(COOKIE_NAME );
					that.emit('join', this.$origin, true)
					for( var key in this.options.clients){
						var client =  new Client( key, this, this.options.clients[ key ] );

						if(!this.$applications[ key ] ){
							this.$applications[ key ] = {};
						}

						this.$applications[key][ client.$created] = client;
						this.listen( client );

						// this.emit('postback-connection-complete', id )

					}

					// subsequent ( re-associations ) should just re-bind
					// to the already established client channels
					this.on('session', functools.bind(function( id ){
						arreach( this.$channels, function( chan){
							that.emit("join", chan, id );
						},this);

					}, this ));
				},this));

				// Privleged methods which have acces to these private
				// variables

				this.connect();
				// window[ "server" + (+new Date() ).toString(36)] = this;
			}

			/**
			 * DESCRIPTION
			 * @method module:io.Client#getApplication
			 * @param {String} application The name of the application to retrieve from memory
			 * @return {}
			 **/
			,getApplication: function(app){
				if ( !this.$applications[ app ] ){
					this.$applications[ app ] = {};
				}
				return this.$applications[ app ];
			}

			/**
			 * removes a registered application from the server
			 * @protected
			 * @method module:io.Client#purgeApplication
			 * @param {String} application ...
			 **/
			,purgeApplication: function( app ){

				object.each( this.$applications[ app ], function( client ){
					client.removeEvents();
				});

				delete this.$applications[ app ];
				delete this[ app ];
				return this;
			}.protect()

			/**
			 * Checks for the existance of a client application by name
			 * @depricated
			 * @method module:io.Client#hasApplication
			 * @param {String} name The name of the application to look up
			 **/
			,hasApplication: function( app ){
				return (app in this.$applications);
			}

			/**
			 * creates teh websocket and handels events accordingly
			 * @protected
			 * @method module:io.Server#connect
			 **/
			,connect: function(){
				if (this.io){
					this.fireEvent("session", this.sessionid );
					return this;
				}
				if( this.options.addr == null ){
					return false;
				}

				require([''+this.options.addr+'/wallace/io.js'], this.primer.bind( this ));

				return this;

			}

			,disconnect: function(){
				this.emit("disconnect-client");
				cookie.dispose( COOKIE_NAME );
				if( this.io ){
					// window.io = this.io;
					this.io = null;
				}
			}

			// I realize that socket.io already does this behavior, but there
			// is also a possibility that `socket` may not exist.
			// IE, when a programmer creates a connection and a subscription
			// all at once.

			/**
			 * Iterates over the internal queue in a non-blocking manner
			 * @chainable
			 * @method module:io.Server#emitQProcess
			 * @return {Server} Current server instnace
			 **/
			,emitQProcess: function(){
				// log.log("Processing Queue", this);
				var id			// The id for the emit interfal
					,queue;	    // an interable clone of the queue

				queue = iter.from( arrayclone( this.queued) );
				this.queued = [];

				id = setInterval( function(){
					try{
						var n = queue.next();
						this.emit.apply(this, n );
					} catch( e ){
						if( e === iter.StopIteration ){
							clearInterval( id );
						} else{
							throw e;
						}
					}
				}.bind( this ), 2 );

				return this;
			}

			/**
			 * Stashes a failed emit attempt for later execution
			 * @chainable
			 * @protected
			 * @method module:io.Server#queue
			 * @param {Array} args Arguments to call the method with
			 * @return {Server} The current server instance
			 **/
			,queue: function(emitArgs){
				this.queued.push(emitArgs);
				return this;
			}.protect()

			/**
			 * Attempts to emit the event. If the socket has not been created or is closed, the attempt will be queued
			 * @chainalbe
			 * @method module:io.Server#emit
			 * @return {Server} The current server instance
			 **/
			,emit: function(){
				// Can we proceed?
				var has_session = !!this.sessionid;
				// log.log("has_session? " , has_session)
				if ( has_session && this.socket && this.socket.socket && this.socket.socket.readyState == this.socket.socket.OPEN){
					return this.socket.send.apply(this.socket, arguments);
				} else {
					this.queue.call(this, arguments);
					// log.warn("queuing event until socket connected")
				}

				return this;
			}

			/**
			 * Links the current server instance to a client for event bindings
			 * @method module:io.Server#listen
			 * @param {Client} client
			 * @param {Object} options
			 * @return {Client} The create client instance
			 **/
			, listen: function( application, opts ){
				opts = opts ||{};
				var client
					,channel;

				if( application instanceof Client ){
					// log.warn("replacing application ", application.$name )

					if(!this.$applications[ application.$name ] ){
						this.$applications[ application.$name ] = {};
					}
					this.$applications[ application.$name ][ application.$created ] = application;

					application.$server = application.$server || this;
					client = application;
					channel = client.$name;
					this[ channel ] = client;
					this.emit("join", channel);

				}

				return client;
			}


			/**
			 * Helper method for generating server Events
			 * @method module:io:Server#initEvents
			 * @param {Object} events An object of event / function pairs
			 * @return {Server} The server instance
			 **/
			, initEvents: function( events ){

				for( var key in events ){
					this.socket.on( key, events[key] );
				}

				return this;

			}

			/**
			 * Attemprs to clean an application and its listeners
			 * @method module:io.Server#remove
			 * @param {String} name The name of a registered client
			 * @return {Server} The current server instance
			 **/
			, remove: function( app ){
				this.purgeApplication( app );
				return this;
			}

			/**
			 * Creates a new Client by name and registers it right away
			 * @method module:io.Server#spawn
			 * @param {String} name The name of the client to create and bind to
			 * @return {Client} A new client instance
			 **/
			,spawn: function( app, opts ){
				var c = new Client( app, this, opts );

				this.listen( c );

				return c;
			}

			,send: function( action, data ){
				if( arraycontains( this.$channels, "wallace:admin" ) ){
					data.channel = "wallace:admin";
					this.emit("message", data );
				}
			}

			,primer: function( ){
				//re-appropriate the IO object

				this.io = Primus;

				// re-establish the session.

				// bind any pending event listeners for the given application

				this.socket = this.io.connect(this.options.addr,{
					transformer: "websockets"
					,"force new connection": true
					,secure:"https:" == document.location.protocol ? true : false
				});

				this.socket.on('open', function(){
					this.fireEvent("socketready", [ this ]);
				}.bind( this ));

				this.initEvents({
					unauthorized:function(app, uuid, status){

						// log.warn( ( "unauthorized event binding " + status ) )
						var app_cache;
						status = status || 401;
						if( this.hasApplication( app ) ){
							app_cache = this.getApplication( app );
							object.each( app_cache, function( obj ){
								obj.purgeStashed( uuid );
							});
						}
					}.bind( this )


					, "wallace-error":function(name, msg, stack ){
						this.fireEvent("error", [name,msg,stack] );
					}.bind( this )


					, "wallace-info":function(msg){
						log.warn("Wallace Info: ", msg);
					}.bind( this )


					,'wallace-session-id-offer': function(sessionid){
						var current_session;
						current_session = cookie.read( COOKIE_NAME );

						// log.log("Session id offer. offer: %s, existing: %s", sessionid, current_session)

						if (current_session && current_session == sessionid ){
							this.socket.send('associate', current_session);
						} else {
							// log.warn("setting session id");

							this.sessionid = sessionid;
							cookie.write( COOKIE_NAME, sessionid );

							this.fireEvent("session",[ sessionid ]);

						}
					}.bind( this )


					,"reconnect": function( id ){
						// log.info("Successful reconnection ", id );
						return id;
					}.bind( this )


					,'wallace-session-ttl': function( id, ttl ){
						cookie.dispose( COOKIE_NAME );
						cookie.write( COOKIE_NAME, id, {
							duration: ( ttl / 60 /60 / 24)
						});

					}.bind( this )


					,'wallace-session-id':function(sessionid){
						// log.log("My session id:", sessionid);
						if (this.sessionid !== sessionid){
							// this.resubscribe();
						}
						// log.warn("setting session id");
						this.sessionid = sessionid;
						cookie.write( COOKIE_NAME, sessionid);


						this.fireEvent("session",[ sessionid ]);

					}.bind( this )


					,'signal': function( evt, data, meta ){
						// log.log( "%s says %s", meta.application, data.status);
						var app_cache;
						if (this.hasApplication(meta.application)){
							app_cache = this.getApplication( meta.application );

							object.each( app_cache, function( obj ){

								if( meta.type === "broadcast" ){
									obj.relay(evt, data, meta );
								} else{
									obj.signal(evt, data, meta );
								}
							});

						}
					}.bind( this )


					,'wallace-session-established': function( session_id ){
						this.sessionid = session_id;
						this.emitQProcess();
					}.bind( this )


					, 'ping': function(){
						this.emit('pong');
					}.bind( this )


					,'wallace-subscribe': function(app, uuid ){
						// log.warn('subscription response ', arguments)
						var app_cache;
						if( this.hasApplication( app ) ){
							app_cache = this.getApplication( app );

							object.each( app_cache, function( obj ){
								// log.warn("subscribe response", app, uuid)
								obj.bindStashed( uuid );
							});

						}
					}.bind( this )

					, "wallace-disconnect": function(){
						this.disconnect();
					}.bind( this )

					,"wallace-application-joined": function( data, auth_required ){
						var app_cache;
						// log.log("application-joined", data.name )
						if (this.hasApplication(data.name)){
							app_cache = this.getApplication(data.name);
							object.each( app_cache, function( obj ){
								obj.fireEvent( "setauth",[ !!data.auth, auth_required ] );
							});
						}
						this.$channels = arrinclude( this.$channels, data.name );
						// log.log("currently in channels ", this.$channels );

					}.bind( this )
				});
				// Avoid confusion (or cause extra confusion...)
				// window.io = undefined;
				this.fireEvent("connect");
			}
		});


		// Look up a server, or create one.
		function getServer(serverAddr ){
			if ( !wallaceServers[serverAddr] ) {
				wallaceServers[serverAddr] = new Server({addr: serverAddr});
			}
			return wallaceServers[serverAddr];
		}

		var PrimusBinder = function(){}

		// This is what you really want.
		exports.connect = function(serverAddr){
			return getServer(serverAddr);
		};

		exports.Server = Server;
		exports.Client = Client;
	}
);

