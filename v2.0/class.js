// ### Class
// The `class` module is exports the **Class** class. As like in most Object orientated Languages,
// A Createing a new Class results in a __mostly__ immutable object that can create instances of itself.

// In previous verions of braveheart
// when you required the `class` module, you had to access the actual Class object
// from the class module. Since class is a reserved word in javascript, you would find
// your self doing this :`_cls.Class`

// Since version 2.0 of braveheart, when you require `class` you are give the *Class* object
// directly

// The `Class` Object also has the libraries default Mixin Classes added to it, so when you
// require the class module, you will have access to everything right away.

/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Provides an abstraction ontop of JavaScript's prototypical inheritence model to look more like the Classical Object Orientated Model.
 * The {@link module:class.Class} system allows for Multiple inheritance, Mixins, dependancy injection instance modification, protected functions and other OOP type abstractions
 * @author Eric Satterwhite
 * @module class
 * @requires module:class.Class
 * @requires module:class.Options
 * @requires module:class.Events
 * @requires module:class.Events.Plus
 * @requires module:class.Storage
 * @requires module:class.Interface
 * @requires module:class.Chain
 * @version 1.1
 */

define(['require', 'module', 'exports', 'class/Class', 'class/Options', 'class/Events', 'class/Events.Plus', 'class/Storage', 'class/Interface', 'class/Chain']
       ,function( require, module, exports, Class, Options, Events, Plus, Storage, Interface, Chain ){

        Class.Storage     = Storage;
        Class.Options     = Options;
        Class.Events      = Events;
        Class.Events.Plus = Plus;
        Class.Chain       = Chain;
        Class.Interface   = Interface;
        return Class;

    }
);

