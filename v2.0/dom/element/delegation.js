/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires module:dom/element
 **/
define([
		  'require'
		, 'module'
		, 'exports'
		, 'class'
		, '../element'
		, 'slick'
		, 'array/each'
	]
	,function(require, module, exports, Class, element, Slick, arreach ){
		var Element     = element.Element
		  , proto       = Element.prototype
		  , addEvent    = proto.addEvent
		  , removeEvent = proto.removeEvent
		  , bubbleUp
		  , delegation
		  , STORAGE_KEY
		  , relay
		  ;

		STORAGE_KEY = '$delegation:';

		var map = element.Events;
		relay = function( old, method ){
			return function( type, fn, capture ){
				if( type.indexOf(':relay') == -1 ){
					return old.call( this, type, fn, capture );
				}

				var parsed = Slick.parse( type ).expressions[0][0];

				if( parsed.pseudos && parsed.pseudos[0].key != 'relay' ){
					return old.call( this, type, fn, capture );
				}

				var newType = parsed.tag;

				arreach( parsed.pseudos.slice(1), function( pseudo ){
					newType += ':' + pseudo.key + ( pseudo.value ? '(' + pseudo.value + ')' : '' );
				});

				old.call( this, type, fn );
				return method.call( this, newType, parsed.pseudos[0].value, fn );
			};
		};

		bubbleUp = function(self, match, fn ,event, target ){
			while( target && target.node != self.node ){
				if( match( target, event ) ){
					return fn.call( target, event, target );
				}

				target = element.id( target.parentNode );
			}


		};

		delegation = {
			addEvent: function( type, match,  fn ){
				var storage = this.retrieve('$delegates', {})
				  , stored  = storage[ type ];

				if( stored ){
					for( var _uid in stored ){
						if( stored[_uid].fn == fn && stored[ _uid ].match == match ){
							return this;
						}
					}
				}

				var _type = type
				  , _match = match
				  , _fn    = fn
				  , _map   = map[ type ] || {};

				// function overloading
				match = function( target ){
					return Slick.match( target.isNode ? target.node : target , _match );
				};

				var elementEvent = element.Events[ _type ];

				if( _map.condition || elementEvent && elementEvent.condition ){
					var __match = match
					  , condition = _map.condition || elementEvent.condition;

					match = function( target, event ){
						return __match( target, event ) && condition.call( target, event, type );
					};
				}


				var self = this
					,uid = (+ new Date()).toString( 36 )
					,delegator;

				delegator = function( event, target ){
					if(!target && event && event.target ){
						target = event.target;
					}
					if( target ){
						( _map.listen ? _map.listen : bubbleUp )( self, match, fn, event, target, uid );
					}
				};

				if( !stored ){
					stored = {};
				}

				stored[ uid ] = {
					match:_match
					,fn:_fn
					,delegator:delegator
				}

				storage[ _type ] = stored;

				return addEvent.call( this, type, delegator, _map.capture)

			}

			,removeEvent: function( type, match, fn, _uid ){
				var storage = this.retrieve("$delegates", {})
				  , stored = storage[ type ]

				if(!stored ){ return this; }

				if( _uid ){
					var _type = type
					  , delegator = stored[ _uid ].delegator
					  ,_map = map[ type ] || {};

					type = _map.base || type;
					if( _map.remove ){
						_map.remove( this, _uid );
						delete stored[ _type ];
						storage[ _type ] = stored;
						return removeEvent.call( this, type, delegator, _map.capture )
					}

					var __uid, _stored;

					for( __uid in stored ){
						_stored = stored[ __uid ];
						if( fn ){
							if( _stored.match == match && _stored.fn == fn ){
								return delegation.removeEvent.call( this, type, match, fn, __uid )
							}
						}

						if( _stored.match == match ){
							delegation.removeEvent.call( this, type, match, _stored.fn, __uid )
						}
					}

					return this;
				}

			}
		};

		Element.implement({
			addEvent: relay( addEvent, delegation.addEvent )
			,addEvents: function( obj ){
				obj = obj || {}
				for( var key in obj ){
					return this.addEvent.call( this, key, obj[key] )
				}
			}
		});
		return Element;
	}
);
