/**
 * @module dom/Window
 * @requires class
 * @requires dom/DOMNode
 * @author Eric Satterwhite
 */
define(function(require, exports){
	var Class = require("class")
		,DOMNode = require("./DOMNode")
		,Node = DOMNode.Node
		,Options = Class.Options
		,Events = Class.Events
		,readyQueue = []
		,isDomReady = false
		,Window;

	exports.Window = new Class({
		Extends:Node

		,initialize: function( el ){
			el = el || window;

			this.parent( el )
		}
		,getWindow: function(){
			return this.node
		}

		,toString: function(){
			return "<window>"
		}

		,repr: function(){

		}

		,get: function( name ){
			return this.node[name];
		}
		,set: function( name, value ){
			this.node[name] = value;
		}
	});
	Window = exports.Window;

	var domready = function(){

	}


	exports.onDomReady = function( fn, scope ){
		if( isDomReady ){
			fn.call(scope)
		}
	}
})
