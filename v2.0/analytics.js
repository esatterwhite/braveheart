/*jshint laxcomma: true, smarttabs: true*/
/*globals define, window, document*/

/**
 * Provides an interface for logging analytic data
 * @module analytics
 */

define(['require', 'exports', 'module', 'core'], function(require, exports, module, core) {

	var typeOf = core.typeOf;

	window._gaq = window._gaq || [];
	window._gaq.push(['_setAccount', 'UA-11421056-2']);
	window._gaq.push(['_setDomainName', 'none']);
	window._gaq.push(['_trackPageview']);

	(function() {
		var ga, s;
		ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();

	/**
	 * Tracks a notable event
	 * @param {Object|Array} Either one or more sets of event parameters to be logged
	 * <ul>
	 *		<li>analytic_category - the category of the event</li>
	 *		<li>analytic_action   - the event action</li>
	 *		<li>analytic_label    - a custom label for the event</li>
	 *		<li>analytic_value    - value to set as the event value (optional)</li>
	 * </ul>
	 */
	exports.trackEvent = function() {

		var arg, i, len;

		for (i = 0, len = arguments.length; i < len; i++) {
			arg = arguments[i];
			if (typeOf(arg) === 'array' && arg.length > 1) {
				arg.splice(0, 0, '_trackEvent');
				window._gaq.push(arg);
			} else if (!!arg.analytic_category && !!arg.analytic_action) {
				window._gaq.push([
					'_trackEvent'
					, arg.analytic_category
					, arg.analytic_action
					, arg.analytic_label
					, arg.analytic_value
				]);
			}
		}

		return exports;

	};

	/**
	 * Manually records a page view
	 */
	exports.trackPageView = function() {
		window._gaq.push('_trackPageview');
		return exports;
	};

	exports.setAccount = function( id ){
		if( id && window._gaq ){
			window._gaq.push(['_setAccount', ( "" + id ) ]);
		}
	};
});
