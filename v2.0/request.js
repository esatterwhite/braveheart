/*jshint laxcomma: true, smarttabs: true*/


/**
 * Provides a standard way for making XMLHttpRequests
 *
 * @author Eric Satterwhite
 * @module request
 * @requires core
 * @requires class
 * @requires object
 * @requires json
 * @requires date
 * @requires functools
 * @requires module:request.Request
 * @requires module:request.Request.SIGNED
 * @requires module:request.Request.JSON
 */
define(function( require, exports ){
	var Request = require("./request/Request")
		,_JSON  = require("./request/JSON")
		,SIGNED = require("./request/SIGNED")
		,HTML   = require("./request/HTML");


	Request.JSON   = _JSON;
	Request.HTML   = HTML;
	Request.SIGNED = SIGNED;

	return Request;

});
