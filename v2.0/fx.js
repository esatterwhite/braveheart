/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

// ## FX

// This is the `fx` module.

/**
 * DESCRIPTION
 * @module fx
 * @author Eric Satterwhite
 * @requires module:fx.Base
 * @exports module:fx.Base
 * @example braveheart(["fx"], function( FX ){
	var effect = new FX({
		duation:700
		,transition:"quad:out"
	});

	effect.start(0, 600)
 })
 **/
define(function(require, exports){
	var  Base = require( 'fx/Base' )

	// All this module does is return the `Base` module
	// It is done this way purely for an organizational stand point
	// so no single module is too large. Also, The common convention is to
	// have only one `Class` per file
	return Base;
});
