/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:request.JSON
 * @author Eric Satterwhite
 * @requires Request
 * @requires Class
 * @requires object
 * @requires json
 **/
define(function(require){
    var  Request = require( './Request' )
        ,Class = require( 'class' )
        ,object = require( 'object' )
        ,_json = require('json')
        ,JSON
        ;

	/**
	 * The Extends Base {@link Request} to work with JSON responses / requests.<br/>
	 * Automatically sends and accepts json and decodes responses. Accepts all the same options as the Base Request Class.
	 * @class module:request.JSON
	 * @extends module:request.Request
	 * @param {Boolean} secure If set to true, the module will attempt to validate the json as safe. default to true
	 */
	JSON = new Class( /** @lends module:request.JSON.prototype */{
		Extends:Request
		,options:{
			secure:true
		}
		,initialize: function( options ){
			this.parent( options );
			object.append( this.headers, {
				"Accept":"application/json",
				"X-Request":"JSON",
				"Content-Type":"application/json"
			});
		}
		,encode: function( data ){
			if( this.options.method.toLowerCase() === "get"){
				return object.toQueryString( data );
			}
			try{
				return _json.encode( data );
			} catch( e){
				return object.toQueryString( data );
			}
		}
		,success: function( text ){
			var json;
			try{
				json = this.response.json = _json.decode( text, this.options.secure );
			} catch (e ){
				this.fireEvent('error', [text, e]);
				return;
			}

			if( json == null){
				this.onFailure();
			} else{
				this.onSuccess( json, text );
			}
		}
	});

	return JSON;
});
