/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @author Eric Satterwhite
 * @requires class
 * @requires object
 * @requires core
 **/
define([
		 "require"
		, "module"
	    , "exports"
	    , "core"
	    , "class"
	    , "object"
	    , "functools"
	    , "string"
    ]
    ,function(require, module, exports, core, Class, object, functools, string ){
		var RequestType
			,hasXHR
			,hasProgress
			,Request
			,execute
			;


		RequestType = (function(){
			var XMLHTTP
				,MSXML2
				,MSXML;


			XMLHTTP = function(){
				return new XMLHttpRequest();
			};

			MSXML2 = function(){
				return new ActiveXObject("MSXML2.XMLHTTP");
			};

			MSXML = function(){
				return new ActiveXObject("Microsoft.XMLHTTP");
			};

			return functools.attempt(
				function(){
					XMLHTTP();
					return XMLHTTP;
				}
				,function(){
					MSXML2();
					return MSXML2;
				}
				,function(){
					MSXML();
					return MSXML;
				}
			);
		}());


		hasXHR = !!RequestType;
		hasProgress = ( 'onprogress' in new RequestType() );

		execute = function( text ){
			if( !text )return text;

			if( window.execScript ){
				window.execScript( text );
			} else {
				var script = document.createElement('script');
				script.setAttribute('type', "text/javascript");
				script.text = text;


				// just get the browser to run the code.
				// it doesn't need to stay there!
				document.head.appendChild( script );
				document.head.removeChild( script );
			}

			return text;
		};

		/**
		   The Base class for making cross browser XMLHttpRequests. Take a single configureation object
		   @class module:request.Request
		   @param {Object} options The options to configure the request class with
		   @param {String} [options.user=''] username used for requests that require credentials
		   @param {String} [options.password=''] password used for requests that require credentials
		   @param {Object} [options.data={}] key value pairs to be sent in the body of the request.
		   @param {Object} [options.headers] headers to be set on the request
		   @param {Boolean} [options.async=true] set to true to make the request asycronous. false for a syncronous. defaults to true
		   @param {String} [options.format=false] if specified, an additional format will be appended to the data string as eg. format=application/json
		   @param {String} [options.link=ignore]
		   @param {Boolean} [options.urlEncode=false] if set to true, the headers will automatically be set to form-url-encoded and formatted appropriatly
		   @param {String} [options.encoding=utf-8] Set the encoding type for the request. defaults to "utf-8"
		   @param {Boolean} [options.evalScripts=false] will execute any javascript returned in the text response returned in the success response
		   @param {Boolean} [options.evalResponse=false] if automatically try to execute any response if set to true. Defaults to false
		   @param {Number} [options.timeout=0] the time after which to considered that the request has timed out. It is suggested not to set this when dealing with large files and is a known constant.
		   @param {Boolean} [options.noCache=false] if set to true, a cache buster string will be appended to the end of the request url
		   @param {Array} [options.middlware=[]] An array of object that have methods <strong>processRequest</strong> or <strong>processRequest</strong> or both. This alows you to automoat the action of processing incomming responses or out going requests.
		   @param {Function} [options.onRequest] default event handler for the request event
		   @param {Function} [options.onLoadstart] default event handler for the loadstart event
		   @param {Function} [options.onProgress] default event handler for the progress event
		   @param {Function} [options.onComplete] default event handler for the complete event
		   @param {Function} [options.onCancel] default event handler for the cancel event
		   @param {Function} [options.onSuccess] default event handler for the success event
		   @param {Function} [options.onFailure] default event handler for the failure event
		   @param {Function} [options.onException] default event handler for the exception event
		   @param {Function} [options.onTimeout] default event handler for the timeout event
		   @example braveheart([ "requrest" ], function(request){
		var xhr = new request.Request({
			method:"post"
			,data:{
				key:"value"
			}
			,onSuccess: function( responseText, responseXML ){
				doMagic( responseText )
			}
			,onFailure: function( xhr ){
				alert("DANGER, WILL ROBINSON!!")
			}
		})

		xhr.send();
	})
		 */
		exports.Request = Request = new Class( /** @lends module:request.Request.prototype */ {
			Implements:[Class.Chain, Class.Events, Class.Options],
			OnEvents:true,
			options:{
				user: '',
				password: '',
				url: '',
				data: '',
				headers: {
					'X-Requested-With': 'XMLHttpRequest',
					'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
				},
				async: true,
				format: false,
				method: 'post',
				link: 'ignore',
				isSuccess: null,
				emulation: false,
				urlEncoded: false,
				encoding: 'utf-8',
				evalScripts: false,
				evalResponse: false,
				timeout: 0,
				noCache: false,
				middleware:[]
				/* these are possible event call backs to listen on.
				 * for documentation only
				 * onRequest: function(){},
				 * onLoadstart: function(event, xhr){},
				 * onProgress: function(event, xhr){},
				 * onComplete: function(){},
				 * onCancel: function(){},
				 * onSuccess: function(responseText, responseXML){},
				 * onFailure: function(xhr){},
				 * onException: function(headerName, value){},
				 * onTimeout: function(){},
				 */
			},

			initialize: function( options ){
				this.xhr = new RequestType();

				this.setOptions( options );

				// just a quick reference.
				this.headers = this.options.headers;
			},
			/**
			 * Called when the request state changes
			 */
			onStateChange: function(){
				var xhr = this.xhr;

				/* READY STATE CODES
				 *
				 * unstent = 0
				 * opened = 1
				 * headers recieved = 2
				 * loading = 3
				 * done = 4
				 */
				if( xhr.readyState !=4 || !this.running ){
					return;
				}
				this.running = false;

				this.status = 0;
				Function.attempt(
					function(){
						var status = xhr.status;

						// ya IE somtimes returns a 1223 instead of a 204. WTF?!
						this.status = ( status == 1223 ) ? 204 : status;
					}.bind( this )
				);// end attempt

				xhr.onreadystatechange = function(){};
				if( hasProgress ){
					xhr.onprogress = xhr.onloadstart = function(){};
				}

				clearTimeout( this.timer );

				this.response = {text: this.xhr.responseText || "", xml: this.xhr.responseXML };
				if( this.isSuccess.call( this, this.status )){


					this.success( this.response.text, this.response.xml );
				} else {
					this.failure();
				}
			},
			isSuccess: function(){
				var status = this.status;
				return ( status >= 200 && status < 300);
			},
			isRunning: function(){
				return !!this.running;
			},
			processScripts: function( text ){
				if( !!this.options.evalResponse || (/(eca|java)script/).test(this.getHeader("Content-Type"))){
					return execute( text );
				}

				return string.stripScripts(text, this.options.evalScripts );
			},
			success: function( text, xml ){
				this.onSuccess( this.processScripts( text ), xml);
			},

			onSuccess: function(){
				var mw
					,x;
				for( x = 0; x < this.options.middleware.length; x++){
					mw = this.options.middleware[x];
					if( typeof mw.processResponse === 'function' ){
						mw.processResponse.apply(this, arguments );
					}
				}
				this.fireEvent('complete', arguments).fireEvent( 'success', arguments).callChain();
			},
			failure: function(){
				this.onFailure();
			},
			/**
			 * executed when the request fails
			 * @fires failure
			 */
			onFailure: function(){
				/**
				 * @event failure
				 * @param {XMLHttpRequest} xhr the xhr request
				 */
				this.fireEvent('complete').fireEvent('failure', this.xhr);
			},
			loadstart: function( event ){
				this.fireEvent('loadstart', [ event, this.xhr ] );
			},
			progress: function( event ){
				this.fireEvent('progress', [event, this.xhr] );
			},
			timeout: function( event ){
				this.fireEvent('timeout', this.xhr);
			},

			/**
			 * Sets the value of a given header
			 * @param {String} name The name of the header to set
			 * @param {String} value The value to set the header as
			 * @returns {Request} The current request instance
			 */
			setHeader: function( name, value ){
				this.headers[name] = value;
				return this;
			},

			/**
			 * Retreives the current value of a given header
			 * @param {String} name The name of the header to retreive
			 * @returns {Strign} The current header value
			 */
			getHeader: function( name ){
				return Function.attempt( function(){
					return this.xhr.getResponseHeader( name );
				}.bind(this));
			},
			check: function(){
				if( !this.running ){
					return true;
				}
				// from the chain class
				switch( this.options.link ){
					case "cancel":
						this.cancel();
						return true;
					case "chain":
						this.chain( this.caller.pass( arguments, this ));
						return false;
				}

				return false;
			},

			/**
			 * Serializes request data for transmission
			 * @method module:request.Request#encode
			 * @param {Object} data The data to serialize
			 * @returns {String} the serialized data
			 */
			encode: functools.protect(function( data ){
				return object.toQueryString( data );
			}),
			/**
			 * executes the current request object
			 * @param {Object} options the options for the request
			 * @returns {Request} the current request
			 */
			send: function( options ){
				var mw;
				if( !this.check(options) ){
					return this;
				}

				this.options.isSuccess = this.options.isSuccess || this.isSuccess;
				this.running = true;

				var type = core.typeOf( options );

				if( type === 'string' || type === 'htmlelement') {
					options ={data: options};
				}

				var old = this.options;
				options  =  object.append( {data:old.data, url:old.url, method: old.method}, options );
				var data = options.data
					,url = String( options.url)
					,method = options.method.toLowerCase();

				switch( core.typeOf( data )){
                    case "object":
					case "array":
						data = this.encode( data );
						break;
					case "element":
						data = {};
						break;

				}

				if( options.format ){
					var format = 'format=' +this.options.format;
					data = !!data ? format + "&" + data : format;
				}

				if( this.options.emulation && !( method == 'post' || method == 'put')){
					var _method = '_method=' + method;
					data = !!data ? _method  +"&" + data : _method;
					method = 'post';
				}

				if( this.options.urlEncoded && (method=='post' || method =='put') ){
					var encoding = !!this.options.encoding ? "; charset=" + this.options.encoding : '';
					this.headers["Content-Type"] = 'application/x-www-form-urlencoded' + encoding;
				}

				if( !url ){
					url = document.location.pathname;
				}

				var trimPosition = url.lastIndexOf( '/' );
				if( trimPosition > -1 && ( trimPosition = url.indexOf("#")) > -1 ){
					url = url.substr(0, trimPosition);
				}

				if( this.options.noCache ){
					url += ( url.indexOf('?') > -1 ? '&':'?') + ( (+ new Date() ).toString( 32 ) );
				}

				if( data && method === 'get' ){
					url += ( ( url.indexOf( "?" ) != -1 ) ? "&" : "?") + data;
					data = null;
				}

				var xhr = this.xhr;
				if( hasProgress ){
					xhr.onloadstart = this.loadstart.bind( this);
					xhr.onprogress = this.progress.bind( this );
				}

				for( var x = 0; x < this.options.middleware.length; x++ ){
					mw = this.options.middleware[x];
					if( typeof mw.processRequest === 'function' ){
						mw.processRequest.call( this, this.xhr, options);
					}
				}

				xhr.open( method.toUpperCase(), url, this.options.async, this.options.user, this.options.password );


				if( this.options.user && 'withCredentials' in xhr ){
					xhr.withCredentials = true;
				}

				xhr.onreadystatechange = this.onStateChange.bind( this );

				object.each( this.headers, function( value, key ){
					try{
						xhr.setRequestHeader( key, value );
					} catch( e ){
						this.fireEvent('exception', [key, value]);
					}
				}, this);

				this.fireEvent('request');
				xhr.send( data );
				if(!this.options.async){
					this.onStateChange();
				} else if( this.options.timeout){
					this.timer = setTimeout( this.timeout.bind( this ) ,this.options.timeout );
				}

				return this;

			},

			/**
			 * Cancels the pending request if it exists
			 * @returns {Request} The curret request
			 */
			cancel: function(){
				if(!this.running){
					return this;
				}

				this.running = false;
				this.xhr.abort();
				clearTimeout( this.timer );
				this.xhr.onreadystatechange = function(){};
				if( hasProgress){
					this.xhr.onprogress = this.xhr.onloadstart = function(){};
				}

				this.xhr = new RequestType();
				this.fireEvent('cancel');
				return this;
			}
		}); // End Request

		return Request;

}
);
