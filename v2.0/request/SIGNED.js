/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Request class the knows how to sign requests for the Corvisa API authentication protocol
 * @author Eric Satterwhite
 * @requires module:class.Class
 * @requires module:request.SIGNED
 * @requires module:object
 **/
define(
  [
    "require"
    , "exports"
    , "module"
    ,"class"
    , "./JSON"
    , "object"
    , "date"
    , "functools"
    , "string"
    , "crypto/base64"
    , "crypto/hmac"
    , "crypto/sha1"
  ]
  ,function(require, exports, module, Class, _JSON, object, date, functools, string, base64, hmac, sha1){
    var SIGNED;
	/**
	 * A Custom Implementation of the JSON class to interface with the LenderX API via API Key
	 * by handeling the request signing process for you. Accepts all the same optiosn as {@link module:request.JSON}
	 * @class module:request.SIGNED
	 * @extends module:request.JSON
	 * @param {Object} options Additional options in addition to those defined in {@link module:request.Request|Request}
	 * @param {String} options.apiKey the api key to use in the request
	 * @param {String} options.apiSecret The secret to match the API key
	 * @param {String} options.userID a string to use as the ID for the user account.
	 */
	SIGNED = new Class(/** @lends module:request.SIGNED.prototype */{
		Extends:_JSON
		,options:{
			apiKey:''
			,apiSecret:''
			,userID:''
			,contentType:"application/json"
		}
		,initialize: function( options ){
			this.parent( options );
		}
		,send: function(){
			var sender,
				_parent;
			sender = function(){

				return this.send();
			}.bind( this );

			if(!this.signed){
				this.addEvent( 'signed', sender );
				this.sign();
			} else {
				this.removeEvent('signed', sender );
				this.parent();
			}
		}
		/**
		 * Signs the current request for LenderX API Authentication
		 * @private
		 * @fires event:signed
		 * @returns {Request} The current request object
		 */
		,sign: functools.protect(function(){
			var that = this;
			if(!this.signed){
				var separator = "\n"
					,verb = that.options.method.toUpperCase()
					,currentDate
					,resource
					,arguments
					,contentType
					,signedString
					,signature
					,api_key
					,api_secret;
				api_secret = that.options.apiSecret; // "00531738-14bb-4e8f-8d5b-f33f60e9bf8c"
				api_key =that.options.apiKey; // "57856984-6662-4b50-9755-6928f14ac284"

				currentDate = new Date();
				currentDate = date.format( currentDate, "%a, %d %b %Y %H:%M:%S %Z" );
				resource = that.options.url;
				contentType = "application/json";
				args = object.toQueryString( that.options.data );

				signedString = [
					verb
					,""
					,contentType
					,currentDate
					,resource
					,args
					, !!that.options.userID ? ( "x-cor-auth-userid:" + that.options.userID ) : ""

				].join( separator );

				signature = base64.encode(
					hmac (
						sha1,
						signedString,
						api_secret,
						{asString:true}
					)
				);
				object.merge(that.headers, {
					"X-Date": currentDate
					,"Content-Type":contentType
					,"x-cor-auth-userid":that.options.userID
					,"Authorization": string.substitute(
										"CONE {api_key}:{signature}", {
											api_key:api_key
											,signature:signature
										}
									)
				});
				// date formatter
				//"%a, %d %B %Y %H:%M:%S %Z"
				that.signed = true;
				that.fireEvent( 'signed' );
			} else {
				/**
				 * Fired when the request is signed
				 * @event
				 */
				this.fireEvent( 'signed' );
			}
		})
	});

	return SIGNED;
});
