/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:request.HTML
 * @author Eric Satterwhite
 * @requires moduel:request.Request
 * @requires class
 * @requires object
 * @requires json
 **/
define(function(require, exports){
    var  Request = require( './Request' )
        ,Class = require( 'class' )
        ,object = require( 'object' )
        ,dom    = require( "dom" )
        ,HTML
        ,exec
        ;

    exec = function(text){
        if (!text) return text;
        if (window.execScript){
            window.execScript(text);
        } else {
            var script = document.createElement('script');
            script.setAttribute('type', 'text/javascript');
            script.text = text;
            document.head.appendChild(script);
            document.head.removeChild(script);
        }
        return text;
    };

	/**
	 * The Extends Base {@link Request} to work with JSON responses / requests.<br/>
	 * Automatically sends and accepts json and decodes responses. Accepts all the same options as the Base Request Class.
	 * @class module:request.HTML
	 * @extends module:request.Request
	 * @borrows module:request.Request#cancel
	 * @param {Object} [options] Config options for the request instance
	 * @param {String|module:element.Element} [options.update=null] If set to true, the module will attempt to validate the json as safe. default to true
	 * @param {String|module:element.Element} [options.append=null] If set to true, the module will attempt to validate the json as safe. default to true
	 * @param {Boolean} [options.evalScripts=true] Set to true to evaluate any scripts found in the html fragement
	 * @param {String} [options.filter="*"] A CSS selector expression used to filter out dom elements from the resulting HTML
	 */
	HTML = new Class( /** @lends module:request.HTML.prototype */{
		Extends:Request
		,options:{
			 update:false
			,append:false
			,evalScripts:true
			,filter: false
			,headers:{
				Accept:"text/html, application/xml, text/xml, */*"
			}
		}

		,success: function( text ){
			var options = this.options		//options cache
				,response = this.response	// response object cacje
				,scripts = ""				// holder for parsed javascripts in html
				,match						// holder for match parts of HTML matcher regex
				,tmp;						// temp div to hold html dom / elements

				text = text.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function(all, code){
								scripts += code + '\n';
								return '';
						});

				response.javascript = scripts;
				response.html = text;

				match = response.html.match(/<body[^>]*>([\s\S]*?)<\/body>/i);

				if( match ){
					response.html = match[1];
				}
				tmp = new dom.Element( 'div' );
				tmp.set('html', response.html );
				response.dom = tmp.get("childNodes");
				response.elements = tmp.getElements( options.filter | "*" );


				if( options.update ){
					dom.id( options.update ).set("html", response.html );
				} else if( options.append ){
					dom.id( options.append ).adopt( response.elements );
				}

				if( options.evalScripts ){
					exec( response.javascript);
				}


				/**
				 * fired on successful request
				 * @event success
				 * @param {NodeList} dom The nodelist created from the html fragment
				 * @param {Elements} elements an instance of {@module:dom.Elements} containing the html from the fragment
				 * @param {String} html the raw html string that was used to parse out the elements
				 * @param {String} javascript any text found inside of any script tags of the html
				 */
				this.onSuccess( response.dom, response.elements, response.html, response.javascript );

		}
	});

	return HTML;
});
