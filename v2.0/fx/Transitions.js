/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:fx.Transitions
 * @author Eric Satterwhite
 * @requires object
 **/
define(function(require, exports){
	var // required easing functions
		 Back    = require( "./functions/Back" )
		,Bounce  = require( "./functions/Bounce" )
		,Circ    = require( "./functions/Circ" )
		,Elastic = require( "./functions/Elastic" )
		,Expo    = require( "./functions/Expo" )
		,Pow     = require( "./functions/Pow" )
		,Sine    = require( "./functions/Sine" )

		,additional
		,Transition
		,Transitions
		,isEnumerable;

	isEnumerable = function( item ){
		return (item != null &&  typeof( item.length ) == 'number' && Object.prototype.toString.call(item) != '[object Function]' );
	}

	quickFrom = function ( item ){
		if(item == null){
			return [];
		}
		if(arguments.length > 1){
			return exports.from(arguments);
			
		}
		return (isEnumerable(item) && typeof item != 'string') ? (Object.toString(item) == '[object Array]') ? item : Array.prototype.slice.call(item) : [item];

	}
	Transition = function( transition, params ){
		params = quickFrom( params );
		var easeIn = function( pos ){
			return transition( pos, params );
		};


		easeIn.easeIn = easeIn;
		easeIn.easeOut = function( pos ){
			return 1- transition( 1 - pos, params )
		};

		easeIn.easeInOut = function( pos ){
			return (pos <= 0.5 ? transition(2 * pos, params) : (2 - transition(2 * (1 - pos), params))) / 2;
		};
		return easeIn;
	};


	Transitions = {
		linear: function( zero ){
			return zero
		}
	};


	Transitions.defineTransitions = function( transitions ){
		for( var trans in transitions ){
			Transitions[trans] = new Transition( transitions[ trans ] )
		}
	};

	//License: Easing Equations v1.5, (c) 2003 Robert Penner, all rights reserved. Open Source BSD License.
	/**
	 * DESCRIPTION
	 * @class module:fx.Transition
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Transitions.defineTransitions(/** @lends module:fx.Transition.prototype */{

		/**
		 * DESCRIPTION
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Number}
		 **/
		Pow: Pow,

		/**
		 * DESCRIPTION
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Number}
		 **/
		Expo: Expo,

		/**
		 * DESCRIPTION
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Number}
		 **/
		Circ: Circ,

		/**
		 * DESCRIPTION
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Number}
		 **/
		Sine: Sine,

		/**
		 * DESCRIPTION
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Number}
		 **/
		Back: Back,

		/**
		 * DESCRIPTION
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Number}
		 **/
		Bounce: Bounce,

		/**
		 * DESCRIPTION
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Number}
		 **/
		Elastic: Elastic
	});

	/**
	 * Adds another Transition to the lookup list
	 * @method module:fx.Transition#Quad
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 **/

	 /**
	  * Adds another Transition to the lookup list
	  * @method module:fx.Transition#Cubic
	  * @param {TYPE} NAME ...
	  * @param {TYPE} NAME ...
	  **/

	  /**
	   * Adds another Transition to the lookup list
	   * @method module:fx.Transition#Quart
	   * @param {TYPE} NAME ...
	   * @param {TYPE} NAME ...
	   **/

	   /**
		* Adds another Transition to the lookup list
		* @method module:fx.Transition#Quint
		* @param {TYPE} NAME ...
		* @param {TYPE} NAME ...
		**/

	additional = ['Quad', 'Cubic', 'Quart', 'Quint'];

	for( var x = 0; x < additional.length; x++ ){
		Transitions[  additional[ x ] ] = new Transition(function(p){
			return Math.pow(p, x + 2);
		});        
	}

	return Transitions;

});