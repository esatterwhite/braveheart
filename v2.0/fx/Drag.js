/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Allows for the dragging of HTML DOM Dlements
 * @module fx/Drag
 * @author Eric Satterwhite
 * @requires module:class.Events
 * @requires module:class.Options
 **/
define(
	["require", "exports", "module", "class", "core", "useragent", "dom", "array"]
	, function(require, exports, module, Class, core, useragent, dom, array){


	var  typeOf = core.typeOf
		,Drag;

	/**
	 * Makes a DOM element draggable
	 * @class module:fx/Drag.Drag
	 * @class module:fx.Drag
	 * @mixes module:class.Events
	 * @mixes module:class.Options
	 * @param {String|module:element.Element} element an element instance or an id of the element to attach to 
	 * @param {Object} [options] Configuration object for the drag instance
	 * @param {Number} [options.snap=6] instance options
	 * @param {String} [options.unit='px'] instance options
	 * @param {Boolean} [options.grid=false] instance options
	 * @param {Boolean} [options.style=true] instance options
	 * @param {Boolean} [options.limit=false] instance options
	 * @param {Boolean} [options.handle=false] instance options
	 * @param {Boolean} [options.invert=false] instance options
	 * @param {Boolean} [options.preventDefault=false] instance options
	 * @param {Boolean} [options.stopPropogation=false] instance options
	 * @param {Object} [options.modifiers={x:'left', y:'top'}] instance options
	 * @example braveheart(['fx'], function( fx ){
	 var d = new fx.Drag("dragger", {unit:'px'});

})*/
	Drag = new Class(/** @lends module:fx/Drag.Drag.prototype */{


		Implements: [Class.Events, Class.Options],

		options: {/*
			onBeforeStart: function(thisElement){},
			onStart: function(thisElement, event){},
			onSnap: function(thisElement){},
			onDrag: function(thisElement, event){},
			onCancel: function(thisElement){},
			onComplete: function(thisElement, event){},*/
			snap: 6,
			unit: 'px',
			grid: false,
			style: true,
			limit: false,
			handle: false,
			invert: false,
			preventDefault: false,
			stopPropagation: false,
			modifiers: {x: 'left', y: 'top'}
		},

		initialize: function(){
			var params = array.link(arguments, {
				'options': function( obj ){
					return typeOf( obj ) == "object";
				},
				'element': function(obj){
					return obj != null;
				}
			});

			this.subject = dom.id(params.element);
			this.document = this.subject.getDocument();
			this.setOptions(params.options || {});
			var htype = typeOf(this.options.handle);
			this.handles = ((htype == 'array' || htype == 'collection') ? dom.$$(this.options.handle) : dom.id(this.options.handle)) || this.subject;
			this.mouse = {'now': {}, 'pos': {}};
			this.value = {'start': {}, 'now': {}};

			this.selection = (useragent.isIE) ? 'selectstart' : 'mousedown';


			if (useragent.ie && !Drag.ondragstartFixed){
				document.node.ondragstart = function( ){ return false; };
				Drag.ondragstartFixed = true;
			}

			this.bound = {
				start: this.start.bind(this),
				check: this.check.bind(this),
				drag: this.drag.bind(this),
				stop: this.stop.bind(this),
				cancel: this.cancel.bind(this),
				eventStop: function(){}
			};
			this.attach();
		},

		/**
		 * adds the mouse events to the handels if their are any
		 * @return {Drag} the drag instance
		 **/
		attach: function(){
			this.handles.addEvent('mousedown', this.bound.start);
			return this;
		},

		/**
		 * Removes the mosue events from the handels if their ar any
		 * @return {Drag} The Drag instance
		 **/
		detach: function(){
			this.handles.removeEvent('mousedown', this.bound.start);
			return this;
		},

		/**
		 * does the set up work for the Drag bahavior
		 * @param {Event} event the event from mouse
		 **/
		start: function(event){
			var options = this.options;

			if (event.rightClick){
				return;
			}

			if (options.preventDefault){
				event.preventDefault();
			}

			if (options.stopPropagation){
				event.stopPropagation();
			}

			this.mouse.start = event.page;

			this.fireEvent('beforeStart', this.subject);

			var limit = options.limit;
			this.limit = {x: [], y: []};

			var z, coordinates;
			for (z in options.modifiers){
				if (!options.modifiers[z]){
					continue;
				}

				var style = this.subject.getStyle(options.modifiers[z]);

				// Some browsers (IE and Opera) don't always return pixels.
				if (style && !style.match(/px$/)){
					if (!coordinates){
						coordinates = this.subject.getCoordinates(this.subject.getOffsetParent());
					}
					style = coordinates[options.modifiers[z]];
				}

				if (options.style){
					this.value.now[z] = parseInt(style || 0, 10);
				}else{
					this.value.now[z] = this.subject[options.modifiers[z]];
				}

				if (options.invert){
					this.value.now[z] *= -1;
				}

				this.mouse.pos[z] = event.page[z] - this.value.now[z];

				if (limit && limit[z]){
					var i = 2;
					while (i--){
						var limitZI = limit[z][i];
						if (limitZI || limitZI === 0){
							this.limit[z][i] = (typeof limitZI == 'function') ? limitZI() : limitZI;
						}
					}
				}
			}

			if (typeOf(this.options.grid) == 'number') this.options.grid = {
				x: this.options.grid,
				y: this.options.grid
			};

			var events = {
				mousemove: this.bound.check,
				mouseup: this.bound.cancel
			};
			events[this.selection] = this.bound.eventStop;
			this.document.addEvents(events);
		},

		/**
		 * Does the checking for the snapping function
		 * @param {Event} event the event from the drag start
		 **/
		check: function(event){
			if (this.options.preventDefault){
				event.preventDefault();
			}
			var distance = Math.round(Math.sqrt(Math.pow(event.page.x - this.mouse.start.x, 2) + Math.pow(event.page.y - this.mouse.start.y, 2)));
			if (distance > this.options.snap){
				this.cancel();
				this.document.addEvents({
					mousemove: this.bound.drag,
					mouseup: this.bound.stop
				});
				this.fireEvent('start', [this.subject, event]).fireEvent('snap', this.subject);
			}
		},

		/**
		 * Does the works of updating element styles with the mouse position
		 * @param {Event} event The dom event triggered by the mouse movement
		 **/
		drag: function(event){
			var options = this.options;

			if (options.preventDefault){
			 event.preventDefault();
			}
			this.mouse.now = event.page;

			for (var z in options.modifiers){
				if (!options.modifiers[z]) continue;
				this.value.now[z] = this.mouse.now[z] - this.mouse.pos[z];

				if (options.invert){
					this.value.now[z] *= -1;
				}

				if (options.limit && this.limit[z]){
					if ((this.limit[z][1] || this.limit[z][1] === 0) && (this.value.now[z] > this.limit[z][1])){
						this.value.now[z] = this.limit[z][1];
					} else if ((this.limit[z][0] || this.limit[z][0] === 0) && (this.value.now[z] < this.limit[z][0])){
						this.value.now[z] = this.limit[z][0];
					}
				}

				if (options.grid[z]){
					this.value.now[z] -= ((this.value.now[z] - (this.limit[z][0]||0)) % options.grid[z]);
				}

				if (options.style){
					this.subject.setStyle(options.modifiers[z], this.value.now[z] + options.unit);
				} else{
				 this.subject[options.modifiers[z]] = this.value.now[z];
				}
			}

			this.fireEvent('drag', [this.subject, event]);
		},

		/**
		 * Stops the dom element from moving
		 * @param {Event} The original mouseevent
		 **/
		cancel: function(event){
			this.document.removeEvents({
				mousemove: this.bound.check,
				mouseup: this.bound.cancel
			});
			if (event){
				this.document.removeEvent(this.selection, this.bound.eventStop);
				this.fireEvent('cancel', this.subject);
			}
		},

		/**
		 * effectively stops the dragging behavior
		 * @param {Event} the original Dom event from the mouse
		 **/
		stop: function(event){
			var events = {
				mousemove: this.bound.drag,
				mouseup: this.bound.stop
			};
			events[this.selection] = this.bound.eventStop;
			this.document.removeEvents(events);
			if (event) this.fireEvent('complete', [this.subject, event]);
		}


	});

	return Drag;
});
