/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

// ## Base FX

// The fx module's Base class, despite what the name may imply, does not do any visual effects.
// What it **does** do, is interpolate values along a mathematical curve for 2 know points. On an
// interval determined by the configuration settings, the next number in the sequence will be generated and returned.
// This provides the basic building block for Animations ( fx ), however, it is not bound to that purpose
/**
 * Base Driver class for FX module, which interpolates values along a mathematical curve
 * @module module:fx.Base
 * @author Eric Satterwhite
 * @requires class
 * @requires string
 * @requires array
 * @requires functools
 * @requires module:fx/Transitions
 * @version 0.1
 **/
define(function(require, exports){
		var  Class   = require( 'class' )
		,array       = require( 'array' )
		,functools   = require( 'functools' )
		,Transitions = require("./Transitions")
		,Events      = Class.Events
		,Options     = Class.Options
		,Chain       = Class.Chain
		,Transition
		,instances
		,timers
		,loop
		,pullInstance
		,pushInstance
		,compute
		,Base
 
	instances = {};
	timers = {};

	// `compute` determines the next value in the sequence
	compute = function( from, to , delta){
		return (to - from) * delta + from;
	}

	// `loop` is the function that is responsible for
	// calling the `step` function on the `Base` instance that have been pushed onto
	// the FX Instance Array. It passes in a new Date stamp
	loop = function(){
		var now  = (+ new Date() );

		for( var x = this.length; x--; ){
			var instance = this[x];
			if( instance){
				instance.step( now )
			}
		}
	};

	// We stash instances of the class in an array under a key that is the FPS for the instance.
	// Push / Pull instance are helper functions to get / stash instances in memory.
	// Pulling away from the class helps compilers get at them.
	pushInstance = function( fps ){
		var list = instances[ fps ] || ( instances[fps] = [] );

		list.push( this );
		if( !timers[fps] ){
			// This is where the interval is set up for the `FX` instance
			timers[fps] = functools.periodical(
							loop
							,Math.round( 1000/fps), list )
		}

	};

	pullInstance = function( fps ){
		var list = instances[ fps ];

		if( list){
			array.erase( list, this );
			if( !list.length && timers[fps]){
				delete instances[fps];
				timers[fps] = clearInterval( timers[fps] )
			}
		}
	}

	/**
	 * Base Class for Mathematical value interpolation
	 * @class module:fx.Base
	 * @param {Object} options Configuration for the Effect
	 * @param {Number} [options.fps=60] The number of times per second a new value is computed in the mathematical curve
	 * @param {String} [options.unit] The numerical unit to use in the computation. Can be "%" or "px". If not specified, numeric value will be returned
	 * @param {Number|String} [options.duration=500] How long the effect will run. Can be any number, or 'short'(250), 'normal'(500), or 'long' (1000)
	 * @param {String} [options.link="ignore"] determines how calls to the start method will be handled. "ignore" to ignore them. "cancel" to cancel current running effect, or "chain" to que them
	 * @param {String|Transition} [options.transition="sine:in:out"] The transition to use to calculae values. Short hand identifiers are in the format "EQUATION:EASING" i.e. "bounce", "bounce:in", "bounce:out", or "bounce:in:out"
	 * @param {Function} [options.set] Called at every step. Can be overridden for driving any kind of behavior
	 * @param {Number} now The current calculated value on a mathemetical curve
	 */
	// ## Base Class

	// The Base Class for FX is really nothing more than a number cruncher
	// Once configured, it can interpolate a series of number from Point A to Point B
	// across a mathematical curve.
	Base = new Class(/** @lends module:fx.Base.prototype */{
		Implements:[Class.Options, Class.Events, Class.Chain]
		,options:{
			fps:60
			,unit: false
			,duration: 500
			,frames: null
			,frameSkip: true
			,link:"ignore"
			,transition:Transitions.Sine.easeInOut
			,set: function( now ){
				return now
			}
		}

		,initialize: function( options ){
			// subject is a reference to the thing we might be acting on
			// the subject is included in all events that the `Class` may fire.
			this.subject = this.subject || this;

			this.setOptions( options )

		}

		/**
		 * Parses out transition shorthand notation
		 * @private
		 * @method module:fx.Base#getTransition
		 * @return {Transition}
		 **/
		,getTransition: function(){

			var trans = this.options.transition || Transitions.Sine.easeInOut;
			var capitalize = function( str ){
				var s = String( str );
				return s.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
			};

			if (typeof trans == 'string'){
				var data = trans.split(':');
				trans = Transitions;
				trans = trans[data[0]] || trans[ capitalize( data[0] ) ];
				if (data[1]) trans = trans['ease' + capitalize(data[1]) + (data[2] ? capitalize(data[2]) : '')];
			}
			return trans;
		}

		/**
		 * determines the next frame and caculate a value
		 * @method module:fx.Base#step
		 * @param {Number} now a date stampe as an integer
		 **/
		,step: function( now ){
			if( this.options.frameSkip ){
				var diff = ( this.time !=null ) ? (now - this.time) : 0
					,frame = diff / this.frameInterval;

				this.time = now;
			 	this.frame += frame
			} else {
				this.frame++;
			}
			if( this.frame < this.frames ){
				var delta = this.transition( this.frame / this.frames );

				this.set( this.compute( this.from, this.to, delta) );
			} else{
				this.frame = this.frames;
				this.set( this.compute( this.from, this.to, 1) );
				this.stop();
			}
		}

		// #### Set

		// Because Classes are immutable, the only way to override the set method would be to
		// subclass `Base`. To alieviate that over head, the set function just calls the set function
		// found in options. End users can configure  and reconfigure options on a `Class` instance at
		// any time.
		/**
		 * Called at every step. Can be overridden for driving any kind of behavior
		 * @method module:fx.Base#set
		 * @param {Number} now The current calculated value on a mathemetical curve
		 * @return {Number} returns the value passed in
		 **/
		,set: function( now ){
			return this.options.set( now );
		}

		/**
		 * Computes the next value from the transition equation
		 * @method module:fx.Base#compute
		 * @param {Number} from The starting number the computation will run from
		 * @param {Number} to the value the computation will end at
		 * @param {Number} delta amount between each value the equation will setp
		 * @return {Number} The computed value from the transition equation
		 **/
		,compute: function( from, to, delta ){
			return  compute( from, to, delta )
		}

		/**
		 * Used to dertermin if the effect should start
		 * @method module:fx.Base#check
		 * @param {Number} from The starting number the computation will run from
		 * @param {Number} to the value the computation will end at
		 * @return {Boolean} true if the effect can be started
		 **/
		,check: function(){
			if( !this.isRunning() ){
				return true;
			}

			switch( this.options.link ){
				case "cancel" :
					this.cancel();
					return true;
				case "chain":
					this.chain( functools.pass( this.caller, arguments, this ) );
					return false;
			}
			return false;
		}

		// #### Start

		// Given a `from` and `to` value, the start method kicks off
		// the actual number crunching
		/**
		 * Interpolates a series of number between two values on a mathematical curve
		 * @method module:fx.Base#start
		 * @param {Number} from that starting number
		 * @param {Number} to The ending number
		 * @return {module:fx.Base} Current fx instance
		 **/
		,start: function(from, to ){

			if( !this.check( from, to ) ){
				return this;
			}

			this.from = from;
			this.to = to;

			this.frame = (this.options.frameSkip) ? 0 : -1;
			this.time = null;
			this.transition = this.getTransition();

			var frames = this.options.frames
				,fps = this.options.fps
				,duration = this.options.duration;

			this.duration = Base.Durations[ duration ] || parseInt( duration, 10 );

			this.frameInterval = 1000 / fps;
			this.frames = frames || Math.round( this.duration / this.frameInterval);

			this.fireEvent("start", [this, this.subject] );
			pushInstance.call( this, fps );

			return this;
		}

		/**
		 * Called at the end of the fx sequence. If ther eare any methods queued up, the new one will be called
		 * @method module:fx.Base#stop
		 * @return {Base}
		 **/
		,stop: function( ){
			if( this.isRunning() ){
				this.time = null;
				pullInstance.call( this, this.options.fps );
				if( this.frames == this.frame ){
					this.fireEvent( "complete", [this, this.subject] );
					if( !this.callChain() ){
						this.fireEvent("chainComplete", [this, this.subject]);
					} else{

						/**
						 * @name module:fx.Thing#top
						 * @event
						 * @param {FX} The Current FX Instance
						 * @param {Element} the dom.Element if defined
						 */
						this.fireEvent( "stop", [this, this.subject] );
					}
				}
			}

			return this;
		}

		/**
		 * Cancels the current fx, returning it to the start
		 * @method module:fx.Base#cancel
		 * @chainable
		 * @return {Base}
		 **/
		,cancel: function( ){
			if( this.isRunning() ){
				this.time = null;

				pullInstance.call( this, this.options.fps );
				this.frame = this.frames;
				this.fireEvent("cancel", [ this, this.subject ]).clearChain();
			}

			return this;
		}

		/**
		 * Pauses the fx
		 * @method module:fx.Base#pause
		 * @return {Base}
		 **/
		,pause: function(){
			if( this.isRunning() ){
				this.time = null;
				pullInstance.call( this, this.options.fps)
			}
			return this
		}

		/**
		 * Attempts to resume the fx at the last know frame
		 * @method module:fx.Base#resume
		 * @return {Base}
		 **/
		,resume: function( ){
			if( this.isPaused() ){
				pushInstance.call( this, this.options.fps )
			}

			return this;
		}

		/**
		 * Retruns true if the fx is current running
		 * @method module:fx.Base#isRunning
		 * @return {Boolean}
		 **/
		,isRunning: function(){
			var list;

			list = instances[ this.options.fps ];

			return list && array.contains( list, this )
		}

		/**
		 * returns true if the fx is currently paused
		 * @method module:fx.Base#isPaused
		 * @return {Boolean}
		 **/
		,isPaused:function( ){
			return ( ( this.frame < this.frames) && !this.isRunning() );
		}

	});

	// ### Durations

	// Export a couple of predifined durations as a nicety.
	// `short`, `normal`, and `long`. Class defaults to `normal`
	Base.Durations = {
		"short":250
		,"normal":500
		,"long":1000
	};

	Base.compute = compute;
	Base.Transitions = Transitions;
	// Just export the Class itself
	return Base
});
