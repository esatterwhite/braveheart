/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * ads the apbility to apply Base FX to multipl styles of a dom Element
 * @module module:fx.Tween
 * @author Eric Satterwhite
 * @requires Base
 * @requires class
 * @requires color
 * @requires core
 * @requires array
 * @requires object
 * @requires element
 **/
define(function(require, exports, module){
	var  Base = require( './Base' )
		,Class   = require( 'class' )
		,clr     = require( 'color' )
		,core    = require( 'core' )
		,string    = require( 'string' )
		,array   = require( 'array' )
		,object  = require( 'object' )
		,dom = require("dom")
		,element = dom
		,typeOf  = core.typeOf
		,Color   = clr.Color
		,selectorCache = {}
		,Parsers
		,Tween
		,computeOne;


	computeOne = function(from, to, delta) {
		var computed = [];
		var times = Math.min(from.length, to.length);
		for (var i = 0; i < times; i++) {
			computed.push({
				value: from[i].parser.compute(from[i].value, to[i].value, delta),
				parser: from[i].parser
			});
		}
		computed.isComputed = true;
		return computed;
	}

	/**
	 * holds the parsers for the tween class
	 * @namespace module:fx.Tween.Parsers
	 */
	Parsers = /** @lends module:fx/Tween.Parsers */{

		/**
		 * handles numeric input to the tween class
		 * @type Object
		 * @return {Object}
		 **/
		"Number": {

			parse: parseFloat,
			compute: Base.compute,
			serve: function(value, unit) {
				return (unit) ? value + unit : value;
			}
		},

		/**
		 * Parses color input to the tween class
		 * @type Object
		 * @return {Object}
		 **/
		"Color": {
			parse: function(value) {
				try {
					return new Color(value);
				} catch (notAColor) {
					return false;
				}
			},
			compute: function(from, to, delta) {
				return array.map( array.from(from), function(value, i) {
					return Math.round(Base.compute(from[i], to[i], delta));
				});
			},
			serve: function(value) {
				return array.map( value, Number );
			}
		},

		/**
		 * Parses String input
		 * @type Object
		 **/
		"String": {
			parse: function() { return false; },
			compute: function(zero, one) {
				return one;
			},
			serve: function(zero) {
				return zero;
			}
		}

	};

	/**
	 * Applies fx/Base to animate styles of a dom.Element
	 * @class module:fx.Tween
	 * @extends module:fx.Base
	 * @example var x = new fx.Tween({});
	 */
	Tween = new Class(/** @lends module:fx.Tween.prototype */{
		Extends:Base

		,initialize: function( el, options ){
			this.subject = dom.id(el);
			this.parent(options);

		}

		/**
		 * Function run at every step of the transition
		 * @method module:fx.Tween#set
		 * @param {Number} now Date stamp as an integer
		 * @return {Tween} the current tween instance
		 **/
		,set: function set(now) {
			if( typeOf( now ) === "string" ){
				properties = this.search( now );
			}
			if (typeOf(now) === 'object') {
				for (var p in now) {
					this.render(this.subject, p, now[p], this.options.unit);
				}
			} else {
				now = this.search( now )
				var property = this.property || this.options.property;
				this.render(this.subject, property, now, this.options.unit);
			}
			return this;
		}

		/**
		 * Starts the animation on one or multiple properties
		 * @method module:fx.Tween#start
		 * @param {String|Object} properties
		 * @param {String|Number} from
		 * @param {String|Number} to
		 **/
		,start: function start(properties, from, to) {
			if (!this.check(properties, from, to)) {
				return this;
			}
			var parsed;
			if( typeOf( properties ) === "string" ){
				properties = this.search( properties );
			}
			if (typeOf(properties) === 'object') {
				from = {};
				to = {};
				for (var p in properties) {
					parsed = this.prepare(this.subject, p, properties[p]);
					from[p] = parsed.from;
					to[p] = parsed.to;
				}
			} else {
				var args = array.from(arguments);
				this.property = this.options.property || args.shift();
				parsed = this.prepare(this.subject, this.property, args);
				from = parsed.from;
				to = parsed.to;
			}
			return this.parent(from, to);
		}

		/**
		 * Create the to / from object
		 * @private
		 * @method module:fx.Tween#prepare
		 * @param {Element} element The dom.Element to act on
		 * @param {String} property The dom property to act on
		 * @param {Array} values the to / from values to usein the animation
		 * @return {Object} the object containing the to / from computed values
		 **/
		,prepare: function(element, property, values){
			values = array.from(values);
			var from = values[0]
				, to = values[1]
				,colorTest ;

			if (to == null){
				to = from;
				from = element.getStyle(property);

				// properties are split on " "
				// rgb( x, x, x) gets all broken
				// ctest for rgb, and convert to Hex
				colorTest = ((/^rgb/).test( from ))
				from = ( colorTest ) ? string.rgbToHex( from ) : from;

				var unit = this.options.unit;
				// adapted from: https://github.com/ryanmorr/fx/blob/master/fx.js#L299
				if (unit && from && typeof from == 'string' &&
					from.slice(-unit.length) != unit && parseFloat(from) != 0){

					element.setStyle(property, to + unit);
					var value = element.getComputedStyle(property);

					// IE and Opera support pixelLeft or pixelWidth
					if (!(/px$/.test(value))){
						value = element.getStyle[('pixel-' + property)];
						value = string.camelCase( value );

						if (value == null){
							// adapted from Dean Edwards' http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291
							var left = element.getStyle( "left" );
							element.setStyle( "left",  to + unit );

							value = element.getStyle( "pixelLeft" );
							element.setStyle( "left", left) ;
						}
					}
					from = (to || 1) / (parseFloat(value) || 1) * (parseFloat(from) || 0);
					element.setStyle(property, from + unit);
				}
			}
			return {from: this.parse(from), to: this.parse(to)};
		}


		/**
		 * Reads input values and attempts to find the correct parser for the data type found.
		 * @method module:fx.Tween#parse
		 * @param {Array} an array of objects suitable for using in the animation
		 * @return {Object}
		 **/
		,parse: function parse(value) {
			//parses a value into an array

			if (typeof value === 'function') {
				value = value();
			}
			value = (typeof value === 'string') ? value.split(' ') : array.from(value);
			return array.map( value, function(val) {
				val = String(val);
				var found = false;
				object.some(Parsers, function(parser, key) {
					var parsed = parser.parse(val);
					if (parsed || parsed === 0) {
						found = {value: parsed, parser: parser};
						return true;
					}
				});
				found = found || {value: val, parser: Parsers.String};
				return found;
			});
		}

		/**
		 * Computes the next value in the step of the transition
		 * @method module:fx.Tween#compute
		 * @param {String|Number} from The starting point of the tween
		 * @param {String|Number} to The end point of the tween
		 * @param {Number} delta The amount to shift between the two values
		 * @return {Number} the resulting value in the sequence
		 **/
		,compute: function compute(from, to, delta) {
			//computes by a from and to prepared objects, using their parsers.
			if (typeOf(from) === 'object') {
				var now = {};
				for (var p in from) {
					now[p] = computeOne(from[p], to[p], delta);
				}
				return now;
			} else {
				return computeOne(from, to, delta);
			}
		}

		/**
		 * converts a value as a settable attribute for a dom Element
		 * @method module:fx.Tween#serve
		 * @param {String|Number} value the value to set
		 * @param {String} unit The unit to use on the dom properties ( % or px )
		 * @return {String} the resulting value
		 **/
		,serve: function(value, unit) {
			//serves the value as settable
			if (!value.isComputed) {
				value = this.parse(value);
			}
			var returned = [];
			array.each(value, function(bit) {
				returned = returned.concat(bit.parser.serve(bit.value, unit));
			});
			return returned;
		}

		/**
		 * Applies the change to the element
		 * @method module:fx.Tween#render
		 * @param {module:dom.Element} The element to apply the transformation to
		 * @param {module:dom.Element} element The element to apply changes to
		 * @param {String} property The css property to chante
		 * @param {String} value the value to move the property to
		 * @param {String} [unit=px] The unit of measure to move the value by
		 **/
		,render: function(element, property, value, unit) {
			element.setStyle(property, this.serve(value, unit));
		}


		,search: function(selector){
			if (selectorCache[selector]) return selectorCache[selector];
			var to = {}
				, selectorTest = new RegExp('^' + string.escapeRegEx( selector ) + '$')
				, searchStyles
				, ShortStyles
				, Styles;

			ShortStyles = dom.Element.ShortStyles;
			Styles = dom.Element.Styles

			searchStyles = function(rules){
				var selectorText

				array.each(rules, function(rule, i){
					if (rule.media){
						searchStyles(rule.rules || rule.cssRules);
						return;
					}
					if (!rule.style){
						return;
					}

					selectorText = (rule.selectorText) ? rule.selectorText.replace(/^\w+/, function(m){
						return m.toLowerCase();
					}) : null;

					if (!selectorText || !selectorTest.test(selectorText)){
						return;
					}

					object.each(dom.Element.Styles, function(value, style){
						if (!rule.style[style] || dom.Element.ShortStyles[style]) return;
						value = String(rule.style[style]);
						to[style] = ((/^rgb/).test(value)) ? string.rgbToHex(value ) : value;
					});
				});
			};

			array.each(document.styleSheets, function(sheet, j){
				var href = sheet.href;
				if (href && href.contains('://') && !href.contains(document.domain)) return;
				var rules = sheet.rules || sheet.cssRules;
				searchStyles(rules);
			});
			return selectorCache[selector] = to;
		}
	});

	dom.Element.defineGetter("tween", function(){
		var tween = this.retrieve('tween');

		if( !tween ){
			tween = new Tween( this, {link:"cancel"});
			this.store( "tween", tween);
		}

		return tween;
	});

	dom.Element.defineSetter("tween", function( options ){
		this.get('tween').cancel().setOptions( options );
		return this;
	});

	dom.Element.implement("tween", function( props ){
		this.get("tween").start( props );
		return this;
	})

	return Tween;
});
