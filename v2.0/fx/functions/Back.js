/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Back Easing function
 * @module module:fx.functions.Back
 * @author Eric Satterwhite
 **/
define(function(require, exports){
	return function Back(p, x){
		x = x && x[0] || 1.618;
		return Math.pow(p, 2) * ((x + 1) * p - x);
	};
});