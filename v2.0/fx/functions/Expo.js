/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Expo easing function
 * @module module:fx.functions.Expo
 * @author Eric Satterwhite
 **/
define(function(require, exports){
	return function Expo(p){
		return Math.pow(2, 8 * (p - 1));
	};
});