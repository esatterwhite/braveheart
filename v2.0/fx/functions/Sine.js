/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * sine easing function
 * @module module:fx.functions.Sine
 * @author Eric Satterwhite
 **/
define(function(require, exports){
	return function Sine( p ){
		return 1 - Math.cos(p * Math.PI / 2);
	};
});