/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Pow function
 * @module module:fx.functions.Pow
 * @author Eric Satterwhite
 **/
define(function(require, exports){
	return function Pow (p, x){
		return Math.pow(p, x && x[0] || 6);
	};
});