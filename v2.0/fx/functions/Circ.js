/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Circ easing function
 * @module module:fx.functions.Circ
 * @author Eric Satterwhite
 **/
define(function(require, exports){
	return function Circ( p ){
		return 1 - Math.sin(Math.acos(p));
	};
});