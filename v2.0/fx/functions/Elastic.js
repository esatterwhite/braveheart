/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Elastic function
 * @module module:fx.functions.Bounce
 * @author Eric Satterwhite
 **/
define(function(require, exports){
	return function Elastic(p, x){
		return Math.pow(2, 10 * --p) * Math.cos(20 * p * Math.PI * (x && x[0] || 1) / 3);
	}
});