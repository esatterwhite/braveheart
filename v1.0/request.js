/*jshint laxcomma: true, smarttabs: true*/


/**
 * Provides a standard way for making XMLHttpRequests
 *
 * @author Eric Satterwhite
 * @module request
 * @requires core
 * @requires class
 * @requires object
 * @requires json
 * @requires date
 * @requires functools
 */
define(
	['require', 'exports','module', "core", "class", 'object', 'json', "date", "functools", "string", 'crypto/base64', 'crypto/hmac', 'crypto/sha1'  ]
	, function( require, exports, module, core, _Class, object, _json, date, functools, string, base64, hmac, sha1 ){
	var RequestType
		,hasXHR
		,hasProgress
		,Request
		,execute
		,Class;

	Class = _Class.Class;

	RequestType = (function(){
		var XMLHTTP
			,MSXML2
			,MSXML;


		XMLHTTP = function(){
			return new XMLHttpRequest();
		};

		MSXML2 = function(){
			return new ActiveXObject("MSXML2.XMLHTTP");
		};

		MSXML = function(){
			return new ActiveXObject("Microsoft.XMLHTTP");
		};

		return functools.attempt(
			function(){
				XMLHTTP();
				return XMLHTTP;
			}
			,function(){
				MSXML2();
				return MSXML2;
			}
			,function(){
				MSXML();
				return MSXML;
			}
		);
	}());


	hasXHR = !!RequestType;
	hasProgress = ( 'onprogress' in new RequestType() );

	execute = function( text ){
		if( !text )return text;

		if( window.execScript ){
			window.execScript( text );
		} else {
			var script = document.createElement('script');
			script.setAttribute('type', "text/javascript");
			script.text = text;


			// just get the browser to run the code.
			// it doesn't need to stay there!
			document.head.appendChild( script );
			document.head.removeChild( script );
		}
	};

	/**
	   The Base class for making cross browser XMLHttpRequests. Take a single configureation object
	   @class
	   @param {String} user username used for requests that require credentials
	   @param {String} password password used for requests that require credentials
	   @param {Object} data key value pairs to be sent in the body of the request.
	   @param {Object} headers headers to be set on the request
	   @param {Boolean} async set to true to make the request asycronous. false for a syncronous. defaults to true
	   @param {String} format if specified, an additional format will be appended to the data string as eg. format=application/json
	   @param {String} ignore
	   @param {Boolean} urlEncode if set to true, the headers will automatically be set to form-url-encoded and formatted appropriatly
	   @param {String} encoding Set the encoding type for the request. defaults to "utf-8"
	   @param {Boolean} evalScripts will execute any javascript returned in the text response returned in the success response
	   @param {Boolean} evalResponse if automatically try to execute any response if set to true. Defaults to false
	   @param {Number} timeout the time after which to considered that the request has timed out. It is suggested not to set this when dealing with large files and is a known constant.
	   @param {Boolean} noCache if set to true, a cache buster string will be appended to the end of the request url
	   @param {Array} middlware An array of object that have methods <strong>processRequest</strong> or <strong>processRequest</strong> or both. This alows you to automoat the action of processing incomming responses or out going requests.
	   @param {Function} onRequest default event handler for the request event
	   @param {Function} onLoadstart default event handler for the loadstart event
	   @param {Function} onProgress default event handler for the progress event
	   @param {Function} onComplete default event handler for the complete event
	   @param {Function} onCancel default event handler for the cancel event
	   @param {Function} onSuccess default event handler for the success event
	   @param {Function} onFailure default event handler for the failure event
	   @param {Function} onException default event handler for the exception event
	   @param {Function} onTimeout default event handler for the timeout event
	   @example braveheart([ "requrest" ], function(request){
	var xhr = new request.Request({
		method:"post"
		,data:{
			key:"value"
		}
		,onSuccess: function( responseText, responseXML ){
			doMagic( responseText )
		}
		,onFailure: function( xhr ){
			alert("DANGER, WILL ROBINSON!!")
		}
	})

	xhr.send();
})
	 */
	exports.Request = new Class( /** @lends module:request.Request.prototype */ {
		Implements:[_Class.Chain, _Class.Events, _Class.Options],
		OnEvents:true,
		options:{
			user: '',
			password: '',
			url: '',
			data: '',
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
				'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
			},
			async: true,
			format: false,
			method: 'post',
			link: 'ignore',
			isSuccess: null,
			emulation: false,
			urlEncoded: false,
			encoding: 'utf-8',
			evalScripts: false,
			evalResponse: false,
			timeout: 0,
			noCache: false,
			middleware:[]
			/* these are possible event call backs to listen on.
			 * for documentation only
			 * onRequest: function(){},
			 * onLoadstart: function(event, xhr){},
			 * onProgress: function(event, xhr){},
			 * onComplete: function(){},
			 * onCancel: function(){},
			 * onSuccess: function(responseText, responseXML){},
			 * onFailure: function(xhr){},
			 * onException: function(headerName, value){},
			 * onTimeout: function(){},
			 */
		},

		initialize: function( options ){
			this.xhr = new RequestType();

			this.setOptions( options );

			// just a quick reference.
			this.headers = this.options.headers;
		},
		/**
		 * Called when the request state changes
		 */
		onStateChange: function(){
			var xhr = this.xhr;

			/* READY STATE CODES
			 *
			 * unstent = 0
			 * opened = 1
			 * headers recieved = 2
			 * loading = 3
			 * done = 4
			 */
			if( xhr.readyState !=4 || !this.running ){
				return;
			}
			this.running = false;

			this.status = 0;
			Function.attempt(
				function(){
					var status = xhr.status;

					// ya IE somtimes returns a 1223 instead of a 204. WTF?!
					this.status = ( status == 1223 ) ? 204 : status;
				}.bind( this )
			);// end attempt

			xhr.onreadystatechange = function(){};
			if( hasProgress ){
				xhr.onprogress = xhr.onloadstart = function(){};
			}

			clearTimeout( this.timer );

			this.response = {text: this.xhr.responseText || "", xml: this.xhr.responseXML };
			if( this.isSuccess.call( this, this.status )){


				this.success( this.response.text, this.response.xml );
			} else {
				this.failure();
			}
		},
		isSuccess: function(){
			var status = this.status;
			return ( status >= 200 && status < 300);
		},
		isRunning: function(){
			return !!this.running;
		},
		processScripts: function( text ){
			if( !!this.options.evalResponse || (/(eca|java)script/).test(this.getHeader("Content-Type"))){
				return execute( text );
			}

			return string.stripScripts(string, this.options.evalScripts );
		},
		success: function( text, xml ){
			this.onSuccess( this.processScripts( text ), xml);
		},

		onSuccess: function(){
			var mw
				,x;
			for( x = 0; x < this.options.middleware.length; x++){
				mw = this.options.middleware[x];
				if( typeof mw.processResponse === 'function' ){
					mw.processResponse.apply(this, arguments );
				}
			}
			this.fireEvent('complete', arguments).fireEvent( 'success', arguments).callChain();
		},
		failure: function(){
			this.onFailure();
		},
		/**
		 * executed when the request fails
		 * @fires failure
		 */
		onFailure: function(){
			/**
			 * @event failure
			 * @param {XMLHttpRequest} xhr the xhr request
			 */
			this.fireEvent('complete').fireEvent('failure', this.xhr);
		},
		loadstart: function( event ){
			this.fireEvent('loadstart', [ event, this.xhr ] );
		},
		progress: function( event ){
			this.fireEvent('progress', [event, this.xhr] );
		},
		timeout: function( event ){
			this.fireEvent('timeout', this.xhr);
		},

		/**
		 * Sets the value of a given header
		 * @param {String} name The name of the header to set
		 * @param {String} value The value to set the header as
		 * @returns {Request} The current request instance
		 */
		setHeader: function( name, value ){
			this.headers[name] = value;
			return this;
		},

		/**
		 * Retreives the current value of a given header
		 * @param {String} name The name of the header to retreive
		 * @returns {Strign} The current header value
		 */
		getHeader: function( name ){
			return Function.attempt( function(){
				return this.xhr.getResponseHeader( name );
			}.bind(this));
		},
		check: function(){
			if( !this.running ){
				return true;
			}
			// from the chain class
			switch( this.options.link ){
				case "cancel":
					this.cancel();
					return true;
				case "chain":
					this.chain( this.caller.pass( arguments, this ));
					return false;
			}

			return false;
		},

		/**
		 * Serializes request data for transmission
		 * @type function
		 * @param {Object} data The data to serialize
		 * @returns {String} the serialized data
		 */
		encode: functools.protect(function( data ){
			return object.toQueryString( data );
		}),
		/**
		 * executes the current request object
		 * @param {Object} options the options for the request
		 * @returns {Request} the current request
		 */
		send: function( options ){
			var mw;
			if( !this.check(options) ){
				return this;
			}

			this.options.isSuccess = this.options.isSuccess || this.isSuccess;
			this.running = true;

			var type = core.typeOf( options );

			if( type === 'string' || type === 'htmlelement') {
				options ={data: options};
			}

			var old = this.options;
			options  =  object.append( {data:old.data, url:old.url, method: old.method}, options );
			var data = options.data
				,url = String( options.url)
				,method = options.method.toLowerCase();

			switch( core.typeOf( data )){
				case "object":
					data = this.encode( data );
					break;
				case "element":
					data = {};
					break;

			}

			if( options.format ){
				var format = 'format=' +this.options.format;
				data = !!data ? format + "&" + data : format;
			}

			if( this.options.emulation && !( method == 'post' || method == 'put')){
				var _method = '_method=' + method;
				data = !!data ? _method  +"&" + data : _method;
				method = 'post';
			}

			if( this.options.urlEncoded && (method=='post' || method =='put') ){
				var encoding = !!this.options.encoding ? "; charset=" + this.options.encoding : '';
				this.headers["Content-Type"] = 'application/x-www-form-urlencoded' + encoding;
			}

			if( !url ){
				url = document.location.pathname;
			}

			var trimPosition = url.lastIndexOf( '/' );
			if( trimPosition > -1 && ( trimPosition = url.indexOf("#")) > -1 ){
				url = url.substr(0, trimPosition);
			}

			if( this.options.noCache ){
				url += ( url.indexOf('?') > -1 ? '&':'?') + ( (+ new Date() ).toString( 32 ) );
			}

			if( data && method === 'get' ){
				url += ( ( url.indexOf( "?" ) != -1 ) ? "&" : "?") + data;
				data = null;
			}

			var xhr = this.xhr;
			if( hasProgress ){
				xhr.onloadstart = this.loadstart.bind( this);
				xhr.onprogress = this.progress.bind( this );
			}

			for( var x = 0; x < this.options.middleware.length; x++ ){
				mw = this.options.middleware[x];
				if( typeof mw.processRequest === 'function' ){
					mw.processRequest.call( this, this.xhr, options);
				}
			}

			xhr.open( method.toUpperCase(), url, this.options.async, this.options.user, this.options.password );


			if( this.options.user && 'withCredentials' in xhr ){
				xhr.withCredentials = true;
			}

			xhr.onreadystatechange = this.onStateChange.bind( this );

			object.each( this.headers, function( value, key ){
				try{
					xhr.setRequestHeader( key, value );
				} catch( e ){
					this.fireEvent('exception', [key, value]);
				}
			}, this);

			this.fireEvent('request');
			xhr.send( data );
			if(!this.options.async){
				this.onStateChange();
			} else if( this.options.timeout){
				this.timer = setTimeout( this.timeout.bind( this ) ,this.options.timeout );
			}

			return this;

		},

		/**
		 * Cancels the pending request if it exists
		 * @returns {Request} The curret request
		 */
		cancel: function(){
			if(!this.running){
				return this;
			}

			this.running = false;
			this.xhr.abort();
			clearTimeout( this.timer );
			xhr.onreadystatechange = function(){};
			if( hasProgress){
				xhr.onprogress = xhr.onloadstart = function(){};
			}

			this.xhr = new RequestType();
			this.fireEvent('cancel');
			return this;
		}
	}); // End Request


	/**
	 * The Extends Base {@link Request} to work with JSON responses / requests.<br/>
	 * Automatically sends and accepts json and decodes responses. Accepts all the same options as the Base Request Class.
	 * @class
	 * @extends module:request.Request as Request
	 * @param {Boolean} secure If set to true, the module will attempt to validate the json as safe. default to true
	 */
	exports.JSON = new Class( /** @lends module:request.JSON.prototype */{
		Extends:exports.Request
		,options:{
			secure:true
		}
		,initialize: function( options ){
			this.parent( options );
			object.append( this.headers, {
				"Accept":"application/json",
				"X-Request":"JSON",
				"Content-Type":"application/json"
			});
		}
		,encode: function( data ){
			if( this.options.method.toLowerCase() === "get"){
				return object.toQueryString( data );
			}
			try{
				return _json.encode( data );
			} catch( e){
				return object.toQueryString( data );
			}
		}
		,success: function( text ){
			var json;
			try{
				json = this.response.json = _json.decode( text, this.options.secure );
			} catch (e ){
				this.fireEvent('error', [text, e]);
				return;
			}

			if( json == null){
				this.onFailure();
			} else{
				this.onSuccess( json, text );
			}
		}
	});

	/**
	 * A Custom Implementation of the JSON class to interface with the LenderX API via API Key
	 * by handeling the request signing process for you. Accepts all the same optiosn as {@link module:request.JSON}
	 * @class
	 * @extends module:request.JSON as Signed
	 * @param {String} apiKey the api key to use in the request
	 * @param {String} apiSecret The secret to match the API key
	 * @param {String} userID a string to use as the ID for the user account.
	 */
	exports.SIGNED = new Class({
		Extends:exports.JSON
		,options:{
			apiKey:''
			,apiSecret:''
			,userID:''
			,contentType:"application/json"
		}
		,initialize: function( options ){
			this.parent( options );
		}
		,send: function(){
			var sender,
				_parent;
			sender = function(){

				return this.send();
			}.bind( this );

			if(!this.signed){
				this.addEvent( 'signed', sender );
				this.sign();
			} else {
				this.removeEvent('signed', sender );
				this.parent();
			}
		}
		/**
		 * Signs the current request for LenderX API Authentication
		 * @private
		 * @fires event:signed
		 * @returns {Request} The current request object
		 */
		,sign: functools.protect(function(){
			var that = this;
			if(!this.signed){
				var separator = "\n"
					,verb = that.options.method.toUpperCase()
					,currentDate
					,resource
					,arguments
					,contentType
					,signedString
					,signature
					,api_key
					,api_secret;
				api_secret = that.options.apiSecret; // "00531738-14bb-4e8f-8d5b-f33f60e9bf8c"
				api_key =that.options.apiKey; // "57856984-6662-4b50-9755-6928f14ac284"

				currentDate = new Date();
				currentDate = date.format( currentDate, "%a, %d %b %Y %H:%M:%S %Z" );
				resource = that.options.url;
				contentType = "application/json";
				args = object.toQueryString( that.options.data );

				signedString = [
					verb
					,""
					,contentType
					,currentDate
					,resource
					,args
					, !!that.options.userID ? ( "x-cor-auth-userid:" + that.options.userID ) : ""

				].join( separator );

				signature = base64.encode(
					hmac (
						sha1,
						signedString,
						api_secret,
						{asString:true}
					)
				);
				object.merge(that.headers, {
					"X-Date": currentDate
					,"Content-Type":contentType
					,"x-cor-auth-userid":that.options.userID
					,"Authorization": string.substitute(
										"CONE {api_key}:{signature}", {
											api_key:api_key
											,signature:signature
										}
									)
				});
				// date formatter
				//"%a, %d %B %Y %H:%M:%S %Z"
				that.signed = true;
				that.fireEvent( 'signed' );
			} else {
				/**
				 * Fired when the request is signed
				 * @event
				 */
				this.fireEvent( 'signed' );
			}
		})
	});

});
