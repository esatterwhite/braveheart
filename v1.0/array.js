/*jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * AMD Module for workind with arrays.
 * @author Eric Satterwhite
 * @module array
 */
;define(['require', 'exports', 'module'],function( require, exports, module){
	function isEnumerable( item ){
		return (item != null &&  typeof( item.length ) == 'number' && Object.prototype.toString.call(item) != '[object Function]' );
	};
	var cloneOf, api, typeOf;
	typeOf = function( item ){
		return ({}).toString.call(item).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
	};


		/**
		 * creates array whos elements are the result of the supplied function when passed the related element of the supplied arrays
		 * @param  {Function} fn the function use to modify the original arrays
		 * @param  {Array} array An array to iterrate over, each element will be passed to the supplied function
		 * @param  {Array} array2..arrayN<optional> if more than one array its elements will be appened as positional arguments to each function call
		 * @return {Array} the resultant array
		 * @example braveheart(['array'], function( array ){
	var result
	result = array.map( [1,2,3,4,5,6], function( item ){
		return item * 2;
	});

	console.log( result ) //[2, 4, 6, 8, 10, 12]
})

		 * @example braveheart(['array'], function( array ){
	var result;
	result = array.map( [1,2,3,4,5,6] [ 6,5,4,3,2,1 ], function( x, y ){
		return x % 2 === 0 ? ( x/2 ) + y : y;
	});

	console.log( result ) //[ 6, 5, 4, 5, 2, 4]
})
		 */
		exports.map = function(arr, fn /*, [arr2 ... ]*/){
			var array = exports
				,rvalue = []
				,x, x2;

			if( arguments.length <= 2 ){
				arr = array.from( arr );

				// if null is passed as the first argument, just return the array
				if( !fn ){
					return arr;
				}

				for( x = 0; x < arr.length; x++ ){
					rvalue.push( fn( arr[ x ], x ) );
				}
				return rvalue;
			} else {
				if( !fn ){
					fn = Array;
				}
				length = null;
				for( x = 1; x < arguments.length; x++ ){
					var args = [];
					for( x2 = 1; x2< arguments.length; x2++ ){
						args.push( arguments[x2][x] );
					}
					rvalue.push( fn.apply( this, args ) );
				}
				return rvalue;
			}
		};

		/**
		 * Executes a function for every item in an array
		 * @param {Array} array the array to iterate over
		 * @param {Function}fn  the function to execute, it will be passed:<br />
		 * <ul>
		 * 	<li><code>item</code> the current item in the array</li>
		 *   <li><code>index</code> the current index the function is working on</li>
		 *  <li><code> array</code> the original array passed in</li>
		 * </ul>
		 * @param {Object} scope the context in which the function should execute
		 * @example braveheart(['array'], function( array ){
	array.each( ["a" "b", "c","d"], function( item ){
		console.log( item.charCodeAt(0) )
	}) // 97, 98, 99, 100
})
		 */
		exports.each = function(arr, fn, scope){
			scope = scope || this;
			var ret = exports.from( arr )
				,x;

			for( x=0; x<ret.length; x++){
				fn.call(scope, ret[x], x, arr);
			}
			return ret;
		};

		/**
		 * attempts to identify at which index an item exists in an array
		 * @param {Array} array the array to look through
		 * @param {Object} item the item to look for
		 * @param {Number} from [optional] the index to start from
		 * @return {Number} the index number the item was found at, -1 if it is not found
		 */
		exports.indexOf = function( arr, item, from ){
			var len = arr.length >>> 0
				,x;

			for( x = ( from < 0 ) ? Math.max(0, len + from) : from || 0; x < len; x++){
				if( arr[x] === item ) return x;
			}
			return -1;
		};

		/**
		 * Adds items to an array if it does not already exist in the array
		 * @param  {Array} arr the array to ann items to
		 * @param  {Object} item the item to include
		 * @return {Array} arr the resulting array
		 */
		exports.include =  function( arr, item ){
			if( !exports.contains( arr, item ) ) arr.push( item );
			return arr;
		};

		/**
		 * this is the combine function
		 * @param  {Array} arrayA
		 * @param  {Array} arrayB
	`	 * return {Array} A single array with all of the elements from both arrays passed in
		 */
		exports.combine = function( a1, a2, skip ){
			var x,l;
			a2 = exports.from( a2 );
			x=0;
			l = a2.length;
			x  = ( typeof skip === 'number' ) ? skip : 0;

			for(x; x< l; x++) {
				exports.include( a1, a2[x]);
			}

			return a1;
		};

		/**
		 * Creates an array out of the passed in item. If the item is an array, it will just be returned.
		 * @param {Object} item the itme to convert to an array
		 * @return {Array} The resultant array
		 *
		 */
		exports.from = function(item) {

			if(item == null)
				return [];
			if(arguments.length > 1)
				return exports.from(arguments);
			return (isEnumerable(item) && typeof item != 'string') ? (Object.toString(item) == '[object Array]') ? item : Array.prototype.slice.call(item) : [item];

		};

		/**
		 * Determines if an item is in the provided array.
		 * @param  {Array} arr [optional] the array to inspect
		 * @param  {Object} item [optional] the item to look for
		 * @param  {Number} from [optional] the index number to start from
		 * @return {Boolean} true if the item was found in the array
		 */
		exports.contains = function(arr, item, from) {
			return exports.indexOf(arr, item, from ) != -1;
		};

		/**
		 * Returns an array with no duplicate values
		 * @param {Array} array the array to work on
		 */
		exports.unique = function(arr) {
			return exports.combine([], arr);
		};

		/**
		 * Filters values ouot of an array
		 * @param {Array} array The array to filter
		 * @param {Function} fn A function used to filter the items in the array. If it returns true, the item will be included. The function will be passed the following </br>
		 * <ul>
		 *    <li>item - The current itme to inspect</li>
		 *    <li>index - the current index in the array</li>
		 *    <li>array - the original array passed in</li>
		 * </ul>
		 */
		exports.filter = function(arr, fn, bind) {
			var results = []
				,i
				,l;
			for(i = 0, l = arr.length; i < l; i++) {
				if(( i in arr) && fn.call(bind, arr[i], i, arr))
					results.push(arr[i]);
			}
			return results;
		};

		/**
		 * Remove all items that are null or undefined
		 * @param {Array} array the array to clean
		 * @return {Array} An array void of invalid elements
		 */
		exports.clean = function( arr ) {
			return exports.filter(arr, function( item ) {
				return item != null;
			});
		};

		/**
		 * takes two arrays and builds key /value pairs from them.
		 * @param {Array} keys array used as the keys for the resultant object
		 * @param {Array} values array used as the values
		 * @return {Object} result An object of key/value pairs derived from the passed in arrays
		 * @example braveheart(['array'], function( array ){
	var result = array.associate(
					["number", "object", "array"],
					[1, {key:"value"}, [1, 2, 3]]
				);

	console.log( result ) // {number: 1, object:{key:"value"}, array:[1,2,3]}
})
		 */
		exports.associate = function( keys, values ){
			var obj = {}
				,length = Math.min( values.length, keys.length );

				for( var x = 0; x < length; x++){
					obj[keys[x]] = values[x];
				}

				return obj;
		};

		/**
		 * empties out the passed in array. Modifies the original array.
		 * @param {Array} arr the array to empty
		 * @param {Boolean} force if true elements in the array will manually be deleted. if false, the length property will be set to 0
		 */
		exports.empty = function(arr, force) {
			var i = arr.length;

			if(force) {
				for(i; i--; ) {
					delete arr[i];
				}
				return arr;
			}
			arr.length = 0;
			return arr;
		};
		/**
		 * unconditionally pushes array to the end of another.
		 * @param {Array} source The host array to receive additional items
		 * @param {Array} dest The array of items to append to the host
		 * @return {Array} array an array with the elements of the two arrays combined
		 * @example braveheart(['array'], function( array ){
	var a = [1,2,3];

	var b = array.append( a, [4,5,6] );

	console.log( a, b )// [1, 2, 3], [1, 2, 3, 4, 5, 6]
})
		 */
		exports.append = function( source, dest ) {
			var cln = exports.from( source );
			cln.push.apply( cln, dest );
			return cln;
		};

		/**
		 * creates a clone of the current array
		 * @return {Array}
		 */
		exports.clone = function( arr ) {
			var len, clone;
			len = arr.length;
			clone = new Array(len);
			while(len--) {
					clone[len] = arr[len];
			}
			return clone;
		};
		/**
		 * Returns the last item in an array
		 * @param  {Array} arr the array to retrive an itme from
		 * @return  {Object} item the last item of the array if one exists
		 */
		exports.getLast = function( arr ) {
				return (arr.length ) ? arr[arr.length - 1] : null;
		};

		/**
		 * Attempts to return a random element out of the given array
		 * @parm  {Array} arr And array to extract a random item from
		 * @return {Object} an object from the array
		 *
		 */
		exports.getRandom = function( arr ){
			var rand = Math.floor( Math.random() * ( arr.length - 1 )  );
			return ( arr.length ) ? arr[ rand ] : null;
		};

		/**
		 * Returns a flattened (one-dimensional) copy of the passed array
		 * @param  {Array} arr the array to flatten
		 */
		exports.flatten = function( arr ){
			var rvalue = new Array
				,x
				,len
				,_type;
			for( x = 0, len = arr.length; x < len; x++ ){
				_type = typeOf( arr[x] );
				if( arr[x] == null ){
					continue;
				}
				rvalue = rvalue.concat( ( _type == "array" || _type =="arguments" || arr[x] instanceof Array ) ? exports.flatten(arr[x]) : arr[x]);
			}
			return rvalue;
		};


		exports.max = function( arr, fn ){
	        if (arr.length && !fn) {
	            return Math.max.apply(Math, arr);
	        } else if (!arr.length) {
	            return Infinity;
	        } else {
	            var result,
	                compare = -Infinity,
	                tmp;
	            exports.each(arr, function(val, i, list){
	                tmp = fn(val, i, list);
	                if (tmp > compare) {
	                    compare = tmp;
	                    result = val;
	                }
	            });
	            return result;
	        }
		};

		exports.min = function( arr, fn ){
	        if (arr.length && !fn) {
	            return Math.min.apply(Math, arr);
	        } else if (!arr.length) {
	            return -Infinity;
	        } else {
	            var result,
	                compare = Infinity,
	                tmp;
	            exports.each(arr, function(val, i, list){
	                tmp = fn(val, i, list);
	                if (tmp < compare) {
	                    compare = tmp;
	                    result = val;
	                }
	            });
	            return result;
	        }
		};

		/**
		 * converts an array of [RR, GG, BB] values to [H, S, B] values
		 */
		exports.rgbToHsb = function( arr ){
			var red = arr[0],
					green = arr[1],
					blue = arr[2],
					hue = 0;
			var max = Math.max(red, green, blue),
					min = Math.min(red, green, blue);
			var delta = max - min;
			var brightness = max / 255,
					saturation = (max != 0) ? delta / max : 0;
			if (saturation != 0){
				var rr = (max - red) / delta;
				var gr = (max - green) / delta;
				var br = (max - blue) / delta;
				if (red == max) hue = br - gr;
				else if (green == max) hue = 2 + rr - br;
				else hue = 4 + gr - rr;
				hue /= 6;
				if (hue < 0) hue++;
			}
			return [Math.round(hue * 360), Math.round(saturation * 100), Math.round(brightness * 100)];
		};

		/**
		 * converts an array of [H, S, B] values to [RR, GG, BB] values
		 */
		exports.hsbToRgb = function( arr ){
			var br = Math.round(arr[2] / 100 * 255);
			if (arr[1] == 0){
				return [br, br, br];
			} else {
				var hue = arr[0] % 360;
				var f = hue % 60;
				var p = Math.round((arr[2] * (100 - arr[1])) / 10000 * 255);
				var q = Math.round((arr[2] * (6000 - arr[1] * f)) / 600000 * 255);
				var t = Math.round((arr[2] * (6000 - arr[1] * (60 - f))) / 600000 * 255);
				switch (Math.floor(hue / 60)){
					case 0: return [br, t, p];
					case 1: return [q, br, p];
					case 2: return [p, br, t];
					case 3: return [p, q, br];
					case 4: return [t, p, br];
					case 5: return [br, p, q];
				}
			}
			return false;
		};


		exports.hexToRgb = function(arr){
			if (arr.length != 3) return null;
			var rgb = api.map(arr, function(value){
				if (value.length == 1){
					value += value;
				}
				return parseInt(value, 16);
			});
			return (arr) ? rgb : 'rgb(' + rgb + ')';
		};

		exports.rgbToHex = function(arr){
			if (arr.length < 3) return null;
			if (arr.length == 4 && arr[3] == 0 && !arr) return 'transparent';
			var hex = [];
			for (var i = 0; i < 3; i++){
				var bit = (arr[i] - 0).toString(16);
				hex.push((bit.length == 1) ? '0' + bit : bit);
			}
			return (arr) ? hex : '#' + hex.join('');
		};

});
