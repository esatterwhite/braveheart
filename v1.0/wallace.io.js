if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

define(["require", "exports", "module", "core", "asstes", "class", "cookie"], function(require, exports,module, core, assets, _class, cookie ){
	var Class = _class.Class
		,Options = _class.Options
		,Events  = _class.Events
		,Storage = _class.Storage
		,Session
		,InvalidSession;


	Invalid.InvalidSession = InvalidSession = new core.error.NamedError("InvalidSession", "Invalid or missing session ID")

	Session = new Class({
		Implements:[Options, Events, Storage]
		,options:{

		}
		,initialize: function( sid, options ){
			if( !sid ){
				throw InvalidSession;
			}

			this.store("session", sid )
			this.store("cookie", cookie.write("session", sid) )

		}
		,genUUID: functools.protect(function( ){
			var S4 = function() {
			   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
			}

		   return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());

		})
	})

	exports.Client = new Class({
		Implements:[ Events, Options ]
		,options:{

		}
		,initialize: function(){

		}
	})
})
