if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 *
 * AMD module for logging function calls. Intended for use with require.js ( http://www.requirejs.org )
 * inspired by Mootools Log ( https://github.com/mootools/mootools-more/blob/1.2x/Source/Core/Log.js )
 * and js-klib ( http://code.google.com/p/js-klib/source/browse/klib.js )
 * @author Eric Satterwhite
 * @module log
 */
define(['require','exports', 'module'],function(require, exports, module){
	var has_log =  (function(){
						var has_console
						try{
							var has_console = typeof console != null;
							console.log();
						} catch ( e ){
							return false;
						}

						return has_console;
					}());
	var _logged = [];
	var api = {};
	var enabled = true
	 
	 
	/**
	 * Although all named function have a name property, the $debug decorator wraps functions with an anonymous
	 * function. namedWithInner returns a wrapped function with the same name as the function it wrapped so we can
	 * still identify the function
	 * @method namedWithInner
	 * @private
	 * @param _fn {Function} the function to wrap
	 * @param orig {Function} the original function the be attatched to the wrapping function
	 * @param prfix {String} the prefix to add to the name of the function
	 * @return augmented {Function} the wrapped and augmented function
	 */
	function namedWithInner( _fn, orig, prefix ){
	 
		//get the name of the original function
		var name = getFnName( orig );
	 
		if( prefix ){
			name = prefix + name;
		}
	 
		// augment the wrapper function with the name of the original
		// and a ref to the original function
		return augment( _fn, {name:name, inner:orig});
	};
	 
	/**
	 * Adds properties to an function or object
	 * @private
	 * @method augment
	 * @param _fn {Object} the object to add properties to
	 * @param obj {Object} object containing the properties to add
	 * @return _fn {Object} Returns the augmented object
	 */
	function augment( _fn, obj ){
		var key;
	 
		for( key in obj ){
			if( obj.hasOwnProperty( key ) ){
				_fn[key] = obj[key]
			}
		}
		return _fn
	};
	 
	/**
	 * Attempts to return the name of a function
	 * @method getFnName
	 * @private
	 * @param fn {Function} A function to retrive a name from
	 * @return name {String} the name of the function, `anonymous` if one is not found
	 */
	function getFnName( fn ){
		return ( fn.name || 'anonymous' );
	};
	 
	/**
	 * returns a wrapped function which attempts to log every function call
	 * @method loggingWrapped
	 * @private
	 * @param _fn   {Function} The function to wrap
	 * @return func {Function} The new function
	 */
	function loggingWraped(_fn, action ){
		action = action || "log";
		return namedWithInner(function(){
			// run the original function and stash the results
			var result = _fn.apply( this, arguments );
	 
			//log the name of the function with the arguments passed and the results
			exports[action](getFnName( _fn ), "( ", arguments , " ) = ", result );
	 
			//return the results as usual
			return result;
		}, _fn, "debug_");
	};
	 
	/**
	 * A wrapper around the "console" object to normalize function calls
	 * @method log
	 * @private
	 */
	function log( ){
		var args = Array.prototype.slice.call( arguments );
		var fnName = args.shift();
		if( has_log && enabled) {
			try{
				console[fnName].apply(console, args[0]);
			} catch ( e ){
				console.log( args[0] );
			}
		} else {
			_logged.push( arguments );
		}
	}
	 
	 
	/**
	 * enables the logging functionality and immediatly logs any back logs
	 * @method enableLog
	 * @private
	 */
	function enableLog( ){
		var x;
		enabled = true;

		for( x = 0; x < _logged.length; x++ ){
			log.apply( this, _logged[ x ] )
		}
		_logged = [];
		return exports;
	};
	/**
	 * disables the logging feature. places any calls to log in a back log queue
	 * @method disableLog
	 * @private
	 */
	function disableLog( ){
		enabled = false;
		return exports;
	};
 
	 /**
	  * Logs a message using the consoles log function if the log is enabled.
	  * If there is no console or it is disabled, messages set to the log function will be queued
	  * in memory until the console becomes available or the log module is enabled
	  * @param {Misc} data log can accept any number of arguments with a varying type
	  * @return {Module} log the log module api object.
	  */
	 exports.log = function(){
		log('log', arguments );
		return this;
	}
	 /**
	  * Logs a message using the consoles warn function if the log is enabled.
	  * If there is no console or it is disabled, messages set to the log function will be queued
	  * in memory until the console becomes available or the log module is enabled
	  * @param {Misc} data warn can accept any number of arguments with a varying type
	  * @return {Module} log the log module api object.
	  */
	exports.warn  = function(){
		log('warn', arguments);
		return this;
	}
	 /**
	  * Logs a message using the consoles error function if the log is enabled.
	  * If there is no console or it is disabled, messages set to the log function will be queued
	  * in memory until the console becomes available or the log module is enabled
	  * @param {Misc} data error can accept any number of arguments with a varying type
	  * @return {Module} log the log module api object.
	  */
	exports.error = function(){
		log('error', arguments);
		return this
	}
	 /**
	  * Logs a message using the consoles debug function if the log is enabled.
	  * If there is no console or it is disabled, messages set to the log function will be queued
	  * in memory until the console becomes available or the log module is enabled
	  * @param {Misc} data debug can accept any number of arguments with a varying type
	  * @return {Module} log the log module api object.
	  */
	exports.debug = function(){
		log('debug', arguments);
		return this;
	}
	 /**
	  * Logs a message using the consoles info function if the log is enabled.
	  * If there is no console or it is disabled, messages set to the log function will be queued
	  * in memory until the console becomes available or the log module is enabled
	  * @param {Misc} data info can accept any number of arguments with a varying type
	  * @return  {Module} the log module api object.
	  */
	exports.info = function(){
		log('info', arguments);
		return this;
	}
	 /**
	  * Logs a message using the consoles dir function if the log is enabled.
	  * If there is no console or it is disabled, messages set to the log function will be queued
	  * in memory until the console becomes available or the log module is enabled
	  * @param {Misc} data info can accept any number of arguments with a varying type
	  * @return  {Module} the log module api object.
	  */
	exports.dir = function(){
		log('dir', arguments);
		return this;
	}

	/**
	 * A wrapper around the log function that understands objects with repr and custom toString function
	 * @param  {Mics} data print can accept any number of arguments of any type. It will pretty print items that implement repr or toString
	 * @return {Module} the log module
	 * @example braveheart([ 'log', 'class' ], function( log, _class ){
 	var Class = _class.Class;
	var MyClass  = new Class({
		initialize: function( val ){
			this.value = val;
		}
		,repr: function( ){
			return "&lt;Myclass: " + this.value + "&gt;"
		}
	});

	var m = new MyClass( "Hello world")

	console.log( m ) // b.Class.c
	log.print( m ) // &lt;Myclass: Hello world &gt;
})
	 */
	exports.print = function( ){
		var args
			,x

		args = Array.prototype.slice.call( arguments );

		for( x =0; x < args.length; x++ ){
			if( args[ x ].hasOwnProperty( 'toString' ) && typeof args[ x ].toString == 'function'){
				args [ x ] = args[ x ].toString()
			} else if( args[ x ].repr && typeof args[ x ].repr == 'function'  ){
				args[ x ] = args[ x ].repr()
			}
		}
		log('log', args);
		return this;
	}
	/**
	 * Effectively enables the logger. Any queued calls will immediatly be executed in the order they were called.
	 * All subsequent calls will be pushed to the console rather than queued
	 * @return {Module} The log module api object
	 */
	exports.enable = function(){
		enableLog();
		return this;
	}
 
	/**
	 * Effectively disables the logger. When one of the api logging functions is called
	 * All calls will automatically be queued for later execution
	 * @return {Module} The log module api object.
	 */
	exports.disable = function(){
		disableLog();
		return this;
	}

	/**
	  * A decorator that, when given an object, will attempt to log out
	  * all successful function calls from that object via the consoles debug method.
	  * @param {Object} obj The object whos methods you want to debugged
	  * @return {Object} returns the module object
	  * @example braveheart(['log'], function( log ){
	var customobject = {
		dummy: function( x ,y ){
			return ( x + y ) * 2;
		}
		,test: function test( a ,b ){
			return a * b
		}
	}

	log.$debug( customobject );

	customobject.dummy( 34, 54 );
	customobject.test( 99, 11 )

	// anonymous ( [ 34, 54 ] ) = 176
	// test ( [ 99, 11 ] ) = 1089
});
	 */
	exports.$debug = function( obj ){
		var  key
 
		for( key in obj ){
			if( obj.hasOwnProperty( key ) ){
				if( typeof obj[key] =='function' ){
					obj[key] = loggingWraped( obj[key], 'debug' );
				}
			}
		}
		return this;
	} 
 
});
