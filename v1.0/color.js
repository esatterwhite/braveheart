/*jshint laxcomma:true, smarttabs:true*/
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * An ADM module for performing complex color operations
 * @module color
 * @requires core
 * @requires accessor
 * @requires array
 */
define(['require','exports','module', 'core', "accessor", "array",'string' ], function( require, exports, module, core, accessor, array, string ){
    var Type = core.Type
        ,Color
        ,limit
        ,listMatch = /([\-.\d]+)\s*,\s*([\-.\d]+)\s*,\s*([\-.\d]+)\s*,?\s*([\-.\d]*)/
        ,hexMatch = /^#?([a-f0-9]{1,2})([a-f0-9]{1,2})([a-f0-9]{1,2})([a-f0-9]{0,2})$/i
        ,parsers
        ,stringify;
    limit = function( num, min, max){
        return Math.min( max, Math.max( min, num ) );
    };

    //converts an array of numbers to a color declaration - > rgba( 0, 0, 0, 0 )
    stringify = function( type, arr ){
        if( arr[3] !== 1 ){
            type += "a";
        } else {
            arr.pop();
        }
        return type + ( '(' + arr.join(', ') + ")");
    };

    parsers = {

        parseRGB: function(color){
            var matches;

            matches = color.match( listMatch ).slice(1);

            return array.map( matches, function(bit, idx ){
                return ( idx < 3 ) ? Math.round( ( ( bit %= 256 ) < 0 ) ? bit + 256 : bit ) : limit( ( bit === '' ) ? 1 : Number( bit ), 0, 1 );
            });
        }

        ,parseHEX: function( color ){

            // FF -> FFFFFF
            if( color.length === 1 ){
                color = color + color + color;
            }

            var matches = color.match( hexMatch ).slice( 1 );

            return array.map( matches, function( bit, idx ){
                if( idx === 3 ){
                    return ( bit ) ? parseInt( bit, 16 ) / 255 : 1;
                }
                return parseInt(   (bit.length === 1 ) ? bit + bit : bit, 16 );
            });
        }


        ,parseHSB: function( color ){
            var hsb = color.match(listMatch).slice(1).map(function(bit, i) {
                if (i === 0) {
                    return Math.round(((bit %= 360) < 0) ? (bit + 360) : bit);
                } else if (i < 3) {
                    return limit(Math.round(bit), 0, 100);
                } else {
                    return limit((bit === '') ? 1 : Number(bit), 0, 1);
                }
            });

            var alpha = hsb[3];
            var br = Math.round(hsb[2] / 100 * 255);
            if (hsb[1] === 0) {
                return [br, br, br, alpha ];
            }

            var hue = hsb[0];
            var f = hue % 60;
            var p = Math.round((hsb[2] * (100 - hsb[1])) / 10000 * 255);
            var q = Math.round((hsb[2] * (6000 - hsb[1] * f)) / 600000 * 255);
            var t = Math.round((hsb[2] * (6000 - hsb[1] * (60 - f))) / 600000 * 255);

            switch (Math.floor(hue / 60)) {
                case 0: return [br, t, p, alpha ];
                case 1: return [q, br, p, alpha ];
                case 2: return [p, br, t, alpha ];
                case 3: return [p, q, br, alpha ];
                case 4: return [t, p, br, alpha ];
                default: return [br, p, q, alpha ];
            }
        }
    };

    /**
     * The main Color Class
     * @class
     * @param {String|Color|Array|Number} Color A color representation
     * @param {String} type The type of format of the color space ( rgb, hex, hsb )
     * @example braveheart([ 'color' ], function( color ){
   var x = new color.Color([66,66,66],'rgb') )
})
     */
    exports.Color = Color = new Type('Color', function( color, type ){
        switch( core.typeOf( color ) ){
            case "string":
                var name = Color.lookupColor( color );
                if( name ){
                    color = name;
                    type = "hex";
                }else if( !type ){
                    type = ( type = color.match(/^rgb|^hsb/)) ? type[0] : 'hex';
                }
                break;

            case "color" :
                color = [color.red, color.green, color.blue, color.alpha ];
                type = null;
                break;
            case 'array':
                type = type || "rgb";
                color = color.toString();
                break;
            case "number":
                type = "hex";
                color = color.toString(16);
                break;
        }
        if( type ){
            type = string.trim( type )
            color = parsers['parse' + type.toUpperCase()](color);
        }

        this[0] = this.red = color[0];
        this[1] = this.green = color[1];
        this[2] = this.blue = color[2];


        this.alpha = color[3] == null ? 1 : color[3];
        this.length = 3;
        return this;
    });
    Color.prototype = new Array()
    // define / lookup Color
    accessor.call( Color, "Color");

    /**
     * returns hex code of a predefined color
     * @method module:color.Color.lookupColor
     * @param {String} color The name of the Color you want.
     * @returns {String} The colors HEX Code
     * @example braveheart([ 'color' ], function( color ){
    var Color = color.Color
    Color.lookupColor("fuchsia") // #ff00ff
})
     */
    Color.defineColors({
        aliceblue: "#f0f8ff", antiquewhite: "#faebd7", aqua: "#00ffff", aquamarine: "#7fffd4",
         azure: "#f0ffff", beige: "#f5f5dc", bisque: "#ffe4c4", black: "#000000", blanchedalmond: "#ffebcd",
         blue: "#0000ff", blueviolet: "#8a2be2", brown: "#a52a2a", burlywood: "#deb887", cadetblue: "#5f9ea0",
         chartreuse: "#7fff00", chocolate: "#d2691e", coral: "#ff7f50", cornflowerblue: "#6495ed",
         cornsilk: "#fff8dc", crimson: "#dc143c", cyan: "#00ffff", darkblue: "#00008b",  darkcyan: "#008b8b",
         darkgoldenrod: "#b8860b", darkgray: "#a9a9a9", darkgreen: "#006400", darkgrey: "#a9a9a9",
         darkkhaki: "#bdb76b", darkmagenta: "#8b008b", darkolivegreen: "#556b2f", darkorange: "#ff8c00",
         darkorchid: "#9932cc", darkred: "#8b0000", darksalmon: "#e9967a", darkseagreen: "#8fbc8f",
         darkslateblue: "#483d8b", darkslategray: "#2f4f4f", darkslategrey: "#2f4f4f", darkturquoise: "#00ced1",
         darkviolet: "#9400d3", deeppink: "#ff1493", deepskyblue: "#00bfff", dimgray: "#696969",
         dimgrey: "#696969", dodgerblue: "#1e90ff", firebrick: "#b22222", floralwhite: "#fffaf0",
         forestgreen: "#228b22", fuchsia: "#ff00ff", gainsboro: "#dcdcdc",ghostwhite: "#f8f8ff",
         gold: "#ffd700", goldenrod: "#daa520", gray: "#808080", green: "#008000", greenyellow: "#adff2f",
         grey: "#808080", honeydew: "#f0fff0", hotpink: "#ff69b4", indianred: "#cd5c5c", indigo: "#4b0082",
         ivory: "#fffff0", khaki: "#f0e68c", lavender: "#e6e6fa", lavenderblush: "#fff0f5",
         lawngreen: "#7cfc00", lemonchiffon: "#fffacd", lightblue: "#add8e6", lightcoral: "#f08080",
         lightcyan: "#e0ffff", lightgoldenrodyellow: "#fafad2", lightgray: "#d3d3d3", lightgreen: "#90ee90",
         lightgrey: "#d3d3d3", lightpink: "#ffb6c1", lightsalmon: "#ffa07a", lightseagreen: "#20b2aa",
         lightskyblue: "#87cefa", lightslategray: "#778899", lightslategrey: "#778899", lightsteelblue: "#b0c4de",
         lightyellow: "#ffffe0", lime: "#00ff00", limegreen: "#32cd32",  linen: "#faf0e6",
         magenta: "#ff00ff", maroon: "#800000", mediumaquamarine: "#66cdaa", mediumblue: "#0000cd",
         mediumorchid: "#ba55d3", mediumpurple: "#9370db", mediumseagreen: "#3cb371", mediumslateblue: "#7b68ee",
         mediumspringgreen: "#00fa9a", mediumturquoise: "#48d1cc", mediumvioletred: "#c71585",
         midnightblue: "#191970", mintcream: "#f5fffa",  mistyrose: "#ffe4e1", moccasin: "#ffe4b5",
         navajowhite: "#ffdead", navy: "#000080", oldlace: "#fdf5e6", olive: "#808000", olivedrab: "#6b8e23",
         orange: "#ffa500", orangered: "#ff4500", orchid: "#da70d6", palegoldenrod: "#eee8aa",
         palegreen: "#98fb98", paleturquoise: "#afeeee",  palevioletred: "#db7093", papayawhip: "#ffefd5",
         peachpuff: "#ffdab9", peru: "#cd853f", pink: "#ffc0cb", plum: "#dda0dd", powderblue: "#b0e0e6",
         purple: "#800080", red: "#ff0000", rosybrown: "#bc8f8f", royalblue: "#4169e1", saddlebrown: "#8b4513",
         salmon: "#fa8072", sandybrown: "#f4a460", seagreen: "#2e8b57", seashell: "#fff5ee", sienna: "#a0522d",
         silver: "#c0c0c0", skyblue: "#87ceeb", slateblue: "#6a5acd", slategray: "#708090",
         slategrey: "#708090",  snow: "#fffafa", springgreen: "#00ff7f", steelblue: "#4682b4",
         tan: "#d2b48c", teal: "#008080", thistle: "#d8bfd8", tomato: "#ff6347", turquoise: "#40e0d0",
         violet: "#ee82ee", wheat: "#f5deb3",  white: "#ffffff",  whitesmoke: "#f5f5f5", yellow: "#ffff00",
         yellowgreen: "#9acd32"

    });

    Color.implement(/** @lends module:color.Color.prototype */{

        /**
         * returns the current color representation in the RGB color space as an array
         * @param {Boolean} asArray if true, will be returned as an array
         * @return {String|Array} An array containg the color valuse [RR, GG, BB]
         */
        toRGB: function(  arr  ){
            var rgb;

            rgb = [this.red, this.green, this.blue, this.alpha];
            return ( arr ) ? rgb : stringify( "rgb", rgb );
        },

        /**
         * returns the current color representation in the HSB color space as an array
         * @param {Boolean} asArray if true, will be returned as an array
         * @return {String|Array} An array containg the color valuse [H, S, B, A]
         */
        toHSB: function(arr){
            var red         = this.red                      // cache for red value
                ,green      = this.green                    // cache for green value
                ,blue       = this.blue                     // cache for blue value
                ,alpha      = this.alpha                    // cache for alpha value
                ,hue        = 0                             // colors hue value
                ,max        = Math.max(red, green, blue)    // Maximun color value
                ,min        = Math.min(red, green, blue)    // minimum color value
                ,delta      = max - min                     // diff of min / mzx
                ,saturation = (max !== 0) ? delta / max : 0 // saturation value
                ,brightness = max / 255                     // brightness value
                ,hsb
                ,rr
                ,gr
                ,br;

            if (saturation) {
                r = (max - red) / delta;
                gr = (max - green) / delta;
                br = (max - blue) / delta;
                hue = (red === max) ? br - gr : (green === max) ? 2 + rr - br : 4 + gr - rr;

                if ((hue /= 6) < 0) {
                    hue++;
                }
            }

            hsb = [Math.round(hue * 360), Math.round(saturation * 100), Math.round(brightness * 100), alpha];

            return (arr) ? hsb : stringify('hsb', hsb);
        },

        /**
         * returns the current color representation in the HEX color space as an array
         * @param {Boolean} asArray if true, will be returned as an array
         * @return {String|Array} An array containg the color valuse [RR, GG, BB, AA]
         */
        toHEX: function( arr ){
            var a = this.alpha
                ,alpha = ((a = Math.round((a * 255)).toString(16)).length === 1) ? a + a : a
                ,hex = [this.red, this.green, this.blue];

            hex = array.map( hex, function(bit) {
                bit = bit.toString(16);
                return (bit.length === 1) ? '0' + bit : bit;
            });


            return ( arr ) ? hex.concat(alpha) : '#' + hex.join('') + ((alpha === 'ff') ? '' : alpha);
        },

        /**
         * Determines if the color is considered a bright color
         * @return {Boolean} returns true if the colors brightness value is greater than 50
         */
        isLight: function( thrsh ){
            return this.toHSB( true )[2] > ( !!thrsh ? thrsh : 50 );
        },

        /**
         * Determines if the color is considered a dark color
         * @return {Boolean} returns true if the colors brightness value is leass than 50
         */
        isDark: function(){
            return !this.isLight()
        },

        /**
         * Determines if the color is considered a bright color
         * @param {Color|String} light the color to be used if the original color is dark
         * @param {Color|String} dark the color to be used if the original color is light
         * @return   {Color} dark if the passed in color is light, will return the dark color and vice versa
         */
        contrast: function( lgt, drk, thrsh ){

            return  this.isLight( thrsh ) ? new Color( drk ) : new Color( lgt );
        },

        toString: function( ){

            return this.toRGB();
        },


        /**
         * Takes any number of color instances and mixes there values
         * @param {Color} colors any number of colors
         * @param {Number} amount The percentage of the colors to mix together. Defaults to 50
         * @return {Color} color A new color as the result of mixing.
         * @example braveheart(['color'], function( color ){
    var white, black;

    white = new color.Color([255,255,255]) // white
    black = color.hex("#000000") // black
    red = color.hex("#ff0000") // red

    white.mix( black ).toRGB() // rgb(127, 127, 127)
    white.mix( black, 0 ).toRGB() // rgb(255, 255, 255)
    white.mix( black, 100 ).toRGB() // rgb(0, 0, 0)
    white.mix( black, 35 ).toRGB() // rgb(166, 166, 166) - deep grey
    white.mix( black, red,  45 ).toRGB() // rgb(192, 77, 77) - dark red
})
         */
        mix: function(){
            var colors = Array.prototype.slice.call( arguments )
                ,alpha = typeof array.getLast( colors )  == 'number' ? colors.pop() : 50
                ,rgb =  this.slice();

                array.each( colors, function( color ){
                    color = new Color ( color );
                    for( var x = 0; x < 3; x ++ ){
                        rgb[x] = Math.round( (rgb[x ] / 100 * (100 - alpha ) ) + ( color[x] / 100 * alpha));
                    }
                });

                return new Color( rgb, 'rgb')
        },
        /**
         * Creates a new color which is an inverse color of the current instance
         * @return {Color} color A new color as the result of inverting
         */
        invert: function(){
            return new Color( array.map( this, function( value ){
                    return 255 - value;
                })
            )
        },
        $family: function(){
            return "color"
        }
        ,repr: function(){
            return "<Color: " + this.toHEX()  + " >"
        }

    });



    // Static methods
    /**
     * returns hex code of a predefined color
     * @param {array} rgb An RBG String to convert into a Color
     * @returns {Color} The colors HEX Code
     * @example braveheart(['color', 'log'], function( color, log ){
    log.print( color.rgb("rgb(122,122,122)" ) ) // &lt;Color: #7a7a7a &gt;
})
     */
    exports.rgb = function( rgb ){
        return new Color( rgb, 'rgb');
    };

    /**
     * returns hex code of a predefined color
     * @param {array} rgb An RBG String to convert into a Color
     * @returns {Color} The colors HEX Code
     * @example braveheart(['color', 'log'], function( color, log ){
    log.print( color.hex("#444444" ) ) // &lt;Color: #444444 &gt;
})
     */
    exports.hex = function( hex ){
        return new Color( hex, 'hex ');
    };

    /**
     * returns hex code of a predefined color
     * @param {array} rgb An RBG String to convert into a Color
     * @returns {Color} The colors HEX Code
     * @example braveheart(['color', 'log'], function( color, log ){
    log.print( color.hsb("hsb(10,10,10)" ) // &lt;Color: #1a1717 &gt;
})
     */
    exports.hsb = function( hsb ){
        return new Color( hsb, 'hsb');
    };


});
