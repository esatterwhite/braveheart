/*jshint laxcomma:true, smarttabs: true */

/**
 * Provides an abstraction ontop of JavaScript's prototypical inheritence model to look more like the Classical Object Orientated Model.
 * The {@link module:class.Class} system allows for Multiple inheritance, Mixins, dependancy injection instance modification, protected functions and other OOP type abstractions
 * @author Eric Satterwhite
 * @module class
 * @requires core
 * @requires array
 * @requires object
 * @requires signals
 * @requires functools
 * @requires log
 */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

define(
    ['require','exports','module','core', 'array', 'object', 'signals', "functools", "log", "accessor" ]
    , function(require, exports, module, core, array, object, signals, functools, log, accessor ){
        var Class               // The Class
            ,reset              // function that attempt to clear references to an object
            ,parentFn           // function used to call the parent function of a class method
            ,wrap               // wrapper function that checks for protected functions
            ,implement          // function override to the Type.implement method. Used to trigger Class Mutators
            ,getInstance        // short cut function to create instances of things
            ,removeOn           // shortcut function to convert event handlers onHandler -> handler
            ,ClassChain         // Chain Mixin
            ,ClassEvents        // Events Mixin
            ,ClassOptions       // Options Mixin
            ,ClassStorage       // Storage Mixin
            ,Interface          // Interface Class
            ,overloadSetter     // shortcut to functools OverloadSetter
            ,overloadGetter     // shortcut to functools OverloadGetter
            ,KEY                // Storage Key prefix
            ,storage            // Memory cache for Storage mixin
            ,storageFor         // method to find the right storage object for a class instance
            ,Type = core.Type   // The Type `Class`
            ,uid = 0;           // an id counter


        KEY = "__store:" +  (+ new Date()).toString(36);
        storage = {};

        overloadSetter = functools.overloadSetter;
        overloadGetter = functools.overloadGetter;

        removeOn = function( str ){
            return str.replace(/^on([A-Z])/, function( full, first ){
                return first.toLowerCase();
            });
        };
        parentFn = function(){
            if( !this.$caller ){
                throw new Error("The method 'parent' cannot be called");
            }

            var name
                ,previous
                ,parent;

            name = this.$caller.$name;
            parent = this.$caller.$owner.parent;
            previous= (!!parent) ? parent.prototype[name] : null;

            if(!previous){
                throw new Error('The method" ' + name + '  "has no parent."');
            }
            return previous.apply(this, arguments);
        };

        reset = function( obj ){
            var key
                ,value;

            for( key in obj ){
                value = obj[ key ];
                switch( core.typeOf( value )){
                    case 'object':
                        var F = function(){};
                        F.prototype = value;
                        obj[key] = reset( new F );
                        break;
                    case 'array':
                        obj[key] = array.clone( value );
                        break;
                }
            }

            return obj;
        };

        wrap = function(self, key, method){
            var wrapper
                ,caller
                ,result
                ,current;

            if (method.$origin) method = method.$origin;
            wrapper = function(){
                if (method.$protected && this.$caller == null){
                    throw new Error('The method "' + key + '" cannot be called.');
                }

                caller = this.caller;
                current = this.$caller;

                this.caller = current;
                this.$caller = wrapper;

                result = method.apply(this, arguments);

                this.$caller = current; this.caller = caller;
                return result;
            };
            wrapper.extend({$owner: self, $origin: method, $name: key, $protected:method.$protected});
            return wrapper;
        };


        implement = function( key, value, retain ){
            if( Class.Mutators.hasOwnProperty( key )){
                value = Class.Mutators[key].call( this, value);
                if( !value ){
                    return this;
                }
            }

            if( typeof value === 'function'){
                if( value.$hidden ){
                    return this;
                }
                this.prototype[ key ] = !!retain ? value : wrap( this,key, value);
            } else{
                object.merge( this.prototype, key, value );
            }
        };

        getInstance = function( klass ){
            var proto;

            klass.$prototyping = true;
            proto = new klass();
            delete klass.$prototyping;
            return proto;
        };

        /**
         * Base Class Type for Braveheart. It is a derivitive of {@link module:core.Type}
         * @class
         * @param {Object} definition An object that defines the methods and properties on the Class Definition.
         * If a method named <strong>initialize</strong> is found, it will be used as the constructor function for the class
         * @return a new Class Definition to be instansiated.
         * @example braveheart(['class'], function( _class ){
    var Class = _class.Class;

    var MyClass = new Class({
        initialize: function( value ){
           // the initialize method is used as the class constructor
           this.value = value;
        }
        ,doStuff: function(){
            console.log( "stuff!")
        }
        ,doMagic: function(){
            console.log( "magic!")
        }
    });

    var myInstance = new MyClass( 12 );

    console.log( myInstance.value ) // 12
    myInstance.doStuff();
    myInstance.doMagic();
})
         **/
        exports.Class = Class = new Type("Class", function( params ){
            var value;

            if( params instanceof Function ){
                params = { initialize: params };
            }

            var newClass = function(){
                reset( this );
                if( newClass.$prototyping ){
                    return this;
                }

                this.$caller = null;
                value = !!this.initialize ? this.initialize.apply( this, arguments ) : this;
                this.$caller = this.caller = null;

                // Extend the function with this ( overload setter ) and implement the passed in params ( object literal )
            };
            newClass.extend( this );
            newClass.implement( params );

            newClass.$constructor = Class;
            newClass.prototype.$constructor = Class;
            newClass.prototype.parent = parentFn;

            return newClass;
        });


        // this overloads the origininal implement with the
        // above implement. which repects class mutators
        Class.implement( 'implement', overloadSetter(implement));

        /**
         * <p> Collcation of method that can alter the definition of a class as it is created
         * A mutator manifests as a key on the class definition whose value is accepted as a parameter</p>
         * @author Eric Satterwhite
         * @type Object
         * @property {Object} Extends The current class, will Inherit from the passed in object. Can be any object ( not just "Classes" )
         * @property {Object|Array} Implements Will inherit only the methods from passed in object. Accepts singal or multiple objects as an array
         * @property {Boolean} GetterSetter Instences of classes using this mutator will have standard <strong>get</strong> / <strong>set</strong> methods. The Class will be modified with {@link module:accessor} allowing you to use <strong>defineSetter</strong> and <strong>defineGetter</strong>
         * @property {module:class.Interface} Interface Upon instanciation of the class it will be checked to ensure it correctly implements the Interface. Execution of the program will be haulted if it does not.
         * @property {Boolean} onEvents For use in conjunction with {@link module:class.Events}. Aliases <strong>addEvent</strong> / <strong>removeEvent</strong> to <strong>on</strong> / <strong>un</strong> respectively
         * @example
         * <h2>GetterSetter Mutator</h2>
 braveheart(['class'], function( cls ){
    var Class = cls.Class
        ,Events       = cls.Events
        ,Options      = cls.Options;

    var MyClass = new Class({
        Implements:[Events, Options]
        ,GetterSetter: true //GetterSetter Mutator will be invo
        ,prop:44
        ,options:{
            thing:2
        }
        ,initialize: function( value ){
            this.value = value

            this.setOptions( options ) // setOptions added by Options Mixin
        }
        ,doStuff: function( val ){
            this.value = this.value + 10

            var val = (this.value + this.options.thing) * val

            this.fireEvent('stuff', val); // fireEvent added by Events Mixin
        }
    });

    var x = new MyClass( 5 );

    //free GET method
    x.get('prop') // 44

    // overrides the existing getter for `prop`
    MyClass.defineGetter("prop", function( ){
        return this.prop * 2 + this.options.thing
    });

    x.get("prop") // 90

    // free SET method
    x.set('prop', 1) // resets the value of prop to 1

    x.get('prop') // 4
})

         * </code/></pre>
         * <h2>OnEvents Mutator</h2>
         * @example
braveheart(['class'], function( cls ){
   var Class = cls.Class
       ,Events  = cls.Events;

    var Evented = new Class({
        Implements:Events // Implements Mutator
        ,OnEvents: true   // OnEvents Mutator!
        ,initalize: function( ){
            this.hash={}
        }
        ,do: function( prop, val ){
            this.hash[prop] = val
            this.fireEvent( prop + "change", val )
        }
    });


    var evt = new Evented();

    evt.on("testchange", function( val ){
        alert( val )
    });

    evt.do('test', 12)// alerts 12;
});
         */
        exports.Mutators = Class.Mutators = {
            /**
             * Extend from a different class
             * Extends does the ugly javascript prototypical inheritance.
             */
            Extends: function( parent ){
                this.parent = parent;
                this.prototype = getInstance( parent );
            }

            /*
             * Implements is effectively for `Mixins`. The parent class will inherit all properties but will not be a subclass
             * Allows for multiple inheretance
             * Inheret the methods from any number of other classes
             */
            ,Implements: function( items ){
                items = array.from( items );

                array.each( items, function(item, idx, arr ){
                    var instance = new item
                        ,key;

                    for( key in instance ){
                        implement.call( this, key, instance[ key ], true );
                    }
                }, this);
            }

            ,GetterSetter: function( enable ){
                if(!enable){
                    return;
                }

                var klass = this;

                accessor.call( klass, "Getter");
                accessor.call( klass, "Setter");

                this.prototype.get = function( name ){
                    var getter = this.constructor.lookupGetter( name );

                    return !!getter ? getter.call( this ) : this[name];
                };

                this.prototype.set = overloadSetter(function(name, value){
                    var setter = this.constructor.lookupSetter( name );

                    if( setter ){
                        setter.call( this, value );
                    } else if( value == null ){
                        this[name] = null;
                    } else {
                        this[name] = value;
                    }

                });
            }
            ,Interface: function( intrfc ){
                var old_init = this.prototype.initialize;
                var klass = this;

                klass.prototype.initialize = function( ){
                    var instance = getInstance( klass );

                    var error_list = []
                        ,error
                        ,classtype
                        ,intrfctye
                        ,members ;

                        members = intrfc.toObject();
                        for( var member in members ){
                            classtype  = core.typeOf( instance[member] );
                            intrfctye = core.typeOf( members[member] );
                            error     = "Failure to accurately implement " +
                                         intrfc + ": " + member +
                                         " was defined as: " +
                                         intrfctye + ", found: " +
                                         classtype;


                            if( core.typeOf( members[member] ) !== core.typeOf( instance[member] ) ){
                                error_list.push( error );
                                log.error( error );
                            }

                        }

                        if( error_list.length){
                            throw array.getLast( error_list );
                        } else {
                            old_init.apply( this, arguments );
                        }

                };
            }

            /**
             * Mixin that aliases addEvent / removeEvent to on / un
             */
            ,OnEvents:function( convert ){
                if(!!convert && this.prototype.addEvent ){
                    this.prototype.on = this.prototype.addEvent;
                    this.prototype.un = this.prototype.removeEvent;
                }
            }
        };

        /**
         * allows for method calls to be queued up for later execution
         * @class
         * @author Eric Satterwhite
         */
        exports.Chain = ClassChain = new Class(/** @lends module:class.Chain.prototype */{
            $chain:[],

            /**
             * appends a number of function to the chain
             * @return {Class} the class instance
             */
            chain: function(){
                this.$chain = array.append(this.$chain, array.flatten( arguments ) );
                return this;
            },

            /**
             * removes the first function off the chain and executes it
             * @return {Mixed} the result of the next function
             */
            callChain: function(){
                return !!this.$chain.length ? this.$chain.shift().apply(this, arguments ) : false;
            },

            /**
             * empties out the current chain
             * @return {Class} returns the class instance
             */
            clearChain: function(){
                array.empty( this.$chain, true);
                return this;
            }
        });

        /**
         * Mixin Class which add event functionality
         * @author Eric Satterwhite
         * @class
         */
        exports.Events = ClassEvents = new Class( /** @lends module:class.Events.prototype */ {
            $events: {},
            /**
             * adds an event handler to the class instance
             * @param  {String} name the name of the event listener
             * @param  {Function} fn the event handler to attach
             * @param  {Boolean} internal if set to true, the event handler cannot be removed
             */
            addEvent:function( name, fn, internal ){
                var type;

                type = removeOn( name, fn, internal );

                if( !this.$events.hasOwnProperty( type ) ){
                    this.$events[type] = new signals.Signal();
                }
                this.$events[type].add( fn );
                fn.internal = !!internal;

            },

            /**
             * removes specified event from a class instance
             * @param  {String} name the name of the method to remove
             * @param  {Function} fn the function to remove. Must be the same function initially used ( non-anonymous )
             * @return {Class} the class instance
             */
            removeEvent: function( name, fn ){
                var type = removeOn( name )
                    ,sig;

                sig = this.$events[name];

                if( sig && fn && !fn.internal){
                    sig.remove( fn );
                }
                return this;
            },

            /**
             * Fires a named event
             * @param  {String} name the name of the event to fire
             * @param  {Array} args the array of arguments to pass to the handler. Single arguments need not be an array
             */
            fireEvent: function( name, args, bind, delay){
                var type = removeOn( name )
                    ,sig
                    ,args;
                sig = this.$events[name];
                if( !sig ){
                    return this;
                }
                args = array.from( args );

                sig.dispatch.apply( ( bind || sig ) , args );
                return this;
            },

            /**
             * Short to addevent for adding Events in bulk as key value pairs where the key is the name of the event name and the value is the event handler<br />
             * Events may be added in this fashion<br/>
             * <code>
             *  MyClassInstance.addEvents({
             *      click: function(){},
             *      cutomeevt:function(){}
             *  });
             *  </code>
             * @param  {Object} events object with name / handler pairs
             * @return {Class} The class Instance
             */
            addEvents: function( events ){
                for( var type in events ){
                    this.addEvent(type, events[type]);
                }
                return this;
            },

            /**
             * Shortcut to remove event allowing key / value pairs to be passed  to perform a bulk operatino
             * @param  {Object} events object containing event names and handler pairs
             * @return {Class} The class instance
             *
             */
            removeEvents: function( events ){
                var type, fns, sig;

                if( core.typeOf( events ) == 'object' ){
                    for( type in events ){
                        this.removeEvent( type, events[type] );
                    }
                    return this;
                }
                sig = removeOn( events );
                if( !!events ){
                    this.$events[ events ].removeAll();
                }

                return this;
            }
        });

        /**
         * Mixin class that allows for default and overrideable class configurations
         * @class
         * @author Eric Satterwhite
         */
        exports.Options = ClassOptions = new Class( /** @lends module:class.Options.prototype */{

            /**
             * merge incomming options into existing class options over riding the defaults
             * @pararm {Object} options key value pairs to use as the class options / configs
             * @return {Class} the current class object
             */
            setOptions: function( options ){
                var opt;
                this.options = object.merge.apply( null, array.append( [{}, this.options], arguments ) );
                options = this.options;

                if( !!this.addEvent ){
                    for( opt in options ){
                        if( core.typeOf( options[ opt ] ) !== 'function' || !(/^on[A-z]/).test(opt)){
                            continue;
                        }
                        this.addEvent( opt, options[ opt ]);
                        delete options[opt];
                    }
                }
                return this;
            }
        });


        storageFor = function( obj, key ){
            var uid = obj[KEY] || ( obj[KEY] = ( (+ new Date()).toString(36) ) );
            return storage[uid] || ( storage[uid] = {} );
        };

        /**
         * Allows Class instance to store arbitrary peices of data in memory
         * @class
         * @author Eric Satterwhite
         * @example braveheart(['class', 'log' ], function( _class, log ){
    var Class = _class.Class
        ,Storage = _class.Storage
        ,MyClass;

    MyClass = new Class({
        Implements:[ Storage ]
        ,initialize: function( value ){
            this.value = value
        }
        ,getValue: function(){
            return this.value;
        }
    });


    var instance = new MyClass( 2 );

    log.info( instance.getValue() ); // 2
    log.info( instance.value ); // 2

    instance.store("hidden", {data: "words"});

    log.info( instance.hidden) // undefined
    log.info( instance.retrieve('hidden') ) // {data: "words"}

    instance.eliminate('hidden');
    log.info( instance.retrieve('hidden') ) // undefined

})
         */
        exports.Storage = ClassStorage = new Class( /** @lends module:class.Storage.prototype */ {
            /**
             * Store and arbitray item in memory storage
             * @param {String} key The name to store the data under
             * @param {Object} item The item to store
             * @returns {Class} The Class instance
             */
            store: function store( key, value ){
                // generates a new id for each class instance so keys don't clobber eachother
                if( !this.$id ){
                    this.$id = (+ new Date() ).toString(16) + ( ++uid );
                }

                var _key = key + ":" + this.$id;
                storageFor(this)[ _key ] = value;
                return this;
            },

            /**
             * Retreives a specific value out of memory
             * @pararm {String} key The name storage key to retreive
             * @returns {Object} The stored object
             */
            retrieve: function retrieve( key, defaultVal ){
                var store = storageFor( this );
                var key = key +":" + this.$id;
                return ( !!store[key] ) ? store[key] : defaultVal;
            },

            /**
             * Clears the data out of the specified storage space
             * @param {String} key the name of the slot to delete
             * @returns {Class} The class instance
             */
            eliminate: function eliminate( key ){
                delete storageFor( this )[ key +":" + this.$id ];
                return this;
            }
        });

        /**
         * Allows for proper class interfaces with type checking on class memvers
         * @class
         * @requires module:class.Options
         * @author Eric Satterwhite
         *
         */
        exports.Interface = Intercace = new Class(/** @lends module:class.Interface.prototype */{

            initialize: function( name, options ){
                this.$name = name;

                this.toObject = function( ){
                    return options || {};
                };
            }

            ,error: function( type ){
                return " error " + type;
            }


            ,toObject: function( ){
                return this.$contract;
            }
            ,toString: function( ){
                return this.$name;
            }

            ,repr: function( ){
                return "<Interface: " + this.$name + " />";
            }


        });



    }
);
