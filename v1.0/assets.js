/*jshint laxcomma:true, smarttabs: true */

/**
 * module that Abstracts the creation of static assets ( javascripts, images and stylesheets)
 * @module assets
 * @author Eric Satterwhite
 * @requires class
 * @requires array
 * @requires string
 * @requires dom
 */
define([ "require", "exports", "module", "class", "array", "string", "dom" ], function( require, exports, module, _class, array, string, dom){
	var Class = _class.Class
		,Events  = _class.Events
		,Options = _class.Options;


	/**
	 * Injects a script into the current document. returns an instance of {@link module:element.Element}
	 * @class
	 * @param {String} source the source url for the script
	 * @param {Object} options Options for the Script element. A special 
	 option can be spcified<br/>
	 *   <ul>
			<li>onLoad - function to be called when the script has completed loading
	 	</ul>
	 * @param {Boolean} appendText if true rather than setting the url to the source param, the inner script text will be set to what ever was passed into the source param.
	 * @example braveheart(['assets'], function( assets ){
	new assests.JS("alert('hello')", null, true) // alerts "hello"	
});
	 * @example braveheart(['assets'], function( assets ){
	new assests.JS("http://www.awesome.com/code.js", {
		onLoad: function(){
			alert("script loaded")
		}
	}) // alerts "script loaded"
});
	 */
	exports.JS = function( source, options, appendText ){
		options = options || {};
		var element = this.element = new dom.Element("script", {
			src:!appendText ? source : null
			,type:"text/javascript"
		});

		var load = options.onLoad || options.onload || options.load;
		var that = this;
		if( load ){
			if( typeof element.onreadystatechant != 'undefined' ){
				element.addEvent( 'readystatechange', function(){
					if( array.contains( ["load", "complete"], this.readyState) ){
						load.call(this);
					}
				});
			} else{
				element.addEvent( 'load', load );
			}
		}

		// you have to delete these or they will be set as DOM properties
		delete options.onLoad;
		delete options.onload;
		delete options.load;

		if( appendText ){
			options.text = source
		}
		 element
					.set( options )
					.inject( dom.Document.head );

		if( appendText && typeof load === "function" ){
			load();
		}

		return element;
	};


	/**
	 * Injects an image into the current document. returns an instance of {@link module:element.Element}
	 * @class
	 * @param {String} source the source url for the script
	 * @param {Object} options Options for the Script element. A special option can be spcified<br/>
	 *   <ul>
			<li>onLoad - function to be called when the script has completed loading
	 	</ul>
	 * @param {Boolean} appendText if the source is a CSS String, it will be injected a style tag rather then used as the href of a link tag
	 */
	exports.CSS = function( source, options, appendText ){
		var  tag  // the type of tag we need to create
			,link // Element instance for the tag
			,node;

		options = options || {};

		tag = !!appendText ? "style" : "link";
		link = this.link = new dom.Element(tag, {
			rel:"stylesheet"
			,media: "screen"
			,type:"text/css"
			,href: !!appendText ? undefined :  source
		});
		var load = options.onLoad || options.onload || options.load;

		if( load ){
			link.addEvent("load", load );
		}
		var el = link.inject( dom.Document.head );

		if( tag === "style"){
			node = link.get('node')
			if( node.styleSheet ){
				node.styleSheet.cssText = source;
			} else{
				link.set("text", source );
			}
		}
		link.set( options )

		delete options.onLoad;
		delete options.onload;
		delete options.load;

		if( appendText && typeof load === "function" ){
			load();
		}
		return el;
	};

	/**
	 * Injects a script into the current document. returns an instance of {@link module:element.Element}
	 * @class
	 * @param {String} source the source url for the script
	 * @param {Object} options Options for the Script element. A special option can be spcified<br/>
	 *   <ul>
			<li>onLoad - function to be called when the script has completed loading
	 	</ul>
	 */
	exports.IMG = function( source, options ){
		options = options || {};
		var image = new Image()
			,element = this.element = new dom.Element("img");

		array.each( ['load', 'abort', 'error'], function( name ){
			var type = "on" + name
				,cap = "on" + string.capitalize( name )
				,event = options[type] || options[cap] || function(){};

			image[type] = function( ){
				if(!image){
					return;
				}
				if(!element.getParent() ){
					element.width = image.width;
					element.height = image.height;
				}
				image = image.onload = image.onabort = image.onerror = null;
				setTimeout( function(){
					event.call( element, element );
				}, 10);
				element.fireEvent(name, element, 1);
			};
		});
		image.src = source;
		element.set("src", source);
		if( image && image.complete){
			image.onload();
		}

		delete options.onLoad;
		delete options.onload;
		delete options.load;

		return element.set( options );
	};
});
