/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * AMD Module for working with an parsing URI strings
 * @author Ghazi Belhaj
 * @module url
 */
define(['require', 'exports', 'module'], function(require, exports, module) {
	var reURI = /^((http.?:)\/\/([^:\/\s]+)(:\d+)*)/ // returns groups for protocol (2), domain (3) and port (4)
		, reParent = /[\-\w]+\/\.\.\// // matches a foo/../ expression
		, reDoubleSlash = /([^:])\/\//g; // matches // anywhere but in the protocol
	/**
	 * Get the domain name from a url.
	 * @param {String} url The url to extract the domain from
	 * @return {String} The domain part of the url
	 * @example url.getDomainName('http://www.amazon.com/best-sellers-books-Amazon/zgbs/books') -> www.amazon.com
	 */
	exports.getDomainName = function(url) {
		return url.match(reURI)[3];
	};
	/**
	 * Get the port for a given URL, or '' if none.
	 * @param {String} url The url to extract the port from
	 * @return {String} The port part of the url
	 */
	exports.getPort = function(url) {
		return url.match(reURI)[4] || '';
	};
	/**
	 * Returns  a string containing the schema, domain and if present the port.
	 * @param {String} url The url to extract the location from
	 * @return {String} The location part of the url
	 */
	exports.getLocation = function(url) {
		var m = url.toLowerCase().match(reURI)
			, proto = m[2]
			, domain = m[3]
			, port = m[4] || '';
		if ((proto === 'http:' && port === ':80') || (proto === 'https:' && port === ':443')) {
			port = '';
		}
		return proto + '//' + domain + port;
	};
	/**
	 * Resolves a relative url into an absolute one.
	 * @param {String} url The path to resolve
	 * @return {String} The resolved url
	 */
	exports.resolveUrl = function(url) {
		var path;
		// replace all // except the one in proto with /
		url = url.replace(reDoubleSlash, '$1/');
		// If the url is a valid url we do nothing
		if (!url.match(/^(http||https):\/\//)) {
			// If this is a relative path
			path = (url.substring(0, 1) === '/') ? '' : location.pathname;
			if (path.substring(path.length - 1) !== '/') {
				path = path.substring(0, path.lastIndexOf('/') + 1);
			}
			url = location.protocol + '//' + location.host + path + url;
		}
		// reduce all 'xyz/../' to just ''
		while(reParent.test(url)) {
			url = url.replace(reParent, '');
		}
		return url;
	};
	/**
	 * Appends the parameters to the given url.<br/>
	 * The base url can contain existing query parameters.
	 * @param {String} url The base url
	 * @param {Object} parameters The parameters to add
	 * @param {Boolean} useHash Whether to use a hash over the query
	 * @return {String} A new valid url with the parameters appended
	 */
	exports.appendQueryParams = function(url, parameters, useHash) {
		var hash = ''
			, idx = url.indexOf("#")
			, key
			, q = [];
		if (idx !== -1) {
			hash = url.substring(idx);
			url = url.substring(0, idx);
		}
		for (key in parameters) {
			if (parameters.hasOwnProperty(key)) {
				q.push(key + '=' + encodeURIComponent(parameters[key]));
			}
		}
		return url + (useHash ? '#' : (url.indexOf('?') == -1 ? '?' : '&')) + q.join('&') + hash;
	};
});
