var requirejs     = require('requirejs')
	,path         = require('path')
	,testCase     = require('nodeunit').testCase
	,rpath        = path.resolve( path.join(__dirname, ".."))

requirejs.config({
	baseUrl:rpath
	,nodeRequire:require
});

var _class = requirejs('class');
var functools    = requirejs('functools');
var Class = _class.Class;
var Events = _class.Events;
var Options = _class.Options;
var Storage = _class.Storage;



var tests = requirejs('./test/modules/class');



exports["Class Creation"] = testCase(tests.creation);

exports["Class Inheritance"] = testCase(tests.inheritance);

exports["Class Options"] = testCase(tests.Options);

exports["Class Events"] = testCase(tests.Events);
exports["Multiple Inheritance"] = testCase(tests.MultiInheritance);

exports["Class Storage"] = testCase(tests.Storage)