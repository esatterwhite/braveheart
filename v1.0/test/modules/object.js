define(['require', 'exports', 'object'], function(require, exports, object) {


	exports.keys = {
		setUp: function( callback ){
			this.obj = {
				 key1:"value1"
				,key2:"value2"
				,key3:"value3"
				,key4:"value4"
				,key5:"value5"
				,key6:"value6"
			};
			callback();
		}
		,"keys length":function( test ){
			var keys = object.keys( this.obj )

			test.strictEqual( keys.length, 6, "Expected 6, got: " + keys.length)

			test.done()
		}
		,"key names": function( test ){
			var keys = object.keys( this.obj )
			test.equal(  keys[0] , "key1");
			test.equal(  keys[1] , "key2");
			test.equal(  keys[2] , "key3");
			test.equal(  keys[3] , "key4");
			test.equal(  keys[4] , "key5");
			test.equal(  keys[5] , "key6");

			test.done();
		}
	};

	exports.values ={
		setUp: function( callback ){
			this.obj = {
				 key1:"value1"
				,key2:"value2"
				,key3:"value3"
				,key4:"value4"
				,key5:"value5"
				,key6:"value6"
			};
			callback();
		}
		,"values length": function( test ){
			var keys = object.keys( this.obj )

			test.strictEqual( keys.length, 6, "Expected 6, got: " + keys.length )

			test.done();
		}
		,"key values": function( test ){
			var keys = object.keys( this.obj )
			test.equal( this.obj [ keys[0] ] , "value1");
			test.equal( this.obj [ keys[1] ] , "value2");
			test.equal( this.obj [ keys[2] ] , "value3");
			test.equal( this.obj [ keys[3] ] , "value4");
			test.equal( this.obj [ keys[4] ] , "value5");
			test.equal( this.obj [ keys[5] ] , "value6");

			test.done();
		}

	};

	exports.each = {
		setUp: function( callback ){
			this.obj = {
				 key1:1
				,key2:2
				,key3:3
				,key4:4
				,key5:5
				,key6:"6"
			};
			callback();
		}
		,"param passing":function( test ){
			var count = 1
			object.each(this.obj, function( val, key, obj ){
				test.ok( this.obj, "each should allow an object passed at the third argument as a function context")
				test.equal( val, count,"got " + count +" expedned " + val );
				test.equal( this.obj[ key], val, "The current key should be passed as the second arguments of the operating function");
				test.equal( obj[ key ], val, "the original object should be passed as the third arguments of the operating function");
				count++;
			},this)
			test.done();
		}
	};

	exports.querystring ={
		setUp: function( callback ){
			this.obj = {
				 key1:"value1"
				,key2:"value2"
				,key3:"value3"
				,key4:"value4"
				,key5:"value5"
				,key6:"value6"
			};
			callback();
		}
		,"args": function( test ){
			var args = object.toQueryString( this.obj )

			test.strictEqual( args, "key1=value1&key2=value2&key3=value3&key4=value4&key5=value5&key6=value6", "expected " + "key1=value1&key2=value2&key3=value3&key4=value4&key5=value5&key6=value6" + ' got ' + args );

			test.done();
		}
	};

	exports.filtering = {
		setUp: function( callback ){
			this.obj ={
				 key1:1
				,key2:2
				,key3:3
				,key4:4
				,key5:5
				,key6:"6"
				,key7:"7"
				,key8:"8"
				,key9:"9"
				,key10:"10"
				,key11:"11"
			}

			callback();
		}
		,"filter numbers":function( test ){
			var obj
			var numberobj = object.filter( this.obj, function( val, key, obj ){
				return typeof val === 'number'
			}, this);

			var values = object.values( numberobj );
			test.strictEqual( values.length, 5, "expected 5, got " + values.length)
			for( var x = 0; x < values.length; x++){
				test.strictEqual( ( x + 1 ), values[x], " expected " + values[x] + " got " +  ( x + 1 ) )
			}


			test.done();
		}
		,"filter strings": function( test ){
			var obj
			var valueobj = object.filter( this.obj, function( val, key, obj ){
				return typeof val === 'string'
			}, this);

			var values = object.values( valueobj );
			test.strictEqual( values.length, 6, "expected 6, got " + values.length)
			for( var x = 0; x < values.length; x++){
				test.equal( ( x + 5 + 1 ), values[x], " expected " + ( x + 5 + 1 ) + " got " +  values[x] )
			}


			test.done();
		}
		,subset: function( test ){
			var sub = object.subset( this.obj, ['key1','key5'])
			test.equal(sub.key1, 1, "expected 1, got " + sub.key1)
			test.equal(sub.key5, 5, "expected 1, got " + sub.key5)
			sub = object.values( sub )
			test.equal( sub.length, 2, "expected 2, got" + sub.length)
			test.done();
		}
		,map: function( test ){

			object.map( this.obj, function( val, key, obj){
				test.equal( val, obj[key], "this should not fail" )
			}, this)
			test.done();
		}

	};

	exports.inspection = {
		setUp: function( callback ){
			this.obj = {
				key1:"value1"
				, key2:"value2"
				, key3:"value3"
				, key4:"value4"
				, key5:"value5"
				, key6:"value6"
				, key7:"value7"
				, key8:"value8"
				, key9:"value9"
			}

			callback();
		}

		, keyOf: function( test ){
			test.expect( 9 );
			test.equal( object.keyOf( this.obj, "value1"), "key1");
			test.equal( object.keyOf( this.obj, "value2"), "key2");
			test.equal( object.keyOf( this.obj, "value3"), "key3");
			test.equal( object.keyOf( this.obj, "value4"), "key4");
			test.equal( object.keyOf( this.obj, "value5"), "key5");
			test.equal( object.keyOf( this.obj, "value6"), "key6");
			test.equal( object.keyOf( this.obj, "value7"), "key7");
			test.equal( object.keyOf( this.obj, "value8"), "key8");
			test.equal( object.keyOf( this.obj, "value9"), "key9");

			test.done();
		}

		,length: function( test ){
			test.equal( object.length( this.obj ), 9, "expected 9, got: " + object.length( this.obj ) );
			delete this.obj["key2"];
			test.equal( object.length( this.obj ), 8, "expected 8, got: " + object.length( this.obj )  )

			test.done();
		}

		,contains: function( test ){

			var x = 12;
			test.ok( object.contains( this.obj, "value2" ) );
			test.ok( !object.contains( this.obj, 12 ) )
			this.obj.x = x;
			test.ok( object.contains( this.obj, 12 ) )


			test.done()
		}

		,every: function( test ){
			var result;

			result = object.every( this.obj, function(value, key ){
				return typeof value === "string"
			})
			test.ok( result );
			test.done()
		}

		,some: function( test ){
			var result;
			this.obj.x = 55
			result = object.some( this.obj, function(value, key ){
				return typeof value === "string"
			});
			test.ok( result );

			result = object.some( this.obj, function(value, key ){
				return typeof value === "string"
			});
			test.ok( result );

			test.done()
		}
	};

	exports.manipulation = {
		setUp: function( callback ){
			this.obj = {
				key1:"value1"
				,key2: "value2"
				,key3: "value3"
				,key4: "value4"
				,nested:{
					 n1:1
					,n2:2
					,n3:3
				}
				,ray:[1,2, {a1:1} ]
			};

			callback();
		}

		,append: function( test ){

			var result;
			test.equal( this.obj.key5, undefined);
			test.equal( this.obj.key6, undefined);
			results = object.append( this.obj, {key5:"value5", key6:"value6"});
			test.equal( this.obj.key5, "value5");
			test.equal( this.obj.key6, "value6");

			results = object.append( this.obj, {key6:"value7"});

			test.equal( this.obj.key6, "value7");

			test.equal( this.obj.nested.n1, 1);

			var obj = object.append( this.obj, { nested:{n1:10}} )

			test.equal( obj.nested.n2, undefined, "expected undefined, got " + obj.nested.n2);
			test.equal( obj.nested.n1, 10, "expedted 10, got, " + obj.nested.n1);

			test.done();
		}
		,merge: function( test ){

			test.equal( this.obj.nested.n1, 1);

			var obj = object.merge( this.obj, { nested:{n1:10}} )
			test.equal( obj.nested.n1, 10);
			test.done();
		}

		,clone: function( test ){
			var clone;

			var obj2 = object.clone( this.obj );
			test.ok(  obj2 !== this.obj );

			test.equal( obj2.key1, this.obj.key1)
			obj2.key1 = "value12";
			test.notEqual( obj2.key1, this.obj.key1)
			test.equal( typeof obj2.ray.length, "number")
			test.equal( typeof obj2.nested, "object")
			test.equal( typeof obj2.nested.n1, "number")
			test.done();
		}
	}

});
