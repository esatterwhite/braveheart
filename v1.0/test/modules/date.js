define(['require', 'exports', 'date'], function(require, exports, date) {

	exports.tests = {
		setUp: function(callback) {
			this.date1 = new Date(1981, 5, 2, 00, 30, 50, 75);
			this.date2 = new Date(2008, 0, 31, 17, 46, 35, 9);
			this.date3 = new Date(1900, 0, 31, 17, 46, 35, 9);
			this.date4 = new Date(1994, 9, 13, 05, 19, 44, 564);
			callback();
		}
		, "Date: clone": function(test) {
			test.expect(4);
			test.deepEqual(date.clone(this.date1), new Date(1981, 5, 2, 00, 30, 50, 75), 'clone() should return an exact copy of the argument date object');
			test.deepEqual(date.clone(this.date2), new Date(2008, 0, 31, 17, 46, 35, 9), 'clone() should return an exact copy of the argument date object');
			test.deepEqual(date.clone(this.date3), new Date(1900, 0, 31, 17, 46, 35, 9), 'clone() should return an exact copy of the argument date object');
			test.deepEqual(date.clone(this.date4), new Date(1994, 9, 13, 05, 19, 44, 564), 'clone() should return an exact copy of the argument date object');
			test.done();
		}
		, "Date: isLeapYear": function(test) {
			test.expect(4);
			test.strictEqual(date.isLeapYear(this.date1), false, 'isLeapYear() should return false');
			test.strictEqual(date.isLeapYear(this.date2), true, 'isLeapYear() should return true');
			test.strictEqual(date.isLeapYear(this.date3), false, 'isLeapYear() should return true');
			test.strictEqual(date.isLeapYear(this.date4), false, 'isLeapYear() should return true');
			test.done();
		}
		, "Date: getOrdinal": function(test) {
			test.expect(6);
			test.strictEqual(date.getOrdinal(this.date1), 'nd', 'getOrdinal() should return "nd"');
			test.strictEqual(date.getOrdinal(this.date2), 'st', 'getOrdinal() should return "st"');
			test.strictEqual(date.getOrdinal(this.date3), 'st', 'getOrdinal() should return "st"');
			test.strictEqual(date.getOrdinal(this.date4), 'th', 'getOrdinal() should return "th"');
			test.strictEqual(date.getOrdinal(new Date(1981, 5, 3, 00, 30, 50, 75)), 'rd', 'getOrdinal() should return "rd"');
			test.strictEqual(date.getOrdinal(new Date(1981, 5, 9, 00, 30, 50, 75)), 'th', 'getOrdinal() should return "th"');
			test.done();
		}
		, "Date: clearTime": function(test) {
			test.expect(2);
			test.deepEqual(date.clearTime(this.date1), new Date(1981, 5, 2, 0, 0, 0, 0), 'clearTime() should zero out the time');
			test.deepEqual(date.clearTime(this.date2), new Date(2008, 0, 31, 0, 0, 0, 0), 'clearTime() should zero out the time');
			test.done();
		}
		, "Date: now": function(test) {
			test.expect(1);
			test.strictEqual(date.now(), new Date().getTime(), 'now() should return the number of milliseconds since the epoch');
			test.done();
		}
		, "Date: diff": function(test) {
			test.expect(7);
			test.strictEqual(date.diff(this.date1, this.date2), 9740, 'diff() should return 9740');
			test.strictEqual(date.diff(this.date3, this.date4), 34587, 'diff() should return 34587');
			test.strictEqual(date.diff(this.date1, this.date2, 'day'), 9740, 'diff() should return 9740');
			test.strictEqual(date.diff(this.date3, this.date4, 'day'), 34587, 'diff() should return 34587');
			test.strictEqual(date.diff(this.date1, this.date2, 'year'), 27, 'diff() should return 27');
			test.strictEqual(date.diff(this.date3, this.date4, 'year'), 95, 'diff() should return 95');
			test.strictEqual(date.diff(this.date1, this.date1), 0, 'diff() should return 0');
			test.done();
		}
		, "Date: within": function(test) {
			test.expect(3);
			test.strictEqual(date.within(this.date4, this.date1, this.date2), true, 'within() should return true');
			test.strictEqual(date.within(this.date1, this.date4, this.date3), false, 'within() should return false');
			test.strictEqual(date.within(this.date1, this.date1, this.date2), false, 'within() should return false');
			test.done();
		}
		, "Date: getLastDayOfMonth": function(test) {
			test.expect(4);
			test.strictEqual(date.getLastDayOfMonth(this.date1), 30, 'getLastDayOfMonth() should return 30');
			test.strictEqual(date.getLastDayOfMonth(this.date2), 31, 'getLastDayOfMonth() should return 31');
			test.strictEqual(date.getLastDayOfMonth(this.date3), 31, 'getLastDayOfMonth() should return 31');
			test.strictEqual(date.getLastDayOfMonth(this.date4), 31, 'getLastDayOfMonth() should return 31');
			// test.strictEqual(date.getLastDayOfMonth(new Date(1984, 1, 2, 00, 30, 10, 75)), 29, 'getLastDayOfMonth() should return 29');
			test.done();
		}
		, "Date: getDayOfYear": function(test) {
			test.expect(4);
			test.strictEqual(date.getDayOfYear(this.date1), 153, 'getDayOfYear() should return 153');
			test.strictEqual(date.getDayOfYear(this.date2), 31, 'getDayOfYear() should return 31');
			test.strictEqual(date.getDayOfYear(this.date3), 31, 'getDayOfYear() should return 31');
			test.strictEqual(date.getDayOfYear(this.date4), 286, 'getDayOfYear() should return 286');
			test.done();
		}
		, "Date: getTimeZone": function(test) {
			test.expect(4);
			test.strictEqual(date.getTimeZone(this.date1), 'CDT', 'getTimeZone() should return "CDT"');
			test.strictEqual(date.getTimeZone(this.date2), 'CST', 'getTimeZone() should return "CST"');
			test.strictEqual(date.getTimeZone(this.date3), 'CST', 'getTimeZone() should return "CST"');
			test.strictEqual(date.getTimeZone(this.date4), 'CDT', 'getTimeZone() should return "CDT"');
			test.done();
		}
	};

});