define(['require','exports','module','functools'], function( require, exports, module, functools ){

	exports["noop"] = {
		returnTest: function( test ){
			for( var x =0;x < 10; x++){
				test.strictEqual( functools.noop(), undefined, "noop should not return anything")
			}
			test.done()
		}
	};

	exports["periodical"] = {
		shortInterval: function( test ){
			var count = 0;
			var id = null;
			test.expect( 9 )
			var dummy = function( num ){
				count++
				var value = num + count
				if( count >= 10 ){
					clearInterval( id );
					test.done();
				}
				test.equal( value, ( num + count ), "periodical should not alter incoming values ")
			};

			id = functools.periodical( dummy, 2, dummy, [1] )[1]
		}
	};

	exports["partial"] = {
		setUp: function( callback ){
			this.dummy = function( ){
				var value = 0
				for( var x = 0; x < arguments.length; x++){
					value += arguments[x];
				}
				return value
			}
			callback();
		}
		,singleargs: function( test ){


			var dummy = functools.partial( this.dummy, 12 );
			test.equal( dummy( 5 ) , 17, "should equal 17");

			test.equal( dummy("hello"), "12hello", "should equal hello12")
			test.done();
		}
		,multiargs: function( test ){
			var dummy = functools.partial( this.dummy, 12, 12 );

			test.equal( dummy( 12, 12), 48, "should equal 48 ");

			test.done()
		}
	};

	exports["overloadSetter"] = {
		setUp: function( callback ){
			this.mock = {
				test:function( key, value ){
					this[key] = value
				}
			}
			callback();
		}
		,"single Values": function( test ){
			test.expect( 4 )
			this.mock.test("key", "value");

			test.equal(this.mock.key, "value", "the test value should be un modified");
			this.mock.test = functools.overloadSetter( this.mock.test );
			this.mock.test("key", "value");
			test.strictEqual(this.mock.key, "value", "overload setter should allow for single key / value params when a string is passed as the first arg");

			this.mock.test( "key", 12 );
			test.strictEqual(this.mock.key, 12, "passing single params through overload setter should leave the values un altered");

			this.mock.test( "key2", "value2" );
			test.strictEqual(this.mock.key2, "value2", "passing single params through overload setter should leave the values un altered");

			test.done();
		}
		,"Key / Value Pairs": function( test ){
			test.expect( 4  )
			this.mock.test("key", "value");
			this.mock.test = functools.overloadSetter( this.mock.test )
			this.mock.test({
				key:"value"
				,key2:'value2'
			});

			test.strictEqual(this.mock.key, "value", "Expected 'value': Passing key/value pairs through overload setter should set each of the values");
			test.strictEqual(this.mock.key2, "value2", "Expected 'value2' : Passing key/value pairs through overload setter should set each of the values");
			this.mock.test({
				key:12
				,key2:44
			});
			test.strictEqual(this.mock.key, 12, "Passing key/value pairs through overload setter should set each of the values");
			test.strictEqual(this.mock.key2, 44, " Passing key/value pairs through overload setter should set each of the values");
			test.done();
		}
	};

	exports["overloadGetter"] = {
		setUp:function( callback ){
			this.mock = {
				test: function( key ){
					return this[key];
				}
			}

			this.mock.test["key1"] = "value1"
			this.mock.test["key2"] = "value2"
			this.mock.test["key3"] = "value3"
			this.mock.test["key4"] = "value4"
			callback()
		}
		,"Single Values": function( test ){
			test.expect( 4 )
			this.mock.test = functools.overloadGetter( this.mock.test )

			test.equal( this.mock.test( "key1" ), "value1", "expected value1 : properties of the overloaded function should remain untouched" );
			test.equal( this.mock.test( "key2" ), "value2", "expected value2 : properties of the overloaded function should remain untouched" );
			test.equal( this.mock.test( "key3" ), "value3", "expected value3 : properties of the overloaded function should remain untouched" );
			test.equal( this.mock.test( "key4" ), "value4", "expected value4 : properties of the overloaded function should remain untouched" );
			test.done()
		}
		,"Key / Value Pairs": function( test ){
			test.expect( 4 );
			var values = {};
			this.mock.test = functools.overloadGetter( this.mock.test );
			values = this.mock.test(['key3', 'key2']);

			test.strictEqual( values["key2"], "value2", "value 2 sepected : Should return an object with orresponding key value pairs");
			test.strictEqual( values["key3"], "value3", "value 3 sepected : Should return an object with orresponding key value pairs");

			values = this.mock.test(['key1', 'key4']);
			test.strictEqual( values["key1"], "value1", "value 1 sepected : Should return an object with orresponding key value pairs");
			test.strictEqual( values["key4"], "value4", "value 4 sepected : Should return an object with orresponding key value pairs");

			test.done();
		}
	}

	exports["attempt"] = {
		setUp: function( callback){
			this.mock = {
				none: function(){
					return functools.attempt(
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ throw new Error()}
					)
				}
				,last: function(){
					return functools.attempt(
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ return 6}
					)
				}
				,first: function(){
					return functools.attempt(
						function(){ return 6},
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ throw new Error()}
					)
				}
				,mid: function(){
					return functools.attempt(
						function(){ throw new Error()},
						function(){ throw new Error()},
						function(){ return 6},
						function(){ throw new Error()},
						function(){ throw new Error()}
					)
				}
			}
			callback()
		}
		,"No values": function( test ){
			test.doesNotThrow( this.mock.none, "attempt should not throw errors")
			test.strictEqual( this.mock.none(), null, "attempt should return null if all functions fail")
			test.done()
		}
		,"Last Value": function( test ){
			test.doesNotThrow( this.mock.first, "attempt should not throw errors")
			test.strictEqual( this.mock.first(), 6, "attempt should should continue to run until it does not recieve an error or return null")
			test.done();
		}
		,"Middle Value":function( test ){
			test.doesNotThrow( this.mock.mid, "attempt should not throw errors")
			test.strictEqual( this.mock.mid(), 6, "attempt should should continue to run until it does not recieve an error or return null")
			test.done();
		}
	};
})