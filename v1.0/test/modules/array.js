define(['require', 'exports', 'array'], function(require, exports, array) {

	exports.tests = {
		setUp: function(callback) {
			this.array = [1, 2, 3, 4, 'test1', 'test2'];
			callback();
		}
		, clean: function(test) {
			var a = [null, null, 1, undefined, 'undefined', 'null'];
			test.expect(1);
			test.equal(array.clean(a).length, 3, "clean() should remove all undefined and null values");
			test.done()
		}
		, combine: function(test) {
			var other_array
			test.equal( this.array.length, 6);

			other_array = array.combine( this.array,  [12, 13, 14, 15] );
			test.equal( other_array.length, 10);
			test.strictEqual( array.indexOf( this.array, 12), 6, 'should return 6');
			test.strictEqual( array.indexOf( this.array, 13), 7, 'should return 7');
			test.strictEqual( array.indexOf( this.array, 14), 8, 'should return 8');
			test.strictEqual( array.indexOf( this.array, 15), 9, 'should return 9');
			test.done();
		}
		, contains: function(test) {
			test.expect(4);
			test.strictEqual(array.contains(this.array, 2), true, 'Array contains 2. Test should return true.')
			test.strictEqual(array.contains(this.array, 44), false, 'Array does not contains 44. Test should return false.')
			test.strictEqual(array.contains(this.array, 2), true, 'Array contains 2. Test should return true.')
			test.strictEqual(array.contains(this.array, 2), true, 'Array contains 2. Test should return true.')
			test.done();
		}
		, each: function(test) {
			var index = 0
			test.strictEqual(this.array.length, 6, "Array has 6 elements. Test should return true.");

			array.each( this.array, function(item, idx, arr ){
				test.equal( index, idx, "index values for each function should match counter");
				if( typeof item === 'number'){
					test.equal( item, (idx + 1), "index counter should increment sequencially" )
				}
				index++
			});
			index = 0;
			test.done()
		}
		, empty: function(test) {
			test.strictEqual( array.empty( this.array).length, 0, "empty should remove all elements in an array");
			test.done();
		}
		, filter: function(test) {
			var a2;

			a2 = array.filter( this.array, function( item ){
				return typeof item === 'string';
			}, this.array)
			test.strictEqual( a2.length, 2, "should return 2 elements")
			a2 = array.filter( this.array, function( item ){
				return typeof item === 'number';
			}, this.array)
			test.strictEqual( a2.length, 4, "should return 4 elements")

			a2 = array.filter( this.array, function( item ){
				return !item;
			}, this.array)
			test.strictEqual( a2.length, 0, "should return 0 elements")

			test.done();
		}
		, from: function(test) {
			test.equal( array.from( ).length, 0, "from() with no arguments should return an empty array ");
			test.equal( array.from( 1 ,2 ).length, 2, "from() with 2 arguments should return an with 2 elements ");
			test.equal( array.from( arguments ).length, 1, "passing arguments to from() should an array with the same number of elements ");
			test.equal( Object.prototype.toString.call( array.from( arguments ) ).match(/array/i)[0] === "Array", true, "passing from() arguments should return an array");
			test.done();
		}
		, getRandom: function(test) {
			var last = null
				,fail = 0
				,failPerc
				,item = null;

			item = array.getRandom(this.array);
			test.notEqual(item, last, "getRandom should return a random object");
			for(var x = 0; x <= 100; x ++ ){
				last = item;
				item = array.getRandom(this.array)

				if( item == last ){
					fail++
				}
			}

			failPerc = (fail/100) * 100
			var didPass = failPerc <= 30
			test.ok(didPass, "getRandom should return a random object... Mostly");
			test.done();
		}
		, indexOf: function(test) {
			test.expect(6)
			test.strictEqual(array.indexOf(this.array, 1), 0, 'Test should return 0.');
			test.strictEqual(array.indexOf(this.array, 2), 1, 'Test should return 1.');
			test.strictEqual(array.indexOf(this.array, 3), 2, 'Test should return 2.');
			test.strictEqual(array.indexOf(this.array, 4), 3, 'Test should return 3.');
			test.strictEqual(array.indexOf(this.array, 'test1'), 4, 'Test should return 4.');
			test.strictEqual(array.indexOf(this.array, 'test2'), 5, 'Test should return 5.');
			test.done();
		}
		, map: function(test) {
			var a2 = array.map(this.array, function(item) {
				if (typeof item === 'number') {
					return item + 2;
				}
			});
			a2 = array.clean(a2);
			test.equal(a2.length, 4, "map function should return 4 elements")
			test.equal(a2[1], 4, "should return 4");
			test.done();
		}
		, unique: function(test) {
			test.expect(1);
			this.array.push(1);
			this.array.push(2);
			this.array.push(3);
			this.array.push(4);
			test.strictEqual(array.unique(this.array).length, 6, 'Array should have 6 elements. Test should return true.')
			test.done();
		}
	};

});