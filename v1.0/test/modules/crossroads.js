define(['require', 'exports', 'core', 'crossroads'], function(require, exports, core, crossroads) {

    exports.add = {
        setUp: function(callback) {
            //this.
            callback();
        }
        , addRoute: function(test) {
            var s1, s2, s3;

            test.expect(13);

            // Simple addRoute
            s1 = crossroads.addRoute('/{foo}');
            test.notEqual(s1, null, 'Should have returned the new route object');
            test.equal(s1.rules, null, 'Route\'s rules should still be undefined');
            test.strictEqual(crossroads.getNumRoutes(), 1, 'Number of routes should be 1');
            test.strictEqual(s1.matched.getNumListeners(), 0, 'Number of listeners to matched should be 0');

            // Add a handler
            s2 = crossroads.addRoute('/{foo}', function() {
                test.ok(true, 'This shouldn\'t be called');
            });
            test.notEqual(s2, null, 'Should have returned the new route object');
            test.equal(s2.rules, null, 'Route\'s rules should still be undefined');
            test.strictEqual(crossroads.getNumRoutes(), 2, 'Number of routes should now be 2');
            test.strictEqual(s2.matched.getNumListeners(), 1, 'Number of listeners to matched should be 1');

            // Try a RegExp for a route pattern!
            s3 = crossroads.addRoute('/{foo}', function() {
                test.ok(true, 'This shouldn\'t be called');
            });
            test.notEqual(s3, null, 'Should have returned the new route object');
            test.equal(s3.rules, null, 'Route\'s rules should still be undefined');
            test.strictEqual(crossroads.getNumRoutes(), 3, 'Number of routes should now be 3');
            test.strictEqual(s3.matched.getNumListeners(), 1, 'Number of listeners to matched should be 1');

            // Remove all routes
            crossroads.removeAllRoutes();
            test.strictEqual(crossroads.getNumRoutes(), 0, 'All routes should have been destroyed');

            test.done();
        }
        // , create: function(test) {



        // }
    };

});