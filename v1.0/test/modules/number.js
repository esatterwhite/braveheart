define(['require', 'exports', 'module', 'number'], function( require, exports, module, number){

	exports.times = {
		times: function( test ){
			test.expect(20)
			var x = {
				prop:11
			}
			number.times(10, function(){
				test.ok(true,' This should be run 10 times')
			});

			number.times(10, function(){
				test.strictEqual( this.prop, 11, 'the third argument to times should be the functions execution context')
			},x)
			test.done();
		}
	}

	exports.from = {
		from: function( test ){
			var x = "12.5"

			test.strictEqual( number.from( x ), 12.5, "from should convert strings to numbers")
			test.strictEqual( typeof number.from( x ), 'number', "from should convert strings to numbers")
			test.strictEqual( number.from( "hello"), null, "from should return null if it can not convert the string")

			test.done();
		}

	}
	exports.limit = {
		limit: function( test ){
			test.strictEqual( typeof number.limit( 100, 0, 10 ), 'number', "limit should return a number")
			test.strictEqual( number.limit( 100, 0, 10 ), 10, "limit should limit value to the desired min and max values")
			test.strictEqual( number.limit( -1, 0, 10 ), 0, "limit should limit value to the desired min and max values")
			test.strictEqual( number.limit( 8, 0, 10 ), 8, "if the value is with in the min and max, the value should be returned")


			test.done();
		}
	}

	exports.format = {
		format: function( test ){
			var n = number.format( 100, {  decimals:2, group:",", decimal:"."} );

			test.equal( typeof n , 'string', "format should return a string")
			test.equal( n.toString(), "100.00", "decimals option should create trailing zeros")

			n = number.format( 1000, {  decimals:2, group:":", decimal:"."} );
			test.equal( n.toString().split(':')[0].length, 1, "the group options should group after 3 digits")

			test.done();
		}
		,"Format Currency": function( test ){
			var n = 100;

			test.equal( number.formatCurrency( n ), "$100.00", "should format a number to USD Format $x,xxx.xx")
			test.done()
		}
		,"Format Percentage": function( test ){
			var n = 100;

			test.equal( number.formatPercentage( n ), "100%", "should format a number to a standard percentage foramt xxx%")
			test.equal( number.formatPercentage( n, 2 ), "100.00%", "Should accept a second param to append leading zeros to numbers")
			test.equal( number.formatPercentage( 100.1, 2 ), "100.10%", "Should accept a second param to append leading zeros to numbers")

			test.done();
		}
	}
	exports.random = {
		random: function( test ){
			test.expect( 20 )
			number.times(10,  function(){
				test.ok ( (number.random(0, 10) <= 10),"random function should never exceed its upper bound" )
			});
			number.times(10,  function(){
				test.ok ( (number.random(0, 10) >= 0),"random function should never exceed its upper bound" )
			});
			test.done();
		}
	}

	exports.round  = {
		roundup: function( test ){
			test.expect(2)
			var x = 1123.12902349
			var r = number.round( x, 1 )

			test.equal(r.toString().split('.')[1].length,1, "round should truncate numbers to the desired precision")

			x = 9.95;
			r = number.round( x, 0 );

			test.equal( r, 10, "round should round up if the decimal is over .5")

			test.done()
		}
		,rounddown: function( test ){
			test.expect( 1 )
			var x = 9.35;
			var r = number.round( x, 0 );
			test.equal( r, 9, "round should round down if the decimal is under .5")
			test.done();
		}
	}
});