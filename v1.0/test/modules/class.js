define(['require', 'exports', 'class','functools'], function( require, exports, _class, functools ){
	var Class = _class.Class;
	var Events = _class.Events;
	var Options = _class.Options;
	var Storage = _class.Storage;
	exports.Creation = {
		setUp: function( callback ){
			this.cls = new Class({
				initialize: function( value ){
					this.value = value
				}
				,getValue: function(){
					return this.value
				}
				,setValue: function( val ){
					this.value = val;
					return this;
				}
			})

			callback();
		}
		,initialize: function( test ){
			var cls = this.cls
			var x = new cls(12);

			test.ok( x.value, "initialize function should accept positional arguments");
			test.equal( typeof x.getValue, "function", "Class creation should copy its template to its instances")
			test.strictEqual( x.getValue(), 12, "functions should not alter value types - expected 12")
			x.setValue("Hello");

			test.equal(x.getValue(),  "Hello", "setValue expected to set the value unaltered ");
			test.done();
		}

	};

	exports.inheritance = {
		setUp: function( callback ){
			this.baseCls = new Class({
				initialize: function( value ){
					this.value = value
				}
				,getValue:function(){
					return this.value;
				}
				,setValue:function( value ){
					this.value = value;
					return this;
				}
				,resetValue: functools.protect(function(){
					this.value = null;
					return this;
				})
			})
			callback();
		}
		,tearDown: function( callback ){
			delete this.baseCls;
			callback();
		}
		,extend: function( test ){
			test.expect( 7 )
			var MyCls;

			MyCls = new Class({
				Extends:this.baseCls
				,initialize: function( value, altValue  ){
					this.altValue = altValue;
					this.parent( value ); // calls the parent class constructor
				}
				,reset: function(){
					this.resetValue();
				}
				,setValue: function( value ){
					this._lastValue = this.value;
					this.parent( value ) // calls parent setValue function
				}
			})

			var x = new MyCls(12, 15 );

			test.ok( x.value, "calling parent should invoke the superclass function ")
			test.ok( x.altValue, "calling parent should not undo other functionality of the same function")
			test.strictEqual(x.getValue(), 12, "Subclasses should inherit all superclass methods")
			try{
				x.resetValue();
				test.ok(fasle ,"protected method", "protected methods should throw if called from an instance" )
			}catch( e ){
				test.ok(true, "this should pass")

			}
			test.doesNotThrow( x.reset.bind( x ), "protected methods", "protected methods should not throw if called from its owner class" );

			x.setValue( 100 );
			test.equal( x.value, 100, "calling parent should call the superclass method of the same name");
			x.setValue( 200 )
			test.equal( x._lastValue, 100, "calling the parent method should not modify the behavior of the host function")
			test.done();
		}
	};
	exports.Options={
		setUp: function( callback ){
			this.cls = new Class({
				Implements:[Options]
				,options:{
					starter:true
				}
				,initialize: function( options ){
					this.setOptions( options )
				}
				,getDefault: function(){
					return this.options.starter
				}
				,update: function( options ){
					this.setOptions( options )
				}
			});
			callback()
		}
		,tearDown: function( callback ){
			delete this.cls;
			callback();
		}
		,setoptions: function( test ){
			test.expect( 5 )
			var x = new this.cls()

			test.strictEqual(x.getDefault(), true, "initialization with no options should leave defaults untouched");
			x = new this.cls({
				starter: false
			})
			test.strictEqual(x.getDefault(), false, "options passed to constructor should override default values.");
			test.equal(x.options.other, null, "options passed to constructor should override default values.");

			x.update({other:true})
			test.strictEqual(x.options.other, true, "setOptions should add values that do not exist in current options hash");

			x.update({other:false})
			test.strictEqual(x.options.other, false, "setOptions should update values that exist in the current options hash");

			test.done();
		}
	};

	exports.Events = {
		setUp: function( callback ) {
			this.cls = new Class({
				Implements:[Events]
				,initialize: function( value ){
					this.value = value;
				}
				,update: function( value ){
					var old_value = this.value

					this.fireEvent('beforechange', this.value );
					this.value = value;
					if( old_value == this.value ){
						this.fireEvent('nochange');
						return;
					}
					this.fireEvent('change', [this.value, old_value] )
				}
			})
			callback()
		}
		,tearDown: function( callback ){
			delete this.cls;
			callback();
		}
		,addevent: function( test ){
			var x = new this.cls( 33 );
			test.expect( 11 )
			x.addEvent('beforechange', function( value ){
				test.ok( value, "Events should pass supplied arguments to handler")
				test.equal( arguments.length, 1, "Events should pass same number of arguemtns to event handlers")
			});
			x.addEvent('change', function( newval, oldval ){
				test.ok( newval, "Events should pass supplied arguments to handler")
				test.equal( arguments.length, 2, "Events should pass same number of arguemtns to event handlers")
			});
			x.addEvent('nochange', function(){
				test.equal(arguments.length, 0, "fireEvent should not append arguments that are not supplied")
			})

			x.update( 2 );
			x.update( "Hello world")
			x.update( "Hello world")
			test.done();
		}
		,removeevent: function( test ){
			var x = new this.cls( 45 );
			var count = 0
			test.expect(  )
			var handler = function( value ){
				count++;
				test.equal( count, 1, "event should only be fired once");
			}

			x.addEvent( 'beforechange', handler );
			x.update( 43 );
			x.removeEvent('beforechange', handler)

			x.update( 53 );
			x.update( 63 );

			test.done();

		}
	};

	exports.MultiInheritance = {
		setUp: function( callback ){
			this.baseCls = new Class({
				initialize: function( value ){
					this.value = value
				}
				,getValue:function(){
					return this.value;
				}
				,setValue:function( value ){
					this.value = value;
					return this;
				}
				,resetValue: functools.protect(function(){
					this.value = null;
					return this;
				})
			})
			callback();
		}
		,tearDown: function( callback ){
			delete this.baseCls;
			callback();
		}
		,EventsMutator: function( test ){
			var MyClass = new Class({
				Extends:this.baseCls
				,Implements:[Options, Events ]
				,OnEvents:true
				,options:{
					onDummy: function(){}
				}
				,initialize: function( options ){
					this.setOptions( options )
				}
				,setValue: function(value){
					this._value = value;
					this.parent( value )
				}
				,dummy: function(){
					this.fireEvent( 'dummy' )
				}
				,added: function(){
					this.fireEvent('added')
				}
				,onned: function(){
					this.fireEvent('onned')
				}
			})

			var x = new MyClass({
				sample:12
				,onDummy: function(){
					test.ok(true,"options starting with `on` should be added as an event")
				}
			});
			x.setValue(33);
			x.dummy();
			x.addEvent('added', function(){
				test.equal( x._value, 33, "OnEvents should not modify addEvent behavior")
			})
			x.on('onned', function(value){
				test.equal(x._value, 33, "OnEvent mutatorshould behave just like ddEvent")
			})
			x.added();
			x.onned();
			test.done();

		}

	};

	exports.Storage = {
		setUp: function( callback ){
			this.cls = new Class({
				Implements:Storage
				,initialize: function( value ){
					this.store( 'value', value )
				}
				,multValue: function( multi ){
					var value = this.retrieve( "value" );
					return value * ( multi || 1 );

				}

			});
			callback();
		}
		,store: function( test ){
			var x = new this.cls( 5 );
			var y = new this.cls("hello")

			test.doesNotThrow( function(){
				x.store('xkey', 12 );
			}, 'class storage', "retrieval of a value should not throw errors" );


			test.done();

		}
		,retireve: function( test ){
			var cls = this.cls
			var x = new cls( 5 );
			var y = new cls( "hello" );
			x.store('xkey', "test" )
			test.notEqual( x.retrieve('value'), y.retrieve('value'), "Keys should be unique between instances")
			test.equal( typeof x.retrieve('value'), 'number', "storage should not convert data types.")

			test.strictEqual( x.retrieve('value'), 5, "storage should not convert data types.")
			test.equal( typeof x.retrieve('xkey'), 'string', "storage should not convert data types.")

			test.strictEqual( y.retrieve('value'), 'hello' , "storage should Alter passed in values")
			test.strictEqual( typeof y.retrieve('value'), 'string' , "storage should not convert data types.")

			test.done();
		}
	}
});