define(['require','exports', 'module', 'iter'], function( require, exports, module, iter ){


	exports.count = {
		setUp: function( callback ){
			this.counter = new iter.count()
			callback()
		}
		, "Base Counter": function( test ){
			test.expect( 2 )
			test.doesNotThrow( function(){ new iter.count()}, "count should not throw an error if passed no parameters" )
			try{

				new iter.count("Hello world")
			} catch( e ){
				test.ok(true, "passing non digit chars should raise an exception")
			}
			test.done();
		}
		,next: function(test){
			for( 0; this.counter.next() < 99;){}

			test.equal( this.counter.next(), 100, "counter should increment by one with every next")
			test.done()
		}
	}

	exports.tostring = {

		tostring:function( test ){
			var x = new iter.cycle(1,2,3);

			test.ok( x.toString().indexOf('Iterable' ) != -1, "Iterable toString method should indeicate it is an Iterable" )
			test.done();
		}
	}

	exports.cycle = {
		setUp: function( callback ){
			this.cycle = new iter.cycle( 1,2,3 )
			callback()
		}
		,infinite: function(test){
			for (var i = 0; i >= 100; i++) {
				this.cycle.next();
			};

			test.ok( true, "The cycle Iteratable should never throw a StopIteration error")
			test.done()
		}
	}

	exports.fromarray = {
		setUp: function( callback){
			this.sequence =  [1,2,3,4];

			callback()
		}
		,order: function( test ){
			var x = iter.from( this.sequence );

			test.equal(typeof x.next, 'function', "iter.from should create a new Iterable")

			test.equal(x.next(), 1, "elements of the Iterable should match that of the sequence")
			test.equal(x.next(), 2, "elements of the Iterable should match that of the sequence")
			test.equal(x.next(), 3, "elements of the Iterable should match that of the sequence")
			test.equal(x.next(), 4, "elements of the Iterable should match that of the sequence")

			test.done();
		}
		,error: function( test ){
			test.expect( 5 )
			var x = iter.from( this.sequence );
			while( true ){
				try{
					test.ok( x.next(), "Calling next should return a value if it can")
				} catch( e ){
					test.strictEqual( e, iter.StopIteration, "A StopIteration should be thrown when an iterable has been exhausted")
					break;
				}
			}
			test.done();
		}
	};

	exports.fromobject = {
		setUp: function( callback ){
			this.obj = {
				key1:1
				,key2:2
				,key3:'value'
			}
			callback();
		}
		,tearDown: function( callback ){
			delete this.obj;
			callback();
		}
		,"From: object - values":function( test ){
			var itr = iter.from( this.obj );
			test.strictEqual(itr.next(), 1)
			test.strictEqual(itr.next(), 2)
			test.strictEqual(itr.next(), "value")
			test.done()
		}
		,"From: Object - error": function( test ){
			test.expect( 4 )
			var x = iter.from( this.obj );
			while( true ){
				try{
					test.ok( x.next(), "Calling next should return a value if it can")
				} catch( e ){
					test.strictEqual( e, iter.StopIteration, "A StopIteration should be thrown when an iterable has been exhausted")
					break;
				}
			}
			test.done();
		}
	};

	exports["iSlice"] = {

		setUp: function( callback ){
			this.sequence = [ 1,2,3,4, "good", "bad", true, false ]
			callback();
		}

		,"iSlice: No Step": function( test ){

			var itr = iter.islice( this.sequence, 3 );
			var val = itr.next()
			test.strictEqual( val , 1, "Expected 1 got " + val )

			var val = itr.next()
			test.strictEqual( val , 2, "Expected 2 got " + val )

			var val = itr.next()
			test.strictEqual( val , 3, "Expected 3 got " + val )

			test.throws(function(){itr.next()}, "Should throw a StopIteration error");


			test.done();
		},
		"iSlice: With Step": function( test ){
			var itr = iter.islice( this.sequence, 0, this.sequence.length, 2)

			var val = itr.next()
			test.strictEqual( val , 1, "Expected 2 got " + val )

			var val = itr.next()
			test.strictEqual( val ,3, "Expected 2 got " + val )

			var val = itr.next()
			test.strictEqual( val , "good", "Expected good got " + val )

			var val = itr.next()
			test.strictEqual( val , true, "Expected true got " + val )

			test.throws( function(){ iter.next() }  )

			test.done()
		}
	}

	exports.sum = {

		"sum with numbers": function( test ){

			var s = iter.sum( [21,32,54,12,44 ] );
			test.strictEqual(s, 163, "sum should add all values in an Iterator")
			test.done()
		}

		,"Sum with start": function( test ){
			var s = iter.sum( [21,32,54,12,44 ], 10);
			test.strictEqual(s, 173, "sum should add all values in an Iterator")
			test.done()
		}
		,"sum: mixed values": function(test){

			var s = iter.sum(['1', "2", "3"])

			test.strictEqual(s, "0123", "sum should not do any type casting");
			test.done();
		}
	};

	exports.each={
		setUp: function( callback ){
			this.arraySequence = [ 1,2,3,4 ]
			this.objSequence = {key1:'value1', key2:'value2', key3:'value3', key4:"value4"}
			callback()
		}
		,"Iter: For Each - Array": function( test ){
			test.expect( 4 )
			iter.forEach( this.arraySequence, function( val, index ){
				test.ok( val )
			})
			test.done()
		}
	};

	exports.sorted = {
		setUp: function( callback ){
			this.arraySequence = [12,43,54,32,21,-4,4]
			this.objSequence = {key1:12,key2:43,key3:54,key4:32,key5:21,key6:-4,key7:4};
			callback()
		}
		,"Iter.sort Array": function( test ){
			var x = iter.sorted( this.arraySequence )

			var itr = iter.from( x );
			var last = itr.next();
			var now = null;
			while( true ){
				try{
					now = itr.next()
					test.ok( last < now, "sorted should return a sorted array" )
					last = now;
				}catch( e ){
					test.strictEqual(e, iter.StopIteration, "Iterable should throw StopIteration when it can not continue")
					break;
				}
			}

			test.done();
		}
		,"Iter.sort Object": function( test ){
			var x = iter.sorted( this.objSequence )

			var itr = iter.from( x );
			var last = itr.next();
			var now = null;
			while( true ){
				try{
					now = itr.next()
					test.ok( last < now, "sorted should return a sorted array" )
					last = now;
				}catch( e ){
					test.strictEqual(e, iter.StopIteration, "Iterable should throw StopIteration when it can not continue")
					break;
				}
			}

			test.done();
		}
	}

	exports.imap = {
		setUp: function( callback ){
			this.iterA = [1,2,3,4];
			this.iterB = [5,6,7,8];
			this.iterC = [9,10,11,12];

			callback()
		}
		,"iMap Multi iters": function( test ){
			var mapped = iter.imap( function( x, y, z ){
				return x + y + z;
			}, this.iterA, this.iterB, this.iterC );
			var value = null;

			test.doesNotThrow( function(){
				value = mapped.next();
			})
			test.strictEqual(value, 15, "Expected 15, got " + value )

			test.doesNotThrow( function(){
				value = mapped.next();
			})
			test.strictEqual(value, 18, "Expected 18, got " + value )

			test.doesNotThrow( function(){
				value = mapped.next();
			})
			test.strictEqual(value, 21, "Expected 21, got " + value )

			test.doesNotThrow( function(){
				value = mapped.next();
			})
			test.strictEqual(value, 24, "Expected 24, got " + value )

			try{
				mapped.next();
			}catch( e ){
				test.strictEqual(e, iter.StopIteration, "Iterables should throw a StopIteration when they cannot continue")
			}
			test.done();
		}
	}
	exports.cycle = {
		setUp: function(callback){
			this.arraySequence = [1,2,3,4,5];
			this.objSequence = {key1:1, key2:2, key3:3, key4:4, key5:5};

			callback();
		}
		,"Cycle: array": function( test ){
			test.expect( 15 );
			var itr = iter.cycle( this.arraySequence);
			for(var x=0; x<3; x++){
				for(var y=1; y<6; y++){
					test.equal( itr.next(), y, "Expected " + y + "got: " + y)
				}
			}
			test.done();
		}
		,"Cycle: object": function( test ){
			test.expect( 15 );
			var itr = iter.cycle( this.objSequence);
			for(var x=0; x<3; x++){
				for(var y=1; y<6; y++){
					test.equal( itr.next(), y, "Expected " + y + " got: " + y)
				}
			}
			test.done();
		}
	};

	exports.reduce = {
		"reduce with array": function( test ){
			var result;
			test.expect( 2 );

			test.doesNotThrow( function(){

				result = iter.reduce( [1,2,3,4,5], function( a,  b ){ return a + b; })
			})

			test.strictEqual( result, 15,  "expected 15, got "  + result );

			test.done()
		}
		,"reduce with object": function( test ){
			test.expect( 2 );

			var result;

			test.doesNotThrow( function(){
				result = iter.reduce({
					key1:1,
					key2:2,
					key3:3,
					key4:4,
					key5:5
				}, function( a, b ){ return a + b ;});
			});

			test.strictEqual( result, 15,  "expected 15, got "  + result );

			test.done();
		}
	}
});
