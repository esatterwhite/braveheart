define(['require', 'exports', 'color'], function(require, exports, color) {


	exports.creation = {
		setUp: function( callback ){

			callback();
		}


		,"Creation via text": function( test ){
			test.equal( new color.Color( "red").toHEX(), "#ff0000" );
			test.equal( new color.Color( "maroon").toHEX(), "#800000" );
			test.equal( new color.Color( "green").toHEX(), "#008000" );
			test.equal( new color.Color( "yellow").toHEX(), "#ffff00" );
			test.equal( new color.Color( "fuchsia").toHEX(), "#ff00ff" );
			test.equal( new color.Color( "white").toHEX(), "#ffffff" );
			test.equal( new color.Color( "lime").toHEX(), "#00ff00" );
			test.done();
		}
		,"Creations via array": function( test ){
			var clr = new color.Color([22,22,22], "rgb")

			test.equal(clr.toRGB(), "rgb(22, 22, 22)", "expected rgb(22, 22, 22), Got " + clr.toRGB() );
			test.equal(clr.invert().toRGB(), "rgb(233, 233, 233)", "expected rgb(233, 233, 233), Got " + clr.invert().toRGB());
			test.equal(clr.toHEX(), "#161616", "expected #161616, Got " + clr.toHEX());
			test.done();
		}
		,"Creations via number": function( test ){
			test.equal( new color.Color( 3 ).toHEX(), "#333333"  );
			test.equal( new color.Color( 333333 ).toHEX(), "#516155"  );
			test.done();
		}
	};

	exports.statics={
		"Creation via rgb method": function( test ){
			test.equal( color.hex("#000000" ).toRGB(), "rgb(0, 0, 0)");
			test.equal( color.hex("#ff00ff").toRGB(), "rgb(255, 0, 255)" );
			test.equal( color.hex("#325500").toRGB(), "rgb(50, 85, 0)" );
			test.done();
		}

		,"Creation via hex method": function( test ){

			test.equal( color.rgb("rgb(0, 0, 0)").toHEX(), "#000000" );
			test.equal( color.rgb("rgb(255, 0, 255)").toHEX(), "#ff00ff" );
			test.equal( color.rgb("rgb(50, 85, 0)").toHEX(), "#325500" );
			test.done()
		}

		,"Creation via hsb method": function( test ){
			test.equal( color.hsb("hsb(0, 0, 0)").toHEX(), "#000000" );
			test.equal( color.hsb("hsb(300, 100, 100)").toHEX(), "#ff00ff" );
			test.equal( color.hsb("hsb(85, 100, 33)").toHEX(), "#315400" );
			test.done()
		}
	};

	exports.manipulation = {
		invert: function( test ){
			test.equal( color.hex("#000000" ).invert().toHEX(), "#ffffff");
			test.equal( color.hex("#FFFFFF").invert().toHEX(), "#000000" );
			test.equal( color.rgb("rgb(155, 0, 200)").invert().toRGB(), "rgb(100, 255, 55)" );
			test.done()
		}
		, mix : function( test ){

			test.done();
		}
	};
})
