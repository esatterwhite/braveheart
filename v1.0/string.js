/*jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * Module for manipulating string objects
 * @module string
 * @author Eric Satterwhite
 * @requires array
 */

define(
    ['require', 'exports', 'module', 'array' ]
    ,function(require, exports, module, array ) {
        var getRegexForTag = function(tag, contents){
            tag = tag || '';
            var regstr = contents ? "<" + tag + "(?!\\w)[^>]*>([\\s\\S]*?)<\/" + tag + "(?!\\w)>" : "<\/?" + tag + "([^>]+)?>",
                reg = new RegExp(regstr, "gi");
            return reg;
        };

        var exec = function(text){
                if (!text){
                    return text;
                }

                if (window.execScript){
                    window.execScript(text);
                } else {
                    var script = document.createElement('script');
                    script.setAttribute('type', 'text/javascript');
                    script.text = text;
                    document.head.appendChild(script);
                    document.head.removeChild(script);
                }
                return text;
            };

            /**
             * Capitalizes a Every word in a string
             * @param {String} str The string to capitalize
             * @returns {String}
             * @example braveheart([ 'string '], function( string ){
    string.capitalize( "this is it" ) // This Is It
})
             */
            exports.capitalize = function( str ){
                var s = String( str );
                return s.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
            };


            /**
             * Removes extraneous white space from a string
             * @param {String} str The string clean
             * @returns {String}
             * @example braveheart([ 'string '], function( string ){
    string.clean( " this     is a lame      sentance") // this is a lame sentance
})
             */
            exports.clean = function(str) {
                var s = String( str );
                return exports.trim((s).replace(/\s+/g, ' '));
            };

            /**
             * Tries to determin if a part of a string exists within a nother string
             * @param {String} substr The string to look for
             * @param {String} str The string to look through
             * @return {Boolean} True if the sub string is found
             * @example braveheart([ 'string '], function( string ){

    string.contains("hell", "hello world") // true
    string.contains("bell", "hello world") // false
})
             */
            exports.contains = function(str, substr) {
                return str != null && substr != null && str.indexOf(substr) >= 0;
            };

            /**
             * Determines if a string ends with a specified bit of text
             * @param {String} str the string to inspect
             * @param {String} suffix The bit of text you want to check for
             * @returns {Boolean} true if the specified string ends with the suffix
             * @example braveheart(['string'], function( string ){
    string.startsWith("Helloworld", "hello") // true
    string.startsWith("Helloworld", "world") // false
})
             */
            exports.endsWith = function(str, suffix) {
                var s = String( str );
                suffix = suffix || "";

                return s.indexOf( suffix, s.length - suffix.length ) !== -1;
            };

            /**
             * Determines if a string starts with a specified bit of text
             * @param {String} str the string to inspect
             * @param {String} prefix The bit of text you want to check for
             * @returns {Boolean} true if the specified string ends with the prefix
            * @example braveheart(['string'], function( string ){
    string.endsWith("Helloworld", "world") // true
    string.endsWith("Helloworld", "hello") // false
})
             */
            exports.startsWith = function(str, prefix) {
                var s = String( str );
                prefix = prefix || "";

                return s.indexOf( prefix ) !== -1;
            };
            /**
             * creates a string from the passed in item
             * @param {Object} item The object to convert to a string
             * @return {String}
             */
            exports.from = function(item) {
                return "" + item;
            };

            /**
             * Pads at the beginning of a string with a passed in string
             * @param {String} str the string to pad
             * @param {Number} minLength The length at witch to stop padding the string
             * @param {String} fillChar The charachters to pad the string with on each iteration
             * @return {String}
             * @example braveheart(['string'], function( string ){
    string.padRight("sh", 4 "x") // shxx
})
             */
            exports.padLeft = function(str, minLength, fillChar) {
                var s = String( str || "" );
                fillChar = fillChar || " ";

                while (s.length < minLength) {
                    s =  fillChar + s;
                }

                return s;
            };
            /**
             * Pads at the end of a string with a passed in string to match the specified length
             * @param {String} str the string to pad
             * @param {Number} minLength The length at witch to stop padding the string
             * @param {String} fillChar The charachters to pad the string with on each iteration
             * @return {String}
             * @example braveheart(['string'], function( string ){
    string.padRight("sh", 4 "x") // shxx
})
             */
            exports.padRight = function(str, minLength, fillChar) {
                var s = String( str || "" );
                fillChar = fillChar || " ";

                while (s.length < minLength) {
                    s += fillChar;
                }

                return s;
            };

            /**
             * Converts a string in the format of 12px and converts it into an integer
             * @param {String} str the string to convert
             * @return {Number}
             */
            exports.pxToInt = function( str ){
                return exports.toInt( str.split('px')[0], 10);
            };

            /**
             * attempts to revers the characters of a string
             * @param {String} str the string to reverse
             * @return {String}
             */
            exports.reverse = function( str ){
                var s = String( str || "");
                return s.split('').reverse().join('');
            };

            /**
             * Splits a string on a specified separator and returns a limited subset of peices
             * @param {String} str The string to split
             * @param {String} separatar The separator to split the string on. Defaults to newline
             * @param {Number} max The maximum number of string peices to return. If not specified, all peices are returned
             *
             **/
            exports.split = function(str, separator, max) {
                if (!str) {
                    return str;
                }
                var s = String( str );
                separator = separator || "\n";

                var bits = s.split(separator);
                if (max == null || max >= bits.length - 1) {
                    return bits;
                }

                bits.splice(max, bits.length, bits.slice(max, bits.length).join(separator));
                return bits;
            };

            /**
             * String all non alpha characters from a string
             * @param {String} str The string to strip
             * @returns {String}
             */
            exports.stripNonAlpha = function( str ){
                var s = String( str );
                return s.replace(/[^A-Za-z ]+/g, "");
            };

            /**
             * Will attempts anything that looks like script tags, and anything inside of script tags from a string
             * @param {String} str The string to strip
             * @param {Boolean|Function} exec If set to true. if a function is passed, it will be used as the function to execute the scripts<br/>
             <ul>
                <li>scripts - the extracted code</li>
                <li>text - the textual content from the script tag</li>
             </ul>
             * @return {String} the text from within the script tag
             */
            exports.stripScripts = function( str, execute ){
                var scripts = "";
                var s = String( str );
                var text = s.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function(all, code){
                                scripts += code + '\n';
                                return '';
                            });

                if( typeof execute === 'boolean' ){
                    exec( scripts );
                } else if( typeof execute === 'function'){
                    execute( scripts );
                }

                return text;
            };

            /**
             * Strips specific tags from html strings
             * @param {String} str The string to strip tags from
             * @param {String} tag
             * @param {Boolean} contents Set to true if you want to contents of the tag striped as well
             * @returns {String}
             * @example braveheart(['string'], function( string ){
    string.stripTags( "<div><span>hello world</span></div>", "span" ) // <div>hello world</div>
    string.stripTags( "<div><span>hello world</span></div>", "span", true ) // <div></div>
})
             */
            exports.stripTags = function(str, tag, contents ){
                var s = String( str );
                return s.replace( getRegexForTag(tag, contents), "" );
            };


            /**
             * Replaces place placeholders( {holder} ) in a string with the values of matching keys in an object
             * @param {String} str The string to manipulate
             * @param {Object} obj The object to pull values from
             * @param {RegEx} regex A regular expression to use istead of the default
             * @example braveheart(['string']. function( string ){
    string.substitute( "Hello {place}, my name is {name}!",
        {
            place:'world',
            name:'Joe'
        }); //Hello world, my name is Joe!
 })
             */
            exports.substitute = function(str, obj, regex) {
                str = str || "";

                var s = String( str );
                s = s.replace(regex || (/\\?\{([^{}]+)\}/g), function(match, name) {
                    if (match.charAt(0) == '\\') {
                        return match.slice(1);
                    }
                    return (obj[name] != null) ? obj[name] : "";
                });

                return s;
            };

            /**
             * attempts to convert a string to a number
             * @param {String} str The string to convert
             * @param {Number} base The numeric base to use as a conversion radix. The default is 10
             * @return {Number} The string as an integer
             *
             */
            exports.toInt = function( str, base ){
                var num = parseInt( str, base || 10 );

                return isNaN( num ) ? 0 : num;
            };

            /**
             * attempts to convert a string to a floating point number
             * @param {String} str The string to convert
             * @return {Number}
             *
             */
            exports.toFloat = function( str ){
                var num = parseFloat( str );

                return isNaN( num ) ? 0.0 : num;
            };


            exports.trim = function(str) {

                var s = String( str );
                if( s.length > 100 ){
                    s = String(s).replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                }else{
                    // regex is pretty slow at jumping the the end of a string
                    // it has to parse the entire string and compare each char with the
                    // previous one. Long strings or strings with just white space
                    // result in an excessive amount of backtracking.

                    //this  does the fast op of removing leading spaces with a regex
                    // and using subscripting of a string to remove trailing space.
                    s = String(s).replace(/^\s+/, "");

                    for( var x = s.length -1; x >=0; x-- ){
                        if( /\S/.test( s.charAt( x ) ) ){
                            s = str.substring( 0, x + 1 );
                            break;
                        }
                    }
                }

                return s;
            };

            /**
             * Generates a unique ID
             * @returns {String}
             *
             */
            exports.id = function( ){
                return (+ new Date()).toString( 36 );
            };

            /**
             * Escapes Regular expression chars from a string
             * @param {String} str The string to escape
             * @returns {String} An escaped string
             */
            exports.escapeRegEx= function( str ){
                return String( str ).replace( /([-.*+?^${}()|[\]\/\\])/g, '\\$1' );
            };
            /**
             * Attempts to struncate a string at a cutoff point
             * @param {String} str The string to truncate
             * @param {Number} max The maximum number of characters
             * @param {String} tail An optional string to append to the string as a truncation indicator. If spcified, the number of characters in the tail will be count towards the character limit
             * @param {String}
             **/
            exports.truncate = function(str, maxLength, tail) {
                var s = String( str );
                if (s == null || s.length <= maxLength || maxLength < 0) {
                    // readable version, not the String object
                    return s.toString();
                } else if (tail != null) {
                    s = s.slice(0, Math.max(0, maxLength - tail.length));
                    if (typeof s === 'string') {
                        return s + tail;
                    } else {
                        return array.append(array.from(s), array.from(tail));
                    }
                } else {
                    return s.slice(0, maxLength);
                }
            };

            /**
             * Takes a hyphenated string and converts it to a camelCased string
             * @param {String} str The string to camelCase
             * @return {String}
             */
            exports.camelCase =function(str) {
                var s = String( str );
                return s.replace(/-\D/g, function(match){
                    return match.charAt(1).toUpperCase();
                });
            };

            /**
             * Takes a CamelCased string string and converts it to a hyphenated string
             * @param {String} str The string to camelCase
             * @return {String}
             */
            exports.hyphenate = function( str ){
                return str.replace( /[A-Z]/g, function( match ){
                    return ("-" + match.charAt(0).toLowerCase() );
                });
            };

            /**
             * Converts an rgb string to an array of HSB values
             * @param {String} str The string to convert
             * @returns {Array} An array of HSB values. will return null if an rgb string is not identified
             * @example braveheart(['string'], function( string ){
    string.rgbToHSB( "rgb( 22, 22, 22 )" ) // [0, 0, 9]
})
             */
            exports.rgbToHsb = function( str ){
                var rgb = str.match(/\d{1,3}/g);
                return (rgb) ? array.rgbToHsb( rgb ) : null;
            };

            /**
             * Converts an HSB string to an array of RGB values
             * @param {String} str The string to convert
             * @returns {Array} An array of rgb values. Will return null if an HSB string is not identified
             * @example braveheart(['string'], function( string ){
    string.hsbToRgb( "hsb( 0, 0, 9 )" ) // [22, 22, 22]
})
             */
            exports.hsbToRgb = function( str ){
                var hsb = str.match(/\d{1,3}/g);
                return (hsb) ? array.hsbToRgb( hsb ) : null;
            };

            /**
             * attempts to remove any non word strings of text from a string
             * param {String} str The string to parse
             * @return {String} A new string with any non words removed
             */
            exports.removeNonWord = function( str ){
                var s = String( ( str|| "") );
                return (s).replace(/[^0-9a-zA-Z\xC0-\xFF \-]/g, ''); //remove non-word chars

            };

            /**
             * replaces any accented words with their regular ascii counter parts
             * @param {String} str the string to parse
             * @return {String} str a new string with no accented characters
             */
            exports.replaceAccents = function( str ) {
                var s = String( str || "" );

                s = s
                    .replace(/[\xC0-\xC5]/g, "A")
                    .replace(/[\xC6]/g, "AE")
                    .replace(/[\xC7]/g, "C")
                    .replace(/[\xC8-\xCB]/g, "E")
                    .replace(/[\xCC-\xCF]/g, "I")
                    .replace(/[\xD0]/g, "D")
                    .replace(/[\xD1]/g, "N")
                    .replace(/[\xD2-\xD6\xD8]/g, "O")
                    .replace(/[\xD9-\xDC]/g, "U")
                    .replace(/[\xDD]/g, "Y")
                    .replace(/[\xDE]/g, "P")
                    .replace(/[\xE0-\xE5]/g, "a")
                    .replace(/[\xE6]/g, "ae")
                    .replace(/[\xE7]/g, "c")
                    .replace(/[\xE8-\xEB]/g, "e")
                    .replace(/[\xEC-\xEF]/g, "i")
                    .replace(/[\xF1]/g, "n")
                    .replace(/[\xF2-\xF6\xF8]/g, "o")
                    .replace(/[\xF9-\xFC]/g, "u")
                    .replace(/[\xFE]/g, "p")
                    .replace(/[\xFD\xFF]/g, "y");

                return s;
            };

            /**
             * conversts a string into a url safe slug
             * @param {String} str the string to convert
             * @return {String} slug a new slug
             */
            exports.slugify = function( str ){
                var s = String( str || "" );

                s = exports.trim(
                        exports.removeNonWord(
                            exports.replaceAccents(
                                s
                            )
                        )
                    );

                s = s.replace(/ +/g, "-").toLowerCase();

                return s;

            };

            /**
             * Attempts to normalize line breaks in a string
             * @param {String} str the string to normalize
             * @param {String} lineEnd The character to use as the break character. Defaults to "\n"
             * @return {String} str a new string with normalized line breaks
             */
            exports.nomalizeLinebreaks = function( str, lineEnd ){
                var s = String( str || "" );

                lineEnd = lineEnd || '\n';

                s = s
                    .replace(/\r\n/g, lineEnd) // DOS
                    .replace(/\r/g, lineEnd)   // Mac
                    .replace(/\n/g, lineEnd);  // Unix

                return s;
            };

            /**
             * attempts to convert a string to an alagous data type ( ex "true" -> true )<br />
             * works on null, booleans, undefineds, number and floats
             * @param {String} str the string to type cast
             * @return {Mixed} a data type that matched the passed in item
             */
            exports.typeCast = function( val ){
                var r
                    ,UNDEF;

                if( val === null || val === 'null'){
                    return null;
                } else if( val === 'true' ){
                    return true;
                } else if( val === 'false' ){
                    return false;
                } else if( val === UNDEF || val === 'undefined' ){
                    r = UNDEF;
                } else if( val === '' || isNaN( val )){
                    r = val;
                } else{
                    r = parseFloat( val );
                }

                return r;
            };

            /**
             * Based on a count, will return eithe a specified singluar or plural version of a word. <br />
             * Will return the singular version if the count is 1
             * @param {Number} count The count to use as a qualifier
             * @param {String} singular The singular form of the word
             * @param {String} plural The plural form of the word
             * @return {String} either the singular or plural word based on the count
             */
            exports.pluralize = function( count, singular, plural ){
                return ( Math.abs( count ) === 1 ) ? singular : plural;
            };

            /**
             * Attempts to infer the plural form of any word
             * @param {String} word The word to pluralize
             * @return {String} The pluraized word
             */
            exports.smartPlural = function( str ){
                var s = str.split( "" )
                    ,lastLetter
                    ,lastTwo;

                lastLetter = s[ s.length - 1 ];
                lastTwo = str.substr( s.length-2 );

                if( lastTwo == "ch" || lastTwo == "sh"){
                    return s.join("") + "es";
                }else if( lastTwo == "ex" || lastTwo == "ix" ){
                    return s.join("").substr( 0, s.length-2) + "cies";
                }else if( lastTwo == "us"){
                    return s.join("").substr(0, s.length -2 ) + "i";
                }else if( lastTwo == "fe" ){
                    return s.join("").substr(0, s.length-2 ) + "ves";
                }else if( lastTwo == "on"){
                    return s.join("").substr(0, s.length-2 ) + "a";
                }


                switch( lastLetter ){
                    case "s":
                    case "z":
                    case "x":
                    case "o":
                        return s.join( "" ) + "es";
                    case "f":
                        return s.join("").substr(0,s.length-1) + "ves";
                    case "y":
                        return ( s.join("").substr(0, s.length -1 ) + "ies"  );

                    default:
                        return s.join("") + "s";
                }
            };

    }
);
