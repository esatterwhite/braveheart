define(['require', 'exports', 'module', 'util'], function( require, exports, module, util){


	return {
		/**
		 * Docs
		 * @method ArrayLike
		 * @param item {Object} the object to inspect
		 * @return {Boolean} true if the item is array like
		 */
		ArrayLike:function( item ){
			var x
				,typ
				,current;

			typ = util.typeOf( item );

			return ( typ === 'array' || util.typeOf( item.length ) === 'number' ) ? true : false;
		}

		/**
		 *
		 * @method Null
		 * @param item {Object} the item to inspect
		 * @return {Boolean} true if the object is null
		 */
		,"Null":function( item ){
			return util.typeOf( item ) === 'null';
		}

		/**
		 *
		 * @method UndefinedOrNull
		 * @param item {Object} the item to inspect
		 * @return {Boolean} true if the object is either undefined or null
		 */
		,UndefinedOrNull:function( item ){
			return item == null;
		}

		/**
		 *
		 * @method Object
		 * @param {item} The item to inspect
		 * @return {Boolean} true if the item is an object
		 */
		,"Object":function( item ){
			return util.typeOf( item ) === 'object'
		}

		/**
		 *
		 * @method DateLike
		 * @param item {Object} the item to inspect
		 * @return {Boolean} return true if the object resembles a date
		 */
		,DateLike:function( item ){
			return util.typeOf( item.getTime ) === 'function';
		}

		/**
		 *
		 * @method String
		 * @param item {Object} the itme to inspect
		 * @return {Boolean} returns true if the object is a string
		 */
		,"String":function( item ){
			return util.typeOf( item ) === 'string';
		}
		,Iterable: function( item ){
			return ( typeof( item.next ) === 'function' || typeof( item.iter ) === 'function' );
		}

		/**
		 * Tells you what the currently passin item is. If it has a repr function, it will call that. Otherwise it will attempt to determine a type<br />
		 * mainly a debugging tool, but may be usefull elsewhere
		 * @method This
		 * @param item {Object} the object to inspect
		 * @return {String}
		 */
		,This: function( item ){
			if( item.repr && util.typeOf( item.repr ) === 'function'){
				return item.repr()
			} else {
				return util.typeOf( item );
			}
		}
	}

})
