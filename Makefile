DESTDIR=release
BUILDDIR=work

# Find out our current branch
GIT_BRANCH_NAME=$(shell scripts/find_branch)

# Find out our build attempt
BUILD_ATTEMPT=1.0test1

ifeq ($(GIT_BRANCH_NAME), master)
  BUILD_PATH = dev
else
  BUILD_PATH = $(GIT_BRANCH_NAME)
endif

build:
	# installs dependancies to build braveheart & run tests
	npm install
	git submodule init
	git submodule update
	# git submodule foreach git checkout master
	# git submodule foreach git pull
	mkdir -p $(BUILDDIR)
	# Build our v1.0
	node ./bin/r.js -o scripts/app.build.v1.0.js
	# Build our v1.1
	node ./bin/r.js -o scripts/app.build.$(BUILD_PATH).js
	 # build the v1.0 docs
	./bin/jsdoc/jsdoc -d $(BUILDDIR)/braveheart/docs/v1.0/ -t include/docstrap/template -c include/docstrap/template/jsdoc/conf.json README.md v1.0/analytics.js v1.0/array.js v1.0/object.js v1.0/core.js v1.0/class.js v1.0/cookie.js v1.0/accessor.js  v1.0/date.js v1.0/functools.js v1.0/iter.js  v1.0/json.js v1.0/log.js v1.0/operator.js v1.0/request.js v1.0/string.js v1.0/swf.js v1.0/url.js v1.0/useragent.js v1.0/color.js v1.0/number.js packages/dom/v1.0/main.js packages/dom/v1.0/element.js packages/dom/v1.0/elements.js packages/dom/v1.0/DOMNode.js packages/dom/v1.0/DOMEvent.js packages/dom/v1.0/Document.js packages/dom/v1.0/Window.js packages/XDM/v1.0/main.js v1.0/modernizr.js v1.0/util.js v1.0/signals.js v1.0/keymaster.js v1.0/assets.js
	 # build the v2.0 docs
	./bin/jsdoc/jsdoc -d $(BUILDDIR)/braveheart/docs/$(BUILD_PATH)/ -u docs/tutorials -t include/docstrap/template -c include/docstrap/template/jsdoc.conf.json README.md $(BUILD_PATH)/analytics.js $(BUILD_PATH)/localstorage.js $(BUILD_PATH)/array.js $(BUILD_PATH)/object.js $(BUILD_PATH)/core.js $(BUILD_PATH)/class.js $(BUILD_PATH)/class/Class.js $(BUILD_PATH)/class/Chain.js $(BUILD_PATH)/class/Options.js $(BUILD_PATH)/class/Events.js $(BUILD_PATH)/class/Storage.js $(BUILD_PATH)/class/Interface.js $(BUILD_PATH)/cookie.js $(BUILD_PATH)/accessor.js  $(BUILD_PATH)/date.js $(BUILD_PATH)/functools.js $(BUILD_PATH)/iter.js  $(BUILD_PATH)/json.js $(BUILD_PATH)/log.js $(BUILD_PATH)/operator.js $(BUILD_PATH)/request.js $(BUILD_PATH)/request/Request.js $(BUILD_PATH)/request/JSON.js $(BUILD_PATH)/request/SIGNED.js $(BUILD_PATH)/string.js $(BUILD_PATH)/swf.js $(BUILD_PATH)/url.js $(BUILD_PATH)/useragent.js $(BUILD_PATH)/color.js $(BUILD_PATH)/number.js packages/dom/$(BUILD_PATH)/main.js packages/dom/$(BUILD_PATH)/element.js packages/dom/$(BUILD_PATH)/elements.js packages/dom/$(BUILD_PATH)/DOMNode.js packages/dom/$(BUILD_PATH)/DOMEvent.js packages/dom/$(BUILD_PATH)/Document.js packages/dom/$(BUILD_PATH)/Window.js packages/XDM/v2.0/main.js $(BUILD_PATH)/modernizr.js $(BUILD_PATH)/util.js $(BUILD_PATH)/signals.js $(BUILD_PATH)/keymaster.js $(BUILD_PATH)/assets.js packages/Native/$(BUILD_PATH)/main.js packages/Native/$(BUILD_PATH)/Array.js packages/Native/$(BUILD_PATH)/Date.js packages/Native/$(BUILD_PATH)/Number.js packages/Native/$(BUILD_PATH)/Object.js packages/Native/$(BUILD_PATH)/String.js packages/Collections/$(BUILD_PATH)/hash.js packages/Collections/$(BUILD_PATH)/main.js packages/Collections/$(BUILD_PATH)/queue.js $(BUILD_PATH)/fx.js $(BUILD_PATH)/fx/Base.js $(BUILD_PATH)/fx/Drag.js $(BUILD_PATH)/fx/Transitions.js $(BUILD_PATH)/fx/Tween.js $(BUILD_PATH)/data.js $(BUILD_PATH)/data/Model.js $(BUILD_PATH)/wallace.io.js $(BUILD_PATH)/data/Controller.js $(BUILD_PATH)/data/router.js $(BUILD_PATH)/data/fields.js $(BUILD_PATH)/data/Fields/Field.js $(BUILD_PATH)/data/Fields/mixins/Mapping.js $(BUILD_PATH)/data/Fields/mixins/Selector.js $(BUILD_PATH)/data/validators.js
	#copy required files
	cp -r ./packages $(BUILDDIR)/braveheart
	cp corvisaxdm.swf $(BUILDDIR)/braveheart/.
	cp braveheart.js $(BUILDDIR)/braveheart/.

test:
	mkdir -p tap
	node ./bin/run-js-tests -d $(BUILDDIR)/braveheart/$(BUILD_PATH) -r junit -o tap

annotate:
	# build annotated source code
	node annotate.js

clean:
	rm -rf tap
	rm -rf $(BUILDDIR)
	rm -rf $(DESTDIR)
	rm -rf ./bin/jsdoc/$(BUILDDIR)
	rm -f braveheart-*.dsc
	rm -f braveheart-*.tar.gz
	rm -f braveheart-*_amd64.changes
	rm -f braveheart-*_amd64.deb

install: build test
#	rm -rf $(BUILDDIR)/braveheart/$(BUILD_PATH)/test/
	cp -r $(BUILDDIR)/* $(DESTDIR)


debian: clean build test

	@echo "Building debian package for $(BUILD_PATH)"

	# Copy our debian repo and substitute our build path
	cp -r dist/debian $(BUILDDIR)
	perl -pi -e 's/%%VERSION%%/$(BUILD_PATH)/g' $(BUILDDIR)/debian/*
	perl -pi -e 's/%%BUILD_ATTEMPT%%/$(BUILD_ATTEMPT)/g' $(BUILDDIR)/debian/*

	cp README $(BUILDDIR)
	cd $(BUILDDIR) && dpkg-buildpackage -rfakeroot -us -uc
