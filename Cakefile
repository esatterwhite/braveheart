{spawn, exec,execFile} = require "child_process"
fs            = require "fs"
path          = require "path"
events 		  = require "events"
util		  = require "util"
colors		  = require "colors"
emitter = new events.EventEmitter

option '-d', "--destdir [DIR]", "Install destination directory"
option '-b', "--buildir [DIR]", "Build temp directory"
option '-v', "--verion [NUM]", "The version to Build"
option '-f', '--lintfile [FILE]', "Path to a file to lint"
option '-e', '--excluds [FILES]', "List of files or paths to exclude from operations"
BUILD_ATTEMPT = "1.0test1"
GIT_BRANCH_NAME = "dev"



setBranch = ->
	exec "git symbolic-ref -q HEAD", ( err, stdin ) ->
		if !err
			branch = path.normalize( stdin)
			branch = branch.split( path.sep )
			branch = branch[branch.length-1]

			GIT_BRANCH_NAME = if branch == "master" then "dev" else branch.trim()
			process.stdout.write "On Branch " + GIT_BRANCH_NAME + "\r\n"
			emitter.emit('branch')

copyFile = ( source, dest, cb ) ->
	console.log( source, dest)
	readStream = fs.createReadStream(source)
	writeStream = fs.createWriteStream(dest)

	readStream.pipe writeStream

	readStream.once "end", cb or ->

formatHintError = ( errs ) ->
	errs = if errs then errs else []

	errs = errs.map (err, idx, arr ) ->
		filename = err.file.split(path.sep)
		filename = filename[ filename.length-1]
		if err.error.reason.indexOf( 'debugger') != -1
			str = "Oh Snap! Debugger Statement Found".red.bold +  "\r\n" + filename.red.underline +  "\r\n" + "line " + err.error.line + ": " + err.error.reason + "\r\n" + "path:" + err.file
			process.stdout.write str
			throw err.error.reason
		else
			str = filename.cyan.underline +  "\r\n" + "line " + err.error.line + ": " + err.error.reason + "\r\n" + "path:" + err.file

		return str + "\r\n"

	return errs

hintFileReporter = ( results  ) ->
	len = results.length
	str = ''
	writer = fs.createWriteStream("lint.txt")
	results.forEach ( result ) ->
		file = result.file;
		error = result.error;
		str += file  + ': line ' + error.line + ', col ' + error.character + ', ' + error.reason + '\r\n'

	if str
		txt = if len == 1 then ' error' else ' errors'
		writer.on "close", ->
			process.stdout.write ""

		writer.write str + "\r\n" + len +  txt  + "\r\n", "utf-8"


task "branch", "Set the branch name", ( options )->
	setBranch()

task "lint", "Lint a file or entire directory of files", ( options )->
	{hint} = require "jshint/lib/hint"

	dir = options.destdir or options.lintfile or "v1.0"
	fullpath = path.resolve dir
	config = JSON.parse( fs.readFileSync( path.resolve "lint.config") )


	fs.stat fullpath, ( err, stats ) ->
		files = []
		if stats.isDirectory()
			files = fs.readdirSync( fullpath );
			files = files.map ( file ) ->
				fpath = path.resolve path.join dir, file
				if fs.statSync( fpath ).isFile()
					return fpath;
				else
					return null;

			files = files.filter ( file ) ->
				return file != null;
		else
			files = [ fullpath ]

		data = hint( files, config, hintFileReporter )

		data = formatHintError data

		process.stdout.write( data.join( "\r\n" ) )

###

###
task "compile", "Build the Production package", ( options ) ->
	setBranch()

	emitter.on "branch", ->
		console.log " branch found"
		bdir = options.builddir or "work"
		mkdirp = require "mkdirp"
		ncp = require 'ncp'
		ioemitter = new events.EventEmitter

		try
			fs.mkdirSync "#{bdir}/packages"
		catch e
			# dir already exists

		ioemitter.once "buildcomplete", ->
			ncp.ncp "./packages", "#{bdir}/braveheart/packages", ( err ) ->
				if err
					process.stdout.write err

				source = path.resolve "corvisaxdm.swf"
				dest = path.resolve  "#{bdir}/braveheart/corvisaxdm.swf"
				copyFile( source , dest  )

				source = path.resolve "braveheart.js"
				dest = path.resolve  "#{bdir}/braveheart/braveheart.js"
				copyFile( source, dest )

		process.stdout.write("Building")
		intervalid = setInterval ->
			process.stdout.write(".")
		, 125

		console.log " building - scripts/app.build.#{GIT_BRANCH_NAME}.js"
		exec "node ./bin/r.js -o scripts/app.build.#{GIT_BRANCH_NAME}.js && node ./bin/r.js -o scripts/app.build.v1.0.js", ( err, stdout ) ->
			clearInterval( intervalid )
			process.stdout.write( stdout )
			process.stdout.write("\r\nBuild Complete\r\n")
			ioemitter.emit("buildcomplete")



task "update", "update dependancies", ( options ) ->
	exec "npm install", ( err, stdout )->
		process.stdout.write( stdout ) + "\r\n"

	exec "git submodule init", ( err, stdout) ->
		process.stdout.write stdout + "\r\n"
		exec "git submodule update", ( err, stdout) ->
			process.stdout.write stdout + "\r\n"
			exec "git submodule foreach git checkout master", ( e, std ) ->
				process.stdout.write std + "\r\n"
				exec "git submodule foreach git pull", ( e, std ) ->
					process.stdout.write std + "\r\n"
###

###
task "docs", "Generates Documantation for the built packages", ( options ) ->
	mkdirp 		  = require "mkdirp"
	bdir = options.builddir or "work"


	console.log bdir
	comm = path.join( "bin" ,"jsdoc", "jsdoc" ) + " -d ./#{bdir}/braveheart/docs/v1.0/ " +
									"v1.0/analytics.js "+
									"v1.0/array.js "+
									"v1.0/object.js "+
									"v1.0/core.js "+
									"v1.0/class.js "+
									"v1.0/cookie.js " +
									"v1.0/accessor.js "+
									"v1.0/date.js "+
									"v1.0/functools.js "+
									"v1.0/iter.js "+
									"v1.0/json.js "+
									"v1.0/log.js "+
									"v1.0/operator.js "+
									"v1.0/request.js "+
									"v1.0/string.js "+
									"v1.0/swf.js "+
									"v1.0/url.js "+
									"v1.0/useragent.js "+
									"v1.0/color.js "+
									"v1.0/number.js "+
									"v1.0/modernizr.js "+
									"v1.0/util.js "+
									"v1.0/signals.js "+
									"v1.0/keymaster.js "+
									"v1.0/assets.js "+
									"packages/dom/v1.0/main.js "+
									"packages/dom/v1.0/element.js "+
									"packages/dom/v1.0/elements.js "+
									"packages/dom/v1.0/DOMNode.js "+
									"packages/dom/v1.0/DOMEvent.js "+
									"packages/dom/v1.0/Document.js "+
									"packages/dom/v1.0/Window.js " +
									"packages/XDM/v1.0/main.js "


	comm2 = path.join( "bin" ,"jsdoc", "jsdoc" ) + " -d " + path.normalize( "#{bdir}/braveheart/docs/v2.0 ") + " -t include/docstrap/template -c include/docstrap/template/jsdoc/conf.json" + 

									path.normalize( "packages/Collections/v2.0/main.js ") +
									path.normalize( "packages/Collections/v2.0/queue.js ") +
									path.normalize( "packages/Collections/v2.0/hash.js ") +
									path.normalize( "packages/dom/v2.0/main.js ") +
									path.normalize( "packages/dom/v2.0/element.js ") +
									path.normalize( "packages/dom/v2.0/elements.js ") +
									path.normalize( "packages/dom/v2.0/DOMNode.js ") +
									path.normalize( "packages/dom/v2.0/DOMEvent.js ") +
									path.normalize( "packages/dom/v2.0/Document.js ") +
									path.normalize( "packages/dom/v2.0/Window.js ") +
									path.normalize( "packages/XDM/v2.0/main.js ") +
									path.normalize( "packages/Crypto/main.js ") +
									path.normalize( "packages/Slick/main.js ") +
									path.normalize( "packages/Swig/main.js ") +
									path.normalize( "v2.0/analytics.js " ) +
									path.normalize( "v2.0/array.js ") +
									path.normalize( "v2.0/object.js ") +
									path.normalize( "v2.0/core.js ") +
									path.normalize( "v2.0/color.js ") +
									path.normalize( "v2.0/class.js ") +
									path.normalize( "v2.0/class/Class.js ") +
									path.normalize( "v2.0/class/Storage.js ") +
									path.normalize( "v2.0/class/Options.js ") +
									path.normalize( "v2.0/class/Events.js " ) +
									path.normalize( "v2.0/cookie.js " ) +
									path.normalize( "v2.0/accessor.js ") +
									path.normalize( "v2.0/date.js ") +
									path.normalize( "v2.0/functools.js ") +
									path.normalize( "v2.0/fx.js ") +
									path.normalize( "v2.0/fx/Base.js ") +
									path.normalize( "v2.0/fx/Drag.js ") +
									path.normalize( "v2.0/fx/Transitions.js ") +
									path.normalize( "v2.0/fx/Tween.js ") +
									path.normalize( "v2.0/iter.js ") +
									path.normalize( "v2.0/json.js ") +
									path.normalize( "v2.0/localstorage.js ") +
									path.normalize( "v2.0/log.js ") +
									path.normalize( "v2.0/operator.js ") +
									path.normalize( "v2.0/request.js ") +
									path.normalize( "v2.0/request/Request.js ") +
									path.normalize( "v2.0/request/JSON.js ") +
									path.normalize( "v2.0/request/HTML.js ") +
									path.normalize( "v2.0/request/SIGNED.js ") +
									path.normalize( "v2.0/string.js ") +
									path.normalize( "v2.0/swf.js ") +
									path.normalize( "v2.0/url.js ") +
									path.normalize( "v2.0/useragent.js ") +
									path.normalize( "v2.0/number.js ") +
									path.normalize( "v2.0/modernizr.js ") +
									path.normalize( "v2.0/util.js ") +
									path.normalize( "v2.0/signals.js ") +
									path.normalize( "v2.0/keymaster.js ") +
									path.normalize( "v2.0/threading.js ") +
									path.normalize( "README.md ") +
									path.normalize( "v2.0/assets.js ")

	process.stdout.write " ヽ(`Д´)ﾉ ⁀ ∀ᴧ∀ſ ƃuıʇɹɐʇS!".blue.bold + "\r\n"
	process.stdout.write "Running Java."

	intervalid = setInterval ->
		process.stdout.write "."
	, 125

	console.log( [ comm, comm2 ].join( " && " ) )

	exec [ comm, comm2 ].join( " && " ), ( err ) ->
		if err
			console.log err

		clearInterval intervalid
		process.stdout.write "\r\nJava Is Done\r\n"
###

###
task "test", "Run the test suit against the built code", ( options ) ->

	mkdirp 		  = require "mkdirp"
	bdir = options.builddir or "work"

	mkdirp "tap"
	comm = path.normalize( "#{bdir}/braveheart/v2.0")
	output = path.normalize(" node bin/run-js-tests -d #{bdir}/braveheart/v2.0  -r junit -o tap")
	console.log output
	mkdirp comm, ( err ) ->
		exec output, ( e, stdout )->
			if e
				console.log e
			process.stdout.write stdout


task "annotate", "Creates annotated source code docs", ( options ) ->
	groc 		  = require "groc"
	groc.CLI process.argv.slice(1) , (error) ->
		if error
			process.exit(1)

task "docs:all", "Generate API Docs and annotated source docs", ( options ) ->
	invoke "annotate"
	invoke "docs"

###

###
task "clean", "Remove artifcats from the build process", ( options ) ->
	bdir = options.builddir or "work"
	ddir = options.destdir or "release"
	ioemitter = new events.EventEmitter
	rmdir = require('rmdir')
	dircount = 0

	ioemitter.once "cleancomplete", ->
		process.stdout.write "\r\nDirectory Clean\r\n"

	fs.exists "tap", ( exists ) ->

		if exists
			rmdir "tap",  ( err )->
				dircount++
				process.stdout.write "tap directory removed\r\n"

				if dircount == 4
					ioemitter.emit "cleancomplete"
	fs.exists "#{bdir}", ( exists ) ->

		if exists
			rmdir "#{bdir}",  ( err )->
				dircount++
				process.stdout.write "#{bdir} directory removed\r\n"

				if dircount == 4
					ioemitter.emit "cleancomplete"

	fs.exists "#{ddir}", ( exists ) ->

		if exists
			rmdir "#{ddir}",  ( err )->
				process.stdout.write "#{ddir} directory removed\r\n"

				dircount++
				if dircount == 4
					ioemitter.emit "cleancomplete"

	fs.exists path.resolve( path.join(".", "bin", "jsdoc", "#{ddir}")), ( exists ) ->

		if exists
			console.log(" removing: ", path.resolve( path.join(".", "bin", "jsdoc", "#{bdir}")))
			rmdir path.resolve( path.join(".", "bin", "jsdoc", "#{ddir}")), ( err ) ->
					dircount++
					process.stdout.write "#{bdir} directory removed\r\n"

					if dircount == 4
						ioemitter.emit "cleancomplete"
###

###
task "build", "build The production package", ( options ) ->
	invoke "clean"
	invoke "compile"
	invoke "docs:all"

task "make", "Build, test and install the production package", ( options ) ->
	ddir = options.destdir or "release"
	bdir = options.builddir or "work"

	exec "cake update && cake lint && cake build && cake test", (err, stdout) ->
		ddir = path.join path.resolve ddir, "braveheart"
		bdir = path.join path.resolve bdir, "braveheart"
		mkdirp = require "mkdirp"
		ncp = require 'ncp'
		rmdir = require('rmdir')

		mkdirp ddir, ( )->
			console.log("moving #{bdir} to #{ddir}")
			ncp.ncp bdir, ddir, ( )->
				# rmdir options.builddir or "work", ->
				process.stdout.write("\r\nInstall Successful\r\n")
