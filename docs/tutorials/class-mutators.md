Mutators are what give the Class system it's flexibilty and as a result it's power. A Mutator can best be thought of as a plugin. However, in the situation of Mutators, they are executed during the creation of the class definition when you call `new Class()`. Mutator functions are executed in the context of the Class definition allowing the plugin to modify ( mutate ) the class as it is being defined. While, you may not know it, you have seen two Mutators already in **Implements** and **Extend**

Mutators are implemented on {@link module:class.Class|Class} via that {@module:accessor|accessor} module allowing you to define your own Mutators anytime you like. Whatever you name your Mutator will become a special key on the class that will perform whatever action you like

### Example - Track Class Instances
Lets take a look at a simple example of creating a new Class Mutator. I would like to be able to keep track of all of the instances of a specific Class so I can perform bulk operations on them. For memory limitation reasons I don't want functionality enabled all of the time, so I'll need to be able to turn it on only when needed.

<pre class='sunlight-highlight-javascript'>
define(['class'], function( Class ){
	Class.DefineMutator("Track", function( allowtrack ){
		if( !allowtrack ){
			// nothing to do
			return;
		}

		// the original constructor function
		var old_init = this.prototype.initialize;
		var cls = this;

		// overload the constructor function
		cls.prototype.initialize = function( ){
			// stash references to the instance in an array
			// create it if it doesn't exist
			( cls.instances = cls.instances || [] ).push( this );

			// call the original constructor
			old_init.apply( this, arguments )
		}
	});
});
</pre>
That's it! When I define a new {@link module:class.Class|Class} I can set a property **Track** to true and it will automatically save all of the instances of that {module:class.Class|Class}

<pre class='sunlight-highlight-javascript'>
braveheart( ['class'],function( Class ){
	var MyClass = new Class({
		initialize: function(){
			this.data = {}
		}
		,Track:true
	});

	var x = new MyClass();
	var y = new MyClass()

	MyClass.instances // [ x, y ]
})
</pre>

### Example - Integrate Class w/ jQuery
jQuery is very popular, functional, domain specific library with a lot of plugins behind it. One of the primary drawbacks is that it does not support any kind of OOP, Class like infrastructure. However, With the power of {@link module:class.Mutators|Mutators} we can create a Class that create a jQuery plugin that creates classes!

<pre class='sunlight-highlight-javascript'>
// mutators.js
define(["class"],function( Class ){

	Class.defineMutators({
		jQuery: function( name ){
			var that = this; // quick reference to the class instance

			// if there is no jQuery... Nothing todo
			if(!jQuery){
				return;
			}

			// the jQuery plugin function
			jQuery.fn[name] = function( arg ){

				// convert arguments to a *real* array
				var args = Array.prototype.slice.call( arguments, 1 );

				// if a string is passed, its a method call on an existing instance
				if( typeof arg == 'string'){
					var instance = jQuery( this ).data(name);

					// if there a class defined  - call the method with the arguments
					if(instance){
						instance[arg].apply( instance, args);

					}
				// Other wise we need to create a store an instance
				// somewhere in a jQuery...
				} else {
					// store a new instance of this Class under the passed in name
					jQuery( this ).data(
						name
						, new that(
							this.selector // selector of the current jQuery object
							, jQuery.extend( that.prototype.options, arg )
						) // end class constructor
					) // end .data()
				}
			}
		}
	})
})
</pre>

We taking advantage of the fact jQuery stores its plugins in a namespace called **fn** and use that to tie our logic into the internals of the jQuery library. Essentially, our mutator takes a `string`, arugument that it will use as the id of our class which will store in instance of that class on a DOM element using jQuery's css selector syntax. Using the same selector will return the instance of the class so you can work with it. So the name sepcified on the jQuery {@link module:class.Class.Mutators|Mutator} will be the name of the jQuery plugin. Calling the plugin with an `Object` will instanciate a new instance of the A {@link module:class.Class|Class}. Calling it again, with a string will call a method with a matching name

In other words, you define a new {@link module:class.Class|Class} as usual, using the jQuery Mutator key and you can use both libraries interchanably. For this example, we'll call it `braveQuery`
It looks something like this:

<pre class='sunlight-highlight-javascript'>
// require the mutators module defined above
// bavequery.js
braveheart(['class', 'mutators'], function(Class, mutators){
	var BraveQuery;

	// Still just a regular Class
	BraveQuery = new Class({
		Implements:[Class.Options]
		,options:{
			crazy:true
			,mind:"calm"
		}
		,jQuery:"braveQuery" // This is the magic
		,initialize: function( selector, options ){
			// The first argument to a jquery plugin is always a CSS selector
			// do soemthing with selector..


			// configure the class
			this.setOptions( options )
		}

		,sayWhat: function( ){
			if( this.options.crazy ){
				alert( "mind = " + this.options.mind )
			}
		}
	});

	return BraveQuery
});
</pre>

#### Usage: Class Style

<pre class='sunlight-highlight-javascript'>
braveheart(['class', 'bravequery'], function(Class, BraveQuery){
   var BraveQuery = Mutators
   // class creation

   var instance = new BraveQuery(null, {}); // OOP style

   // call sayWhat method
   instance.sayWhat() // alerts mind = calm
})
</pre>

**Working example:** http://jsfiddle.net/9yekJ/

#### Usage: jQuery Style

<pre class='sunlight-highlight-javascript'>

// class creation
$("#someElement").braveQuery({mind:"blown"}) // jQuery style

// call sayWhat method
$("#someElement").braveQuery("sayWhat") // alerts: mind = blown
</pre>

**Working example:** https://jsfiddle.net/mvUDY
