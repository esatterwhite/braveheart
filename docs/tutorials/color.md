The color module provides a means to parse normalize, and mix color codes from different color spaces Via the {@link module:color.Color|Color} Class and a few helper methods. For example you could you may not know how to combine `rgb(0, 12, 56)` with `hsb(0, 0.5, 1)` - or even know what that means. But the color module does!

## Basic Usage
The color module has a very handy lookup method that allows you to lookup color hex codes by name. It optionally takes a fallback color code incase it can not find the color you specified
<pre class='sunlight-highlight-javascript'>
braveheart(['color', 'log'], function( color, log ){
    log.log( color.lookup( "lightgoldenrodyellow" ) // #fafad2;
    log.log( color.lookup( "madeupcolor", "#FFFFFF" ) // #FFFFFF;
})
</pre>

### Simple Conversion
The public API of the {@link module:color|color} module is used to convert a string representation of a color ( "#FFFFFF", rgb(255,255,255), etc ) to an instance of the {@link module:color.Color|Color} Class so you can maniuplate them in a normailzied manner. Their are three primary module method: {@link module:color.Color#rgb|rgb}, {@link module:color.Color#hsb|hsb}, and {@link module:color.Color#hex|hex} that convert specific string formats to their respective {@link module:color.Color|Color} Types
<pre class='sunlight-highlight-javascript'>
braveheart(['color', "log"], function( color, log ){
	var red   = color.hex( "#FF0000" );
	var green = color.rgb( "rgb(0,255,0)" );
	var blue  = color.hsb( "hsb(240,100,100)" );

	log.log( red ) #1a1717
	log.log( green ) #00ff00
	log.log( blue ) #0000ff
});
</pre>
